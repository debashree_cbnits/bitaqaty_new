import React, { Component } from "react";
import { Provider } from "react-redux";
import AppContainer from "./src/navigation/AppNavigator"
import { SafeAreaView } from "react-native"
import store from './src/redux/store';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "",
      locale: ""
    };
  }
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{flex:1}}>
          <AppContainer />
        </SafeAreaView>
      </Provider>
    );
  }
}
