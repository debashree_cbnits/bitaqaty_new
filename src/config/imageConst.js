import logoEn from '../Component/assets/image/logo_en.png'
import logoar from '../Component/assets/image/logo_ar.png'
import newCard from '../Component/assets/image/newcard.png'
import rightArrow from '../Component/assets/image/right-arrow.png'
import marchant1 from '../Component/assets/image/marchant1.jpg'
import marchant2 from '../Component/assets/image/marchant2.jpg'
import marchant3 from '../Component/assets/image/marchant3.jpg'
import marchant4 from '../Component/assets/image/marchant4.jpg'
import marchantBanner from '../Component/assets/image/marchantBanner.jpg'
import heart from '../Component/assets/image/heart.png'
import percent from '../Component/assets/image/percent.jpg'
import whiteCart from '../Component/assets/image/whitecart.png'
import close from '../Component/assets/image/round-close-24px.png'
import headerLogo from '../Component/assets/image/logo.png' 
import sighnupscreenImage from '../Component/assets/image/sighnupscreenImage.png'
import newCardEmpty from '../Component/assets/image/newCardEmpty.png'
import bestSelEmpty from '../Component/assets/image/bestSelEmpty.png'
import offerEmpty from '../Component/assets/image/offerEmpty.png'



import starImg from '../Component/assets/image/start1.png'
import tagImg from '../Component/assets/image/tagg.png'
import sliderImage1 from '../Component/assets/image/avatar-4.jpg'
import sliderImage2 from '../Component/assets/image/avatar-5.jpg'
import sliderImage3 from '../Component/assets/image/avatar-6.jpg'


// product image
import productImg1 from '../Component/assets/image/brandLogo1.png'
import productImg2 from '../Component/assets/image/brandLogo2.png'
import productImg3 from '../Component/assets/image/brandLogo3.png'
import productImg4 from '../Component/assets/image/brandLogo4.png'
import productImg5 from '../Component/assets/image/brandLogo5.png'
import productImg6 from '../Component/assets/image/brandLogo6.png'
import product7 from '../Component/assets/image/product.jpg'
import vartiParcentage from '../Component/assets/image/varPar.png'
import heartY from '../Component/assets/image/heartY.png'


//menu screen images
import loginIcon from '../Component/assets/image/person-24px.png'
import home1 from '../Component/assets/image/home1.png'
import category2 from '../Component/assets/image/catagory2.png'
import downarrow from '../Component/assets/image/downarrow.png'
import contact from '../Component/assets/image/contact.png'
import chat from '../Component/assets/image/chat.png'
import setting from '../Component/assets/image/setting.png'
import about from '../Component/assets/image/about.png'
import facebook from '../Component/assets/image/facebook.png'
import twitter from '../Component/assets/image/twitter.png'
import google from '../Component/assets/image/google.png'
import instagram from '../Component/assets/image/instagram.png'
import youtube from '../Component/assets/image/youtube.png'
import flag2 from '../Component/assets/image/flag2.png'
import user from '../Component/assets/image/user.png'
import heartBlack from '../Component/assets/image/heartBlack.png'
import orderHistory from '../Component/assets/image/orderHistory.png'
import changePassword from '../Component/assets/image/changePassword.png'
import about80 from '../Component/assets/image/about80.png'
import homeIcon from '../Component/assets/image/home.png'
import shoppingIcon from '../Component/assets/image/shoppingIcon.png'
import loginUserIcon from '../Component/assets/image/login.png'
import chatWithUs from '../Component/assets/image/chatWithUs.png'
import beReseller from '../Component/assets/image/seller.png'


//login screen images
import googleIcon from '../Component/assets/image/google-icon-logo.png'
import facebookIcon from '../Component/assets/image/facebook-icon-logo.png'
import twitterIcon from '../Component/assets/image/twitter-icon-logo.png'
import password from '../Component/assets/image/passwordShow.png'
import passwordOff from '../Component/assets/image/passwordShowOff.png'
import forgotImage from '../Component/assets/image/forgotPassword2.png'
import forgotImageY from '../Component/assets/image/forgotPassword2_Y.png'
import linkVerification from '../Component/assets/image/linkVerification.png'
import linkVerificationY from '../Component/assets/image/linkVerification_Y.png'
import changePassword2 from '../Component/assets/image/changePassword2.png'
import changePassword2Y from '../Component/assets/image/changePassword2_Y.png'
import createAccount2 from '../Component/assets/image/createAccount2.png'
import createAccount2Y from '../Component/assets/image/createAccount2_Y.png'
import createAccount3 from '../Component/assets/image/createAccount3.png'
import createAccount3Y from '../Component/assets/image/createAccount3_Y.png'
import createAccount4 from '../Component/assets/image/createAccount4.png'
import createAccount4Y from '../Component/assets/image/createAccount4_Y.png'
import createAccount5 from '../Component/assets/image/createAccount5.png'
import createAccount5Y from '../Component/assets/image/createAccount5_Y.png'
import createAccount6 from '../Component/assets/image/createAccount6.png'
import createAccount6Y from '../Component/assets/image/createAccount6_Y.png'
import changePasswordWlcm from '../Component/assets/image/changePasswordWlcm.png'
import changePasswordWlcm_Y from '../Component/assets/image/changePasswordWlcm_Y.png'
import createAccount1_Y from '../Component/assets/image/createAccount1_Y.png'
import error from '../Component/assets/image/error.png'
import setPass from '../Component/assets/image/setPass.png'
import setPass_Y from '../Component/assets/image/setPass_Y.png'
import loginError from '../Component/assets/image/loginError.png'



//search screen images
import search from '../Component/assets/image/search_magnifier.png'
import searchSm from '../Component/assets/image/searchsm.png'
import noresult from '../Component/assets/image/noresult.png'
import searchClose from '../Component/assets/image/search_close.png'

//cart screen
import deleteIcon from '../Component/assets/image/delete.png'
import americanExp from '../Component/assets/image/americanExp.png'
import applePay from '../Component/assets/image/applePay.png'
import paypal from '../Component/assets/image/paypal.png'
import visa from '../Component/assets/image/VISA.png'
import mada from '../Component/assets/image/mada.png'
import masterCard from '../Component/assets/image/masterCard.jpg'
import minusIcon from '../Component/assets/image/minus.png'
import plusIcon from '../Component/assets/image/plus.png'
import removeBig from '../Component/assets/image/removeBig.png'
import visaPayment from '../Component/assets/image/visaPayment.png'
import cameraPayment from '../Component/assets/image/cameraPayment.png'

//other screen
import rightTick from '../Component/assets/image/right-tick.png'
import forgotError from '../Component/assets/image/forgotError.png'
import googleBig from '../Component/assets/image/googleBig.png'
import smile from '../Component/assets/image/smile.png'
import Maroof from '../Component/assets/image/maroof.png'
import heartB from '../Component/assets/image/heartB.png'
import orderHB from "../Component/assets/image/orderHB.png"
import welcomeHeart from "../Component/assets/image/welcomeHeart.png"
import facebookBig from '../Component/assets/image/facebookBig.png'
import twitterBig from '../Component/assets/image/witterBig.png'

//chat screen
import chatClose from '../Component/assets/image/chat_close.png'
import chatDownarrow from '../Component/assets/image/chat_downarrow.png'
import chatUser from '../Component/assets/image/chatUser.png'
import like from '../Component/assets/image/like.png'
import dislike from '../Component/assets/image/dislike.png'
import attach from '../Component/assets/image/attach.png'
import send from '../Component/assets/image/send.png'

//profile details
import editIcon from '../Component/assets/image/editIcon.png'
import proUser from '../Component/assets/image/proUser.png'
import proLike from '../Component/assets/image/proLike.png'
import profileError from '../Component/assets/image/profileError.png'



//order History
import roundArrowDown from '../Component/assets/image/round_arrow_down.png'
import roundCalendar from '../Component/assets/image/round_calendar.png'
import connection from '../Component/assets/image/connection.png'
import calendarSm from '../Component/assets/image/calendarSm.png'
import calendarGrey from '../Component/assets/image/calendarGrey.png'
import editIconWhite from '../Component/assets/image/editIconWhite.png'


export const orderHistoryImage = {
    roundArrowDown,
    roundCalendar,
    editIconWhite
}

export const profile = {
    editIcon,
    proUser,
    proLike,
    profileError,
    connection
}

export const chatScreen = {
    chatClose,
    chatDownarrow,
    chatUser,
    like,
    dislike,
    attach,
    send
}

export const cartImage = {
    deleteIcon,
    minusIcon,
    plusIcon,
    americanExp,
    applePay,
    paypal,
    visa,
    mada,
    masterCard,
    removeBig,
    visaPayment,
    cameraPayment
}


export const menuScreen = {
    loginIcon,
    home1,
    category2,
    downarrow,
    contact,
    chat,
    setting,
    about,
    facebook,
    twitter,
    google,
    instagram,
    youtube,
    flag2,
    user,
    heartBlack,
    orderHistory,
    changePassword,
    close,
    headerLogo,
    about80,
    loginUserIcon,
    chatWithUs,
    shoppingIcon,
    homeIcon,
    beReseller
}

export const HomeImagePath = {
    logoEn,
    logoar,
    newCard,
    starImg,
    tagImg,
    sliderImage1,
    sliderImage2,
    sliderImage3,
    newCardEmpty,
    bestSelEmpty,
    offerEmpty
}

export const CatagoryImagePath = {
    rightArrow
}

export const MarchantImagePath = {
    marchant1,
    marchant2,
    marchant3,
    marchant4,
    marchantBanner,
    newCard,
}

export const productImagePath = {
    heart,
    percent,
    whiteCart,
    productImg1,
    productImg2,
    productImg3,
    productImg4,
    productImg5,
    productImg6,
    product7,
    vartiParcentage,
    heartY,
}

export const loginScreen = {
    googleIcon,
    facebookIcon,
    twitterIcon,
    password,
    passwordOff,
    forgotImage,
    forgotImageY,
    linkVerification,    
    linkVerificationY,
    changePassword2,
    changePassword2Y,
    sighnupscreenImage,
    createAccount2,
    createAccount2Y,
    createAccount3,
    createAccount3Y,
    createAccount4,
    createAccount4Y,
    createAccount5,
    createAccount5Y,
    createAccount6,
    createAccount6Y,
    changePasswordWlcm,
    changePasswordWlcm_Y,
    createAccount1_Y,
    error,
    setPass,
    setPass_Y,
    loginError,
}

export const SearchIcon = {
    search,
    searchSm,
    noresult,
    searchClose
}

export const othersImage = {
    rightTick,
    forgotError,
    googleBig,
    smile,
    Maroof,
    heartB,
    orderHB,
    welcomeHeart,
    calendarSm,
    calendarGrey,
    facebookBig,
    twitterBig
}

