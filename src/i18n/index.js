import ReactNative from 'react-native';
import I18n from 'react-native-i18n';
import eng from './Locales/eng'
import ar from './Locales/ar';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  eng,
  ar
};
export default I18n;