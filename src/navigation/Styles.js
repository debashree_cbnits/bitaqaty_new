export default {
    containerStyle: {
      backgroundColor: "#fff",
      width: "100%",
      padding: 5,
      alignItems: "center",
      borderTopColor: '#00000014',
      borderTopWidth: 1,
    },
    iconStyle: { height: 26, width: 26 }, 
    cartProductCountStyle: {
      fontSize: 12,
      fontFamily: "Tajawal-Bold",
      backgroundColor: '#FFC300',
      height: 18,
      paddingHorizontal: 4,
      borderRadius: 4,
      marginBottom: 2,
      // marginHorizontal: 2
    },
    cartProductCountStyle_ar: {
      fontSize: 12,
      fontFamily: "Tajawal-Bold",
      backgroundColor: '#FFC300',
      height: 18,
      paddingHorizontal: 4,
      borderRadius: 4,
      marginBottom: 2,
      // marginHorizontal: 2
    },
    bottomTabTextStyle: {
      fontSize: 12,
      fontFamily: "Tajawal-Regular",
      color: '#727272'
    }
  };
  