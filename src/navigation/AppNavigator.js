import { Linking, SafeAreaView, Alert } from "react-native";
import React from "react";
import Splash from "../Component/pages/Splash";
import ChooseLanguage from "../Component/pages/Chooselanguage";
import SignUp from "../Component/pages/Auth/SignUp";
import Login from "../Component/pages/Auth/Login";
import Email from "../Component/pages/Auth/EmailVerify";
import SetNewPass from "../Component/pages/Auth/SetNewPass";

import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import TabsEn from "./MainTabNavigatorEn";
import TabsAr from "./MainTabNavigatorAr";

import Mobile from "../Component/pages/Auth/Mobile";

import AsyncStorage from "@react-native-community/async-storage";
import GatewaySuccess from "../Component/pages/Payment/GatewaySuccess";
import GatewayLoading from "../Component/pages/Payment/GatewayLoading";
import GatewayFail from "../Component/pages/Payment/GatewayFail";
import { TapGestureHandler } from "react-native-gesture-handler";

import googleSignUp from '../Component/pages/Auth/googleSignUp';
import facebookSignUp from '../Component/pages/Auth/facebookSignup';


const AuthLoadingStack = createStackNavigator(
  {
    Splash: Splash
  },
  {
    headerMode: null
  }
);

const languageStack = createStackNavigator(
  {
    ChooseLanguage: ChooseLanguage
  },
  {
    headerMode: null
  }
);

const authStack = createStackNavigator(
  {
    SignUp: SignUp,
    facebookSignUp: facebookSignUp,
    googleSignUp: googleSignUp,
    Login: Login,
  },

  {
    headerMode: null
  }
);

const emailStack = createStackNavigator(
  {
    Mobile: Mobile
  },

  {
    headerMode: null
  }
);

const SetNewPassStack = createStackNavigator(
  {
    SetNewPass: SetNewPass
  },

  {
    headerMode: null
  }
);

const MainStackEn = createStackNavigator(
  {
    Home: {
      screen: TabsEn
    }
  },
  {
    headerMode: null,
    initialRouteName: "Home"
  }
);
const MainStackAr = createStackNavigator(
  {
    Home: {
      screen: TabsAr
    }
  },
  {
    headerMode: null,
    initialRouteName: "Home"
  }
);

const switchStack1 = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingStack,
    language: languageStack,
    auth: authStack,
    email: emailStack,
    MainStackEn: MainStackEn,
    MainStackAr: MainStackAr
  },
  {
    initialRouteName: "AuthLoading"
  }
);

const switchStack2 = createSwitchNavigator(
  {
    Mobile: emailStack,
    auth: authStack,
    MainStackEn: MainStackEn,
    MainStackAr: MainStackAr
  },
  {
    initialRouteName: "Mobile"
  }
);

const switchStack3 = createSwitchNavigator(
  {
    SetNewPass: SetNewPassStack,
    email: emailStack,
    auth: authStack,
    MainStackEn: MainStackEn,
    MainStackAr: MainStackAr
  },
  {
    initialRouteName: "SetNewPass"
  }
);

const switchStack4 = createSwitchNavigator({
  GatewaySuccess: GatewaySuccess,
  GatewayLoading: GatewayLoading,
  GatewayFail: GatewayFail,
  MainStackEn: MainStackEn,
  MainStackAr: MainStackAr
}, {
  initialRouteName: "GatewayLoading"
})

const AppContainer1 = createAppContainer(switchStack1);
const AppContainer2 = createAppContainer(switchStack2);
const AppContainer3 = createAppContainer(switchStack3);
const AppContainer4 = createAppContainer(switchStack4);

export default class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "",
      resourcePath: "",
      id: ""
    };

  }

  componentDidMount() {
    this.getstorageData();
  }

  getstorageData = () => {
    AsyncStorage.getItem("paymentstart")
      .then((res) => {
        this.url(res);
      })
  }

  url = (res) => {
    Linking.getInitialURL().then(url => {
      console.log(url, 'ur;------')
      if (url == null && res == null) {
        this.setState({ url: 1 });
      }
      else if (url == null && res == "paystart") {
        this.setState({ url: 4 });
      } else if (url.includes("hyperpay") === false) {
        const route = url.replace(/.*?:\/\//g, "");
        const id = route.match(/\/([^\/]+)\/?$/)[1];
        const linkname = route.split("/")[2];
        if (linkname == "resetPassword") {
          this.setState({ url: 3 });

          const requestDate = route.split("/")[5];
          const requestID = route.split("/")[4];
          const userName = route.split("/")[3];

          AsyncStorage.setItem("userName", userName);
          AsyncStorage.setItem("requestDate", requestDate);
          AsyncStorage.setItem("requestID", requestID);
        } else {
          this.setState({ url: 2 });

          const userName = route.split("/")[3];
          const activationCode = route.split("/")[4];

          AsyncStorage.setItem("userName", userName);
          AsyncStorage.setItem("activationCode", activationCode);
        }
      } else {
        let regex = /[?&]([^=#]+)=([^&#]*)/g,
          params = {},
          match;
        while ((match = regex.exec(url))) {
          params[match[1]] = match[2];
        }
        const { id, resourcePath } = params;
        this.setState({ url: 4, resourcePath, id });
      }
    });
  };

  render() {
    const { resourcePath, id } = this.state;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
        {this.state.url == 1 ? <AppContainer1 /> : null}

        {this.state.url == 2 ? <AppContainer2 /> : null}

        {this.state.url == 3 ? <AppContainer3 /> : null}

        {this.state.url == 4 ? <AppContainer4 screenProps={{ resourcePath, id }} /> : null}
      </SafeAreaView>
    );
  }
}