import React from "react";
import { View, Text, Image } from "react-native";
import I18n from "../i18n/index";
import AsyncStorage from "@react-native-community/async-storage";
import store from "../redux/store";
import styles from "./Styles";

export default class BottomTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Cart",
      count: 0,
      array: [],
      locale: "",
      language: "",
      cartArray: [],
      cartData: [],
      totalPrice: 0,
      checkNetworkConnection: "",
      totalVat: 0,
      optionDisplayIndex: 9999999999999,
      show: false,
      showUnavailableAction: false,
      showCostLimitALert: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          cartData: props.cartData
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          cartData: props.cartData
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        cartData: []
      };
    }
  }

  componentDidMount = async () => {
    let array = JSON.parse(await AsyncStorage.getItem("CartItemList"));
    this.setState({ array });
    store.subscribe(() => this.forceUpdate());
  };

  shouldComponentUpdate = () => {
    return true;
  };

  render() {
    return (
      <View style={styles.containerStyle}>
        {this.props.label === "Cart" || this.props.label === "عربة التسوق" ? (
          this.props.label === "Cart" ? (
            <View
              style={{
                flexDirection: "row",
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                style={styles.iconStyle}
                source={
                  this.props.focused ? this.props.activeImage : this.props.image
                }
              />
              <Text style={styles.cartProductCountStyle}>
                {
                  store.getState().Storage.cartArrayChanged ?
                    store.getState().Storage.cartArrayLength :
                    (this.state.array !== null ? this.state.array.length : 0)
                }
              </Text>
            </View>
          ) : (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Image
                  style={styles.iconStyle}
                  source={
                    this.props.focused ? this.props.activeImage : this.props.image
                  }
                />
                <Text
                  style={styles.cartProductCountStyle_ar}
                >
                  {store.getState().Storage.cartArrayChanged ?
                    store.getState().Storage.cartArrayLength :
                    (this.state.array !== null ? this.state.array.length : 0)}
                </Text>
              </View>
            )
        ) : (
            <Image
              style={styles.iconStyle}
              source={
                this.props.focused ? this.props.activeImage : this.props.image
              }
            />
          )}
        <Text style={styles.bottomTabTextStyle}>{this.props.label}</Text>
      </View>
    );
  }
}