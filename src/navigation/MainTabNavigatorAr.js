import React from "react";
import { Image, TabBarIcon } from "react-native";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";
import Cart from "../Component/pages/Cart/Cart";
import Catagory from "../Component/pages/Catagory/Catagory";
import HomeScreen from "../Component/pages/Home";
import Menu from "../Component/pages/Menu";
import Search from "../Component/pages/Search/";
import CatagoryView from "../Component/pages/Home/CatagoryView/CatagoryView";
import Tab from "../Component/pages/Home/Tab/Tab";
import Merchants from "../Component/pages/Catagory/Merchants/Merchants";
import ContactUs from "../Component/pages/Contact Us/ContactUs";
import Login from "../Component/pages/Auth/Login";
import LoginFirst from "../Component/pages/Auth/LoginFirst";
import ForgotPassword from "../Component/pages/Auth/ForgotPassword";
import LinkVerification from "../Component/pages/Auth/LinkVerification";
import SignUp from "../Component/pages/Auth/SignUp";
import ChangePassword from "../Component/pages/Auth/ChangePassword";
import PaymentOption from "../Component/pages/Cart/PaymentOption";
import ContactSuccess from "../Component/pages/Contact Us/ContactSuccess";
import Faq from "../Component/pages/Contact Us/Faq";
import LangSelection from "../Component/pages/Setting & Language";
import SetNewPass from '../Component/pages/Auth/SetNewPass';
import ForgotError from '../Component/pages/Auth/ForgotError';
import LoginSuccess from '../Component/pages/Auth/LoginSuccess';
import Welcome1 from '../Component/pages/Auth/Welcome1';
import Email from "../Component/pages/Auth/Email";
import EmailVerify from "../Component/pages/Auth/EmailVerify";
import Mobile from "../Component/pages/Auth/Mobile";
import LoginWlcm from "../Component/pages/Auth/LoginWlcm";
import ChatWithUs from '../Component/pages/Chat/ChatWithUs';
import ProfileDetails from '../Component/pages/Profile Details/ProfileDetails'
import OrderHistory from "../Component/pages/orderHistory/orderHistory"
import About from "../Component/pages/About Bitaqaty/About"
import HowToBuy from "../Component/pages/About Bitaqaty/HowToBuy"
import Favourites from '../Component/pages/Favourites/Favourites'
import Internet from '../Component/pages/Network/Internet'
import BankDetails from '../Component/pages/Payment/BankDetails'
import Welcome2 from '../Component/pages/Auth/Welcome2';
import BottomTab from "../navigation/BottomTab"
import Calender from '../Component/pages/orderHistory/Calender'
import LoginSuccessFB from '../Component/pages/Auth/LoginSuccessFB'
import LoginSuccessTW from '../Component/pages/Auth/LoginSuccessTW'
import Privacy from '../Component/pages/About Bitaqaty/Privacy';
import Terms from '../Component/pages/About Bitaqaty/Terms';
import Receipt from '../Component/pages/orderHistory/Receipt';
import {NavigationActions, StackActions} from 'react-navigation';


const homeStack = createStackNavigator(
  {
    HomeScreen: HomeScreen,
    CatagoryView: CatagoryView,
    Tab: Tab
  },
  {
    headerMode: null,
    initialRouteName: "HomeScreen",
  }
);

homeStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarIcon: ({ focused }) => (
      <BottomTab
        label="الرئيسية"
        image={require("../Component/assets/image/home2.png")}
        focused={focused}
        activeImage={require("../Component/assets/image/home-active.png")}
      />
    )
  };
};

const searchStack = createStackNavigator(
  {
    Search: Search
  },
  {
    headerMode: null
  }
);

searchStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarIcon: ({ focused }) => (
      <BottomTab
        label="بحث"
        image={require("../Component/assets/image/search2.png")}
        focused={focused}
        activeImage={require("../Component/assets/image/search-active.png")}
      />
    )
  };
};

const menuStack = createStackNavigator(
  {
    Menu: Menu,
    ContactUs: ContactUs,
    Login: Login,
    LoginFirst: LoginFirst,
    ForgotPassword: ForgotPassword,
    LinkVerification: LinkVerification,
    ChangePassword: ChangePassword,
    SignUp: SignUp,
    ContactSuccess: ContactSuccess,
    Faq: Faq,
    Privacy: Privacy,
    Terms: Terms,
    SettingLanguage: LangSelection,

    Email: Email,
    EmailVerify: EmailVerify,

    Mobile: Mobile,

    LoginWlcm: LoginWlcm,
    ChatWithUs: ChatWithUs,
    // Message: Message,
    SetNewPass: SetNewPass,
    ForgotError: ForgotError,
    LoginSuccess: LoginSuccess,
    Welcome1: Welcome1,
    Welcome2: Welcome2,
    ProfileDetails: ProfileDetails,
    OrderHistory: OrderHistory,
    About: About,
    HowToBuy: HowToBuy,
    Favourites: Favourites,
    Internet: Internet,
    Calender: Calender,
    LoginSuccessFB: LoginSuccessFB,
    LoginSuccessTW: LoginSuccessTW,
  },
  {
    headerMode: null,
    initialRouteName: 'Menu'
  }
);

menuStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  let routeName = navigation.state.routes[navigation.state.index].routeName
  if (routeName == 'Login' || routeName == 'SignUp' || routeName == 'ChangePassword' || routeName == 'ForgotPassword' || routeName == "SettingLanguage") {
    tabBarVisible = false
  }
  return {
    tabBarVisible,
    tabBarIcon: ({ focused }) => (
      <BottomTab
        label="القائمة"
        image={require("../Component/assets/image/roundMenu.png")}
        focused={focused}
        activeImage={require("../Component/assets/image/menu-active.png")}
      />
    )
  };
};

const cartStack = createStackNavigator(
  {
    Cart: Cart,
    PaymentOption: PaymentOption,
    BankDetails: BankDetails,
    Receipt: Receipt
  },
  {
    headerMode: null,
    initialRouteName: 'Cart'
  }
);

cartStack.navigationOptions = ({ navigation }) => {
  tabBarVisible = true

  let routeName = navigation.state.routes[navigation.state.index].routeName
  if (routeName == 'BankDetails') {
    tabBarVisible = false
  }

  return {
    tabBarVisible,
    tabBarIcon: ({ focused }) => (
      <BottomTab
        label="عربة التسوق"
        image={require("../Component/assets/image/cart2.png")}
        focused={focused}
        activeImage={require("../Component/assets/image/cartActive.png")}
      />
    )
  };
};

const catagoryStack = createStackNavigator(
  {
    Catagory: Catagory,
    Merchants: Merchants
  },
  {
    headerMode: null
  }
);

catagoryStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarIcon: ({ focused }) => (
      <BottomTab
        label="الاقسام"
        image={require("../Component/assets/image/catagory2.png")}
        focused={focused}
        activeImage={require("../Component/assets/image/category-active.png")}
      />
    )
  };
};

export default createBottomTabNavigator(
  {
    Menu: { screen: menuStack,
    //   navigationOptions: ({navigation}) => ({
    //     tabBarOnPress: (scene, jumpToIndex) => { 
    //     // console.warn (navigation)

    //         navigation.dispatch(
    //             NavigationActions.navigate({
    //                 routeName: 'Menu', //which is your route to be reset
    //                 action: StackActions.reset({
    //                     index: 0,
    //                     actions: [
    //                         NavigationActions.navigate({ routeName: 'Menu' }) //which is the default stack to be shown on pressing the tab
    //                     ]
    //                 })
    //             })
    //         );
    //     }        
    // }) 
  },
    Catagory: { screen: catagoryStack },
    Cart: { screen: cartStack },
    Search: { screen: searchStack },
    Home: { screen: homeStack,
    //   navigationOptions: ({navigation}) => ({
    //     tabBarOnPress: (scene, jumpToIndex) => { 
    //     // console.warn (navigation)

    //         navigation.dispatch(
    //             NavigationActions.navigate({
    //                 routeName: 'HomeScreen', //which is your route to be reset
    //                 action: StackActions.reset({
    //                     index: 0,
    //                     actions: [
    //                         NavigationActions.navigate({ routeName: 'HomeScreen' }) //which is the default stack to be shown on pressing the tab
    //                     ]
    //                 })
    //             })
    //         );
    //     }        
    // })
   },
  },
  {
    initialRouteName: "Home",
    tabBarOptions: {
      keyboardHidesTabBar: false,
      activeTintColor: "#1a1a1a",
      inactiveTintColor: "#727272",
      showLabel: false
    },
    defaultNavigationOptions: ({ navigation, defaultHandler }) => ({
      tabBarOnPress: async ({ navigation, defaultHandler }) => {
        navigation.popToTop();
        navigation.pop();
        defaultHandler();
      }
    })
  }
);