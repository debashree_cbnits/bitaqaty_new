import  {SELECTED_LANGUAGE}

from "./types";

const intialState = {
   error: '',
   setlanguage: {},
};

export default function(state = intialState, action) {
  switch (action.type) {
    case SELECTED_LANGUAGE:
      return {
        ...state,
        setlanguage: action.payload,
      };
    default:
      return state;
  }
}
