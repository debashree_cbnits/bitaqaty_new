import {
  SELECTED_LANGUAGE 
} from './types';
//import Api from '../../../../Api/Api';

export const setSelectedLanguage = setSelectedLanguage => dispatch => {
  dispatch({type: SELECTED_LANGUAGE, payload: {...setSelectedLanguage}});
};

