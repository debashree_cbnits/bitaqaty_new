import {combineReducers} from 'redux';

import LanguageReducer from '../language/reducer';
import StorageReducer from '../storage/reducer';

// import postReducer from './postReducer';
// import authReducer from './AuthReducer';

export default combineReducers({
  Language: LanguageReducer,
  Storage: StorageReducer,
});
