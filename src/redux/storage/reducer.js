import { CART_DETAILS, CART_ARRAY_CHANGED, CART_ARRAY_LENGTH } from "./types";

import AsyncStorage from "@react-native-community/async-storage";

const intialState = {
  CartItemList: [],
  cartArrayChanged: false,
  cartArrayLength: 0,
  dataArray: [],
  cartData: {}
};

export default function (state = intialState, action) {
  switch (action.type) {
    case CART_DETAILS:
      let cartData = action.payload;
      let array = [];
      AsyncStorage.getItem("CartItemList").then(cartArray => {
        if (cartArray !== null) {
          state.CartItemList = JSON.parse(cartArray);
          array = [...state.CartItemList, cartData];
          let dataArray = array.filter(
            (element, index) =>
              index === array.findIndex(elem => elem.id === element.id)
          );
          state.dataArray = dataArray;
          AsyncStorage.setItem("CartItemList", JSON.stringify(dataArray));
        } else if (cartArray === null || (cartArray !== null && JSON.parse(cartArray).length == 0)) {
          array = [...state.CartItemList, cartData];
          let dataArray = array.filter(
            (element, index) =>
              index === array.findIndex(elem => elem.id === element.id)
          );
          state.dataArray = dataArray;
          AsyncStorage.setItem("CartItemList", JSON.stringify(dataArray));
        }
      });

      return {

        ...state,
        CartItemList: state.dataArray,
        cartArrayLength: state.dataArray.length
      };

    case CART_ARRAY_CHANGED:
      return { ...state, cartArrayChanged: true };

    case CART_ARRAY_LENGTH:
      return {
        ...state,
        cartArrayLength: action.payload
      };
    default:
      return state;
  }
}
