import {
  CART_DETAILS, CART_ARRAY_CHANGED, CART_ARRAY_LENGTH 
} from './types';


export const setCartDetails = data => dispatch => {
  dispatch({type: CART_DETAILS, payload: data});
};

export const setCartArrayLength = data => dispatch => {
  dispatch({type: CART_ARRAY_LENGTH, payload: data});
};

export const setCartArrayChanged = data => dispatch => {
  dispatch({type: CART_ARRAY_CHANGED, payload: data});
};

