import React from 'react';
import { setCartDetails } from "../redux/storage/action";
import store from '../redux/store';
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../services/api.client";

export default class StorageFunc {

  static addToCart(data) {
    if (data) {
      let cartData = JSON.parse(data);
      let data1 = {}
      let cartid = cartData.itemID
      data1 = { [cartid]: cartData }
      store.dispatch(setCartDetails(cartData))
    }
  }

  static profileDetails(usertoken) {
    apiClient
      .getProfileRequest("/account-profile", usertoken)
      .then(profileDetailsRes => {
        AsyncStorage.setItem('profileDetails', JSON.stringify(profileDetailsRes))
      }).catch(err => console.log(err));
  }
}
