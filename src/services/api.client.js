import RNFetchBlob from 'rn-fetch-blob';
import { Platform } from 'react-native';
const baseURL = "https://stagewrapper.ocstaging.net/bitaqatywrapper";
// const baseURL = "https://wrapper.bitaqaty.com"

const fetchClient = RNFetchBlob.config({
  trusty: true
})
const headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json, text/plain, */*',
  'app-name': 'Bitaqaty',
  'country': '2',
  'username': 'Bitaqaty',
}

const apiClient = {

  getRequest: (path, userToken) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": userToken
      };

      fetchClient
        .fetch("GET", baseURL + path, headers)
        .then(res => {
          const response =
            Platform.OS === "android"
              ? JSON.parse(decodeURIComponent(escape(res.text())))
              : JSON.parse(res.data);
          res.respInfo
            ? res.respInfo.status === 200
              ? resolve(response)
              : reject(response)
            : reject(response);
        })
        .catch(err => {
          reject(err);
        });
      // }
    }),
  getProfileRequest: (path, userToken) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": userToken
      };

      fetchClient
        .fetch("GET", baseURL + path, headers)
        .then(res => {
          const response =
            Platform.OS === "android"
              ? JSON.parse((res.text()))
              : JSON.parse(res.data);
          // console.log(response, 'profile')
          res.respInfo
            ? res.respInfo.status === 200
              ? resolve(response)
              : reject(response)
            : reject(response);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
      // }
    }),
  postRequest: (path, data, userToken) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": userToken
      };
      if (data) {
        fetchClient
          .fetch("POST", baseURL + path, headers, JSON.stringify(data))
          .then(res => {
            const response =
              Platform.OS === "android"
                ? JSON.parse(decodeURIComponent(escape(res.text())))
                : JSON.parse(res.data);
            res.respInfo
              ? res.respInfo.status === 200
                ? resolve(response)
                : reject(response)
              : reject(response);
          })
          .catch(err => {
            reject(err);
          });
      } else {
        fetchClient
          .fetch("POST", baseURL + path, headers)
          .then(res => {
            const response =
              Platform.OS === "android"
                ? JSON.parse(decodeURIComponent(escape(res.text())))
                : JSON.parse(res.data);
            res.respInfo
              ? res.respInfo.status === 200
                ? resolve(response)
                : reject(response)
              : reject(response);
          })
          .catch(err => {
            reject(err);
          });
      }
    }),
  paymentpostRequest: (path, data, userToken) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": userToken
      };
      if (data) {
        fetchClient
          .fetch("POST", baseURL + path, headers, JSON.stringify(data))
          .then(res => res.text())
          .then(text => {
            try {
              const data11 = JSON.parse(text);
              resolve(data11)
            } catch (err) {
              resolve(JSON.stringify(text));
            }
          })
          .catch(err => {
            reject(err);
          });
      } else {
        fetchClient
          .fetch("POST", baseURL + path, headers)
          .then(res => {
            const response =
              Platform.OS === "android"
                ? JSON.parse(decodeURIComponent(escape(res.text())))
                : JSON.parse(res.data);
            res.respInfo
              ? res.respInfo.status === 200
                ? resolve(response)
                : reject(response)
              : reject(response);
          })
          .catch(err => {
            reject(err);
          });
      }
    }),
  postWithoutToken: (path, data) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
      };
      if (data) {

        fetchClient
          .fetch("POST", baseURL + path, headers, JSON.stringify(data))
          .then(res => {
            const response =
              Platform.OS === "android"
                ? JSON.parse(decodeURIComponent(escape(res.text())))
                : JSON.parse(res.data);
            res.respInfo
              ? res.respInfo.status === 200
                ? resolve(response)
                : reject(response)
              : reject(response);
          })
          .catch(err => {
            reject(err);
          });
      }
    }),
  putRequest: (path, data, userToken) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": userToken
      };
      fetchClient
        .fetch("PUT", baseURL + path, headers, JSON.stringify(data))
        .then(res => {
          const response =
            Platform.OS === "android"
              ? JSON.parse(decodeURIComponent(escape(res.text())))
              : JSON.parse(res.data);
          res.respInfo
            ? res.respInfo.status === 200
              ? resolve(response)
              : reject(response)
            : reject(response);
        })
        .catch(err => {
          reject(err);
        });
    }),
  deleteRequest: (path, data, userToken) =>
    new Promise((resolve, reject) => {
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": userToken
      };
      fetchClient
        .fetch("DELETE", baseURL + path, headers, data)
        .then(res => {
          const response =
            Platform.OS === "android"
              ? JSON.parse(decodeURIComponent(escape(res.text())))
              : JSON.parse(res.data);
          res.respInfo
            ? res.respInfo.status === 200
              ? resolve(response)
              : reject(response)
            : reject(response);
        })
        .catch(err => {
          reject(err);
        });
    })
}
export default apiClient;

