import React, { Component } from "react";
import { Text, View, ScrollView, Image, ActivityIndicator } from "react-native";
import styles from "./style";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { othersImage, productImagePath } from "../../../config/imageConst";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";
import Product from "../../partial/ProductBox/Product";


class Favourites extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      favListRes: [],
      isLoading: true,
      backmsg: this.props.navigation.state.params && this.props.navigation.state.params.backmsg ? this.props.navigation.state.params.backmsg : "",

    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  componentDidMount() {
    this.getFavouriteList()
  }

  getFavouriteList = () => {
    AsyncStorage.getItem("userToken")
      .then((results) => {
        if (results) {
          let data = {
            data: ''
          }
          apiClient
            .postRequest("/get-favorites", data, results)
            .then(favListRes => {
              this.setState({ favListRes: favListRes, isLoading: false })
            }).catch(err => console.log(err));
        }
        else {
          this.setState({ isLoading: false })
        }
      })
  }
  getList() {
    this.getFavouriteList()
  }

  goToCartPage = (cart) => {
    this.props.navigation.navigate('Cart')
  }

  backValueReceive = (value) => {
    if (value == "back") {
      if (this.state.backmsg == "profile") {
        let edit = "non";
        this.props.navigation.navigate("ProfileDetails", { edit: edit });
      }
      else {
        this.props.navigation.navigate("Menu");
      }
    }
  };

  render() {

    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        <View style={styles.container}>
          <Text style={styles.textTitle}>{I18n.t("Favourites")}</Text>

          {this.state.isLoading ? <ActivityIndicator style={styles.activityIndicator}
            size="large"
            color="#1A1A1A" /> :
            <ScrollView contentContainerStyle={{ paddingHorizontal: 5 }}>

              {
                this.state.favListRes && this.state.favListRes.length > 0 ?
                  this.state.favListRes.map((item, key) => {
                    return item.discountPercentage > 0 ? (
                      <Product
                        doNotNavigate={true}
                        item={item}
                        key={key}
                        outOfStock={!item.avaiable}
                        productImage={item.backImagePath}
                        productTitle={
                          this.state.locale === "eng" ? item.nameEn : item.nameAr
                        }
                        productStore={
                          this.state.locale === "eng"
                            ? item.merchantNameEn
                            : item.merchantNameAr
                        }
                        before={I18n.t("Before")}
                        productBeforePrice={item.individualPrice}
                        beforeUnit={I18n.t('SAR')}
                        percentage={productImagePath.percent}
                        productPrice={item.individualPriceAfter}
                        productCurrency={I18n.t('SAR')}
                        goToCart={value => this.goToCartPage(value)}
                        navigation={this.props.navigation}
                        fromFav={true}
                        goToFav={value => this.getList()}

                      />
                    ) : (
                        <Product
                          key={key}
                          item={item}
                          outOfStock={!item.avaiable}
                          productImage={item.backImagePath}
                          productTitle={
                            this.state.locale === "eng" ? item.nameEn : item.nameAr
                          }
                          productStore={
                            this.state.locale === "eng"
                              ? item.merchantNameEn
                              : item.merchantNameAr
                          }
                          productPrice={item.individualPrice}
                          productCurrency={I18n.t('SAR')}
                          goToCart={value => this.goToCartPage(value)}
                          navigation={this.props.navigation}
                          doNotNavigate={true}
                          fromFav={true}
                          goToFav={value => this.getList()}
                        />
                      );
                  })
                  :
                  <View style={styles.emptyHead}>
                    <View style={{ width: "100%", alignItems: "center" }}>
                      <Image
                        source={othersImage.heartB}
                        style={{ marginBottom: 20 }}
                      />
                      <Text style={styles.emptyText}>
                        {I18n.t("You haven't added any favorite cards yet")}
                      </Text>
                    </View>
                  </View>
              }
            </ScrollView>
          }
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Favourites);
