const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    backgroundColor: "#fff",
  },
  container: {
    flex: 1,
  },
  headText: {
      marginVertical: 15,
      fontFamily: 'Tajawal-Bold',
      color: '#1A1A1A',
      fontSize: 20,
  },
  emptyHead: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: "4%",
  },
  emptyTextHead: {
    marginBottom: 35,
    fontSize: 20,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
  },
  emptyText: {
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
  },
  textTitle: {
    fontSize: 20,
    fontFamily: "Tajawal-Bold",
    marginTop: 15,
    color: "#1A1A1A",
    paddingHorizontal: 15
  },
  activityIndicator: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
};
