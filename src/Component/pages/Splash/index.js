import React, { Component } from "react";
import {
  Text,
  View,
  Button,
  Image,
  ImageBackground,
  Linking,
  Alert,
} from "react-native";
import I18n from "../../../i18n/index";
import AsyncStorage from "@react-native-community/async-storage";
import styles from "./style.js";
import { setSelectedLanguage } from "../../../redux/language/action";
import { connect } from "react-redux";
import commonStyle from "../../../../commonStyle.js";
import { ActivityIndicator } from "react-native-paper";

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Splash Screen",
      locale: "",
      language: "",
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  _checkLang = () => {
    AsyncStorage.getItem("language")
      .then((res) => {
        const { language, locale } = JSON.parse(res);
        this.props.setSelectedLanguage({ language, locale });
        I18n.locale = locale;
        setTimeout(() => {
          this.props.navigation.navigate(
            locale === "eng" ? "MainStackEn" : "MainStackAr"
          );
        }, 3000);
      })
      .catch((err) => {
        setTimeout(() => {
          this.props.navigation.navigate("ChooseLanguage");
        }, 3000);
      });
  };

  componentDidMount() {
    this._checkLang();
  }

  render() {
    return (
      <View style={styles.container1}>
        <ImageBackground
          source={require("../../assets/image/BG.png")}
          style={styles.image1}
        >
          <Image
            style={{ height: 168, width: 140 }}
            source={require("../../assets/image/LOGOSPLASH.png")}
          />
          <View style={styles.loadingHead}>
            <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A" />
            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    ...state.Language,
  };
}
export default connect(
  mapStateToProps,
  { setSelectedLanguage }
)(Splash);
