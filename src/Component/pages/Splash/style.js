export default {
    container1: {
        flex: 1,
        flexDirection: "column"
      },
      image1: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        alignItems: 'center'
      },

      loadingHead: {
        position: "absolute",
        width: "100%",
        bottom: 15,
        alignItems: 'center'
      },
}