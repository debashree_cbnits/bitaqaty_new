const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    backgroundColor: "#fff",
  },
  container: {
    flex: 1,
    paddingHorizontal: "4%",
  },
  
  headText: {
    marginTop: 13,
    color: "#1A1A1A",
    fontFamily: "Tajawal-Bold",
    fontSize: 20,
  },
  head14Text: {
    marginVertical: 13,
    color: "#1A1A1A",
    fontFamily: "Tajawal-Bold",
    fontSize: 14,
  },
  aboutText: {
    marginTop: 15,
    color: "#4F4F4F",
    fontFamily: "Tajawal-Medium",
    fontSize: 16,
    lineHeight: 20,
  },
  
  emailText: {
    color: "#4F4F4F",
    fontFamily: "Tajawal-Medium",
    fontSize: 16,
  },
  partnersSec: {
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  partnersSec_ar: {
    width: "100%",
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  partners: {
    width: '48%',
    paddingVertical: 10,
    marginBottom: 15,
    alignItems:'center',
    backgroundColor: '#fff',
    elevation: 3,
    borderRadius: 4
  },
  partnersText: {
    fontSize: 12,
    fontFamily: "Tajawal-Medium",
    color: "#1A1A1A",
  },
  lightText: {
    color: '#4F4F4F',
    fontFamily: 'Tajawal-Light',
    fontSize: 14,
    marginTop: 5,
  },
  stepSec: {    
    height: 48,
    paddingHorizontal: '3%',
    backgroundColor: '#fff',    
    flexDirection: 'row',
    justifyContent: 'space-between',
    elevation: 3,
    borderRadius: 4,
    alignItems: 'center'
  },
  stepSec_ar: {    
    height: 48,
    paddingHorizontal: '3%',
    backgroundColor: '#fff',    
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    elevation: 3,
    borderRadius: 4,
    alignItems: 'center'
  },
  stepSecText: {
    fontFamily: 'Tajawal-Medium',
    fontSize: 14,
    color: '#1A1A1A',
  },
  stepBody: {
    backgroundColor: '#F2F0F0',
    padding: 10,
    borderRadius: 4,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
  },
  boldText: {
    marginTop: 20,
    marginBottom: 10,
    fontFamily: 'Tajawal-Bold',
    color: '#1A1A1A',
    fontSize: 16,
  },
  medText: {
    color: '#4F4F4F',
    fontFamily: 'Tajawal-Medium',
    fontSize: 16,
  },
  medBoldText: {
    marginVertical: 12,
    color: '#4F4F4F',
    fontFamily: 'Tajawal-Bold',
    fontSize: 14,
  },
  medText2: {
    marginBottom: 16,
    color: '#4F4F4F',
    fontFamily: 'Tajawal-Medium',
    fontSize: 16,
  },
  boldText2: {
    marginTop: 20,
    marginBottom: 10,
    fontFamily: 'Tajawal-Bold',
    color: '#1A1A1A',
    fontSize: 16,
  },
  containerForPrivacy :{
    flex: 1,
    width: "92%",
    marginHorizontal: "4%",    
    paddingTop: 10,
  },
  pageTitle: {
    fontSize: 20,
    color: "#1A1A1A",
    marginBottom: 5,
    fontFamily: "Tajawal-Bold",
  },
  smallText: {
    color: "#DF14C5",
    fontSize: 10,
    height: 29,
    borderWidth: 1,
    borderStyle: "dashed",
    borderColor: "#DF14C5",
    width: 180,
    borderRadius: 3,
    textAlign: "center",
    paddingTop: 7,
  },
  infoSec: {
    paddingHorizontal: "3%",
    paddingVertical: 15,
    elevation: 2,
    backgroundColor: "#fff",
    borderRadius: 4,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    marginVertical: 5,
  },
  infoSec_ar: {
    paddingHorizontal: "3%",
    paddingVertical: 15,    
    elevation: 2,
    backgroundColor: "#fff",
    borderRadius: 4,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row-reverse",
    marginVertical: 2,
  },
  infoSec_ar2: {
    paddingHorizontal: "3%",
    paddingVertical: 15,
    elevation: 2,
    backgroundColor: "#fff",
    borderRadius: 4,
    alignItems: "center",
    flexDirection: "row-reverse",
    marginVertical: 2,
  },
  dropText: {
    color: "#1A1A1A",
    fontSize: 14,
    fontFamily: "Tajawal-Medium",
    width: '85%'
  },
  dropText2: {
    color: "#1A1A1A",
    fontSize: 14,
    fontFamily: "Tajawal-Medium",
    width: '90%'
  },
  dropTextLight: {
    marginBottom: 20,
    color: "#4F4F4F",
    fontSize: 14,
    fontFamily: "Tajawal-Light",
  },
  dropTextLight2: {
    color: "#4F4F4F",
    fontSize: 14,
    fontFamily: "Tajawal-Light",
  },
  collapseBody: {
    backgroundColor: "#F2F0F0",
    padding: 15,
  },
  textB: {
    fontSize: 25,
    lineHeight: 26
  },
  termsText: {
    paddingHorizontal: 10,
    fontSize: 14,
    fontFamily: "Tajawal-Light",
    color: '#4F4F4F',
  },
  paraRow : {
    flexDirection:'row', 
    marginBottom: 10
  },
  paraRowRev : {
    flexDirection:'row-reverse', 
    marginBottom: 10
  }
};
