import React, { Component } from "react";
import { Text, View, ScrollView, Image } from "react-native";
import styles from "./style";
import { menuScreen } from "../../../config/imageConst";
import { WebView } from "react-native-webview";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import commonStyle from "../../../../commonStyle";

class HowToBuy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      showArrowIcon: false,
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Menu");
    }
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.headText}>{I18n.t("How to buy")}</Text>
            <View
              style={{
                marginTop: 10,
                height: 157,
                borderRadius: 8,
                overflow: "hidden",
              }}
            >
              <WebView
                style={styles.WebViewContainer}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: "https://www.youtube.com/embed/-Fs41BWA84U" }}
              />
            </View>
            <Text style={styles.lightText}>
              {I18n.t(
                "A detailed explanation of the steps of the purchase in English"
              )}
            </Text>

            <Collapse style={{ marginVertical: 20 }} isCollapsed={true} onToggle={() => this.setState({ showArrowIcon: !this.state.showArrowIcon })}>
              <CollapseHeader>
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.stepSec
                      : styles.stepSec_ar
                  }
                >
                  <Text>{I18n.t("Steps")}</Text>
                  <Image
                    source={menuScreen.downarrow}
                    style={[commonStyle.rotate90, { transform: this.state.showArrowIcon ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }] }]}
                  />
                </View>
              </CollapseHeader>

              <CollapseBody style={styles.stepBody}>
                <Text style={styles.medText}>
                  {I18n.t("HowPart1_1")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("HowPart1_2")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("HowPart1_3")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("HowPart1_4")}
                </Text>

                <Text style={styles.boldText}>
                  {I18n.t(
                    "If you didn’t register before the next step is registration"
                  )}
                </Text>

                <Text style={styles.medText}>
                  {I18n.t("5- Click on Create free account")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("6- You will fill all fields")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("I) Name at least first and second name")}
                </Text>
                <Text style={styles.medText}>{I18n.t("II) Email")}</Text>
                <Text style={styles.medText}>
                  {I18n.t(
                    "III) Password: by English letters, It contains at least 8 letters, one of them is Capital letter and at least one Number"
                  )}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("IV) Repeat fill password")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t(
                    "V) Your mobile number is Saudi number"
                  )}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("VI) Click on I am not Robot")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("VII) Click on Create Account")}
                </Text>

                <Text style={styles.medBoldText}>
                  {I18n.t("Verify your email")}
                </Text>
                <Text style={styles.medText2}>
                  {I18n.t("VI) Click on I am not Robot")}
                </Text>

                <Text style={styles.medBoldText}>
                  {I18n.t("verify your email")}
                </Text>
                <Text style={styles.medText2}>
                  {I18n.t("8- enter the code that sent on email")}
                </Text>

                <Text style={styles.boldText2}>
                  {I18n.t("The receipt will appear")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("Choose the payment methods")}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("Fill the bank account data")}
                </Text>

                <Text style={styles.medBoldText}>
                  {I18n.t(
                    "Your bank data will not save for secure your bank account"
                  )}
                </Text>
                <Text style={styles.medText}>
                  {I18n.t("Enter password of bank account")}
                </Text>

                <Text style={styles.boldText2}>
                  {I18n.t("Now Cards between your hands!")}
                </Text>
              </CollapseBody>
            </Collapse>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(HowToBuy);
