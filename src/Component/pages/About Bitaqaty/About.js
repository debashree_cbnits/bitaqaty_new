import React, { Component } from "react";
import { Text, View, Image, ScrollView, TouchableOpacity } from "react-native";
import styles from "./style";
import { othersImage } from "../../../config/imageConst";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";

class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Menu");
    }
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.headText}>{I18n.t("About Us")}</Text>
            <Text style={styles.aboutText}>{I18n.t("1st Paragraph")}</Text>
            <Text style={styles.aboutText}>{I18n.t("2nd Paragraph")}</Text>
            <Text style={styles.aboutText}>{I18n.t("3rd Paragraph")}</Text>
            <Text style={styles.aboutText}>{I18n.t("4th Paragraph")}</Text>
            <Text style={styles.aboutText}>{I18n.t("5th Paragraph")}</Text>

            <View style={{ paddingVertical: 20 }}>
              <Text style={styles.emailText}>{I18n.t("About Email")} <Text>info@bitaqaty.com</Text></Text>
              <Text style={styles.emailText}>{I18n.t("About Phone")} <Text>966920033472</Text></Text>
            </View>

            <Text style={styles.head14Text}>{I18n.t("Bitaqaty Partners")}</Text>

            <View
              style={
                this.state.locale == "eng"
                  ? styles.partnersSec
                  : styles.partnersSec_ar
              }
            >
              <View style={styles.partners}>
                <Image source={othersImage.Maroof} />
                <Text style={styles.partnersText}>{I18n.t("Maroof")}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(About);
