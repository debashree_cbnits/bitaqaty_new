import React, { Component } from "react";
import { Text, View, Image, ScrollView } from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import { othersImage } from "../../../config/imageConst";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import { menuScreen } from "../../../config/imageConst";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";

class Terms extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      showArrowIcon1: false,
      showArrowIcon2: false,
      showArrowIcon3: false,
      showArrowIcon4: false,
      showArrowIcon5: false,
      showArrowIcon6: false,
      showArrowIcon7: false,
      showArrowIcon8: false,

    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Menu");
    }
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        <ScrollView>
          <View style={{ ...styles.containerForPrivacy, paddingVertical: "5%" }}>
            <View
              style={
                this.state.locale == "eng" ? styles.faqHead : styles.faqHead_ar
              }
            >
              <Text style={styles.pageTitle}>{I18n.t("Terms of use")}</Text>
            </View>

            <Collapse onToggle={() => this.setState({ showArrowIcon1: !this.state.showArrowIcon1 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar
                }
              >
                <Text style={styles.dropText2}>{I18n.t("First Title")}</Text>
                <Image source={menuScreen.downarrow} style={{ transform: this.state.showArrowIcon1 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.dropTextLight2}>{I18n.t("First Paragraph")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle={() => this.setState({ showArrowIcon2: !this.state.showArrowIcon2 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar
                }
              >
                <Text
                  style={this.state.locale == 'eng' ?
                    [commonStyle.align_en, styles.dropText2] :
                    [commonStyle.align_ar, styles.dropText2]
                  }>
                  {I18n.t(
                    "Second Title"
                  )}
                </Text>
                <Image source={menuScreen.downarrow} style={{ transform: this.state.showArrowIcon2 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.dropTextLight2}>{I18n.t("Second Paragraph1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("Second Paragraph2")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle={() => this.setState({ showArrowIcon3: !this.state.showArrowIcon3 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >
                <Text style={{ alignSelf: 'flex-start' }}>{I18n.t("One")}</Text>
                <Text style={styles.dropText}>{I18n.t("Third Title")}</Text>

                <Image style={{ marginLeft: 'auto', transform: this.state.showArrowIcon3 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <View>
                  <Text style={{ ...styles.termsText, marginBottom: 15 }}>{I18n.t("3rd Para1")}</Text>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para11")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para12")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para13")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para14")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para15")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para16")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para17")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para18")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("3rd Para19")}</Text></View>
                </View>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle={() => this.setState({ showArrowIcon4: !this.state.showArrowIcon4 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >

                <Text style={{ alignSelf: 'flex-start' }}>{I18n.t("Two")}</Text>
                <Text style={styles.dropText}>{I18n.t("Fourth Title")}</Text>
                <Image style={{ marginLeft: 'auto', transform: this.state.showArrowIcon4 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("4th Para1")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("4th Para2")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("4th Para3")}</Text></View>
                </View>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle={() => this.setState({ showArrowIcon5: !this.state.showArrowIcon5 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >
                <Text style={{ alignSelf: 'flex-start' }}>{I18n.t("Three")}</Text>
                <Text style={styles.dropText}>{I18n.t("Fifth Title")}</Text>
                <Image style={{ marginLeft: 'auto', transform: this.state.showArrowIcon5 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <View>
                  <Text style={{ ...styles.termsText, marginBottom: 15 }}>{I18n.t("5th Para1")}</Text>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para11")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para12")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para121")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para122")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para13")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para14")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para15")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("5th Para16")}</Text></View>
                </View>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle={() => this.setState({ showArrowIcon6: !this.state.showArrowIcon6 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >
                <Text style={{ alignSelf: 'flex-start' }}>{I18n.t("Four")}</Text>
                <Text style={styles.dropText}>{I18n.t("Sixth Title")}</Text>
                <Image style={{ marginLeft: 'auto', transform: this.state.showArrowIcon6 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para1")}</Text></View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para2")}</Text></View>

                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para21")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para22")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para23")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para24")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para25")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para26")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para27")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para28")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para29")}</Text></View>
                  <View style={{ flexDirection: 'row', marginBottom: 10, marginLeft: 20 }}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para210")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para3")}</Text></View>

                  <View style={this.state.locale == "eng" ? [styles.paraRow, { marginLeft: 20 }] : [styles.paraRowRev, { marginRight: 20 }]}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para31")}</Text></View>

                  <View style={this.state.locale == "eng" ? [styles.paraRow, { marginLeft: 20 }] : [styles.paraRowRev, { marginRight: 20 }]}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para32")}</Text></View>

                  <View style={this.state.locale == "eng" ? [styles.paraRow, { marginLeft: 20 }] : [styles.paraRowRev, { marginRight: 20 }]}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para33")}</Text></View>

                  <View style={this.state.locale == "eng" ? [styles.paraRow, { marginLeft: 20 }] : [styles.paraRowRev, { marginRight: 20 }]}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("6th Para34")}</Text></View>

                </View>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle={() => this.setState({ showArrowIcon7: !this.state.showArrowIcon7 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >
                <Text style={{ alignSelf: 'flex-start' }}>{I18n.t("Five")}</Text>
                <Text style={styles.dropText}>{I18n.t("Seventh Title")}</Text>
                <Image style={{ marginLeft: 'auto', transform: this.state.showArrowIcon7 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.dropTextLight2}>{I18n.t("Seventh Paragraph")}</Text>
              </CollapseBody>
            </Collapse>
            <Collapse onToggle={() => this.setState({ showArrowIcon8: !this.state.showArrowIcon8 })}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >
                <Text style={{ alignSelf: 'flex-start' }}>{I18n.t("Six")}</Text>
                <Text style={styles.dropText}>{I18n.t("Eighth Title")}</Text>
                <Image style={{ marginLeft: 'auto', transform: this.state.showArrowIcon8 ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <View>
                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}>
                    <Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}>
                      <Text style={{ fontWeight: 'bold' }}>{I18n.t("Governing Law")}
                      </Text>{I18n.t("8th Para1")}</Text>
                  </View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}>
                    <Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Dispute Resolution")}</Text>{I18n.t("8th Para2")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Relationship of the Parties")}</Text>{I18n.t("8th Para3")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Further Assurances")}</Text>{I18n.t("8th Para4")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Entire Agreement")}</Text>{I18n.t("8th Para5")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Amendment")}</Text>{I18n.t("8th Para6")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Severability")}</Text>{I18n.t("8th Para7")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Force Majeure")}</Text>{I18n.t("8th Para8")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("No Waiver")}</Text>{I18n.t("8th Para9")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Communications")}</Text>{I18n.t("8th Para10")}</Text></View>

                  <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022'}</Text><Text style={styles.termsText}><Text style={{ fontWeight: 'bold' }}>{I18n.t("Survival")}</Text>{I18n.t("8th Para11")}</Text></View>
                </View>
              </CollapseBody>
            </Collapse>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Terms);
