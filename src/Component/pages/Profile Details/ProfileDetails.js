import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Modal,
  ActivityIndicator,
  Alert,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from "react-native-simple-radio-button";
import I18n from "../../../i18n/index";
import {
  profile,
  CatagoryImagePath,
  menuScreen,
  orderHistoryImage,
} from "../../../config/imageConst";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";
import { CheckBox } from "react-native-elements";
import ModalProfileUpdate from '../Modal/ModalProfileUpdate';


const deviceHeight = Dimensions.get("window").height;

class ProfileDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      profileDetailsData: "",
      userDetails: {},
      name: "",
      email: "",
      mobile: "",
      city: "",
      country: "",
      isLoading: true,
      selectedPreferenceValue: "Phone Calls",
      selectedPreferenceIndex: 4,
      gender: [
        { label: I18n.t("Male"), value: I18n.t("Male") },
        { label: I18n.t("Female"), value: I18n.t("Female") },
      ],
      show: false,
      showProfileArrowIcon: false,
      showArrowIcon: false,
      showmodalsave: false,
      socialLoginType: "",
    };
    // Alert.alert("Constructor")
  }

  getDataFromApi = async () => {
    let socialtype = await AsyncStorage.getItem("socialLoginType")
    this.setState({ socialLoginType: socialtype })
    apiClient
      .getRequest("/preferences")
      .then((preferencess) => {
        this.setState({ preferences: preferencess })
        // this.getProfileDetails();
      })
      .catch(err => {
        console.log("err", err);
      });

    AsyncStorage.getItem("loginTypeInfo")
      .then((results) => {
        if (results == "normal") {
          this.setState({ changePassShow: true })
        } else {
          this.setState({ changePassShow: false })
        }
      }).catch(err => console.log(err))

  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }
  async componentDidMount() {
    this.getDataFromApi();
    this.props.navigation.addListener('willFocus', () => {
      this.setState({ isLoading: true })
      this.getDataFromApi();
      console.log("willFocus", this.props.navigation.state.params.edit);
      apiClient
        .getProfileRequest("/account-profile", this.state.userToken)
        .then((profileDetailsRes) => {
          console.log("profileDetailsRes FOCUS", profileDetailsRes);
          this.setState({ profileDetailsData: profileDetailsRes, isLoading: false })
          let edit = this.props.navigation.state.params.edit ? this.props.navigation.state.params.edit : "";
          if (edit == "edit") {
            console.log("edit");
            this.setState({ show: true })
          }
          setTimeout(() => {
            this.setState({ show: false })
          }, 3000);
        })
        .catch(err => {
        });
    });

    try {
      const value = await AsyncStorage.getItem("userToken");
      if (value !== null) {
        this.setState({ userToken: value });
        apiClient
          .getProfileRequest("/account-profile", this.state.userToken)
          .then((profileDetailsRes) => {
            this.setState({ profileDetailsData: profileDetailsRes, isLoading: false })
            let edit = this.props.navigation.state.params.edit ? this.props.navigation.state.params.edit : "";
            if (edit == "edit") {
              console.log("edit");
              this.setState({ show: true })
            }
            setTimeout(() => {
              this.setState({ show: false })
            }, 3000);
          })
          .catch(err => {
            this.setState({ isLoading: false })
          });
      }
    } catch (error) {
    }
  }

  // componentDidMount() {
  //   this.getUserProfileDetails()     
  //   this.props.navigation.addListener('didFocus', () => {                  
  //     let edit = this.props.navigation.state.params.edit ? this.props.navigation.state.params.edit : "";
  //     if (this.props.navigation.state.params && this.props.navigation.state.params.edit){
  //       Alert.alert("blur")
  //       this.setState({ showmodalsave : false })        
  //       this.getUserProfileDetails()       
  //     }
  //   });
  //   this.props.navigation.addListener('didBlur', () => {                  
  //     let edit = this.props.navigation.state.params.edit ? this.props.navigation.state.params.edit : "";
  //     if (this.props.navigation.state.params && this.props.navigation.state.params.edit){
  //       Alert.alert("blur")
  //       this.setState({ showmodalsave : false })        
  //       this.getUserProfileDetails()       
  //     }
  //   });
  // }
  // async getUserProfileDetails(){    
  //   try {
  //     const value = await AsyncStorage.getItem("userToken");
  //     if (value !== null) {
  //       this.setState({ userToken: value });
  //       apiClient
  //         .getProfileRequest("/account-profile", this.state.userToken)
  //         .then((profileDetailsRes) => {
  //           console.log(profileDetailsRes, 'profileDetailsRes')
  //           this.setState({ profileDetailsData: profileDetailsRes, isLoading: false })
  //         })
  //         .catch(err => {
  //           console.log("err", err);
  //           this.setState({ isLoading: false })
  //         });
  //     }
  //   } catch (error) {
  //   }
  // }

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Menu");
    }
  };

  signOut = async () => {
    AsyncStorage.removeItem('userToken');
    AsyncStorage.removeItem('loginuserName');
    AsyncStorage.removeItem('loginEmailId');
    AsyncStorage.removeItem('userDetails');
    AsyncStorage.removeItem('loginTypeInfo');
    AsyncStorage.removeItem("checkoutData");
    AsyncStorage.removeItem("paymentstart")

    let no = 0;
    let noOfAttems = { 'no': no };
    AsyncStorage.setItem("noOfAttems", JSON.stringify(noOfAttems));
    this.props.navigation.navigate("HomeScreen")
  };

  goToProfileEdit = () => {
    this.setState({ showmodalsave: true })

    var gen_date = "";
    var selectedGenderValue = "";
    var selectedGender = true;

    var userpreferences = [];
    if (this.state.profileDetailsData.birthDateString != null) {
      let gender = this.state.profileDetailsData.birthDateString;
      var gen = gender.split(' ');
      gen_date = gen[0];
      var dateshow1 = gen_date.split('-');
      var dateshow = dateshow1[2] + '/' + dateshow1[1] + '/' + dateshow1[0];
    }

    if (this.state.profileDetailsData.genderId != null) {
      selectedGenderValue = this.state.profileDetailsData.genderEn == "Male" ? 0 : 1;
      selectedGender = false;
    }

    var preferenceOfUser = this.state.preferences;

    if (this.state.profileDetailsData.preferencesDTO && this.state.profileDetailsData.preferencesDTO.length) {
      userpreferences = this.state.profileDetailsData.preferencesDTO;
      for (let i = 0; i < userpreferences.length; i++) {
        for (let j = 0; j < preferenceOfUser.length; j++) {
          if (userpreferences[i].id == preferenceOfUser[j].id) {
            preferenceOfUser[j].preferenceChecked = userpreferences[i].preferenceChecked;
          }
        }
      }
    }

    this.props.navigation.navigate('EditProfile',
      {
        name: this.state.profileDetailsData.userName,
        email: this.state.profileDetailsData.email,
        mobile: this.state.profileDetailsData.mobileNumber && this.state.profileDetailsData.mobileNumber.length > 0 && this.state.profileDetailsData.mobileNumber.slice(5),
        country: this.state.locale == 'eng' ? this.state.profileDetailsData.countryNameEn : this.state.profileDetailsData.countryNameAr,
        city: this.state.locale == 'eng' ? this.state.profileDetailsData.cityNameEn : this.state.profileDetailsData.cityNameAr,
        dob: dateshow,
        dobapi: gen_date,
        selectedGenderValue: selectedGenderValue,
        selectedGender: selectedGender,
        preferences: preferenceOfUser,
        userDetails: this.state.profileDetailsData
      })
  }

  goToFavorite = () => {
    let backmsg = "profile";
    this.props.navigation.navigate("Favourites", { backmsg: backmsg })
  }

  goToOrderHistory = () => {
    let backmsg = "profile";
    this.props.navigation.navigate("OrderHistory", { backmsg: backmsg })
  }

  goToChangePas = () => {
    let backmsg = "profile";
    this.props.navigation.navigate("ChangePassword", { backmsg: backmsg })
  }


  render() {
    const { userDetails } = this.state;
    var selectedGenderValue = "";
    var selectedGender = true;
    var gen_date1 = "";

    if (this.state.profileDetailsData.birthDateString != null) {
      let gender = this.state.profileDetailsData.birthDateString;
      var gen = gender.split(' ');
      gen_date1 = gen[0];
      var dateshow1 = gen_date1.split('-');
      var gen_date = dateshow1[2] + '/' + dateshow1[1] + '/' + dateshow1[0];
    }
    if (this.state.profileDetailsData.genderId != null) {
      selectedGenderValue = this.state.profileDetailsData.genderEn == "Male" ? 0 : 1;
      selectedGender = false;
    }

    var preferenceOfUser = this.state.preferences;

    if (preferenceOfUser && preferenceOfUser.length >= 1) {
      var userpreferences = this.state.profileDetailsData.preferencesDTO;
      if (this.state.profileDetailsData.preferencesDTO && this.state.profileDetailsData.preferencesDTO.length >= 1) {
        for (let i = 0; i < preferenceOfUser.length; i++) {
          for (let j = 0; j < userpreferences.length; j++) {
            if (userpreferences[j].id == preferenceOfUser[i].id) {
              preferenceOfUser[i].preferenceChecked = true;
              break
            }
            preferenceOfUser[i].preferenceChecked = false;
          }
        }
      }
    }

    return (
      <View style={styles.mainSection}>
        <Modal transparent={true} visible={this.state.show}>
          <ModalProfileUpdate />
        </Modal>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        {this.state.isLoading ? (
          <ActivityIndicator style={styles.activityIndicator}
            size="small"
            color="#1A1A1A" />
        ) : (
            <ScrollView>
              <View style={styles.container}>
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.userEditSec
                      : styles.userEditSec_ar
                  }
                >
                  <View
                    style={
                      this.state.locale == "eng"
                        ? [commonStyle.rowSec, { alignItems: 'center' }]
                        : [commonStyle.rowSec_ar, { alignItems: 'center' }]
                    }
                  >
                    <Text style={styles.helloMed16}>{I18n.t("Hello")}</Text>
                    <Text style={styles.pageTitle}> {this.state.profileDetailsData.userName} </Text>
                  </View>
                  <TouchableOpacity
                    onPress={this.goToProfileEdit}
                    style={
                      this.state.locale == "eng"
                        ? styles.editSec
                        : styles.editSec_ar
                    }
                  >
                    <Image source={profile.editIcon} />
                    <Text>{I18n.t("Edit")}</Text>
                  </TouchableOpacity>
                </View>

                {/*## PROFILE COLLAPSE SECTION ##*/}
                <Collapse isCollapsed={true} onToggle={() => this.setState({ showProfileArrowIcon: !this.state.showProfileArrowIcon })}>
                  <CollapseHeader>
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.tabDrop
                          : styles.tabDrop_ar
                      }
                    >
                      <View
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.rowSec
                            : commonStyle.rowSec_ar
                        }
                      >
                        <Image source={profile.proUser} />
                        <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                          {I18n.t("Profile")}
                        </Text>
                      </View>
                      <Image
                        source={CatagoryImagePath.rightArrow}
                        style={{ transform: this.state.showProfileArrowIcon ? [{ rotate: '90deg' }] : [{ rotate: '270deg' }] }}
                      />
                    </View>
                  </CollapseHeader>

                  {/*## Profile COLLAPSE BODY SECTION ##*/}
                  <CollapseBody style={[commonStyle.cbEli, { padding: 8 }]}>
                    <View style={commonStyle.inputMar}>
                      <Text
                        //style={commonStyle.inputText}
                        style={
                          this.state.locale == "eng"
                            ? [commonStyle.inputText, commonStyle.none]
                            : [commonStyle.inputText, commonStyle.rightAlign]
                        }
                      >{I18n.t("Name")}</Text>
                      <TextInput
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.inputField_en
                            : commonStyle.inputField_ar
                        }
                        value={this.state.profileDetailsData.userName}
                        placeholder={I18n.t("Ali")}
                        placeholderTextColor="#4F4F4F"
                        editable={false}
                      />
                    </View>
                    <View style={commonStyle.inputMar}>
                      <Text style={
                        this.state.locale == "eng"
                          ? [commonStyle.inputText, commonStyle.none]
                          : [commonStyle.inputText, commonStyle.rightAlign]
                      }>{I18n.t("Email")}</Text>
                      <TextInput
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.inputField_en
                            : commonStyle.inputField_ar
                        }
                        editable={false}
                        value={this.state.profileDetailsData.email}
                        placeholder="ali@mail.com"
                        placeholderTextColor="#4F4F4F"
                        editable={false}
                      />
                    </View>
                    <View style={commonStyle.inputMar}>
                      <Text style={
                        this.state.locale == "eng"
                          ? [commonStyle.inputText, commonStyle.none]
                          : [commonStyle.inputText, commonStyle.rightAlign]
                      }>
                        {I18n.t("Mobile Number")}
                        <Text style={commonStyle.countryText}>
                          {" "}
                          {I18n.t("(Saudi)")}{" "}
                        </Text>
                      </Text>
                      <View
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.rowSec
                            : commonStyle.rowSec_ar
                        }
                      >
                        <View
                          style={
                            this.state.locale == "eng"
                              ? commonStyle.mobileInput
                              : commonStyle.mobileInput_ar
                          }
                        >
                          <View style={commonStyle.countryCode}>
                            <Text style={commonStyle.countryCodeText}>966</Text>
                          </View>
                          <TextInput
                            style={
                              this.state.locale == "eng"
                                ? commonStyle.mobileTextInput
                                : commonStyle.mobileTextInput_ar
                            }
                            value={this.state.profileDetailsData.mobileNumber && this.state.profileDetailsData.mobileNumber.slice(5)}
                            placeholder=" 123456789"
                            keyboardType="numeric"
                            underlineColorAndroid="transparent"
                            editable={false}

                          />
                        </View>

                        <View style={{ width: "15%", justifyContent: "center" }}>
                          <Image
                            style={
                              this.state.locale == "eng"
                                ? commonStyle.flagImage
                                : commonStyle.flagImage_ar
                            }
                            source={menuScreen.flag2}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={commonStyle.inputMar}>
                      <Text style={
                        this.state.locale == "eng"
                          ? [commonStyle.inputText, commonStyle.none]
                          : [commonStyle.inputText, commonStyle.rightAlign]
                      }>
                        {I18n.t("Country")}
                      </Text>
                      <Collapse>
                        <CollapseHeader>
                          <View
                            style={
                              this.state.locale == "eng"
                                ? [commonStyle.rowSec, styles.dropField]
                                : [commonStyle.rowSec_ar, styles.dropField_ar]
                            }
                          >
                            <Text style={styles.placeText}>
                              {this.state.locale == 'eng' ? this.state.profileDetailsData.countryNameEn : this.state.profileDetailsData.countryNameAr}
                            </Text>
                            <Image
                              source={CatagoryImagePath.rightArrow}
                              style={{ ...commonStyle.rotate90, opacity: 0.3 }}
                            />
                          </View>
                        </CollapseHeader>
                      </Collapse>
                    </View>

                    <View style={commonStyle.inputMar}>
                      <View
                        style={
                          this.state.locale == "eng"
                            ? [commonStyle.rowSec, { alignItems: "center" }]
                            : commonStyle.rowSec_ar
                        }
                      >
                        <Text style={commonStyle.inputText}>
                          {I18n.t("City")}
                        </Text>
                        {this.state.profileDetailsData.cityNameEn == null ? (
                          <Image
                            source={profile.profileError}
                            style={commonStyle.marHori}
                          />
                        ) : null}
                      </View>

                      <Collapse>
                        <CollapseHeader>
                          <View
                            style={
                              this.state.locale == "eng"
                                ? [commonStyle.rowSec, styles.dropField]
                                : [commonStyle.rowSec_ar, styles.dropField_ar]
                            }
                          >
                            {this.state.profileDetailsData.cityNameEn == null ? (
                              <Text style={styles.placeText}>{I18n.t("Choose Your City")}</Text>
                            ) : (
                                <Text style={styles.placeText}>{this.state.locale == 'eng' ? this.state.profileDetailsData.cityNameEn : this.state.profileDetailsData.cityNameAr}</Text>

                              )}
                            <Image
                              source={CatagoryImagePath.rightArrow}
                              style={{ ...commonStyle.rotate90, opacity: 0.3 }}
                            />
                          </View>
                        </CollapseHeader>

                        {/* <CollapseBody style={styles.clpsBody}>
                        <View style={styles.clipBodyCntn}>
                          <Text style={styles.placeText}>{I18n.t("City")}</Text>
                        </View>
                      </CollapseBody> */}
                      </Collapse>
                    </View>

                    <View style={commonStyle.inputMar}>
                      <View
                        style={
                          this.state.locale == "eng"
                            ? [commonStyle.rowSec, { alignItems: "center" }]
                            : commonStyle.rowSec_ar
                        }
                      >
                        <Text style={commonStyle.inputText}>
                          {I18n.t("Birth Date")}
                        </Text>
                        {this.state.profileDetailsData.birthDateString == null ? (
                          <Image
                            source={profile.profileError}
                            style={commonStyle.marHori}
                          />
                        ) : null}
                      </View>
                      <View
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.rowSec
                            : commonStyle.rowSec_ar
                        }
                      >
                        <View
                          style={
                            this.state.locale == "eng"
                              ? [commonStyle.rowSec, styles.dateField]
                              : [commonStyle.rowSec_ar, styles.dateField_ar]
                          }
                        >
                          {this.state.profileDetailsData.birthDateString == null ? (
                            <Text style={styles.placeText}>
                              {I18n.t("Day / Month / Year")}
                            </Text>
                          ) : (
                              <Text style={styles.placeText}>
                                {gen_date}
                              </Text>
                            )}
                          <Image
                            source={CatagoryImagePath.rightArrow}
                            style={{ ...commonStyle.rotate90, opacity: 0.3 }}
                          />
                        </View>
                        <TouchableOpacity
                          style={{ width: "15%", justifyContent: "center" }}
                        >
                          <Image
                            style={
                              this.state.locale == "eng"
                                ? commonStyle.cldrImage
                                : commonStyle.cldrImage_ar
                            }
                            source={orderHistoryImage.roundCalendar}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>

                    <View style={commonStyle.inputMar}>
                      <View
                        style={
                          this.state.locale == "eng"
                            ? [commonStyle.rowSec, { alignItems: "center" }]
                            : commonStyle.rowSec_ar
                        }
                      >
                        <Text style={commonStyle.inputText}>
                          {I18n.t("Gender")}
                        </Text>
                        {selectedGender ? (
                          <Image
                            source={profile.profileError}
                            style={commonStyle.marHori}
                          />
                        ) : null}
                      </View>
                      <View
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.radiofSrt
                            : commonStyle.radioRotate
                        }
                      >
                        <RadioForm
                          animation={true}
                          disabled={false}
                          formHorizontal={false}
                        >
                          {this.state.gender.map((obj, i) => {
                            var onPress = (value, index) => {
                              // this.setState({
                              //   genderValue: value,
                              //   selectedGenderValue: index,
                              // });
                            };
                            return (
                              <RadioButton key={i}>
                                <RadioButtonInput
                                  obj={obj}
                                  index={i}
                                  isSelected={
                                    selectedGenderValue === i
                                  }
                                  onPress={onPress}
                                  buttonInnerColor={"#241125"}
                                  buttonOuterColor={
                                    selectedGenderValue === i
                                      ? "#241125"
                                      : "#241125"
                                  }
                                  buttonSize={10}
                                  buttonOuterSize={20}
                                //buttonWrapStyle={{ marginLeft: 10 }}
                                />
                                <RadioButtonLabel
                                  obj={obj}
                                  index={i}
                                  labelHorizontal={true}
                                  onPress={onPress}
                                  labelStyle={
                                    this.state.locale == "eng" &&
                                      selectedGenderValue === i
                                      ? commonStyle.radioBoldText
                                      : this.state.locale == "ar" &&
                                        selectedGenderValue === i
                                        ? commonStyle.radioSelRotate
                                        : this.state.locale == "ar"
                                          ? commonStyle.radioSelRotate2
                                          : commonStyle.radioLightText
                                  }
                                />
                              </RadioButton>
                            );
                          })}
                          {/* ))} */}
                        </RadioForm>
                      </View>
                    </View>
                  </CollapseBody>
                </Collapse>

                <Collapse isCollapsed={true} onToggle={() => this.setState({ showArrowIcon: !this.state.showArrowIcon })}>
                  <CollapseHeader>
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.tabDrop
                          : styles.tabDrop_ar
                      }
                    >
                      <View
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.rowSec
                            : commonStyle.rowSec_ar
                        }
                      >
                        <Image source={profile.proLike} />
                        <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                          {I18n.t("Preferences")}
                        </Text>
                      </View>
                      <Image
                        source={CatagoryImagePath.rightArrow}
                        style={{ transform: this.state.showArrowIcon ? [{ rotate: '90deg' }] : [{ rotate: '270deg' }] }}
                      />
                    </View>
                  </CollapseHeader>

                  {/*## Preferences COLLAPSE BODY SECTION ##*/}
                  <CollapseBody style={{ ...commonStyle.cbEli, paddingTop: 20, paddingRight: 8 }}>
                    {preferenceOfUser && preferenceOfUser.length >= 1 ?
                      preferenceOfUser.map((item, key) => {
                        return (
                          <View
                            style={this.state.locale == "eng" ? styles.checkBoxViewEng : styles.checkBoxViewAr}
                          >

                            <CheckBox
                              disabled
                              style={{ color: "black" }}
                              checked={item.preferenceChecked}
                              checkedColor="#CCCCCC"
                            />

                            <Text
                              style={this.state.locale == "eng" ? [styles.checkboxEng, {color: '#CCCCCC'}] : [styles.checkboxAr, {color: '#CCCCCC'}]}
                            >
                              {this.state.locale == 'eng' ? item.preferenceNameEn : item.preferenceNameAr}
                            </Text>
                          </View>
                        )
                      }) : null}

                  </CollapseBody>
                </Collapse>

                <Collapse>
                  <CollapseHeader>
                    <TouchableOpacity
                      style={
                        this.state.locale == "eng"
                          ? styles.tabDrop2
                          : styles.tabDrop2_ar
                      }
                      onPress={this.goToOrderHistory}
                    >
                      <Image
                        source={menuScreen.orderHistory}
                        style={styles.profileImg}
                      />
                      <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                        {I18n.t("Order History")}
                      </Text>
                    </TouchableOpacity>
                  </CollapseHeader>
                </Collapse>

                <Collapse>
                  <CollapseHeader>
                    <TouchableOpacity
                      onPress={this.goToFavorite}
                      style={
                        this.state.locale == "eng"
                          ? styles.tabDrop2
                          : styles.tabDrop2_ar
                      }
                    >
                      <Image
                        source={menuScreen.heartBlack}
                        style={styles.profileImg}
                      />
                      <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                        {I18n.t("Your Favorite Cards")}
                      </Text>
                    </TouchableOpacity>
                  </CollapseHeader>
                </Collapse>

                <Collapse>
                  <CollapseHeader>
                    {this.state.changePassShow ?
                      <TouchableOpacity
                        onPress={this.goToChangePas}
                        style={
                          this.state.locale == "eng"
                            ? styles.tabDrop2
                            : styles.tabDrop2_ar
                        }
                      >
                        <Image
                          source={menuScreen.changePassword}
                          style={styles.profileImg}
                        />
                        <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                          {I18n.t("Change Password")}
                        </Text>
                      </TouchableOpacity>
                      : null
                    }
                  </CollapseHeader>
                </Collapse>
                {this.state.changePassShow == false ?
                  <View
                    style={
                      this.state.locale == "eng" ? styles.connSec : styles.connSec_ar
                    }
                  >
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.connCntn
                          : styles.connCntn_ar
                      }
                    >
                      <Image source={profile.connection} />
                      <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                        {I18n.t("Connection with")}
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={
                        this.state.locale == "eng"
                          ? styles.connCntn
                          : styles.connCntn_ar
                      }
                    >
                      {this.state.socialLoginType == 'socialloginfb' &&
                        <>
                          <Text style={[commonStyle.marHori, styles.placeText]}>
                            {I18n.t("Facebook")}
                          </Text>
                          <Image
                            source={menuScreen.facebook}
                            style={{ ...styles.profileImg, opacity: 0.5, marginBottom: 8 }}
                          />
                        </>
                      }
                      {this.state.socialLoginType == 'sociallogingmail' &&
                        <>
                          <Text style={[commonStyle.marHori, styles.placeText]}>
                            {I18n.t("Google")}
                          </Text>
                          <Image
                            source={menuScreen.google}
                            style={{ ...styles.profileImg, opacity: 0.5, marginBottom: 8 }}
                          />
                        </>
                      }
                    </TouchableOpacity>
                  </View>
                  : null}
                <TouchableOpacity style={styles.signOut} onPress={() => this.signOut()}>
                  <Text
                    style={
                      this.state.locale == "eng"
                        ? [styles.signOutText, commonStyle.none]
                        : [styles.signOutText, commonStyle.rightAlign]
                    }
                  >{I18n.t("Sign Out")}</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          )}
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ProfileDetails);