import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Modal,
} from "react-native";
import styles from "./style";
import RNFetchBlob from 'rn-fetch-blob';
import commonStyle from "../../../../commonStyle.js";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from "react-native-simple-radio-button";
import I18n from "../../../i18n/index";
import {
  profile,
  CatagoryImagePath,
  menuScreen,
  orderHistoryImage,
} from "../../../config/imageConst";
import { connect } from "react-redux";
import CalendarPicker from "react-native-calendar-picker";
import AsyncStorage from "@react-native-community/async-storage";
import { CheckBox } from "react-native-elements";
import apiClient from "../../../services/api.client";
import ModalProfileDiscard from "../Modal/ModalProfileDiscard";
import ModalProfileClose from "../Modal/ModalProfileClose";
import moment from 'moment'

const deviceHeight = Dimensions.get("window").height;

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      discard: false,
      showTheCalender: false,
      city_collaps: false,
      country_collaps: true,
      errorText: "",
      errorText1: "",
      errorText2: "",
      name: this.props.navigation.state.params.name,
      email: this.props.navigation.state.params.email,
      mobile: this.props.navigation.state.params.mobile,
      country: this.props.navigation.state.params.country,
      city: this.props.navigation.state.params.city,
      dob: this.props.navigation.state.params.dob,
      selectedGenderValue: this.props.navigation.state.params.selectedGenderValue,
      selectedGender: this.props.navigation.state.params.selectedGender,
      preferences: this.props.navigation.state.params.preferences,
      checkbox: false,
      check: 1,
      showProfileDiscard: false,
      pagename: "",
      showArrowIcon: false,
      showProfileArrowIcon: false,
      showcity: false,
      cityCollapsArrow: false,
      isCollapsed: true,
      showCalArrow: false,
      changePassShow: true,
      socialLoginType: "",
    };
    this.getDataFromApi();
  }

  getDataFromApi = async () => {
    let socialtype = await AsyncStorage.getItem("socialLoginType")
    this.setState({ socialLoginType: socialtype })
    apiClient
      .getRequest("/countries")
      .then((country) => {
        this.setState({ countryListArray: country })
      })
      .catch((err) => this.setState({ isLoading: false }));

    apiClient
      .postRequest("/cities", { countryId: 2 })
      .then((city) => {
        this.setState({ cityListArray: city })
      })
      .catch((err) => this.setState({ isLoading: false }));

    AsyncStorage.getItem("loginTypeInfo")
      .then((results) => {
        if (results == "normal") {
          this.setState({ changePassShow: true })
        } else {
          this.setState({ changePassShow: false })
        }
      }).catch(err => console.log(err))

  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  responseFromModal = (value) => {
    if (value == "discard") {
      this.setState({ discard: false })
      let edit = "non"
      this.props.navigation.navigate("ProfileDetails", { edit: edit });
    }
    else if (value == "continueEdIt") {
      this.setState({ discard: false })
    }
  };

  showTheCalender = () => {
    if (this.state.showTheCalender == true) {
      this.setState({ showTheCalender: false, showCalArrow: false })
    }
    else {
      this.setState({ showTheCalender: true, showCalArrow: true })
    }
  }

  onDateChange = date => {
    this.setState({ showTheCalender: false })

    var increaseMonth = date._i.month + 1;

    let dateOfBirth1 = date._i.year + '-' + increaseMonth + '-' + date._i.day;
    let dateOfBirth2 = date._i.day + '-' + increaseMonth + '-' + date._i.year;

    this.setState({ dob: dateOfBirth2, dob1: dateOfBirth1 })
  }

  country = (key, item) => {
    this.setState({ country_collaps: false })
    this.setState({ country: this.state.locale == 'eng' ? item.countryNameEn : item.countryNameAr })
    let countryid = this.state.countryListArray[key].id;
    this.setState({ countryid })
  }

  city = (key, item) => {
    this.setState({ city_collaps: false })
    this.setState({ city: this.state.locale == 'eng' ? item.cityNameEn : item.cityNameAr })
    let cityid = this.state.cityListArray[key].id;
    this.setState({ cityid })
  }

  preferences = item => {
    this.setState({ preferencevalue: item })
    let preferenceid = this.state.preferences[item].id;
    this.setState({ preferenceid })
  }

  nameValidation = (e) => {
    let nameRegex = /^[a-zA-Z ]*$/
    let value = e
    if (!nameRegex.test(value)) {
      this.setState({ errorText: "namevalidation" });
    } else {
      this.setState({
        name: value, errorText: ""
      })
    }
  }

  emailValidation = (e) => {
    let emailRegex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    let value = e
    if (!emailRegex.test(value)) {
      this.setState({ errorText1: "mailFormat" });
    } else {
      this.setState({
        email: value, errorText1: ""
      })
    }
  }

  mobileValidation = (e) => {
    let value = e
    if (value.length != 9) {
      this.setState({ errorText2: "mobileError" });
    } else {
      this.setState({
        mobile: value, errorText2: ""
      })
    }
  }

  editprofile = () => {
    if (this.state.name === "") {
      this.setState({ errorText: "noName" });
      return;
    }

    // let regex1 = /^[a-zA-Z ]*$/
    let regex1 = /^[a-zA-Z\u0621-\u064A ]*$/

    if (!regex1.test(this.state.name)) {
      this.setState({ errorText: "namevalidation" });
      return
    }

    if (this.state.email === "") {
      this.setState({ errorText1: "noEmailValidation" });
      return;
    }

    let regex2 = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (!regex2.test(this.state.email)) {
      this.setState({ errorText1: "mailFormat" });
      return
    }

    if (this.state.mobile === "") {
      this.setState({ errorText2: "noNumber" });
      return;
    }


    let preference = [];
    if (this.state.check == 2) {
      preference = this.state.editedPreferenceArray;
    }
    else {
      preference = this.props.navigation.state.params.userDetails.preferencesDTO;
    }

    let preferenceArray = [];
    if (preference && preference.length) {
      for (let i = 0; i < preference.length; i++) {
        let id = preference[i].id;
        preferenceArray.push(id);
      }
    }
    else {
      preferenceArray = preference;
    }

    AsyncStorage.getItem("userToken").then((token) => {

      let editUserData = {
        countryId: this.state.countryid ? this.state.countryid : this.props.navigation.state.params.userDetails.countryId,
        userName: this.state.name ? this.state.name : this.props.navigation.state.params.userDetails.userName,
        mobileNumber: this.state.mobile ? this.state.mobile : this.props.navigation.state.params.userDetails.mobileNumber,
        birthDate: this.state.dob1 ? this.state.dob1 : this.props.navigation.state.params.dobapi,
        cityId: this.state.cityid ? this.state.cityid : this.props.navigation.state.params.userDetails.cityId,
        genderId: this.state.selectedGenderValue ? 330 : 329,
        preferencesIds: preferenceArray
      }

      const fetchClient = RNFetchBlob.config({
        trusty: true
      })
      const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
        "user-token": token
      };
      fetchClient
        .fetch("POST", 'https://stagewrapper.ocstaging.net/bitaqatywrapper/edit-profile', headers, JSON.stringify(editUserData))
        .then(res => {
          if (res.respInfo.status == 200) {
            let edit = "edit";
            this.props.navigation.navigate("ProfileDetails", { edit: edit });
          }
        })
        .catch(err => {
          console.log("err", err);
        });
    });

  }

  backToMenu = () => {
    this.setState({ discard: true })
  }

  checkboxHandler = (item, key) => {
    this.setState({ check: 2 })
    let prefenceArrayChange = this.state.preferences;
    prefenceArrayChange[key].preferenceChecked = !prefenceArrayChange[key].preferenceChecked;

    this.setState({ preferences: prefenceArrayChange })
    let editedPreferenceArray = [];
    for (let j = 0; j < prefenceArrayChange.length; j++) {
      if (prefenceArrayChange[j].preferenceChecked == true) {
        let arrayajesonatrue = prefenceArrayChange[j];
        editedPreferenceArray.push(arrayajesonatrue)
      }
    }
    this.setState({ editedPreferenceArray })
  }

  // backPageEditReceive = (value) => {
  //   let backmsg = "profile";
  //   if (value == "saveEditChanges") {
  //     this.setState({ showProfileDiscard: false })
  //     this.editprofile();
  //   }
  //   else if (value == "dontSave") {
  //     this.setState({ showProfileDiscard: false })
  //     if (this.state.pagename == "orderhistory") {
  //       this.props.navigation.navigate("OrderHistory", { backmsg: backmsg })
  //     }
  //     else if (this.state.pagename == "fav") {
  //       this.props.navigation.navigate("Favourites", { backmsg: backmsg })
  //     }
  //     else if (this.state.pagename == "changepass") {
  //       this.props.navigation.navigate("ChangePassword", { backmsg: backmsg })
  //     }
  //   }
  //   else if (value = "continueEdit") {
  //     this.setState({ showProfileDiscard: false })
  //   }
  // }

  backPageEditReceive = (value) => {
    let backmsg = "profile";
    if(value == "saveEditChanges"){
      this.setState({showProfileDiscard : false})
      this.editprofile();
    }
    else if(value == "dontSave"){
      this.setState({showProfileDiscard : false})
      if(this.state.pagename == "orderhistory"){
        this.props.navigation.navigate("OrderHistory",{backmsg : backmsg})
      }
      else if(this.state.pagename == "fav"){
        this.props.navigation.navigate("Favourites",{backmsg : backmsg})
      }
      else if(this.state.pagename == "changepass"){
        this.props.navigation.navigate("ChangePassword",{backmsg :backmsg})
      }
      else if(this.state.pagename == "signout"){
        this.signOut();
      }
    }
    else if(value = "continueEdit"){
      this.setState({showProfileDiscard : false})
    }
  }
  signOut = async () => {

    AsyncStorage.removeItem('userToken');
    AsyncStorage.removeItem('loginuserName');
    AsyncStorage.removeItem('loginEmailId');
    AsyncStorage.removeItem('userDetails');
    AsyncStorage.removeItem('loginTypeInfo');
    AsyncStorage.removeItem("checkoutData");
    AsyncStorage.removeItem("paymentstart")

    let no = 0;
    let noOfAttems = { 'no': no };
    AsyncStorage.setItem("noOfAttems", JSON.stringify(noOfAttems));
    this.props.navigation.navigate("HomeScreen")
  };

  opencollaps = () => {
    this.setState({ collaps: true })
  }
  opencollaps = () => {
    if (this.state.showcity == true) {
      this.setState({ showcity: false, cityCollapsArrow: false })
    }
    else {
      this.setState({ showcity: true, cityCollapsArrow: true })
    }
  }

  city = (key, item) => {
    this.setState({ showcity: false, cityCollapsArrow: false })
    this.setState({ city: this.state.locale == 'eng' ? item.cityNameEn : item.cityNameAr })
    let cityid = this.state.cityListArray[key].id;
    this.setState({ cityid })
  }

  render() {
    if (this.state.locale == 'eng') {
      var gender = [
        { label: "Male", value: "Male" },
        { label: "Female", value: "Female" },
      ]
    }
    else {
      var gender = [
        { label: "ذكر", value: "ذكر" },
        { label: "أنثى", value: "أنثى" },
      ]
    }
    return (
      <View style={styles.mainSection}>
        {/* ###@@@@@@@@@@@@@@@ profile close section @@@### */}

        <Modal transparent={true} visible={this.state.showProfileDiscard}>
          <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
            <ModalProfileClose
              backPageEdit={value => this.backPageEditReceive(value)} />
          </View>
        </Modal>

        {/* ###@@@@@@@@@@@@@@@ profile discard section @@@### */}

        <Modal transparent={true} visible={this.state.discard}>
          <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
            <ModalProfileDiscard
              goToEditProfile={value => this.responseFromModal(value)}
            />
          </View>
        </Modal>


        {/* ###@@@@@@@@@@@@@@@ edit profile section @@@### */}

        <View
          style={
            this.state.locale == "eng" ? styles.editProf : styles.editProf_ar
          }
        >
          <TouchableOpacity style={styles.blackBtn} onPress={this.editprofile} >
            <Text style={styles.blackBtnText}>{I18n.t("Save Changes")}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.backToMenu}
            style={styles.whiteButton}
          >
            <Text style={styles.whiteBtnText}>{I18n.t("Cancel")}</Text>
          </TouchableOpacity>

        </View>

        <ScrollView>
          <View style={styles.container}>
            <View
              style={
                this.state.locale == "eng"
                  ? styles.userEditSec
                  : styles.userEditSec_ar
              }
            >
              <View
                style={
                  this.state.locale == "eng"
                    ? [commonStyle.rowSec, { alignItems: 'center' }]
                    : [commonStyle.rowSec_ar, { alignItems: 'center' }]
                }
              >
                <Text style={styles.helloMed16}>{I18n.t("Hello")}</Text>
                <Text style={styles.pageTitle}> {this.state.name} </Text>
              </View>
            </View>

            {/*## PROFILE COLLAPSE SECTION ##*/}
            <Collapse isCollapsed={true} onToggle={() => this.setState({ showProfileArrowIcon: !this.state.showProfileArrowIcon })}>
              <CollapseHeader>
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.tabDrop
                      : styles.tabDrop_ar
                  }
                >
                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <Image source={profile.proUser} />
                    <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                      {I18n.t("Profile")}
                    </Text>
                  </View>
                  <Image
                    source={CatagoryImagePath.rightArrow}
                    style={{ transform: this.state.showProfileArrowIcon ? [{ rotate: '90deg' }] : [{ rotate: '270deg' }] }}
                  />
                </View>
              </CollapseHeader>

              {/*## Profile COLLAPSE BODY SECTION ##*/}
              <CollapseBody style={[commonStyle.cbEli, { padding: 8 }]}>
                <View style={commonStyle.inputMar}>
                  <Text style={
                    this.state.locale == "eng"
                      ? [commonStyle.inputText, commonStyle.none]
                      : [commonStyle.inputText, commonStyle.rightAlign]
                  }>{I18n.t("Name")}</Text>
                  <TextInput
                    style={[
                      this.state.locale == "eng"
                        ? [commonStyle.inputField_en, { backgroundColor: '#fff' }]
                        : [commonStyle.inputField_ar, { backgroundColor: '#fff' }],
                      (
                        this.state.errorText === "noName" ||
                        this.state.errorText === "namevalidation"
                      ) && {
                        borderColor: "#ff0000"
                      }
                    ]}
                    placeholder={I18n.t("Full Name")}
                    value={this.state.name}
                    placeholderTextColor="#4F4F4F"
                    onChangeText={(text) => this.setState({ name: text }, () => {
                      this.nameValidation(this.state.name)
                    })}
                  />
                  {this.state.errorText !== "" ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t(this.state.errorText)}
                    </Text>
                  ) : null}
                </View>
                <View style={commonStyle.inputMar}>
                  <Text style={
                    this.state.locale == "eng"
                      ? [commonStyle.inputText, commonStyle.none]
                      : [commonStyle.inputText, commonStyle.rightAlign]
                  }>{I18n.t("Email")}</Text>
                  <TextInput
                    style={[
                      this.state.locale == "eng"
                        ? commonStyle.inputField_en
                        : commonStyle.inputField_ar,
                      (
                        this.state.errorText1 === "noEmailValidation" ||
                        this.state.errorText1 === "mailFormat"
                      ) && {
                        borderColor: "#ff0000"
                      }
                    ]}
                    placeholder="ali@mail.com"
                    value={this.state.email}
                    placeholderTextColor="#4F4F4F"
                    editable={false}
                  />
                  {this.state.errorText1 !== "" ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t(this.state.errorText1)}
                    </Text>
                  ) : null}
                </View>
                <View style={commonStyle.inputMar}>
                  <Text style={
                    this.state.locale == "eng"
                      ? [commonStyle.inputText, commonStyle.none]
                      : [commonStyle.inputText, commonStyle.rightAlign]
                  }>
                    {I18n.t("Mobile Number")}
                    <Text style={commonStyle.countryText}>
                      {" "}
                      {I18n.t("(Saudi)")}{" "}
                    </Text>
                  </Text>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <View
                      style={[
                        this.state.locale == "eng"
                          ? commonStyle.mobileInput
                          : commonStyle.mobileInput_ar,
                        (
                          this.state.errorText2 === "noNumber" ||
                          this.state.errorText2 === "mobileError"
                        ) && {
                          borderColor: "#ff0000"
                        }
                      ]}
                    >
                      <View style={commonStyle.countryCode}>
                        <Text style={commonStyle.countryCodeText}>966</Text>
                      </View>
                      <TextInput
                        style={[
                          this.state.locale == "eng"
                            ? commonStyle.mobileTextInput
                            : commonStyle.mobileTextInput_ar
                        ]}
                        placeholder=" 123456789"
                        value={this.state.mobile}
                        keyboardType="numeric"
                        underlineColorAndroid="transparent"
                        editable={false}
                      />
                    </View>
                    <View style={{ width: "15%", justifyContent: "center" }}>
                      <Image
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.flagImage
                            : commonStyle.flagImage_ar
                        }
                        source={menuScreen.flag2}
                      />
                    </View>
                  </View>
                  {this.state.errorText2 !== "" ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t(this.state.errorText2)}
                    </Text>
                  ) : null}
                </View>

                <View style={commonStyle.inputMar}>
                  <Text style={
                    this.state.locale == "eng"
                      ? [commonStyle.inputText, commonStyle.none]
                      : [commonStyle.inputText, commonStyle.rightAlign]
                  }>
                    {I18n.t("Country")}
                  </Text>
                  <Collapse isCollapsed={this.state.country_collaps}>
                    <CollapseHeader >
                      <View
                        style={
                          this.state.locale == "eng"
                            ? [commonStyle.rowSec, styles.dropField, { backgroundColor: '#F2F0F0' }]
                            : [commonStyle.rowSec_ar, styles.dropField_ar, { backgroundColor: '#F2F0F0' }]
                        }
                      >
                        <Text style={styles.placeText}>
                          {this.state.country}
                        </Text>
                        <Image
                          source={CatagoryImagePath.rightArrow}
                          style={{ ...commonStyle.rotate90, opacity: 0.3 }}
                        />
                      </View>
                    </CollapseHeader>
                  </Collapse>
                </View>

                <View style={commonStyle.inputMar}>
                  <View style={commonStyle.inputMar}>
                    <View
                      style={
                        this.state.locale == "eng"
                          ? [commonStyle.rowSec, { alignItems: "center" }]
                          : commonStyle.rowSec_ar
                      }
                    >
                      <Text style={commonStyle.inputText}>
                        {I18n.t("City")}
                      </Text>
                      {this.state.city == null || this.state.city == undefined ? (
                        <Image
                          source={profile.profileError}
                          style={commonStyle.marHori}
                        />
                      ) : null}
                    </View>

                    <TouchableOpacity
                      onPress={this.opencollaps}
                      style={
                        this.state.locale == "eng"
                          ? [commonStyle.rowSec, styles.dropField, { backgroundColor: '#fff' }]
                          : [commonStyle.rowSec_ar, styles.dropField_ar, { backgroundColor: '#fff' }]
                      }
                    >
                      {this.state.city == null || this.state.city == undefined ? (
                        <Text style={styles.placeText}>{I18n.t("Choose Your City")}</Text>
                      ) : (
                          <Text style={styles.placeText}>{this.state.city}</Text>
                        )}
                      {this.state.cityCollapsArrow ?
                        <Image
                          source={CatagoryImagePath.rightArrow}
                          style={[styles.rotate_up, { opacity: 0.3 }]}
                        />
                        :
                        <Image
                          source={CatagoryImagePath.rightArrow}
                          style={[styles.rotate_down, { opacity: 0.3 }]}
                        />}
                    </TouchableOpacity>
                    {this.state.showcity &&
                      <View style={styles.clpsBody}>
                        <ScrollView style={{ height: 200, overflow: 'hidden' }} nestedScrollEnabled>
                          {this.state.cityListArray && this.state.cityListArray.map((item, key) => (
                            <TouchableOpacity
                              key={key}
                              onPress={() => {
                                this.city(key, item);
                              }}
                              style={
                                this.state.locale == "eng"
                                  ? styles.clipBodyCntn
                                  : styles.clipBodyCntn
                              }
                            >
                              <Text style={
                                this.state.locale == "eng"
                                  ? [styles.tabBoldText, commonStyle.none]
                                  : [styles.tabBoldText, commonStyle.rightAlign]
                              }>
                                {this.state.locale == "eng" ? item.cityNameEn : item.cityNameAr}
                              </Text>
                            </TouchableOpacity>
                          ))}
                        </ScrollView>
                      </View>
                    }
                  </View>
                </View>


                <View style={commonStyle.inputMar}>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? [commonStyle.rowSec, { alignItems: "center" }]
                        : commonStyle.rowSec_ar
                    }
                  >
                    <Text style={commonStyle.inputText}>
                      {I18n.t("Birth Date")}
                    </Text>
                    {this.state.dob == "" || this.state.dob == undefined ? (
                      <Image
                        source={profile.profileError}
                        style={commonStyle.marHori}
                      />
                    ) : null}
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <View
                      style={
                        this.state.locale == "eng"
                          ? [commonStyle.rowSec, styles.dateField, { backgroundColor: '#fff' }]
                          : [commonStyle.rowSec_ar, styles.dateField_ar, { backgroundColor: '#fff' }]
                      }
                    >
                      {this.state.dob == "" || this.state.dob == undefined ? (
                        <Text style={styles.placeText}>
                          {I18n.t("Day / Month / Year")}
                        </Text>
                      ) : (
                          <Text style={styles.placeText}>
                            {this.state.dob}
                          </Text>
                        )}
                      <TouchableOpacity
                        onPress={this.showTheCalender}
                      >
                        {this.state.showCalArrow ?
                          <Image
                            source={CatagoryImagePath.rightArrow}
                            style={[styles.rotate_up, { ...commonStyle.rotate, opacity: 0.3 }]}
                          /> :
                          <Image
                            source={CatagoryImagePath.rightArrow}
                            style={[styles.rotate_down, { ...commonStyle.rotate90, opacity: 0.3 }]}
                          />}
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                      style={{ width: "15%", justifyContent: "center" }}
                      onPress={this.showTheCalender}
                    >
                      <Image
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.cldrImage
                            : commonStyle.cldrImage_ar
                        }
                        source={orderHistoryImage.roundCalendar}
                      />
                    </TouchableOpacity>
                  </View>
                  {this.state.showTheCalender ? (
                    <View style={
                      this.state.locale === "eng"
                        ? styles.calender_en
                        : styles.calender_ar
                    }>
                      <CalendarPicker
                        startFromMonday={false}
                        weekdays={[
                          I18n.t("Su"), I18n.t("Mo"), I18n.t("Tu"), I18n.t("We"), I18n.t("Th"), I18n.t("Fr"), I18n.t("Sa")
                        ]}
                        months={[
                          I18n.t("January"), I18n.t("February"), I18n.t("March"), I18n.t("April"), I18n.t("May"), I18n.t("June"), I18n.t("July"), I18n.t("August"), I18n.t("September"), I18n.t("October"), I18n.t("November"), I18n.t("December"),
                        ]}
                        // maxDate={new Date()}      
                        maxDate={moment().subtract(1, "years")}
                        initialDate={moment().subtract(1, "years")}
                        todayBackgroundColor="#0000"
                        selectedDayColor="#FFC300"
                        selectedDayTextColor="#1A1A1A"
                        selectedDayStyle={styles.selectedDay}
                        scaleFactor={400}
                        previousTitle=" "
                        nextTitle=" "
                        dayShape="square"
                        dayLabelsWrapper={
                          this.state.locale === "eng"
                            ? styles.dayLables_en
                            : styles.dayLables_ar
                        }
                        monthYearHeaderWrapperStyle={
                          this.state.locale === "eng"
                            ? styles.monthCalendar
                            : styles.monthCalendar_ar
                        }
                        textStyle={
                          this.state.locale === "eng"
                            ? styles.calendarText_en
                            : styles.calendarText_ar
                        }
                        onDateChange={this.onDateChange}
                      />
                    </View>
                  ) : null}
                </View>

                {/* //gender radio button code// */}
                <View style={commonStyle.inputMar}>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? [commonStyle.rowSec, { alignItems: "center" }]
                        : commonStyle.rowSec_ar
                    }
                  >
                    <Text style={commonStyle.inputText}>
                      {I18n.t("Gender")}
                    </Text>
                    {this.state.selectedGender == true ? (
                      <Image
                        source={profile.profileError}
                        style={commonStyle.marHori}
                      />
                    ) : null}
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.radiofSrt
                        : commonStyle.radioRotate
                    }
                  >
                    <RadioForm
                      animation={true}
                      disabled={false}
                      formHorizontal={false}
                    >
                      {gender.map((obj, i) => {
                        var onPress = (value, index) => {
                          this.setState({
                            selectedGenderValue: index,
                            selectedGender: false
                          });
                        };
                        return (
                          <RadioButton key={i}>
                            <RadioButtonInput
                              obj={obj}
                              index={i}
                              isSelected={
                                this.state.selectedGenderValue === i
                              }
                              onPress={onPress}
                              buttonInnerColor={"#241125"}
                              buttonOuterColor={
                                this.state.selectedGenderValue === i
                                  ? "#241125"
                                  : "#241125"
                              }
                              buttonSize={10}
                              buttonOuterSize={20}
                            />
                            <RadioButtonLabel
                              obj={obj}
                              index={i}
                              labelHorizontal={true}
                              onPress={onPress}
                              labelStyle={
                                this.state.locale == "eng" &&
                                  this.state.selectedGenderValue === i
                                  ? commonStyle.radioBoldText
                                  : this.state.locale == "ar" &&
                                    this.state.selectedGenderValue === i
                                    ? commonStyle.radioSelRotate
                                    : this.state.locale == "ar"
                                      ? commonStyle.radioSelRotate2
                                      : commonStyle.radioLightText
                              }
                            />
                          </RadioButton>
                        );
                      })}
                      {/* ))} */}
                    </RadioForm>
                  </View>
                </View>
              </CollapseBody>
            </Collapse>

            {/*## Preferences COLLAPSE SECTION ##*/}

            <Collapse isCollapsed={true} onToggle={() => this.setState({ showArrowIcon: !this.state.showArrowIcon })} >
              <CollapseHeader>
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.tabDrop
                      : styles.tabDrop_ar
                  }
                >
                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <Image source={profile.proLike} />
                    <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                      {I18n.t("Preferences")}
                    </Text>
                  </View>
                  <Image
                    source={CatagoryImagePath.rightArrow}
                    style={{ transform: this.state.showArrowIcon ? [{ rotate: '90deg' }] : [{ rotate: '270deg' }] }}
                  />
                </View>
              </CollapseHeader>

              {/*## Preferences COLLAPSE BODY SECTION ##*/}
              <CollapseBody style={{ ...commonStyle.cbEli, paddingTop: 20, paddingRight: 8 }}>
                {this.state.preferences && this.state.preferences.length >= 1 ?
                  this.state.preferences.map((item, key) => {
                    return (
                      <View
                        style={this.state.locale == "eng" ? styles.checkBoxViewEng : styles.checkBoxViewAr}
                      >

                        <CheckBox
                          style={{ color: "black" }}
                          checked={item.preferenceChecked}
                          onPress={() => { this.checkboxHandler(item, key) }}
                          checkedColor="#000"
                        />

                        <Text
                          style={this.state.locale == "eng" ? styles.checkboxEng : styles.checkboxAr}
                        >
                          {this.state.locale == 'eng' ? item.preferenceNameEn : item.preferenceNameAr}
                        </Text>
                      </View>
                    )
                  }) : null}
              </CollapseBody>
            </Collapse>

            <Collapse>
              <CollapseHeader>
                <TouchableOpacity
                  style={
                    this.state.locale == "eng"
                      ? styles.tabDrop2
                      : styles.tabDrop2_ar
                  }
                  onPress={() => { this.setState({ showProfileDiscard: true, pagename: "orderhistory" }) }}
                >
                  <Image
                    source={menuScreen.orderHistory}
                    style={styles.profileImg}
                  />
                  <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                    {I18n.t("Order History")}
                  </Text>
                </TouchableOpacity>
              </CollapseHeader>
            </Collapse>

            <Collapse>
              <CollapseHeader>
                <TouchableOpacity
                  onPress={() => { this.setState({ showProfileDiscard: true, pagename: "fav" }) }}
                  style={
                    this.state.locale == "eng"
                      ? styles.tabDrop2
                      : styles.tabDrop2_ar
                  }
                >
                  <Image
                    source={menuScreen.heartBlack}
                    style={styles.profileImg}
                  />
                  <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                    {I18n.t("Your Favorite Cards")}
                  </Text>
                </TouchableOpacity>
              </CollapseHeader>
            </Collapse>

            <Collapse>
              <CollapseHeader>
                {this.state.changePassShow ?
                  <TouchableOpacity
                    onPress={() => { this.setState({ showProfileDiscard: true, pagename: "changepass" }) }}
                    style={
                      this.state.locale == "eng"
                        ? styles.tabDrop2
                        : styles.tabDrop2_ar
                    }
                  >
                    <Image
                      source={menuScreen.changePassword}
                      style={styles.profileImg}
                    />
                    <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                      {I18n.t("Change Password")}
                    </Text>
                  </TouchableOpacity> : null}
              </CollapseHeader>
            </Collapse>
            {this.state.changePassShow == false ?
              <View
                style={
                  this.state.locale == "eng" ? styles.connSec : styles.connSec_ar
                }
              >
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.connCntn
                      : styles.connCntn_ar
                  }
                >
                  <Image source={profile.connection} />
                  <Text style={[commonStyle.marHori, commonStyle.bold14]}>
                    {I18n.t("Connection with")}
                  </Text>
                </View>
                <TouchableOpacity
                  style={
                    this.state.locale == "eng"
                      ? styles.connCntn
                      : styles.connCntn_ar
                  }
                >
                  {this.state.socialLoginType == 'socialloginfb' &&
                    <>
                      <Text style={[commonStyle.marHori, styles.placeText]}>
                        {I18n.t("Facebook")}
                      </Text>
                      <Image
                        source={menuScreen.facebook}
                        style={{ ...styles.profileImg, opacity: 0.5, marginBottom: 8 }}
                      />
                    </>
                  }
                  {this.state.socialLoginType == 'sociallogingmail' &&
                    <>
                      <Text style={[commonStyle.marHori, styles.placeText]}>
                        {I18n.t("Google")}
                      </Text>
                      <Image
                        source={menuScreen.google}
                        style={{ ...styles.profileImg, opacity: 0.5, marginBottom: 8 }}
                      />
                    </>
                  }
                </TouchableOpacity>
              </View>
              : null}
            <TouchableOpacity style={styles.signOut} onPress={() => { this.setState({ showProfileDiscard: true, pagename: "signout" }) }}>
              <Text
                style={
                  this.state.locale == "eng"
                    ? [styles.signOutText, commonStyle.none]
                    : [styles.signOutText, commonStyle.rightAlign]
                }
              >{I18n.t("Sign Out")}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(EditProfile);