const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    //backgroundColor: "red"
  },

  arrow_en: {
    height: 26,
    width: 22,
    marginLeft: 12,
  },
  arrow_ar: {
    height: 26,
    width: 22,
    marginRight: 12,
  },
  headerHead: {
    width: "100%",
    elevation: 1,
    paddingVertical: 15,
    backgroundColor: "#fff",
  },
  headerLogo: {
    width: "100%",
    justifyContent: "center",
    flexDirection: "row",
    position: "absolute",
    top: 0,
  },
  logoText: {
    marginHorizontal: 4,
    fontSize: 16,
    fontWeight: "bold",
    marginTop: 4,
    fontFamily: "Tajawal-Bold",
  },
};
