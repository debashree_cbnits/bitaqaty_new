import React, { Component } from "react";
import { View, Image } from "react-native";
import I18n from "../../../../i18n/index";
import { RTLView, RTLText, AsyncStorage } from "react-native-rtl-layout";

export default class HeaderArrow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Cart Screen",
      locale: ''
    };
    this.language();
  }

  async language() {    

    try {
      const value = await AsyncStorage.getItem("language");      
      if (value == "eng") {
        this.setState({ locale: "en" });
      } else {
        this.setState({ locale: "ar" });
      }
      this.forceUpdate();

      if (value == null) {
        this.setState({ locale: "en" });
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <View>
        <View
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            marginTop: 20,
            flexDirection: "row"
          }}
        >
          <RTLView locale={this.state.locale}>
            <Image
              style={{ height: 33.5, width: 27.5 }}
              source={require("../../../assets/image/logo.png")}
            />
            <RTLText
              style={{ marginHorizontal: 4, fontSize: 16, fontWeight: "bold", marginTop: 4 }}
            >
              {I18n.t("BITAQATY")}
            </RTLText>
          </RTLView>
        </View>
      </View>
    );
  }
}
