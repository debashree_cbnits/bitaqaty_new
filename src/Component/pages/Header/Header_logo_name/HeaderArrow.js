import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import I18n from "../../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import Styles from "./Styles";

export default class HeaderArrow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Cart Screen",
      locale: "",
      language: "",
    };    
  }

  static getDerivedStateFromProps(props, state) {    
    try {
      const { language, locale } = props;
      I18n.locale = locale;
      return {
        language,
        locale,
      };
    } catch {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }


  backArrowHome = () => {
    let backvalue = "back";
    this.props.backPage(backvalue);
  };
  

  render() {
    return (
      <View style={Styles.headerHead}>
        <RTLView locale={this.state.locale}>
          <View style={{ width: "12%", zIndex: 1 }}>
            <TouchableOpacity onPress={this.backArrowHome}>
              <Image
                style={
                  this.state.locale === "eng"
                    ? Styles.arrow_en
                    : Styles.arrow_ar
                }
                source={
                  this.state.locale === "eng"
                    ? require("../../../assets/image/arrow1.png")
                    : require("../../../assets/image/arrow2.png")
                }
              />
            </TouchableOpacity>
          </View>
          <View style={Styles.headerLogo}>
            <Image
              style={{ height: 29, width: 24 }}
              source={require("../../../assets/image/logo.png")}
            />
            <RTLText style={Styles.logoText}>{I18n.t("BITAQATY")}</RTLText>
          </View>
          <View style={{ width: "10%" }} />
        </RTLView>
      </View>
    );
  }
}
