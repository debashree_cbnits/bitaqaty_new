import React, { Component } from "react";
import { Text, View, Image, Linking, SafeAreaView, ActivityIndicator, Alert } from "react-native";
import styles from "./style.js";
import commonStyle from "../../../../commonStyle.js";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import {
  Collapse,
  CollapseHeader,
  CollapseBody
} from "accordion-collapse-react-native";
import { menuScreen } from "../../../config/imageConst";
//import { ListItem, Separator } from "native-base";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import HeaderArrow from "../Header/Header_logo_name/HeaderArrow";
import { connect } from "react-redux";
import apiClient from "../../../services/api.client.js";
import AsyncStorage from "@react-native-community/async-storage";
import Toast from "react-native-tiny-toast";
import { GoogleSignin } from "@react-native-community/google-signin"
import { Thumbnail } from "native-base";

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      isLoading: true,
      shoppingCate: null,
      hasUser: false,
      userName: '',
      collasible: false,
      collasiblemenu: true,
      showArrowIcon: false,
      showArrowIconForAbout: false,
      showCollasp: false,
      showCollaspForAbout: false,
      aboutCate: [
        {
          itemText: "About Us",
          navigate: this.About
        },
        {
          itemText: "FAQ",
          navigate: this.FAQ
        },
        {
          itemText: "Policies",
          navigate: this.Privacy
        },
        {
          itemText: "Terms of use",
          navigate: this.Terms
        },
        {
          itemText: "How to buy",
          navigate: this.HowToBuy
        }
      ],
      othersOptn1: [
        {
          itemText: "New Cards",
          img: require("../../../Component/assets/image/new.png"),
          navigate: this.NewCardtab
        },
        {
          itemText: "Best Selling",
          img: require("../../../Component/assets/image/start1.png"),
          navigate: this.BestSellingtab
        },
        {
          itemText: "Offers Of The Day",
          img: require("../../../Component/assets/image/tagg.png"),
          navigate: this.Offertab
        }
      ]
    };
    this._getCatagoriesFromApi();
  }
  _getCatagoriesFromApi() {
    apiClient
      .getRequest("/categories")
      .then(shoppingCate => this.setState({ shoppingCate, isLoading: false }))
      .catch(err => this.setState({ shoppingCate: null, isLoading: false }));
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }
  componentDidMount() {
    this.getUserName();
    this.props.navigation.addListener("didFocus", () => {
      this.getUserName();
      // this.setState({showCollasp: false, showArrowIcon: false, showCollaspForAbout: false})
    });

    this.props.navigation.addListener("didBlur", () => {
      this.getUserName()
      // this.setState({showCollasp: false, showArrowIcon: false,showArrowIconForAbout: false,  showCollaspForAbout: false})

    });
  }

  getUserToken(pageName) {
    AsyncStorage.getItem("userToken")
      .then((results) => {
        if (results) {
          this.setState({ hasUser: true }, () => {
            if (pageName == "ChangePassword") {
              this.props.navigation.navigate("ChangePassword");
            } else if (pageName == "Favourites") {
              this.props.navigation.navigate("Favourites");
            } else if (pageName == "ContactUs") {
              this.props.navigation.navigate("ContactUs");
            } else if (pageName == "ProfileDetails") {

              apiClient
                .getProfileRequest("/account-profile", results)
                .then((profileDetailsRes) => {
                  this.props.navigation.navigate("ProfileDetails", { profileDetailsData: profileDetailsRes });

                })
                .catch((err) => this.setState({ isLoading: false }));
            } else if (pageName == "ContactUs") {
              this.props.navigation.navigate("ContactUs");
            }
          })
        } else {
          this.setState({ hasUser: false }, () => {
            Toast.show(I18n.t("Please login first"))
            this.props.navigation.navigate("Login");
          })
        }

      }).catch(err => console.log(err))
  }

  getUserName() {
    AsyncStorage.getItem("loginuserName")
      .then((results) => {
        if (results) {
          this.setState({ userName: results, collasible: true, collasiblemenu: false })
        } else {
          this.setState({ userName: '', collasible: false, collasiblemenu: true })
        }
      }).catch(err => console.log(err))

    AsyncStorage.getItem("loginTypeInfo")
      .then((results) => {
        if (results == "normal") {
          this.setState({ changePassShow: true })
        } else {
          this.setState({ changePassShow: false })
        }
      }).catch(err => console.log(err))
  }

  NewCardtab = () => {
    if (this.state.locale == "eng") {
      this.props.navigation.navigate("Tab", { index: 0 });
    } else {
      this.props.navigation.navigate("Tab", { index: 2 });
    }
  };

  BestSellingtab = () => {
    this.props.navigation.navigate("Tab", { index: 1 });
  };

  Offertab = () => {
    if (this.state.locale == "eng") {
      this.props.navigation.navigate("Tab", { index: 2 });
    } else {
      this.props.navigation.navigate("Tab", { index: 0 });
    }
  };
  categoryPage = item => {
    this.props.navigation.navigate("Merchants", { item });
  };

  onClickBtn = () => {
    this.props.navigation.navigate("ContactUs");
  };

  openLogIn = () => {
    this.props.navigation.navigate("Login");
  };

  backValueReceive = value => {
    if (value == "back") {
      this.props.navigation.navigate("HomeScreen");
    }
  };

  onChangePassBtn = () => {
    this.getUserToken("ChangePassword")
  };

  langSetting = () => {
    this.props.navigation.navigate("SettingLanguage");
  };

  onChatUS = () => {
    this.props.navigation.navigate("ChatWithUs");
  };

  onFavourits = () => {
    this.getUserToken("Favourites")
  };

  onProfileDetails = () => {
    this.getUserToken("ProfileDetails")
  };
  onClickOrderHistory = () => {
    this.props.navigation.navigate("OrderHistory");
  }
  About = () => {
    this.props.navigation.navigate("About", { index: 0 });
  };
  FAQ = () => {
    this.props.navigation.navigate("Faq", { index: 1 });
  };
  Privacy = () => {
    this.props.navigation.navigate("Privacy", { index: 2 });
  };
  Terms = () => {
    this.props.navigation.navigate("Terms", { index: 3 });
  };
  HowToBuy = () => {
    this.props.navigation.navigate("HowToBuy", { index: 4 });
  };

  signOut = async () => {
    await GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"],
      webClientId:
        "33602206675-sk89db5rf09lbu4en1cm51u7497ii98c.apps.googleusercontent.com",
      offlineAccess: true,
      forceCodeForRefreshToken: true
    });
    let isGoogleSignedIn = await GoogleSignin.isSignedIn();
    if (isGoogleSignedIn) {
      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      } catch (error) {
        console.error(error);
      }
    }
    AsyncStorage.removeItem("userToken");
    AsyncStorage.removeItem("loginuserName");
    AsyncStorage.removeItem("loginEmailId");
    AsyncStorage.removeItem("userDetails");
    AsyncStorage.removeItem("checkoutData");
    AsyncStorage.removeItem("loginTypeInfo");
    AsyncStorage.removeItem("paymentstart")

    this.props.navigation.navigate("HomeScreen");

  };

  render() {
    console.log(this.state.showCollasp, 'collasp')
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={value => this.backValueReceive(value)}
        />
        <SafeAreaView>
          <ScrollView>

            {/* ## MENU TAB SECTION ## */}
            {this.state.userName ?
              <View style={{
                width: "100%",
                paddingHorizontal: "6%",
              }}>
                <Text style={[styles.tabText, styles.tabTextBlue, { color: '#000',fontFamily: "Tajawal-Medium",fontSize:16}]}>
                  {I18n.t('Hello')}
                  <Text style={{color: '#000', fontFamily: "Tajawal-Bold",fontSize:20}}> {this.state.userName}</Text>
                </Text>
              </View> :
              <TouchableOpacity
                onPress={this.openLogIn}
                style={
                  this.state.locale == "eng" ? styles.menuTab : styles.menuTab_ar
                }
              >
                <RTLView locale={this.state.locale}>
                  <Image
                    style={{ ...styles.menuCateImg, ...styles.menuIconNew }}
                    source={menuScreen.loginUserIcon}
                  />
                  <Text style={styles.tabTextBlue}>
                    {I18n.t("Login")}
                  </Text>
                </RTLView>
              </TouchableOpacity>
            }
            <TouchableOpacity
              style={
                this.state.locale == "eng" ? styles.menuTab : styles.menuTab_ar
              }
              onPress={() => this.props.navigation.navigate("HomeScreen")}
            >
              <RTLView locale={this.state.locale}>
                <Image style={{ ...styles.menuCateImg, ...styles.menuIconNew }} source={menuScreen.homeIcon} />
                <Text style={styles.tabText}>{I18n.t("Home Page")}</Text>
              </RTLView>
            </TouchableOpacity>

            {/*## SHOPPING CATE COLLAPSE SECTION ##*/}
            <Collapse isCollapsed={false} onToggle={() => this.setState({ showArrowIcon: !this.state.showArrowIcon , showCollasp: true})} >
              <CollapseHeader >
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.menuTabWhite
                      : styles.menuTabWhite_ar
                  }
                >
                  <RTLView locale={this.state.locale}>

                    <View>
                      <Image
                        style={{ ...styles.menuCateImg, ...styles.menuIconNew }}
                        source={menuScreen.shoppingIcon}
                      />
                    </View>

                    <View style={{ width: "85%" }}>
                      <RTLView locale={this.state.locale}>
                        <Text style={styles.tabText}>
                          {I18n.t("Shopping Categories")}
                        </Text>
                      </RTLView>
                    </View>

                    <View style={{ width: "5%" }}>
                      <Image
                        style={[styles.menuCateImg2, { transform: this.state.showArrowIcon ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }]}
                        source={menuScreen.downarrow}
                      />
                    </View>
                  </RTLView>
                </View>
              </CollapseHeader>

              {/*## SHOPPING CATE COLLAPSE BODY SECTION ##*/}
              
              <CollapseBody style={styles.cbEli}>
                {this.state.isLoading && <ActivityIndicator style={styles.activityIndicator}
                  size="small"
                  color="#1A1A1A" />}
                  
                {
                // this.state.showCollasp &&
                 this.state.shoppingCate && this.state.shoppingCate.map((item, key) => (                  
                  <TouchableOpacity
                    key={key}
                    onPress={() => {
                      this.categoryPage(item);
                    }}
                    style={
                      this.state.locale == "eng"
                        ? styles.menuOption
                        : styles.menuOption_ar
                    }
                  >
                    <Text style={styles.tabBoldText}>
                      {this.state.locale == "eng" ? item.nameEn : item.nameAr}
                    </Text>
                  </TouchableOpacity> 
                ))}
              </CollapseBody>
            </Collapse>

            {/*## OTHERS OPTION SECTION ##*/}
            {this.state.othersOptn1.map((item, key) => (
              <TouchableOpacity
                key={key}
                onPress={item.navigate}
                style={
                  this.state.locale == "eng"
                    ? styles.menuOption
                    : styles.menuOption_ar
                }
                key={key}
              >
                <RTLView locale={this.state.locale}>
                  <Image style={styles.menuCateImg} source={item.img} />
                  <Text style={styles.tabText}>
                    {I18n.t(`${item.itemText}`)}
                  </Text>
                </RTLView>
              </TouchableOpacity>
            ))}

            {/*## DISABLED OPTION SECTION ##*/}
            {this.state.collasible ?
              <TouchableOpacity
                onPress={this.onProfileDetails}
                style={
                  this.state.locale == "eng"
                    ? styles.menuGrey
                    : styles.menuGrey_ar
                }
              >
                <RTLView locale={this.state.locale}>
                  <Image style={styles.menuCateImg} source={menuScreen.user} />
                  <Text style={styles.tabText}>{I18n.t("Profile Details")}</Text>
                </RTLView>
              </TouchableOpacity>
              : null}

            {this.state.collasible ?
              <TouchableOpacity
                onPress={this.onFavourits}
                style={
                  this.state.locale == "eng"
                    ? styles.menuGrey
                    : styles.menuGrey_ar
                }
              >
                <RTLView locale={this.state.locale}>
                  <Image
                    style={styles.menuCateImg}
                    source={menuScreen.heartBlack}
                  />
                  <Text style={styles.tabText}>{I18n.t("Favourites")}</Text>
                </RTLView>
              </TouchableOpacity>
              : null}

            {this.state.collasible ?
              <TouchableOpacity
                style={
                  this.state.locale == "eng"
                    ? styles.menuGrey
                    : styles.menuGrey_ar
                }
                onPress={this.onClickOrderHistory}
              >
                <RTLView locale={this.state.locale}>
                  <Image
                    style={styles.menuCateImg}
                    source={menuScreen.orderHistory}
                  />
                  <Text style={styles.tabText}>{I18n.t("Order History")}</Text>
                </RTLView>
              </TouchableOpacity>
              : null}

            {this.state.collasible && this.state.changePassShow ?
              <TouchableOpacity
                onPress={this.onChangePassBtn}
                style={
                  this.state.locale == "eng"
                    ? styles.menuGrey
                    : styles.menuGrey_ar
                }
              >
                <RTLView locale={this.state.locale}>
                  <Image
                    style={styles.menuCateImg}
                    source={menuScreen.changePassword}
                  />
                  <Text style={styles.tabText}>{I18n.t("Change Password")}</Text>
                </RTLView>
              </TouchableOpacity>
              : null}

              <TouchableOpacity
                onPress={()=>Linking.openURL("https://www.bitaqatybusiness.com/")}
                style={
                  this.state.locale == "eng"
                    ? styles.menuOption
                    : styles.menuOption_ar
                }
              >
                <RTLView locale={this.state.locale}>
                  <Image
                    style={styles.menuCateImg}
                    source={menuScreen.beReseller}
                  />
                  <Text style={styles.tabText}>
                    {I18n.t("Be a Reseller")}
                  </Text>
                </RTLView>
              </TouchableOpacity>
            {/*## OTHERS OPTION SECTION ##*/}
            <TouchableOpacity
              onPress={this.onClickBtn}
              style={
                this.state.locale == "eng"
                  ? styles.menuOption
                  : styles.menuOption_ar
              }
            >
              <RTLView locale={this.state.locale}>
                <Image style={styles.menuCateImg} source={menuScreen.contact} />
                <Text style={styles.tabText}>{I18n.t("Contact Us")}</Text>
              </RTLView>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.onChatUS}
              style={
                this.state.locale == "eng"
                  ? styles.menuOption
                  : styles.menuOption_ar
              }
            >
              <RTLView locale={this.state.locale}>
                <Image style={styles.menuCateImg} source={menuScreen.chatWithUs} />
                <Text style={styles.tabText}>{I18n.t("Chat With Us")}</Text>
              </RTLView>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.langSetting}
              style={
                this.state.locale == "eng"
                  ? styles.menuOption
                  : styles.menuOption_ar
              }
            >
              <RTLView locale={this.state.locale}>
                <Image style={styles.menuCateImg} source={menuScreen.setting} />
                <Text style={styles.tabText}>
                  {I18n.t("Setting & Language")}
                </Text>
              </RTLView>
            </TouchableOpacity>

            {/*## ABOUT COLLAPSE SECTION ##*/}
            <Collapse onToggle={() => this.setState({ showArrowIconForAbout: !this.state.showArrowIconForAbout, showCollaspForAbout: true })}>
              <CollapseHeader>
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.menuOption
                      : styles.menuOption_ar
                  }
                >
                  <RTLView locale={this.state.locale}>
                    <View>
                      <Image
                        style={styles.menuCateImg}
                        source={menuScreen.about}
                      />
                    </View>
                    <View style={{ width: "85%" }}>
                      <RTLView locale={this.state.locale}>
                        <Text style={styles.tabText}>
                          {I18n.t("About The Application")}
                        </Text>
                      </RTLView>
                    </View>
                    <View style={{ width: "5%" }}>
                      <Image
                        style={[styles.menuCateImg2, { transform: this.state.showArrowIconForAbout ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }] }]}
                        source={menuScreen.downarrow}
                      />
                    </View>
                  </RTLView>
                </View>
              </CollapseHeader>

              {/*## ABOUT COLLAPSE BODY SECTION ##*/}

              <CollapseBody style={styles.cbEli}>
              {
              // this.state.showCollaspForAbout &&
                this.state.aboutCate.map((item, key) => (
                  <TouchableOpacity
                    onPress={item.navigate}
                    style={
                      this.state.locale == "eng"
                        ? styles.menuOption
                        : styles.menuOption_ar
                    }
                  >
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.tabBoldText}>
                        {I18n.t(`${item.itemText}`)}
                      </Text>
                    </RTLView>
                  </TouchableOpacity>
                ))}
              </CollapseBody>
            </Collapse>

            {/*## SIGN OUT SECTION ##*/}
            {this.state.userName ?
              <TouchableOpacity
                style={
                  this.state.locale == "eng"
                    ? styles.menuGrey
                    : styles.menuGrey_ar
                }
                onPress={() => this.signOut()}
              >
                <Text style={styles.blueText}>{I18n.t("Sign Out")}</Text>
              </TouchableOpacity>
              :
              null
            }

            {/*## FOOTER SECTION ##*/}
            <View style={styles.socialBg}>
              <View style={styles.imageRow}>
                <RTLView locale={this.state.locale}>
                  <TouchableOpacity>
                    <Image
                      style={styles.menuCateImg3}
                      source={menuScreen.facebook}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image
                      style={styles.menuCateImg3}
                      source={menuScreen.twitter}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image
                      style={styles.menuCateImg3}
                      source={menuScreen.instagram}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Image
                      style={styles.menuCateImg3}
                      source={menuScreen.youtube}
                    />
                  </TouchableOpacity>
                </RTLView>
              </View>
              <View
                style={
                  this.state.locale == "eng"
                    ? [commonStyle.rights, commonStyle.rowSec, { marginBottom: 0 }]
                    : [commonStyle.rights, commonStyle.rowSec_ar, { marginBottom: 0 }]
                }
              >
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                </Text>
                <Text
                  style={[
                    commonStyle.screenText,
                    { fontSize: 12, fontFamily: "Tajawal-Bold" },
                  ]}
                >
                  {I18n.t(" Bitaqaty ")}
                </Text>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("@2019")}
                </Text>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(
  mapStateToProps,
  {}
)(Menu);
