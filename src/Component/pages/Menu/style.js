const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    backgroundColor: "white"
  },
  userSec: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 44,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#FAFAFA"
  },
  userHello: {
    marginRight: 5,
    fontSize: 16,
    lineHeight: 30,
    fontFamily: "Tajawal-Medium"
  },
  boldText: {
    fontWeight: "bold",
    fontSize: 20,
    fontFamily: "Tajawal-Bold"
  },
  menuCateImg: {
    width: 18,
    height: 18,
    marginTop: 5,
    marginHorizontal: 7,
  },
  menuCateImg2: {
    justifyContent: "flex-end",
    paddingTop: 8,    
    marginTop: 5,
  },
  menuCateImg3: {
    width: 32,
    height: 32,
    marginHorizontal: 10
  },
  menuIconNew: {
    width: 16,
    height: 16,
  },
  menuTab: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#fff"
  },
  menuTab_ar: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)",
    alignItems: "center",
    justifyContent: 'flex-end',
    flexDirection: "row",
    backgroundColor: "#fff"
  },
  menuTabWhite: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#fff"
  },
  menuTabWhite_ar: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)",
    alignItems: "center",
    justifyContent: 'flex-end',
    flexDirection: "row",
    backgroundColor: "#fff"
  },
  menuOption: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#fff"
  },
  menuOption_ar: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    alignItems: "center",
    flexDirection: "row",    
    justifyContent: 'flex-end',
    backgroundColor: "#fff"
  },
  menuGrey: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#FAFAFA"
  },
  menuGrey_ar: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 51,
    alignItems: "center",
    flexDirection: "row",   
    justifyContent: 'flex-end',
    backgroundColor: "#FAFAFA"
  },
  tabText: {
    fontSize: 14,
    fontFamily: "Tajawal-Medium",
    paddingTop: 4
  },
  tabTextName : {
    fontSize: 14,
    fontFamily: "Tajawal-Medium",
    paddingTop: 4,
    paddingLeft : 10
  },
  tabTextBlue:{
    fontFamily: "Tajawal-Bold",
    color:"#2700EB",
    fontSize:16
  },
  blueText: {
    fontSize: 14,
    color: "#2700EB",
    fontFamily: "Tajawal-Bold",
    marginHorizontal: 7,
  },
  tabBoldText: {
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
    marginHorizontal: 7,
  },
  cbEli: {
    backgroundColor: "#fff",
    elevation: 1
  },
  imageRow: {
    justifyContent: "center",
    flexDirection: "row"
  },
  socialBg: {
    backgroundColor: "#F2F0F0",
    paddingTop: 30,
    paddingBottom: 70
  },
  activityIndicator: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
};
