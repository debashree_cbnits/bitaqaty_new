import React, { Component } from "react";
import { Text, View, ScrollView,Dimensions, Image, Modal, TouchableOpacity, Alert, ActivityIndicator} from "react-native";
import styles from "./style";
import {cartImage, CatagoryImagePath, loginScreen,} from "../../../config/imageConst";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { connect } from "react-redux";
import HeaderArrow from "../Header/Header_logo_name/HeaderArrow";
import AsyncStorage from "@react-native-community/async-storage";
import commonStyle from "../../../../commonStyle.js";
import { Collapse, CollapseHeader, CollapseBody, } from "accordion-collapse-react-native";
import store from "../../../redux/store";
import {setCartArrayChanged, setCartArrayLength}from "../../../redux/storage/action";
import apiClient from "../../../services/api.client";
import imageUrl from '../../../services/config';


const deviceHeight = Dimensions.get('window').height;

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Cart",
      count: 0,
      locale: "",
      language: "",
      cartArray: [],
      cartData: [],
      totalPrice: 0,
      totalVat: 0,
      optionDisplayIndex: 9999999999999,
      show: false,
      showUnavailableAction: false,
      showCostLimitALert: false,
      applicationSettings:[],
      dayMaxPayment:'',
      maxcartProducts:'',
      maxPayment: '',
      totalQuantity:1,
      token:'',
      maxcartProductsReached: false,
      isLoading: false
    };
  }
  componentDidMount() {
    this.getcart();
    this.props.navigation.addListener("didFocus", () => {
      this.getcart();    
    });

    this.props.navigation.addListener("didBlur", () => {
      this.getcart();
    });
  } 

  getCartFromSettings(){
    this.setState({isLoading:true})   
    AsyncStorage.getItem("userToken")
    .then((token) => {            
      apiClient.getRequest ('/bitaqatySettings',token).then ((bitaqatySettingsRes) => {   
        this.setState({isLoading: false, applicationSettings : bitaqatySettingsRes.applicationSettings, token: token},()=>{
          for (let i=0; i<this.state.applicationSettings.length;i++){
            if (this.state.applicationSettings[i].propertyName =="dayMaxPayment"){
              this.setState({dayMaxPayment:Number(this.state.applicationSettings[i].propertyValue )})
            } else if (this.state.applicationSettings[i].propertyName == "maxcartProducts"){
              this.setState({maxcartProducts:Number(this.state.applicationSettings[i].propertyValue) })
            }else if (this.state.applicationSettings[i].propertyName == "maxPayment"){
              this.setState({maxPayment:Number(this.state.applicationSettings[i].propertyValue) })
            }
          }  
        }) 
      }).catch (err =>{
        this.setState({isLoading:false})      
      })
  }).catch(err => { this.setState({isLoading:false})      
  }) 
  }
  
  getcart = () => {
    let totalQuantity =0;
    this.getCartFromSettings();
    if (this.state.cartData && this.state.cartData.length > 0) {
      this.setState({ cartArray: this.state.cartData }, () => {
        let totalPrice = 0;
        let totalVat = 0;
        for (let i = 0; i < this.state.cartArray.length; i++) {
          this.state.cartArray[i].prodQuantity = this.state.cartArray[i]
            .prodQuantity
            ? this.state.cartArray[i].prodQuantity
            : 1;
            totalQuantity += this.state.cartArray[i].prodQuantity
          if (this.state.cartArray[i].discountPercentage > 0){
            this.state.cartArray[i].prodTotalPrice =
            parseInt(this.state.cartArray[i].prodQuantity)*(this.state.cartArray[i].vat + this.state.cartArray[i].individualPriceAfter )
            

            this.state.cartArray[i].totalPrice =
            this.state.cartArray[i].prodQuantity *
            this.state.cartArray[i].individualPriceAfter;
          totalPrice +=
            parseInt(this.state.cartArray[i].prodQuantity) *
            this.state.cartArray[i].individualPriceAfter;
            totalVat += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].vat          
          } else {
            this.state.cartArray[i].prodTotalPrice = parseInt(this.state.cartArray[i].prodQuantity)*(this.state.cartArray[i].individualPrice )
            this.state.cartArray[i].totalPrice =
            this.state.cartArray[i].prodQuantity *
            this.state.cartArray[i].vat + this.state.cartArray[i].individualPrice;
          
            totalPrice += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].individualPrice;
            totalVat += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].vat 
          }          
        }        
        this.setState({ totalPrice: totalPrice, totalQuantity: totalQuantity, totalVat: totalVat })        
      });
    } else {
      AsyncStorage.getItem("CartItemList")
        .then((results) => {
          if (results) {
            let totalPrice = 0;
            let totalVat = 0;
            this.setState({ cartArray: JSON.parse(results) }, () => {
              for (let i = 0; i < this.state.cartArray.length; i++) {
                this.state.cartArray[i].prodQuantity = this.state.cartArray[i]
                  .prodQuantity
                  ? this.state.cartArray[i].prodQuantity
                  : 1;
                  totalQuantity += this.state.cartArray[i].prodQuantity
                  
                  if (this.state.cartArray[i].discountPercentage > 0){
                    this.state.cartArray[i].prodTotalPrice =
                    parseInt(this.state.cartArray[i].prodQuantity)*(this.state.cartArray[i].vat + this.state.cartArray[i].individualPriceAfter) 

                    this.state.cartArray[i].totalPrice =
                      this.state.cartArray[i].prodQuantity *
                      this.state.cartArray[i].individualPriceAfter;
                    totalPrice +=
                      parseInt(this.state.cartArray[i].prodQuantity) *
                      this.state.cartArray[i].individualPriceAfter;
                    totalVat += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].vat 
                  } else {
                    this.state.cartArray[i].prodTotalPrice = 
                    parseInt(this.state.cartArray[i].prodQuantity)*(this.state.cartArray[i].vat + this.state.cartArray[i].individualPrice )
                    this.state.cartArray[i].totalPrice =
                    this.state.cartArray[i].prodQuantity *
                    this.state.cartArray[i].individualPrice;                  
                    totalPrice += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].individualPrice;
                    totalVat += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].vat 
                  } 
              }            
              this.setState({ totalPrice: totalPrice, totalVat: totalVat , totalQuantity: totalQuantity });
            });
          } else {
            this.setState({ cartArray: [] });
          }
        })
        .catch((err) => {          
        });
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          cartData: props.cartData,
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          cartData: props.cartData,
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        cartData: [],
      };
    }    
  }
  _removeCartItem(index) {
    let newArray = [...this.state.cartArray];
    newArray[index].openAlertBox = true;
    this.setState({ cartArray: newArray, optionDisplayIndex: index });
  }

  cancelAlertBox = (index) => {
    let newArray = [...this.state.cartArray];
    newArray[index].openAlertBox = false;
    this.setState({ cartArray: newArray });
  };

  removeCart = index => {
    this.state.cartArray.splice(index, 1);
    AsyncStorage.setItem("CartItemList", JSON.stringify(this.state.cartArray));
    store.dispatch(setCartArrayChanged(true));
    AsyncStorage.getItem("CartItemList").then(cartArray => {
      if (cartArray === null || (cartArray !== null && JSON.parse(cartArray).length == 0)) {
        store.dispatch(setCartArrayLength(0));
      } else {       
        let totalQuantity = 0;           
        // store.dispatch(setCartArrayLength(JSON.parse(cartArray).length));      
        for (let i = 0; i < this.state.cartArray.length; i++){
          totalQuantity += this.state.cartArray[i].prodQuantity          
        }
        store.dispatch(setCartArrayLength(totalQuantity));
      }
      this.forceUpdate();
    });
    this.getcart();
    // this.forceUpdate();
  };

  increment = (index, quantity) => {
    let totalQuantity = 0;
    let totalPrice = 0;
    let totalVat = 0;
    let newArray = [...this.state.cartArray];
    let quantity1 = quantity + 1;
    newArray = [...this.state.cartArray];
    newArray[index].prodQuantity = quantity1;
    newArray[index].prodTotalPrice =
      quantity1 * (newArray[index].individualPrice + newArray[index].vat);
    this.setState({ cartArray: newArray }, () => {
      for (let i = 0; i < this.state.cartArray.length; i++) {
        totalQuantity += this.state.cartArray[i].prodQuantity;
        totalPrice +=
          parseInt(this.state.cartArray[i].prodQuantity) *
          this.state.cartArray[i].individualPrice;
        totalVat += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].vat 

      }
      store.dispatch(setCartArrayLength(totalQuantity));
      this.setState({ totalPrice: totalPrice, totalVat: totalVat, totalQuantity: totalQuantity});
      AsyncStorage.setItem(
        "CartItemList",
        JSON.stringify(this.state.cartArray)
      );
    });
  };

  decrement = (index, quantity) => {
    let totalQuantity = 0;
    let totalPrice = 0;
    let totalVat = 0;
    let newArray = [...this.state.cartArray];
    if (quantity > 1) {
      let quantity1 = quantity - 1;
      newArray = [...this.state.cartArray];
      newArray[index].prodQuantity = quantity1;
      newArray[index].prodTotalPrice =
        quantity1 * (newArray[index].individualPrice +  newArray[index].vat);
      this.setState({ cartArray: newArray }, () => {
        for (let i = 0; i < this.state.cartArray.length; i++) {
          totalQuantity += this.state.cartArray[i].prodQuantity;
          totalPrice +=
            parseInt(this.state.cartArray[i].prodQuantity) *
            this.state.cartArray[i].individualPrice;
          totalVat += parseInt(this.state.cartArray[i].prodQuantity) * this.state.cartArray[i].vat 
        }
        store.dispatch(setCartArrayLength(totalQuantity));
        this.setState({ totalPrice: totalPrice, totalVat: totalVat, totalQuantity: totalQuantity });
      });
      AsyncStorage.setItem(
        "CartItemList",
        JSON.stringify(this.state.cartArray)
      );
    } else {
      this._removeCartItem(index);
    }
  };

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Home");
    }
  };
  // receiptPage = () =>{
  //   this.props.navigation.navigate ("Receipt")
  // }

  onPaymentOptn = () => {
    let newArray = [...this.state.cartArray];        
    let array = [];  
    this.setState({isLoading:true})   
    AsyncStorage.getItem("userToken")
      .then(results => {         

        if (results) {
          for (let i = 0; i<this.state.cartArray.length; i++) {
            newArray[i].quantity = newArray[i].prodQuantity;
          }    
          this.setState({ cartArray: newArray }, ()=>{
            for (let i=0; i<this.state.cartArray.length; i++){
              array.push ({"productId":this.state.cartArray[i].id, "quantity":this.state.cartArray[i].prodQuantity})
            }
          })
          
          let validateQuantityData ={
            "orderProducts": array,
            "action": "validateQuantity"
          }
          apiClient.postRequest ('/confirmOrder', validateQuantityData, this.state.token).then ((confirmOrderRes1) => {     
            if (confirmOrderRes1.validPrductsList && confirmOrderRes1.validPrductsList.length > 0) {
              let vatCalculationData ={
                "orderProducts": array,
                "action": "vatCalculation"
              }

              apiClient.postRequest ('/confirmOrder', vatCalculationData, this.state.token).then ((confirmOrderRes2) => {                
                this.setState({isLoading:false})   
                if (confirmOrderRes2.validPrductsList && confirmOrderRes2.validPrductsList.length > 0) {
                  let unavailableItemCount = this.state.cartArray.filter((item) => {
                    return item.avaiable === false
                  })    
                  if (unavailableItemCount.length > 0) {
                    this.setState({ showUnavailableAction: true })
                  }                 
                  else {                    
                    let payment = (this.state.totalPrice + this.state.totalVat)
                    this.props.navigation.navigate('PaymentOption', {
                      cartData: this.state.cartArray, totalPayment: payment,
                      totalPrice: this.state.totalPrice.toFixed(2),
                      totalQuantity: this.state.totalQuantity,
                      totalVat: this.state.totalVat.toFixed(2)
                    })          
                  }
                }
              }).catch (err =>{
                this.setState({isLoading:false})      
              })
            } else if (confirmOrderRes1.error){
              this.setState({isLoading:false}) 
              if (confirmOrderRes1.error == "todayMaxPaymentExceed"){
                this.setState({dayMaxPayment : confirmOrderRes1.maxPayment, showCostLimitALert: true})
              }else if (confirmOrderRes1.error == "todayMaxOrdersExceed"){
                this.setState({maxcartProducts : confirmOrderRes1.maxOrders, maxcartProductsReached: true})
              }
            }  else {
              this.setState({maxcartProductsReached: false,showCostLimitALert: false, isLoading: false})
            }
          }).catch (err =>{
            this.setState({isLoading:false})      
          })             
        } else {
          this.props.navigation.navigate('Login', {'cartWithoutLogin': "yes"})
        }
      }).catch(err => console.log(err));
    
  };

  render() {        
    const { cartArray } = this.state;
    let unavailableItemArray = this.state.cartArray.filter((item) => {
      return item.avaiable === false
    })
    let availableItemArray = this.state.cartArray.filter((item) => {
      return item.avaiable != true
    })

    return (
      <>
       {this.state.isLoading ? (
          <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A" />
              ) : ( 
        <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />

        {/* #####@@@@@@ Cart Cash Limit alert @@@@@@##### */}

        <View style={{ flex: 1, width: "100%", position: "absolute", top: 0 }}>
          <Modal transparent={true} visible={this.state.showCostLimitALert}>
            <View style={{ flex: 1, backgroundColor: "#0000008a" }}>
              <View style={styles.cartErrSec}>
                <View style={{ alignItems: "center", marginTop: "20%" }}>
                  <Image source={loginScreen.loginError} />
                  <Text style={styles.cartErrBoldText}>
                    {I18n.t(
                      "You have reached limit to purchase per day")} {this.state.dayMaxPayment && this.state.dayMaxPayment} {" "} {I18n.t("SAR")} 
                    {/* {" "}{I18n.t("(Error explanation)")} */}
                  </Text>
                  <Text style={styles.cartErrMsg}>
                    {I18n.t(
                      "Please modify the cart to continue the payment process (Solution)"
                    )}
                  </Text>
                </View>

                <TouchableOpacity style={commonStyle.blackBtn} onPress = {()=> this.setState({showCostLimitALert:!this.state.showCostLimitALert})}>
                  <Text style={commonStyle.blackBtnText}>
                    {I18n.t("Back to Edit Cart")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>

        {/* #####@@@@@@ Cart Out of stock alert @@@@@@##### */}

        <View style={{ flex: 1, width: "100%", position: "absolute", top: 0 }}>
          <Modal transparent={true} visible={this.state.showUnavailableAction}>
            <View style={{ flex: 1, backgroundColor: "#0000008a" }}>
              <View style={styles.cartErrSec}>
                <View style={{ alignItems: "center", marginTop: "20%" }}>
                  <Image source={loginScreen.loginError} />
                </View>
                {availableItemArray && availableItemArray.length>0 ?
                <ScrollView style={{ height: 120,marginTop: 30, paddingVertical: 15, }}>                  
                   {availableItemArray.map((item, index) => {
                    return (
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ width: "20%" }}>
                          <Image                            
                            source={{uri: imageUrl.productImageUrl+ item.backImagePath}}                           
                            style={styles.cartProdImg}
                          />
                        </View>

                        <View style={{ width: "80%", paddingLeft: 10 }}>
                          <Text style={styles.cartErrBoldText2}>
                            {I18n.t("Only")}{" "}{availableItemArray && availableItemArray.length}{" "}{I18n.t("cards are now available from")}
                          </Text>
                          <Text style={styles.cartErrMsg2}>
                            {this.state.locale === "eng"
                              ? item.nameEn
                              : item.nameAr}
                          </Text>
                          <Text style={styles.cartErrMsg2}>
                            {this.state.locale === "eng"
                              ? item.merchantNameEn
                              : item.merchantNameAr}
                          </Text>
                        </View>
                      </View>
                       )
                    })
                  } 
                </ScrollView>
                : null }

              {unavailableItemArray && unavailableItemArray.length>0 ?
                <ScrollView style={{ height: 120, marginVertical: 15, paddingVertical: 15, }}>
                  
                  {unavailableItemArray && unavailableItemArray.map((item, index) => {
                    return (
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ width: "20%" }}>
                          <Image                            
                            source={{uri: imageUrl.productImageUrl+ item.backImagePath}}                           
                            style={styles.cartProdImg}
                          />
                        </View>

                        <View style={{ width: "80%", paddingLeft: 10 }}>
                          <Text style={styles.cartErrBoldText2}>
                            {I18n.t("This card is out of stock")}
                          </Text>
                          <Text style={styles.cartErrMsg2}>
                            {this.state.locale === "eng"
                              ? item.nameEn
                              : item.nameAr}
                          </Text>
                          <Text style={styles.cartErrMsg2}>
                           {this.state.locale === "eng"
                              ? item.merchantNameEn
                              : item.merchantNameAr}
                          </Text>
                        </View>
                      </View>
                    )
                    })}                
                </ScrollView>
                : null }

                <TouchableOpacity style={commonStyle.blackBtn} onPress={() => this.setState({ showUnavailableAction: !this.state.showUnavailableAction })}>
                  <Text style={commonStyle.blackBtnText}>
                    {I18n.t("Back to Edit Cart")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>

        {unavailableItemArray && unavailableItemArray.length>0 ? (
          <View
            style={
              this.state.locale == "eng"
                ? commonStyle.errorMsg
                : commonStyle.errorMsg_ar
            }
          >
            <Image source={loginScreen.error} />
            <Text style={commonStyle.errorMsgText}>
              {I18n.t(
                "We have out of stock on some cards, please modify the cart to proceed"
              )}
            </Text>
          </View>
        ) : null}
        {this.state.totalPrice && this.state.totalPrice+this.state.totalVat > this.state.maxPayment ? 
          <View
            style={
              this.state.locale == "eng"
                ? commonStyle.errorMax
                : commonStyle.errorMax_ar
            }
          >
            <Text style={commonStyle.errorMsgcenter}>
              {I18n.t("Max")} {this.state.maxPayment && this.state.maxPayment} {I18n.t("SAR")}
            </Text>
          </View> : null}

          {this.state.maxcartProductsReached ? 
          <View
            style={
              this.state.locale == "eng"
                ? commonStyle.errorMax
                : commonStyle.errorMax_ar
            }
          >
            <Text style={commonStyle.errorMsgcenter}>
              {I18n.t("Max. allowed for a day:")} {" "}{this.state.maxcartProducts}{" "}{I18n.t("order")}
            </Text>
          </View> : null}

          
          {this.state.totalQuantity  && this.state.totalQuantity > this.state.maxcartProducts ? 
          <View
            style={
              this.state.locale == "eng"
                ? commonStyle.errorMax
                : commonStyle.errorMax_ar
            }
          >
            <Text style={commonStyle.errorMsgcenter}>
              {I18n.t("Max")} {" "}{this.state.maxcartProducts}{" "}{I18n.t("products")}
            </Text>
          </View> : null}
        <ScrollView style={{ marginBottom: 115 }}>
          {cartArray && cartArray.length > 0 ? (
            cartArray.map((item, index) => {
              return (
                <View
                key={index}
                  style={
                    item.avaiable
                      ? 
                      styles.cartMain : styles.cartMain_red
                  }
                >
                  <View style={styles.cartOrder}>
                    <RTLView locale={this.state.locale}>
                      <View style={styles.orderImage}>
                        <Image
                          source={{uri: imageUrl.productImageUrl+ item.backImagePath}}                           
                          style={styles.cartProdImg}
                        />
                      
                      </View>

                      <View
                        style={
                          this.state.locale == "eng"
                            ? styles.orderDetails
                            : styles.orderDetails_ar
                        }
                      >
                        <View>
                          <RTLView locale={this.state.locale}>
                            <RTLText style={styles.medText}>
                              {this.state.locale === "eng"
                                ? item.nameEn
                                : item.nameAr}
                            </RTLText>
                          </RTLView>
                        </View>

                        <View>
                          <RTLView locale={this.state.locale}>
                            <RTLText style={styles.medBlueText}>
                              {this.state.locale === "eng"
                                ? item.merchantNameEn
                                : item.merchantNameAr}
                            </RTLText>
                          </RTLView>
                        </View>

                        <View>
                          <RTLView locale={this.state.locale}>
                            <RTLText style={styles.medLightText}>
                              {I18n.t("Price")}{" "}
                              <Text style={styles.BoldText}>
                                {item.discountPercentage > 0 ? item.individualPriceAfter : item.individualPrice}
                              </Text>{" "}
                                {I18n.t("SAR")}
                            </RTLText>
                          </RTLView>
                        </View>

                        <View>
                          <RTLView locale={this.state.locale}>
                            <RTLText style={styles.medLightText}>
                              {I18n.t("VAT")}{" "}
                              <Text style={styles.BoldText}>{item.vat}</Text>{" "}
                                 {I18n.t("SAR")}
                            </RTLText>
                          </RTLView>
                        </View>
                      </View>
                    </RTLView>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <RTLView locale={this.state.locale}>
                      <View style={styles.price}>
                        <Text style={styles.priceText}>{I18n.t("Remove")}</Text>
                        <TouchableOpacity
                          onPress={() => this._removeCartItem(index)}
                        >
                          <Image source={cartImage.deleteIcon} />
                        </TouchableOpacity>
                      </View>
                      <View style={styles.odrQuantity}>
                        <Text style={{ marginBottom: 7 }}>
                          {I18n.t("Quantity")}
                        </Text>                        
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.increDecre
                              : styles.increDecre_ar
                          }
                        >
                          <TouchableOpacity
                          style={{
                            opacity: item.prodQuantity == 1 ? 0.3 : 1
                          }}
                          disabled = {item.prodQuantity == 1 ? true: false}
                            onPress={() =>
                              this.decrement(index, item.prodQuantity)
                            }
                          >
                            <Image
                              source={cartImage.minusIcon}
                              style={{ marginHorizontal: 20 }}
                            />
                          </TouchableOpacity >
                          <Text
                            style={{ fontSize: 16, fontFamily: "Tajawal-Bold" }}
                          >
                            {item.prodQuantity ? item.prodQuantity : 1}
                          </Text>
                          <TouchableOpacity 
                          style={{
                            opacity: item.prodQuantity >= 5 ? 0.3 : 1
                          }}
                          disabled = {item.prodQuantity >= 5 ? true: false}
                            onPress={() =>
                              this.increment(index, item.prodQuantity)
                            }
                          >
                            <Image
                              source={cartImage.plusIcon}
                              style={{ marginHorizontal: 20 }}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={styles.price}>
                        <Text style={styles.priceText}>{I18n.t("Price")}</Text>
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.totalPrice
                              : styles.totalPrice_ar
                          }
                        >
                          <Text style={styles.priceTag}>
                            {item.prodTotalPrice
                              ? (item.prodTotalPrice).toFixed(2)
                              : ""}
                          </Text>
                          <Text style={styles.priceSym}>
                               {I18n.t("SAR")}
                          </Text>
                        </View>
                      </View>
                    </RTLView>
                  </View>

                  {!item.avaiable ? (
                    <View style={styles.cartErrorSec}>
                      <Text style={styles.cartErrorText}>
                        {I18n.t(
                          "This card is currently unavailable (cause of error)"
                        )}
                      </Text>
                    </View>
                  ) : null}
                  {item.openAlertBox &&
                    this.state.optionDisplayIndex === index ? (
                      <View style={styles.rmvCartSec}>
                        <Image source={cartImage.removeBig} />
                        <Text style={styles.rmvBoldText}>
                          {I18n.t(
                            "Are you sure you want to remove that product?"
                          )}
                        </Text>
                        <View 
                        style={
                          this.state.locale == "eng"
                            ? styles.rmvCartHead
                            : styles.rmvCartHead_ar
                        }>
                          <TouchableOpacity
                            style={styles.removeBtn}
                            onPress={() => this.removeCart(index)}
                          >
                            <Text style={styles.rmvBtnText}>
                              {I18n.t("Remove the card now")}
                            </Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={styles.cancelBtn}
                            onPress={() => this.cancelAlertBox(index)}
                          >
                            <Text style={styles.cnclBtnText}>
                              {I18n.t("Keep the card")}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    ) : null}
                </View>
              );
            })
          ) : (
              <View style={commonStyle.emptymsgSec}>
                <Image
                  source={require("../../assets/image/emptyCart.png")}
                  style={commonStyle.marginAuto}
                />
                <Text style={commonStyle.emptymsg}>
                  {I18n.t(
                    "shopping cart is empty"
                  )}
                </Text>
              </View>
            )}
          {cartArray && cartArray.length > 0 ? (
            <View style={{ width: "100%", marginTop: deviceHeight / 7}}>
              <View style={styles.footerSec}>
                <TouchableOpacity  onPress={()=>this.props.navigation.navigate("ContactUs")}>
                  <Text style={styles.footerText}>{I18n.t("Contact Us")}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress ={()=> this.props.navigation.navigate("Terms", { index: 3 })}>
                  <Text style={styles.footerText}>
                    {I18n.t("Terms Of Use")}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("Privacy",{ index: 2 })}>
                  <Text style={styles.footerText}>
                    {I18n.t("Policies")}
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={
                  this.state.locale == "eng"
                    ? [commonStyle.rights, commonStyle.rowSec]
                    : [commonStyle.rights, commonStyle.rowSec_ar]
                }
              >
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                </Text>
                <Text
                  style={[
                    commonStyle.screenText,
                    { fontSize: 12, fontFamily: "Tajawal-Bold" },
                  ]}
                >
                  {I18n.t(" Bitaqaty ")}
                </Text>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("@2019")}
                </Text>
              </View>
            </View>
          ) : null}
        </ScrollView>

        {cartArray && cartArray.length > 0 ? (
          <View style={styles.bottomArea}>
            <Collapse>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.totalPriceSec
                    : styles.totalPriceSec_ar
                }
              >
                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.totalPrice
                      : styles.totalPrice_ar
                  }
                >
                  <Text style={styles.medLightText}>
                    {I18n.t("Total Payment")}{" "}
                  </Text>
                  <Text style={styles.totalCartPrice}>
                    {this.state.totalPrice
                      ? (this.state.totalPrice + this.state.totalVat).toFixed(2)
                      : ""}{" "}
                  </Text>
                  <Text style={styles.tagSym}>{I18n.t("SAR")} </Text>
                </View>
                <Image
                  source={CatagoryImagePath.rightArrow}
                  style={styles.rotateImage}
                />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.lightText}>
                {I18n.t("Number of Cards")}{" "}
                  <Text style={styles.medText}>
                    {this.state.totalQuantity && this.state.totalQuantity}
                  </Text>
                </Text>
                <Text style={styles.lightText}>
                {I18n.t("Total Price")}{" "}
                  <Text style={styles.medText}>
                    {this.state.totalPrice
                      ? (this.state.totalPrice).toFixed(2)
                      : 0}{" "}
                  </Text>{" "}
                  {I18n.t("SAR")}
                </Text>
                <Text style={styles.lightText}>
                {I18n.t("Total VAT")}{" "}
                  <Text style={styles.medText}>
                    {this.state.totalVat ? this.state.totalVat.toFixed(2) : 0}{" "}
                  </Text>{" "}
                  {I18n.t("SAR")}
                </Text>
              </CollapseBody>
            </Collapse>

            <TouchableOpacity
              onPress={this.onPaymentOptn}
              // onPress={this.receiptPage}

              disabled = {
                this.state.totalPrice && this.state.totalPrice+this.state.totalVat > this.state.maxPayment
                || this.state.totalQuantity  && this.state.totalQuantity > this.state.maxcartProducts 
                || this.state.maxcartProductsReached
                ? true : false}
              style={styles.pymtBtn}
            >
              <Text style={styles.btnText}>{I18n.t("Proceed Payment")}</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
      )}
      </>
    );
  }
}
mapStateToProps = (state) => ({
  cartData: state.Storage.CartItemList,
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Cart);