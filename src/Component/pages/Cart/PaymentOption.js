import React, { Component } from "react";
import { Text, View, ScrollView, Image, TouchableOpacity, ActivityIndicator, Alert } from "react-native";
import styles from "./style";
import { cartImage, CatagoryImagePath } from "../../../config/imageConst";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
// import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import HeaderArrow from "../Header/Header_logo_name/HeaderArrow";
import apiClient from "../../../services/api.client";
import Toast from "react-native-tiny-toast";
import commonStyle from "../../../../commonStyle";
import AsyncStorage from "@react-native-community/async-storage";
import cardimagePathUrl from '../../../services/config';

class PaymentOption extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Payment Option",
      locale: "",
      language: "",
      isLoading : true,
      paymentMethodArray: [],
      selectedData: {},
      selcetedIndex:9999999999,
      isSelected: false,
      token:'',
      orderProductArray:[],
      cartDataArray : this.props.navigation.state.params ? this.props.navigation.state.params.cartData : [],
      totalPayment : this.props.navigation.state.params ? this.props.navigation.state.params.totalPayment : '' ,
      totalQuantity: this.props.navigation.state.params ? this.props.navigation.state.params.totalQuantity : '' ,
      totalVat: this.props.navigation.state.params ? this.props.navigation.state.params.totalVat : '' ,
      totalPrice: this.props.navigation.state.params ? this.props.navigation.state.params.totalPrice : '' ,
      token: ''
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }
  componentDidMount (){    
    AsyncStorage.getItem("userToken")
    .then((token) => {
      this.setState({token: token})
    this.getPaymentMenthod(token);
    }).catch(err => console.log(err)) 
    
  }

  getPaymentMenthod (token){ 
    this.setState({isLoading:true})
    let data = {};    
    apiClient.postRequest ('/paymentMethods', data,token).then ((paymentMethodRes) => {           
      this.setState({isLoading:false, paymentMethodArray : paymentMethodRes})
    }).catch (err =>{
      this.setState({isLoading:false})
      
    })
  }

  backValueReceive = (value) => {    
    if (value == "back") {      
      this.props.navigation.navigate("Cart");
    }
  };

  onBankDetails = () => {  
    this.setState({isLoading:true})
    let array =[]  
  
    for (let i=0; i<this.state.cartDataArray.length; i++){
      array.push ({"productId":this.state.cartDataArray[i].id, "quantity":this.state.cartDataArray[i].prodQuantity})
    }
    let validateReserveData ={
      "orderProducts": array,
      "action": "reserveOrder"
    }     
    apiClient.postRequest ('/confirmOrder', validateReserveData, this.state.token).then ((confirmOrderRes1) => {        

      if (this.state.isSelected){
        this.setState({isLoading:false})
        this.props.navigation.navigate("BankDetails", {
          cartDataArray: this.state.cartDataArray,
          totalPayment: this.state.totalPayment,
          cardname: this.state.selectedData.name,
          cardLogo: this.state.selectedData.logo,        
          fee: this.state.selectedData.fee,
        })
      } else {
        this.setState({isLoading:false})
        Toast.show(I18n.t('Please select one payment method'))
      }
    })
  };

  selectPaymethod = (index, data) => {
    this.setState({ selcetedIndex: index, selectedData: data, isSelected: true })
  }

  render() {
    const {paymentMethodArray} = this.state ;    
    const imagePathUrl = "https://www.bitaqaty.com/assets/images/paymentMethods/" ;     
    return (
      <View style={{ ...styles.mainSection, backgroundColor: "#F2F0F0" }}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />

        <Collapse>
          <CollapseHeader
            style={
              this.state.locale == "eng"
                ? [styles.totalPriceSec, styles.paymentOptn]
                : [styles.totalPriceSec_ar, styles.paymentOptn]
            }
          >
            <View
              style={
                this.state.locale == "eng"
                  ? styles.totalPrice
                  : styles.totalPrice_ar
              }
            >
              <Text style={styles.medLightText}>
                {I18n.t("Total Payment")}{" "}
              </Text>
              <Text style={styles.totalCartPrice}>{this.state.totalPayment && this.state.totalPayment.toFixed(2)}</Text>
              <Text style={styles.tagSym}> {I18n.t("SAR")} </Text>
            </View>
            <Image
              source={CatagoryImagePath.rightArrow}
              style={styles.rotateImage}
            />
          </CollapseHeader>
          <CollapseBody style={styles.collapseBody}>
            <Text style={styles.lightText}>
            {I18n.t("Number of Cards")} <Text style={styles.medText}>{this.state.totalQuantity}</Text>
            </Text>
            <Text style={styles.lightText}>
            {I18n.t("Total Price")} <Text style={styles.medText}>{this.state.totalPrice}</Text> {I18n.t("SAR")}
            </Text>
            <Text style={styles.lightText}>
            {I18n.t("Total VAT")} <Text style={styles.medText}>{this.state.totalVat != 0 ? this.state.totalVat : 0}</Text> {I18n.t("SAR")}
            </Text>
            { this.state.isSelected ? 
            <Text style={styles.lightText}>
            {I18n.t("Extra fees")} <Text style={styles.medText}>{this.state.selectedData.fee}</Text> {I18n.t("SAR")}
            </Text> 
            : null}
          </CollapseBody>
        </Collapse>

        
        <ScrollView style={{ marginBottom: 70 }}>   
        {this.state.isLoading ? 
          <ActivityIndicator
          style={styles.activityIndicator}
          size="small"
          color="#1A1A1A"
          /> :      
          <View style={{ width: "100%", paddingHorizontal: "4%" }}>
            <View>
              <RTLView locale={this.state.locale}>
                <RTLText style={{ ...styles.medText, marginVertical: 13 }}>
                  {I18n.t("Choose Payment Method")}
                </RTLText>
              </RTLView>
            </View>
            <View
              style={
                this.state.locale == "eng"
                  ? styles.payMethodOptn
                  : styles.payMethodOptn_ar
              }
            >
              {
                paymentMethodArray && paymentMethodArray.length>0 &&
                  paymentMethodArray.map ((data, index) => {
                    return(
                    <TouchableOpacity style={this.state.selcetedIndex === index ? styles.PyMtSelect :styles.payMethod} onPress={()=>this.selectPaymethod(index, data)}>
                      <Image 
                        source={{
                        uri: cardimagePathUrl.cardimagePathUrl + data.logo}}                          
                        style={{ width :  '100%', height: 80, marginBottom: 5,}}
                      />
                      <Text style={styles.pymtMthdTxt}>{data.name}</Text>
                    </TouchableOpacity>
                    )
                  })
              }
            </View>
            { this.state.isSelected ? 
            <View style={[styles.paymentHead, {flexDirection: this.state.locale == "eng" ? 'row' : 'row-reverse'}]}>
              <Text style={styles.medLightText}>
                {I18n.t("Extra fees")}{" "}
              </Text>
            <Text style={styles.BoldText}>{this.state.selectedData.fee}  </Text>
            <Text style={styles.tagSym}>{I18n.t("SAR")} </Text>
            </View> 
            : null
            }
          </View>  
          }        
        </ScrollView>        
        <View style={styles.bottomArea}>
          <TouchableOpacity onPress={this.onBankDetails} style={this.state.isSelected ? styles.pymtBtn : styles.disableBtnClr}>
            <Text style={this.state.isSelected ? styles.btnText : styles.disableBtnText}>{I18n.t("Pay Now")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(PaymentOption);
