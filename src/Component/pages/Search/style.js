
const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    padding: 10,
    paddingHorizontal: "4%",
    width: "100%",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  container1: {
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "center",
    height: "100%",
    backgroundColor: "#FFFFFF",
  },

  Textbox_en: {
    borderStyle: "solid",
    borderColor: "#241125",
    borderWidth: 1,
    padding: 10,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
    width: "88%",
    height: 40,
    fontFamily: "Tajawal-Light",
    fontSize: 14,
  },
  Textbox_ar: {
    height: 48,
    borderStyle: "solid",
    borderColor: "#241125",
    borderWidth: 1,
    padding: 10,
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    width: "88%",
    height: 40,
    fontFamily: "Tajawal-Light",
    fontSize: 14,
  },
  searchButton_en: {
    width: "12%",
    backgroundColor: "#241125",
    height: 42,
    justifyContent: "center",
    alignItems: "center",
  },
  searchButton_ar: {
    width: "12%",
    backgroundColor: "#241125",
    height: 42,
    justifyContent: "center",
    alignItems: "center",
  },

  container: {
    backgroundColor: "#fff",
    flex: 1,
    // marginTop: 16
  },

  viewSearchHistory: {
    width: "100%",
    padding: 5,
    marginBottom: 10,
  },

  viewItemSearchHistory: {
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    // borderBottomColor: "#ececec",
    // borderBottomWidth: 1,
    justifyContent: "space-between",
    //paddingHorizontal: 10,
    //marginTop: 16
  },

  viewItemSearchHistory2: {
    backgroundColor: "#fff",
    //paddingHorizontal: 10,
    //marginTop: 16
  },

  // autocompleteContainer_en: {
  //   width: "100%",
  //   backgroundColor: "#fff",
  //   // borderColor: "#241125",
  //   // borderWidth: 1,
  //   // borderTopLeftRadius: 3,
  //   // borderBottomLeftRadius: 3,
  //   borderBottomColor: "#0000",
  //   //overflow: 'hidden',
  //   //paddingLeft: 7,
  //   height: 42,
  // },
  // autocompleteContainer_ar: {
  //   width: "100%",
  //   backgroundColor: "#fff",
  //   // borderColor: "#241125",
  //   // borderWidth: 1,
  //   // borderTopRightRadius: 3,
  //   // borderBottomRightRadius: 3,
  //   borderBottomColor: "#0000",
  //   //overflow: 'hidden',
  //   //paddingRight: 7,
  //   height: 42,
  //   textAlign: "right",
  // },
  searchInputHead: {
    width: "100%",
    height: 42,
    flexDirection: "row",
    borderColor: "#241125",
    borderWidth: 1,
    borderRadius: 4,
    overflow: "hidden",
  },
  searchInputHead_ar: {
    width: "100%",
    height: 42,
    flexDirection: "row-reverse",
    borderColor: "#241125",
    borderWidth: 1,
    borderRadius: 4,
    overflow: "hidden",
  },
  searchInput: {
    width: "90%",
    height: 42,
    backgroundColor: "#fff",
    borderBottomColor: "#0000",
    paddingHorizontal: 7,
    // textAlign: "left",
  },

  searchInput_ar: {
    width: "90%",
    height: 42,
    backgroundColor: "#fff",
    borderBottomColor: "#0000",
    paddingHorizontal: 7,
    // textAlign:"right",
  },
  itemText1: {
    fontSize: 14,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
    fontFamily: "Tajawal-Medium",
  },
  itemText2: {
    fontSize: 14,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
    fontFamily: "Tajawal-Medium",
    textAlign: 'right'
  },
  infoText: {
    textAlign: "center",
    fontSize: 16,
  },
  listContainerStyle: {
    width: "200%",
    //marginLeft: -8,
    backgroundColor: "red",
  },
  secHead: {
    width: "100%",
    paddingVertical: 10,
    backgroundColor: "#FAFAFA",
  },
  BoldText: {
    paddingHorizontal: "3%",
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
    alignSelf: "flex-start",
  },
  BoldTextAr: {
    paddingHorizontal: "3%",
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
    alignSelf: "flex-end",
  },
  noResult: {
    paddingTop:'20%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  noResultText: {
    marginTop: 15,
    fontFamily: 'Tajawal-Bold',
    color: '#1A1A1A',
    fontSize: 16,
  },
  paddingHori: {
    paddingHorizontal: '4%'
  },
  searchText: {
    fontFamily: 'Tajawal-Light',
    fontSize: 12,
    color: '#4F4F4F'
  },
  searchResultText: {
    fontFamily: 'Tajawal-Bold',
    fontSize: 12,
    color: '#4F4F4F'
  },

  //sharmistha//
  noSearchView_en: {
    // marginTop: -20,
    paddingBottom: 5,
    paddingLeft: 16,
    flexDirection: 'row'
  },
  noSearchView_ar: {    
    // marginTop: -20,
    paddingBottom: 5,
    paddingRight: 16,    
    flexDirection: 'row-reverse'
  },
  noSearchText_en: {
    fontSize: 12,
    fontFamily: "Tajawal-Light",
  },
  noSearchText_ar: {
    fontSize: 12,
    fontFamily: "Tajawal-Light",
    marginRight: 16
  },
  noSearchQuery: {
    fontSize: 12,
    fontFamily: "Tajawal-Bold",
  },
  containerNoSearch: {
    width: '100%', 
    resizeMode: "cover", 
    justifyContent: 'center', 
    alignItems: 'center',
    marginTop: '40%', 
    marginBottom: '40%'
  },
  textNoSearch: {
    fontSize: 16,
    fontFamily: "Tajawal-Bold",
    marginTop: 12
  },
  searchClose: {
    width: "88%",
    flexDirection: 'row',
    alignItems: 'center'
  },
  searchClose_ar: {
    width: "88%",
    flexDirection: 'row-reverse',
    alignItems: 'center'
  },
  activityIndicator: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
};
