import React, { Component } from "react";
import { Text, View, ScrollView, Image } from "react-native";
import styles from "./style.js";
import { RTLView, RTLText } from "react-native-rtl-layout";
import I18n from "../../../i18n/index";
import Search from "./Search";
import HeaderArrow from "../Header/Header_logo_name/HeaderArrow";
import { connect } from "react-redux";
import { productImagePath, SearchIcon } from "../../../config/imageConst";
import Product from "../../partial/ProductBox/Product";
import ProductHorizontal from "../../partial/ProductBox/ProductHorizontal";
import apiClient from "../../../services/api.client.js";
import { ActivityIndicator } from "react-native-paper";

class SearchPage1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      language: "",
      locale: "",
      isLoading: false,
      searchResult: null,
      query: "",
      relatedProduct: "",
      lessThatThree: "",
      autoSelectedCard: ""
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.props.navigation.addListener("willFocus", () => {
      this.setState({ searchResult: null, lessThatThree: "", autoSelectedCard: "" })
    });
  }

  goToLoginPage = (login) => {
    this.props.navigation.navigate("Login");
  }

  goToCartPage = (cart) => {
    this.props.navigation.navigate("Cart");
  }

  renderHorizontal = () => {
    return !this.state.isLoading &&
      this.state.searchResult &&
      this.state.searchResult.merchantRelatedProductMapDTO &&
      Object.entries(
        this.state.searchResult.merchantRelatedProductMapDTO
      ).length > 0 ? (
        <View style={styles.secHead}>
          <Text
            style={
              this.state.locale === "eng" ? styles.BoldText : styles.BoldTextAr
            }
          >
            {I18n.t("People Also Buy This")}
          </Text>
          <ScrollView
            nestedScrollEnabled
            horizontal
          >
            {!this.state.isLoading &&
              this.state.searchResult &&
              this.state.searchResult.merchantRelatedProductMapDTO &&
              Object.entries(
                this.state.searchResult.merchantRelatedProductMapDTO
              ).map((value, index) =>
                value[1].map((item, key) => {
                  return item.discountPercentage > 0 ? (
                    <ProductHorizontal
                      goToLogin={value => this.goToLoginPage(value)}
                      item={item}
                      key={key}
                      outOfStock={!item.avaiable}
                      productImagePath={item.backImagePath}
                      productHzTitle={
                        this.state.locale === "eng" ? item.nameEn : item.nameAr
                      }
                      productHzStore={
                        this.state.locale === "eng"
                          ? item.merchantNameEn
                          : item.merchantNameAr
                      }
                      productHzPriceH={I18n.t("Price")}
                      productHzPrice={item.individualPriceAfter}
                      productHzTag={I18n.t("SAR")}
                      prodWish={productImagePath.heart}
                      prodVarPar={productImagePath.vartiParcentage}
                      beforeHz={I18n.t("Before")}
                      beforeHzPrice={item.individualPrice}
                      beforeHzUnit={I18n.t("SAR")}
                      goToCart={value => this.goToCartPage(value)}
                      navigation={this.props.navigation}
                      doNotNavigate={true}
                    />
                  ) : (
                      <ProductHorizontal
                        goToLogin={value => this.goToLoginPage(value)}
                        item={item}
                        key={key}
                        outOfStock={!item.avaiable}
                        productImagePath={item.backImagePath}
                        productHzTitle={
                          this.state.locale === "eng" ? item.nameEn : item.nameAr
                        }
                        productHzStore={
                          this.state.locale === "eng"
                            ? item.merchantNameEn
                            : item.merchantNameAr
                        }
                        productHzPriceH={I18n.t("Price")}
                        productHzPrice={item.individualPrice}
                        productHzTag={I18n.t("SAR")}
                        prodWish={productImagePath.heart}
                        goToCart={value => this.goToCartPage(value)}
                        navigation={this.props.navigation}
                        doNotNavigate={true}
                      />
                    );
                })
              )}
          </ScrollView>
        </View>
      ) : (
        <></>
      );
  };


  _searchItemFromApi = (query, data) => {

    if (data == "QUERY") {
      this.setState({ query: query })
      if (query && query.length < 3) {

        this.setState({ lessThatThree: 1, searchResult: [], autoSelectedCard: [] })
      }
      else {
        this.setState({ isLoading: true, lessThatThree: "" }, () =>
          query ?
            apiClient
              .postRequest('/search/Products', { productName: query, locale: this.state.locale === "eng" ? "en" : "ar", pageNumber: 1, pageSize: 16 })
              .then((searchResult) => {
                this.setState({ searchResult, isLoading: false, relatedProduct: searchResult.merchantRelatedProductMapDTO });
              })
              .catch((err) => {
                this.setState({ searchResult: null, isLoading: false });
              })
            :
            this.setState({ searchResult: null, isLoading: false })
        )
      }
    }
    else {
      this.setState({ lessThatThree: "", autoSelectedCard: query, searchResult: [] })

    }

  }

  backValueReceive = value => {
    if (value == "back") {
      this.props.navigation.navigate("HomeScreen");
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{ width: "100%" }}>
          <HeaderArrow
            locale={this.state.locale}
            language={this.state.language}
            backPage={value => this.backValueReceive(value)}
          />

        </View>
        <Search fetchFromApi={this._searchItemFromApi} />
        {this.state.isLoading && <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A" />}
        {!this.state.isLoading && this.state.searchResult && this.state.searchResult.searchProducts &&
          <View>
            <View>
              {this.state.searchResult.searchProducts.length ? (
                <ScrollView contentContainerStyle =  {{paddingBottom : 
                  this.state.searchResult.searchProducts && this.state.searchResult.merchantRelatedProductMapDTO && this.state.searchResult.merchantRelatedProductMapDTO.length>0 ? 40 :  150}}>
                  <View style={this.state.locale == 'eng' ? styles.noSearchView_en : styles.noSearchView_ar}>
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? styles.noSearchText_en
                          : styles.noSearchText_ar
                      }
                    >
                      {I18n.t("Search Result For:")}
                    </Text>
                    <Text
                      style={styles.noSearchQuery}
                    >
                      {" "} {this.state.query}  {" "}
                    </Text>
                  </View>
                  {this.state.searchResult.searchProducts.map((item) => {                    
                    return item.discountPercentage > 0 ? (
                      <View style={{ paddingVertical: 0 }}>
                        <Product
                          goToLogin={value => this.goToLoginPage(value)}
                          goToCart={value => this.goToCartPage(value)}
                          item={item}
                          outOfStock={!item.avaiable}
                          productImage={item.backImagePath}

                          productTitle={
                            this.state.locale === "eng" ? item.nameEn : item.nameAr
                          }
                          productStore={
                            this.state.locale === "eng"
                              ? item.merchantNameEn
                              : item.merchantNameAr
                          }
                          before={I18n.t("Before")}
                          productBeforePrice={item.individualPrice}
                          beforeUnit={I18n.t("SAR")}
                          percentage={productImagePath.percent}
                          productPrice={item.individualPriceAfter}
                          productCurrency={I18n.t("SAR")}
                          navigation={this.props.navigation}
                          doNotNavigate={true}
                        />
                      </View>
                    ) : (
                        <View style={{ paddingVertical: 0 }}>
                          <Product
                            goToLogin={value => this.goToLoginPage(value)}
                            goToCart={value => this.goToCartPage(value)}
                            item={item}
                            outOfStock={!item.avaiable}
                            productImage={item.backImagePath}
                            productTitle={
                              this.state.locale === "eng" ? item.nameEn : item.nameAr
                            }
                            pointsCard={I18n.t("Points Card")}
                            productStore={
                              this.state.locale === "eng"
                                ? item.merchantNameEn
                                : item.merchantNameAr
                            }
                            productPrice={item.individualPrice}
                            productCurrency={I18n.t("SAR")}
                            navigation={this.props.navigation}
                            doNotNavigate={true}
                          />
                        </View>
                      )
                  })}
                  {this.renderHorizontal()}                  
                </ScrollView>
              ) : (
                  <View style={this.state.locale == 'eng' ? {} : { paddingHorizontal: 16 }}>
                    <View style={this.state.locale == 'eng' ? styles.noSearchView_en : styles.noSearchView_ar}>
                      <RTLView locale={this.state.locale}>
                        <RTLText
                          style={styles.noSearchText_en}
                        >
                          {I18n.t("Search Result For:")}
                        </RTLText>
                        <RTLText
                          style={styles.noSearchQuery}
                        >
                          {" "} {this.state.query} {" "}
                        </RTLText>
                      </RTLView>
                    </View>
                    <View style={styles.containerNoSearch}>
                      <Image
                        source={SearchIcon.noresult}
                      />
                      <Text style={styles.textNoSearch}>{I18n.t("No Results")}</Text>
                    </View>
                  </View>
                )}
            </View>
          </View>
        }
        {this.state.lessThatThree == 1 ? (
          <View style={this.state.locale == 'eng' ? {} : { paddingHorizontal: 16 }}>
            <View style={this.state.locale == 'eng' ? styles.noSearchView_en : styles.noSearchView_ar}>
              <RTLView locale={this.state.locale}>
                <RTLText
                  style={styles.noSearchText_en}
                >
                  {I18n.t("Search word shouldn't be less than 3 letters")}
                </RTLText>
              </RTLView>
            </View>
            <View style={styles.containerNoSearch}>
              <Image
                source={SearchIcon.noresult}
              />
              <Text style={styles.textNoSearch}>{I18n.t("No Results")}</Text>
            </View>
          </View>
        ) : (
            null
          )}


        {this.state.autoSelectedCard && Object.keys(this.state.autoSelectedCard).length ? (
          this.state.autoSelectedCard.discountPercentage > 0 ? (
            <View style={{ paddingVertical: 0 }}>
              <View style={this.state.locale == 'eng' ? styles.noSearchView_en : styles.noSearchView_ar}>
                <RTLView locale={this.state.locale}>
                  <Text
                    style={this.state.locale == 'eng' ? { ...styles.noSearchText_en, width: '23%' } : { ...styles.noSearchText_ar, width: '23%' }}
                  >
                    {I18n.t("Search Result For:")}
                  </Text>
                  <RTLText
                    style={this.state.locale == 'eng' ? { ...styles.noSearchQuery, width: '77%', } : { ...styles.noSearchQuery, width: '77%', textAlign: 'right' }}
                  >
                    {
                      this.state.locale === "eng" ? this.state.autoSelectedCard.nameEn : this.state.autoSelectedCard.nameAr
                    }
                  </RTLText>
                </RTLView>
              </View>
              <Product
                doNotNavigate={true}
                navigation={this.props.navigation}
                goToLogin={value => this.goToLoginPage(value)}
                goToCart={value => this.goToCartPage(value)}
                item={this.state.autoSelectedCard}
                outOfStock={!this.state.autoSelectedCard.avaiable}
                productImage={item.backImagePath}
                productTitle={
                  this.state.locale === "eng" ? this.state.autoSelectedCard.nameEn : this.state.autoSelectedCard.nameAr
                }
                productStore={
                  this.state.locale === "eng"
                    ? this.state.autoSelectedCard.merchantNameEn
                    : this.state.autoSelectedCard.merchantNameAr
                }
                before={I18n.t("Before")}
                productBeforePrice={this.state.autoSelectedCard.individualPrice}
                beforeUnit={I18n.t("SAR")}
                percentage={productImagePath.percent}
                productPrice={this.state.autoSelectedCard.individualPriceAfter}
                productCurrency={I18n.t("SAR")}
              />
            </View>
          ) : (
              <View style={{ paddingVertical: 0 }}>
                <View style={this.state.locale == 'eng' ? styles.noSearchView_en : styles.noSearchView_ar}>
                  <RTLView locale={this.state.locale}>
                    <RTLText
                      style={this.state.locale == 'eng' ? { ...styles.noSearchText_en, width: '23%' } : { ...styles.noSearchText_ar, width: '23%' }}
                    >
                      {I18n.t("Search Result For:")}
                    </RTLText>
                    <RTLText
                      style={this.state.locale == 'eng' ? { ...styles.noSearchQuery, width: '77%', } : { ...styles.noSearchQuery, width: '77%', textAlign: 'right' }}
                    >
                      {
                        this.state.locale === "eng" ? this.state.autoSelectedCard.nameEn : this.state.autoSelectedCard.nameAr
                      }
                    </RTLText>
                  </RTLView>
                </View>
                <Product
                  doNotNavigate={true}
                  navigation={this.props.navigation}
                  goToLogin={value => this.goToLoginPage(value)}
                  goToCart={value => this.goToCartPage(value)}
                  item={this.state.autoSelectedCard}
                  outOfStock={!this.state.autoSelectedCard.avaiable}
                  productImage={item.backImagePath}
                  productTitle={
                    this.state.locale === "eng" ? this.state.autoSelectedCard.nameEn : this.state.autoSelectedCard.nameAr
                  }
                  pointsCard={I18n.t("Points Card")}
                  productStore={
                    this.state.locale === "eng"
                      ? this.state.autoSelectedCard.merchantNameEn
                      : this.state.autoSelectedCard.merchantNameAr
                  }
                  productPrice={this.state.autoSelectedCard.individualPrice}
                  productCurrency={I18n.t("SAR")}
                />
              </View>
            )
        ) : (null)}
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    setlanguage: state.Language.setlanguage
  };
}
export default connect(
  mapStateToProps,
  {}
)(SearchPage1);