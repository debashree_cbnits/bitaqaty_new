import React, { Component } from "react";
import { Text, View, Image, TextInput, Keyboard } from "react-native";
import { TouchableOpacity, FlatList } from "react-native-gesture-handler";
import styles from "./style.js";
import I18n from "../../../i18n/index";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/dist/Ionicons";
import { setSelectedLanguage } from "../../../redux/language/action";
import { connect } from "react-redux";
import { SearchIcon } from "../../../config/imageConst";
import { withNavigation } from "react-navigation";
import apiClient from "../../../services/api.client.js";
import { RTLView, RTLText } from "react-native-rtl-layout";

class SearchBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFound: false,
      locale: "",
      language: "",
      isSearched: false,
      searchData: [],
      autoSearchData1: [],
      autoFillResult: [],
      query: "",
      show: 1,
      noAutoFill: 2,
      count: 1,
    };
    this._loadHistoryFromAsyncStorage();
  }


  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.setState({ query: "", autoFillResult: [], show: 1, count: 1 })
      this.searchBar.focus();
    });

    this.props.navigation.addListener("willFocus", () => {
      this.setState({ query: "", autoFillResult: [], show: 1, isSearched: false })
      this._findSearchData(this.state.query);

    });
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  _saveHistoryIntoAsyncStorage(searchData) {
    AsyncStorage.setItem("@searchdata", JSON.stringify({ searchData }));
  }

  _loadHistoryFromAsyncStorage() {
    AsyncStorage.getItem("@searchdata")
      .then((results) => {
        const { searchData } = JSON.parse(results);
        this.setState({ searchData });
      })
      .catch((err) => {
        this.setState({ searchData: [] });
      });
  }

  _removeSeachHistoryItem(value) {
    const searchData = this.state.searchData.filter((item) => item !== value);
    this.setState({ searchData }, () =>
      this._saveHistoryIntoAsyncStorage(searchData)
    );
  }

  _findSearchData(query) {
    const { searchData } = this.state;
    if (query === "") {
      return searchData;
    }
    const regex = new RegExp(`${query.trim()}`, "i");
    return searchData.filter((item) => item.search(regex) >= 0);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  slectAutoSearchItem = (key) => {
    let count1 = this.state.count;
    count1 = count1 + 1;
    this.setState({ count: count1 })
    let autoFillArray = this.state.autoFillResult;
    let merchatname = this.state.locale == 'eng' ? autoFillArray[key].companyNameEn : autoFillArray[key].companyNameAr;
    this.setState({ query: merchatname })
    setTimeout(() => {
      this.setState({ query: merchatname, autoFillResult: [], show: 2 })
      this.findPrevSearch(merchatname);
      this.props.fetchFromApi(merchatname, "QUERY")
    }, 800);
  }


  autoSearchItem = (query1) => {
    let query2 = query1.trim();
    let querytrim = query1.trim();
    if (querytrim.length > 0) {
      if (query1 && query1.length > 3) {
        this.setState({ show: 2 })
        apiClient
          .postRequest("/search-merchants", {
            productName: query1,
            locale: this.state.locale === "eng" ? "en" : "ar",
          })
          .then((res) => {
            if (res.length >= 1) {
              if (this.state.noAutoFill == 1) {
                this.setState({ autoFillResult: res })
              }
            }
          })
          .catch((err) => { });
      }
      else {
        this.setState({ autoFillResult: [] })
      }
    }
  }

  findPrevSearch = (searchdata) => {
    if (this.state.searchData.length >= 1) {
      let newsearchData = this.state.searchData;
      for (let i = 0; i < newsearchData.length; i++) {
        if (newsearchData[i] == searchdata) {
          newsearchData.splice(i, 1);
        }
      }
      newsearchData.reverse();
      newsearchData.push(searchdata);
      this.setState({ searchData: newsearchData }, () =>
        this._saveHistoryIntoAsyncStorage(newsearchData)
      );
    }
    else {
      let newsearchData = [];
      newsearchData.push(searchdata);
      this.setState({ searchData: newsearchData }, () =>
        this._saveHistoryIntoAsyncStorage(newsearchData)
      );
    }
    this.state.searchData.reverse();
  }

  _searchItem() {
    Keyboard.dismiss();
    let querytrim = this.state.query.trim();
    if (querytrim.length > 0) {
      if (this.state.query) {
        this.setState({ autoFillResult: [], autoSearchData1: [], noAutoFill: 2 })
        const { fetchFromApi } = this.props;
        const { query } = this.state;
        const searchData = this._findSearchData(query);
        this.findPrevSearch(this.state.query);
        this.setState({ isSearched: true }, () => fetchFromApi(query, "QUERY"));
      }
    }
  }
  searchFromPrev = (item) => {
    let searchdata = this.state.searchData;
    for (let i = 0; i < searchdata.length; i++) {
      if (searchdata[i] == item) {
        searchdata.splice(i, 1);
      }
    }
    this.setState({ query: item, autoFillResult: [], show: 2 })
    this.findPrevSearch(item);
    this.props.fetchFromApi(item, "QUERY")
  }

  searchTextField = (query1) => {
    this.setState({ count: 1 })
    if (query1 == this.state.query) {
    }
    else {
      this.setState({ query: query1, isSearched: false, show: 1, noAutoFill: 1 })
      this.props.fetchFromApi(),
        this.autoSearchItem(query1)
    }
  }

  render() {

    const { query } = this.state;

    return (
      <View style={styles.mainSection}>
        <View
          style={
            this.state.locale == "eng"
              ? styles.searchInputHead
              : styles.searchInputHead_ar
          }
        >
          <View style={
            this.state.locale == "eng"
              ? styles.searchClose
              : styles.searchClose_ar
          }>
            <TextInput
              textAlign={this.state.locale === "eng" ? "left" : "right"}
              ref={(input) => (this.searchBar = input)}
              value={this.state.query}
              onSubmitEditing={() => this._searchItem()}
              returnKeyType="search"
              onChangeText={(query) =>
                this.searchTextField(query)
              }
              underlineColorAndroid="transparent"
              style={
                this.state.locale == "eng"
                  ? styles.searchInput
                  : styles.searchInput_ar
              }
            />
            {this.state.query.length > 0 ?
              <TouchableOpacity style={{width: 20, height: 20, justifyContent: 'center', alignItems: 'center'}} onPress={() => this.setState({ query: '', autoFillResult: [] })}
              >
                <Image source={SearchIcon.searchClose} style={{width: 12, height: 12}}/>
              </TouchableOpacity>
              :
              null}
          </View>
          <View
            style={
              this.state.locale == "eng"
                ? styles.searchButton_en
                : styles.searchButton_ar
            }
          >
            <TouchableOpacity onPress={() => this._searchItem()}
            >
              <Image
                style={{
                  width: 17.5,
                  height: 17.5,
                }}
                source={SearchIcon.search}
              />
            </TouchableOpacity>
          </View>
        </View>
        {this.state.count < 3 ? (
          <>
            {(this.state.autoFillResult && this.state.autoFillResult.length) ?
              (
                <View style={styles.viewSearchHistory}>
                  <View>
                  </View>
                  <FlatList
                    data={this.state.autoFillResult}
                    renderItem={({ item, index }) => {
                      return (
                        <View key={index} style={styles.viewItemSearchHistory2}>
                          {this.state.locale === "eng" && (
                            <>
                              <TouchableOpacity
                                style={{
                                  width: '100%',
                                }}
                                activeOpacity={0.7}
                                onPress={() => { this.slectAutoSearchItem(index) }
                                }
                              >
                                <Text style={styles.itemText1}>{this.state.locale == 'eng' ? item.companyNameEn : item.companyNameAr}</Text>
                              </TouchableOpacity>
                            </>
                          )}
                          {this.state.locale === "ar" && (
                            <>
                              <TouchableOpacity
                                style={{
                                  width: '100%',
                                }}
                                activeOpacity={0.7}
                                onPress={() => { this.slectAutoSearchItem(index) }
                                }
                              >
                                <Text style={styles.itemText2}>{item.companyNameAr}</Text>
                              </TouchableOpacity>
                            </>
                          )}
                        </View>
                      );
                    }}
                    showsVerticalScrollIndicator={false}
                  />
                </View>
              ) :
              (
                <View style={styles.viewSearchHistory}>
                  {this.state.searchData.length > 0 && !this.state.isSearched && this.state.show == 1 ? (
                    <View>
                      <View>
                        <RTLView locale={this.state.locale}>
                          <RTLText
                            style={[
                              styles.itemText,
                              {
                                fontSize: 16,
                                fontFamily: "Tajawal-Bold",
                                size: 14,
                                paddingTop: 20,
                                paddingBottom: 4
                              },
                            ]}
                          >
                            {I18n.t("Previous Searches")}
                          </RTLText>
                        </RTLView>
                      </View>
                      <FlatList
                        data={this.state.searchData}
                        renderItem={({ item, index }) => {
                          return (
                            <View key={index} style={styles.viewItemSearchHistory}>
                              {this.state.locale === "eng" && (
                                <>
                                  <TouchableOpacity
                                    style={{
                                      flexDirection: "row",
                                      alignItems: "center",
                                    }}
                                    activeOpacity={0.7}
                                    onPress={() =>
                                      this.setState({ query: item }, () =>
                                        this.searchFromPrev(item)
                                      )
                                    }
                                  >
                                    <Image source={SearchIcon.searchSm} style={{ marginRight: 7 }} />
                                    <Text style={styles.itemText1}>{item}</Text>
                                  </TouchableOpacity>
                                  <TouchableOpacity
                                    activeOpacity={0.7}
                                    style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() => this._removeSeachHistoryItem(item)}
                                  >
                                    <Icon name="md-close" size={20} color="#CCCCCC" />
                                  </TouchableOpacity>
                                </>
                              )}
                              {this.state.locale === "ar" && (
                                <>
                                  <TouchableOpacity
                                    activeOpacity={0.7}
                                    style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() => this._removeSeachHistoryItem(item)}
                                  >
                                    <Icon name="md-close" size={20} color="#1b1b1b" />
                                  </TouchableOpacity>
                                  <TouchableOpacity
                                    style={{
                                      flexDirection: "row-reverse",
                                      alignItems: "center",
                                      paddingVertical: 5
                                    }}
                                    activeOpacity={0.7}
                                    onPress={() =>
                                      this.setState({ query: item }, () =>
                                        this._searchItem()
                                      )
                                    }
                                  >
                                    <Image source={SearchIcon.searchSm} style={{ marginLeft: 7 }} />
                                    <Text style={styles.itemText}>{item}</Text>
                                  </TouchableOpacity>
                                </>
                              )}
                            </View>
                          );
                        }}
                        showsVerticalScrollIndicator={false}
                      />
                    </View>
                  ) : null}
                </View>
              )
            }
          </>
        ) : null}

      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.Language,
});
export default connect(
  mapStateToProps,
  {
    setSelectedLanguage,
  }
)(withNavigation(SearchBody));