import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { othersImage } from "../../../config/imageConst";
import { connect } from "react-redux";
import HeaderArrow from "../Header/Header_logo_name/HeaderArrow";
import network from './check';

class Internet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: ""
    };
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }

  backValueReceive = value => {
    if (value == "back") {
    }
  }

  checkNetConnect = () => {
    var net = network();
    this.props.netConnectReload(net);
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={value => this.backValueReceive(value)}
        />
        <View style={styles.container}>
          <View>
            <Text style={styles.headText}>
              {I18n.t("Something went wrong!")}
            </Text>
            <Image
              source={require("../../assets/image/wifi.png")}
              style={styles.marginAuto}
            />
            <Text style={commonStyle.emptymsg}>
              {I18n.t("There seems to be an error connecting to the internet")}
            </Text>
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity onPress={this.checkNetConnect}
              style={commonStyle.blackBtn}>
              <Text style={commonStyle.blackBtnText}>
                {I18n.t("Try to reconnect again")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(Internet);
