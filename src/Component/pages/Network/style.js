const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    mainSection: {
        flex: 1,
        backgroundColor: '#FAFAFA',
    },
    container: {
        flex: 1,
        paddingHorizontal: '4%',
        justifyContent: "space-between",
    },
    headText: {
        marginVertical: 15,
        fontFamily: 'Tajawal-Bold',
        color: '#1A1A1A',
        fontSize: 20,
    },
    marginAuto: {
        marginTop: 20,
        marginLeft: "auto",
        marginRight: "auto",
      },
      bottom: {
          paddingVertical: 15
      }
}