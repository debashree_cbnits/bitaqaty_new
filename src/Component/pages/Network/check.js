import NetInfo from "@react-native-community/netinfo";

const network = () => {
  var net;

  NetInfo.addEventListener(state => {
    if (state.isConnected == true) {
      net = true;
    } else {
      net = false;
    }
  });

  return net;
};

export default network;
