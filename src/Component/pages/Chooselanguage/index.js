import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import styles from "./style.js";
import { SafeAreaView } from "react-native-safe-area-context";
import { TouchableOpacity } from "react-native-gesture-handler";
import { RTLView, RTLText } from "react-native-rtl-layout";
import I18n from "../../../i18n/index";
import { setSelectedLanguage } from "../../../redux/language/action";
import { connect } from "react-redux";

class ChooseLanguage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "english",
      locale: "eng"
    };
  }
  _setLang = () => {
    const { language, locale } = this.state;
    AsyncStorage.setItem("language", JSON.stringify({ language, locale }))
      .then(() => {
        this.props.navigation.navigate(
          locale === "eng" ? "MainStackEn" : "MainStackAr"
        );
      })
      .catch(err => {
      });
  };
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }

  setSelectLanguage = language => {
    if (language == "english") {
      this.setState({ language: "english", locale: "eng" }, () =>
        this.props.setSelectedLanguage({ language: "english", locale: "eng" })
      );
      I18n.locale = "eng";
    } else {
      this.setState({ language: "arabic", locale: "ar" }, () =>
        this.props.setSelectedLanguage({ language: "arabic", locale: "ar" })
      );
      I18n.locale = "ar";
    }
  };

  render() {

    return (
      <View style={styles.mainSection}>
        <View style={styles.container}>
          <View style={{ width: "100%" }}>
            <View style={styles.top}>
              <Image
                style={{ height: 30, width: 24.5 }}
                source={require("../../assets/image/logo.png")}
              />
            </View>
            <View style={{ marginTop: "8%" }}>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text
                    style={[
                      styles.screenText,
                      { fontSize: 14, fontStyle: "normal" }
                    ]}
                  >
                    {I18n.t("Please Choose Language")}
                  </Text>
                </RTLView>
              </View>
              <View style={styles.langBtn}>
                <View style={{ width: "47%" }}>
                  <TouchableOpacity
                    onPress={language => this.setSelectLanguage("english")}
                    style={
                      this.state.language && this.state.language == "english"
                        ? styles.blackBtn
                        : styles.whiteBtn
                    }
                  >
                    <Text
                      style={
                        this.state.language && this.state.language == "english"
                          ? styles.blackBtnText
                          : styles.whiteBtnText
                      }
                    >
                      English
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ width: "47%" }}>
                  <TouchableOpacity
                    onPress={language => this.setSelectLanguage("arabic")}
                    style={
                      this.state.language && this.state.language == "arabic"
                        ? styles.blackBtn
                        : styles.whiteBtn
                    }
                  >
                    <Text
                      style={
                        this.state.language && this.state.language == "arabic"
                          ? styles.blackBtnText
                          : styles.whiteBtnText
                      }
                    >
                      العربية
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.selectCountry}>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={[styles.screenText, { fontSize: 14 }]}>
                    {I18n.t("Bitaqaty Service available in")}
                  </Text>
                </RTLView>
              </View>
              <View
                style={
                  this.state.locale == "eng"
                    ? styles.greyBtn_en
                    : styles.greyBtn_ar
                }
              >
                <Image
                  style={{ height: 26, width: 38, borderRadius: 4 }}
                  source={require("../../assets/image/flag2.png")}
                />
                <Text
                  style={[styles.rightsGreyText, { fontSize: 14, margin: 8 }]}
                >
                  {I18n.t("Saudi Arabia")}
                </Text>
              </View>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={[styles.screenText, { fontSize: 12 }]}>
                    {I18n.t("More countries will be available soon")}
                  </Text>
                </RTLView>
              </View>
            </View>
          </View>

          <View
            style={styles.bottomSec}
          >
            <TouchableOpacity
              onPress={this._setLang}
              // onPress={() => this.props.navigation.navigate("SignUp")}

              style={styles.nextBtn}
            >
              <Text style={styles.blackBtnText}> {I18n.t("Continue")} </Text>
            </TouchableOpacity>

            <View style={styles.rights}>
              <RTLView locale={this.state.locale}>
                <Text style={[styles.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}{" "}
                </Text>
                <Text
                  style={[
                    styles.screenText,
                    { fontSize: 12, fontWeight: "bold" }
                  ]}
                >
                  {I18n.t(" Bitaqaty ")}{" "}
                </Text>
                <Text style={[styles.screenText, { fontSize: 12 }]}>
                  {I18n.t("@2019")}
                </Text>
              </RTLView>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  ...state.Language
});
export default connect(mapStateToProps, {
  setSelectedLanguage
})(ChooseLanguage);
