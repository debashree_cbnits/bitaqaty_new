const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
    width: "84%",
    justifyContent: "space-between",
  },
  top: {
    marginTop: "14.5%",
    justifyContent: "center",
    alignItems: "center",
  },
  screenText: {
    marginBottom: 15,
    color: "#4F4F4F",
    fontFamily: "Tajawal-Light",
  },

  langBtn: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    // backgroundColor: "blue"
  },
  blackBtn: {
    width: "100%",
    height: 50,
    backgroundColor: "#241125",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  whiteBtn: {
    width: "100%",
    height: 50,
    borderColor: "#241125",
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  blackBtnText: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
  },
  whiteBtnText: {
    color: "#1A1A1A",
    fontWeight: "bold",
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
  },
  selectCountry: {
    width: "100%",
    marginTop: "20%",
  },
  greyBtn_en: {
    paddingHorizontal: "3%",
    width: "100%",
    height: 50,
    backgroundColor: "#F2F0F0",
    borderRadius: 5,
    alignItems: "center",
    marginBottom: 10,
    flexDirection: "row",
  },
  greyBtn_ar: {
    paddingHorizontal: "3%",
    width: "100%",
    height: 50,
    backgroundColor: "#F2F0F0",
    borderRadius: 5,
    alignItems: "center",
    marginBottom: 10,
    //flexDirection: 'row',
    flexDirection: "row-reverse",
  },
  nextBtn: {
    width: "100%",
    height: 50,
    marginBottom: 15,
    backgroundColor: "#241125",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  rights: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  rightsGreyText: {
    fontFamily: "Tajawal-Light",
    color: "#4F4F4F",
  },
  text1: {
    color: "grey",
    fontSize: 30,
    fontWeight: "bold",
  },
  bottomSec: {
    width: "100%",
    paddingBottom: "5%",
  },
};
