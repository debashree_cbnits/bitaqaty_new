import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  Alert
} from "react-native";
import { RTLView, RTLText } from "react-native-rtl-layout";
import I18n from "../../../i18n/index";
import { ScrollView } from "react-native-gesture-handler";
import Tab from "./Tab/Tab";
import MasonryList from "react-native-masonry-list";
import { HomeImagePath, productImagePath } from "../../../config/imageConst";
import commonStyles from "../../../../commonStyle.js";
import styles from "./style.js";
import { setSelectedLanguage } from "../../../redux/language/action";
import { connect } from "react-redux";
import { SafeAreaView } from "react-native-safe-area-context";
import LinearGradient from "react-native-linear-gradient";
import apiClient from "../../../services/api.client";
import { ActivityIndicator } from "react-native-paper";
import { SearchIcon } from "../../../config/imageConst";
import Toast from "react-native-tiny-toast";
import Carousel, { Pagination } from "react-native-snap-carousel";
import imageUrl from '../../../services/config';
import network from "../Network/check";
import Internet from "../Network/Internet";

const { height, width } = Dimensions.get("window");


const baseUri = "https://stagewrapper.ocstaging.net/bitaqatywrapper";

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "HomeScreen Screen",
      slider1: [
        {
          id : 1,
          image: 'https://www.bitaqaty.com/assets/images/heroImages/Hero Image-02.png',
        },
        {
          id : 2,
          image: 'https://www.bitaqaty.com/assets/images/heroImages/Hero Image-04.jpg',
        },
        {
          id : 3,
          image : 'https://www.bitaqaty.com/assets/images/heroImages/Hero Image-06.jpg',
        }
      ],
      slider2: [
        {
          id : 1,
          image: 'https://www.bitaqaty.com/assets/images/heroImages/Hero%20Image-01.png',
          title: "آنثيم",
          text: "حمل الآن و استمتع بالعرض",
          slide : 2
        },
        {
          id : 3,
          image: 'https://www.bitaqaty.com/assets/images/heroImages/Hero Image-05.jpg',
          title: "آنثيم",
          text: "حمل الآن و استمتع بالعرض",
          slide : 1
        },
        {
          id : 2,
         image: 'https://www.bitaqaty.com/assets/images/heroImages/Hero Image-03.jpg',
          title: "آنثيم",
          text: "حمل الآن و استمتع بالعرض" ,
          slide : 0
        }
      ],
      locale: "",
      language: "",
      isLoading: true,
      MasonaryImageSizes: [
        {
          width: 50,
          height: 26
        },
        {
          width: 50,
          height: 46
        },
        {
          width: 50,
          height: 46
        },
        {
          width: 50,
          height: 26
        },
        {
          width: 50,
          height: 26
        },
        {
          width: 50,
          height: 26
        }
      ],
      MasonaryImageSizes1: [
        {
          width: 60,
          height: 30
        },
      ],
      MasonaryImageSizes2: [
        {
          width: 50,
          height: 46
        },
        {
          width: 50,
          height: 46
        },
        {
          width: 50,
          height: 26
        },
        {
          width: 50,
          height: 26
        },
        {
          width: 50,
          height: 26
        },
        {
          width: 50,
          height: 26
        }
      ],
      MarchantList: null,
      MarchantList1: null,
      MarchantList2: null,
      MarchantList3: null,
      MarchantList4: null,
      MarchantList5: null,
      activeSlide: 0,
      checkNetworkConnection: "",
      activeSlide: 0,
      activeSlide1: 2,
    };
    this._fetchMasonaryFromApi();
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", () => {
      const net = network();
      this.setState({ checkNetworkConnection: net });
      this._fetchMasonaryFromApi();
    });
    const net = network();

    if (net == undefined) {
      this.setState({ checkNetworkConnection: true });
    } else {
      this.setState({ checkNetworkConnection: net });
    }
  }

  netConnectReload = value => {
    this._fetchMasonaryFromApi();
    this.setState({ checkNetworkConnection: value });
  };

  _fetchMasonaryFromApi() {
    apiClient
      .getRequest("/mobile-top-brands")
      .then(res => {
        var list = res;

        if (list && list.length) {
          if (list.length <= 6) {
            var list1 = res;
            this.setState({
              MarchantList1: list1,
              isLoading: false
            });
          }
          else {
            if (list.length > 7) {
              if (list.length > 13) {
                if (list.length == 14) {
                  var list = res;
                  var list1 = list.slice(0, 6);
                  var list2 = list.slice(6, 7);
                  var list3 = list.slice(7, 13);
                  var list4 = list.slice(13, 14);
                  this.setState({
                    MarchantList1: list1,
                    MarchantList2: list2,
                    MarchantList3: list3,
                    MarchantList4: list4,
                    isLoading: false
                  });
                }
                else {
                  var list = res;
                  var list1 = list.slice(0, 6);
                  var list2 = list.slice(6, 7);
                  var list3 = list.slice(7, 13);
                  var list4 = list.slice(13, 14);
                  var list5 = list.slice(14, length);
                  var length = list.length;
                  this.setState({
                    MarchantList1: list1,
                    MarchantList2: list2,
                    MarchantList3: list3,
                    MarchantList4: list4,
                    MarchantList5: list5,
                    isLoading: false
                  });
                }
              }
              else {
                var list = res;
                var length = list.length;
                var list1 = list.slice(0, 6);
                var list2 = list.slice(6, 7);
                var list3 = list.slice(7, length);

                this.setState({
                  MarchantList1: list1,
                  MarchantList2: list2,
                  MarchantList3: list3,
                  isLoading: false
                });
              }
            }
            else {

              var list = res;
              var list1 = list.slice(0, 6);
              var list2 = list.slice(6, 7);
              this.setState({
                MarchantList1: list1,
                MarchantList2: list2,
                isLoading: false
              });
            }
          }
        }
      })
      .catch(err => {
        this.setState({ isLoading: false });
      });
  }

  goToDetails = (merchantInfo, categoryId, isCategory, categoryInfo) => {
    const net = network();
    this.setState({ checkNetworkConnection: net });

    if (!isCategory) {
      if (categoryId && merchantInfo) {
        this.setState({ isLoading: true })
        apiClient
          .postRequest("/merchant/Products", {
            merchantId: merchantInfo.id,
            categoryId: categoryId
          })
          .then(productApiResponse => {
            this.setState({ isLoading: false })
            this.props.navigation.navigate("CatagoryView", {
              merchantInfo,
              categoryId,
              productApiResponse,
              back: "Home"
            });
          })
          .catch(err => {
            this.setState({ isLoading: false })
            Toast.show(I18n.t("No data found in this Category"));
          });
      }
    } else if (isCategory) {
      if (categoryInfo) {
        this.props.navigation.navigate("Merchants", { item: categoryInfo, fromWhere: '' });
      } else {
        Toast.show(I18n.t("No data found in this Category"));
      }
    } else {
      Toast.show(I18n.t("No data found in this Category"));
    }
  };

  get pagination() {
    const { slider1, slider2, activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={
          this.state.locale == "eng" ? slider1.length : slider2.length
        }
        activeDotIndex={activeSlide}
        containerStyle={{
          backgroundColor: "#FFFFFF",
          width: "100%",
          paddingTop: 12,
          paddingBottom: 4
        }}
        dotStyle={{
          width: 6.5,
          height: 6.5,
          borderRadius: 5,
          // marginHorizontal: 8,
          backgroundColor: "#000000"
        }}
        inactiveDotStyle={{
          backgroundColor: "#CCCCCC"
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  get pagination1() {
    const { slider1, slider2, activeSlide1 } = this.state;
    return (
      <Pagination
        dotsLength={
          this.state.locale == "eng" ? slider1.length : slider2.length
        }
        activeDotIndex={activeSlide1}
        containerStyle={{
          backgroundColor: "#FFFFFF",
          width: "100%",
          paddingTop: 12,
          paddingBottom: 4
        }}
        dotStyle={{
          width: 6.5,
          height: 6.5,
          borderRadius: 5,
          // marginHorizontal: 8,
          backgroundColor: "#000000"
        }}
        inactiveDotStyle={{
          backgroundColor: "#CCCCCC"
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }
  indexset = (index) => {
    if(index == 2){
      this.setState({activeSlide1 : 1})
    }
    else if(index == 1){
      this.setState({activeSlide1 : 0})
    }
    else if(index == 0){
      this.setState({activeSlide1 : 2})
    }
  }
  NewCardtab = () => {
    if (this.state.locale == "eng") {
      this.props.navigation.navigate("Tab", { index: 0 });
    } else {
      this.props.navigation.navigate("Tab", { index: 2 });
    }
  };

  BestSellingtab = () => {
    this.props.navigation.navigate("Tab", { index: 1 });
  };

  Offertab = () => {
    if (this.state.locale == "eng") {
      this.props.navigation.navigate("Tab", { index: 2 });
    } else {
      this.props.navigation.navigate("Tab", { index: 0 });
    }
  };

  redirectbanner1 = (id) => {    
    if(id == 1){
      this.Offertab();
    }
    else if(id == 2){
      apiClient
        .postRequest("/merchants-full-data", {
          categoryId: 40
        })
        .then(res => {
          this.props.navigation.navigate("Merchants", { item: res.categoryModel , fromWhere: ''});
        })
        .catch(err => {
          console.log("Merchant List (API error) ==> ", err);
        });
    }
    else if(id == 3){
      apiClient
        .postRequest("/merchants-full-data", {
          categoryId: 49
        })
        .then(res => {
          this.props.navigation.navigate("Merchants", { item: res.categoryModel , fromWhere: ''});
        })
        .catch(err => {
          console.log("Merchant List (API error) ==> ", err);
        });
    }
  }

  render() {
    var net = network();
    return (
      <SafeAreaView style={commonStyles.flexOne}>
        {net == false ? (
          <Internet netConnectReload={value => this.netConnectReload(value)} />
        ) : (
            <View style={commonStyles.mainBody}>
              <View style={styles.topArea}>
                {this.state.locale == "eng" ? (
                  <Image source={HomeImagePath.logoEn} />
                ) : (
                    <Image source={HomeImagePath.logoar} />
                  )}

                {/* #####  demo seach ##### */}
                <TouchableOpacity
                  style={
                    this.state.locale == "eng"
                      ? styles.searchInputHead
                      : styles.searchInputHead_ar
                  }
                  onPress={() =>
                    this.props.navigation.navigate("Search", { focused: true })
                  }
                >
                  {/* <Search pageType="SearchFromHome" /> */}
                  <View style={styles.searchInput}>
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? styles.searchText
                          : styles.searchText_ar
                      }
                    >
                      {I18n.t("Search")}
                    </Text>
                  </View>
                  <View style={styles.srcIconHead}>
                    <Image style={styles.searchIcon} source={SearchIcon.search} />
                  </View>
                </TouchableOpacity>
              </View>              
              <View style={styles.slider}>
                {this.state.locale == 'eng' ?
                  (
                    <View style={styles.sliderWrap}>
                      <Carousel
                        layout={"default"}
                        ref={ref => (this.refCarousel = ref)}
                        data={this.state.slider1}
                        inactiveSlideScale={1}
                        inactiveSlideOpacity={0.6}
                        slideStyle={styles.sliderShift}                        
                        renderItem={({ item, index }) => {
                          return (
                            <TouchableOpacity style={{flex: 1}}  onPress={() => {
                              this.redirectbanner1(item.id);
                            }}>
                            <ImageBackground
                              key={index}
                              //source={item.image}
                              source={{ uri: item.image }}
                              style={styles.imageSlider}
                              imageStyle={{ borderRadius: 12 }}
                            >
                              <LinearGradient
                                start={{ x: 0.1, y: 0.6 }}
                                end={{ x: 0.1, y: 0 }}
                                colors={["rgba(0,0,0,0.7)", "#54545400"]}
                                style={{ borderRadius: 12 }}
                              >
                              </LinearGradient>
                            </ImageBackground>
                            </TouchableOpacity>
                          );
                        }}
                        onSnapToItem={index => this.setState({ activeSlide: index })}
                        enableSnap={true}
                        loop={true}
                        sliderWidth={width}
                        itemWidth={width - 22}
                        activeSlideAlignment={"center"}
                      />
                      {this.pagination}
                    </View>
                  ) : (
                    <View style={styles.sliderWrap}>
                      <Carousel
                        layout={"default"}
                        ref={ref => (this.refCarousel = ref)}
                        data={this.state.slider2}
                        inactiveSlideScale={1}
                        inactiveSlideOpacity={0.6}
                        slideStyle={styles.sliderShift}
                        renderItem={({ item, index }) => {
                          return (
                            <TouchableOpacity style={{flex: 1}}  onPress={() => {
                              this.redirectbanner1(item.id);
                            }}>
                            <ImageBackground
                              key={index}
                              source={{ uri: item.image }}
                              style={styles.imageSlider}
                              imageStyle={{ borderRadius: 12 }}
                            >
                              <LinearGradient
                                start={{ x: 0.1, y: 0.6 }}
                                end={{ x: 0.1, y: 0 }}
                                colors={["rgba(0,0,0,0.7)", "#54545400"]}
                                style={{ borderRadius: 12 }}
                              >
                              </LinearGradient>
                            </ImageBackground>
                            </TouchableOpacity>
                          );
                        }}
                        onSnapToItem={index => this.indexset(index)}
                        enableSnap={true}
                        loop={true}
                        sliderWidth={width}
                        itemWidth={width - 22}
                        activeSlideAlignment={"center"}
                      />
                      {this.pagination1}
                    </View>
                  )}
              </View>

              <View style={styles.tabArea}>
                <RTLView locale={this.state.locale}>
                  <View style={styles.tabBox}>
                    <TouchableOpacity
                      onPress={this.NewCardtab}
                      style={styles.tabTouchArea}
                    >
                      <Image source={HomeImagePath.newCard} />
                      <Text style={styles.tabText}>{I18n.t("New Cards")}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.tabBox}>
                    <TouchableOpacity
                      onPress={this.BestSellingtab}
                      style={styles.tabTouchArea}
                    >
                      <Image source={HomeImagePath.starImg} />
                      <Text style={styles.tabText}>{I18n.t("Best Selling")}</Text>
                    </TouchableOpacity>
                  </View>

                  <View style={styles.tabBox}>
                    <TouchableOpacity
                      onPress={this.Offertab}
                      style={styles.tabTouchArea}
                    >
                      <Image source={HomeImagePath.tagImg} />
                      <Text style={styles.tabText}>
                        {" "}
                        {I18n.t("Offers Of The Day")}{" "}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </RTLView>
              </View>
              {this.state.isLoading && <ActivityIndicator style={styles.activityIndicator}
                size="small"
                color="#1A1A1A" />}
              <ScrollView>                
                <View style={styles.wrap}>
                  {!this.state.isLoading && this.state.MarchantList1 && (
                    <MasonryList
                      onPressImage={({ merchantInfo, categoryId, isCategory, categoryInfo
                      }) => 
                        this.goToDetails(merchantInfo, categoryId, isCategory, categoryInfo)
                      }
                      columns={2}
                      spacing={3}
                      imageContainerStyle={{resizeMode: "cover", backgroundColor:'transparent' }}
                      images={this.state.MarchantList1.map((merchantInfo, i) => {                        
                        return {
                          merchantInfo: merchantInfo.localMerchant,
                          categoryInfo: merchantInfo.category,
                          isCategory: merchantInfo.isCategory,
                          categoryId: merchantInfo.category.id,
                          ...this.state.MasonaryImageSizes[i],
                          source: { uri: imageUrl.productImageUrl + merchantInfo.largeLogoPath }
                        };
                      })}
                    />
                  )}
                  {!this.state.isLoading && this.state.MarchantList2 && (
                    <MasonryList
                      onPressImage={({ merchantInfo, categoryId, isCategory, categoryInfo }) =>
                        this.goToDetails(merchantInfo, categoryId, isCategory, categoryInfo)
                      }
                      columns={1}
                      spacing={3}
                      imageContainerStyle={{resizeMode: "contain",backgroundColor:'transparent'  }}
                      images={this.state.MarchantList2.map((merchantInfo, i) => {
                        return {
                          merchantInfo: merchantInfo.localMerchant,
                          categoryInfo: merchantInfo.category,
                          isCategory: merchantInfo.isCategory,
                          categoryId: merchantInfo.category.id,
                          ...this.state.MasonaryImageSizes1[i],
                          source: { uri: imageUrl.productImageUrl + merchantInfo.largeLogoPath }
                          //source: productImagePath.productImg3
                        };
                      })}
                    />
                  )}
                  {!this.state.isLoading && this.state.MarchantList3 && (
                    <MasonryList
                      onPressImage={({ merchantInfo, categoryId, isCategory, categoryInfo }) =>
                        this.goToDetails(merchantInfo, categoryId, isCategory, categoryInfo)
                      }
                      columns={2}
                      spacing={3}
                      imageContainerStyle={{resizeMode: "cover", backgroundColor:'transparent' }}
                      images={this.state.MarchantList3.map((merchantInfo, i) => {
                        return {
                          merchantInfo: merchantInfo.localMerchant,
                          categoryInfo: merchantInfo.category,
                          isCategory: merchantInfo.isCategory,
                          categoryId: merchantInfo.category.id,
                          ...this.state.MasonaryImageSizes2[i],
                          source: { uri: imageUrl.productImageUrl + merchantInfo.largeLogoPath }
                          //source: productImagePath.productImg3
                        };
                      })}
                    />
                  )}
                  {!this.state.isLoading && this.state.MarchantList4 && (
                    <MasonryList
                      onPressImage={({ merchantInfo, categoryId, isCategory, categoryInfo }) =>
                        this.goToDetails(merchantInfo, categoryId, isCategory, categoryInfo)
                      }
                      columns={1}
                      spacing={3}
                      imageContainerStyle={{ resizeMode: "contain", backgroundColor:'transparent'  }}
                      images={this.state.MarchantList4.map((merchantInfo, i) => {
                        return {
                          merchantInfo: merchantInfo.localMerchant,
                          categoryInfo: merchantInfo.category,
                          isCategory: merchantInfo.isCategory,
                          categoryId: merchantInfo.category.id,
                          ...this.state.MasonaryImageSizes1[i],
                          source: { uri: imageUrl.productImageUrl + merchantInfo.largeLogoPath }
                          //source: productImagePath.productImg3
                        };
                      })}
                    />
                  )}
                  {!this.state.isLoading && this.state.MarchantList5 && (
                    <MasonryList
                      onPressImage={({ merchantInfo, categoryId, isCategory, categoryInfo }) =>
                        this.goToDetails(merchantInfo, categoryId, isCategory, categoryInfo)
                      }
                      columns={2}
                      spacing={3}
                      imageContainerStyle={{resizeMode: "contain" , backgroundColor:'transparent' }}
                      images={this.state.MarchantList5.map((merchantInfo, i) => {
                        return {
                          merchantInfo: merchantInfo.localMerchant,
                          categoryInfo: merchantInfo.category,
                          isCategory: merchantInfo.isCategory,
                          categoryId: merchantInfo.category.id,
                          ...this.state.MasonaryImageSizes[i],
                          source: { uri: imageUrl.productImageUrl + merchantInfo.largeLogoPath }
                          //source: productImagePath.productImg3
                        };
                      })}
                    />
                  )}
                </View>
              </ScrollView>
            </View>
          )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {
  setSelectedLanguage
})(HomeScreen);
