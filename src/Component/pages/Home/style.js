const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  wrap: {
    marginBottom: 100,
  },
  tabWrap: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-around",
  },
  borderSelect: {
    borderBottomWidth: 4,
    borderBottomColor: "#000000",
    borderStyle: "solid",
    width: "33.33%",
    justifyContent: "center",
    alignItems: "center",
  },
  borderUnSelect: {
    borderBottomWidth: 4,
    borderBottomColor: "#eeeeee",
    borderStyle: "solid",
    width: "33.33%",
    justifyContent: "center",
    alignItems: "center",
  },
  topArea: {
    alignItems: "center",
    paddingHorizontal: 15,
    height: 116,
  },
  tabArea: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  tabBox: {
    width: "33.33%",
    paddingHorizontal: 5,
  },
  tabTouchArea: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FAFAFA",
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.6,
    shadowRadius: 2,
    elevation: 2,
    borderRadius: 6.5,
    paddingVertical: 5,
  },
  tabText: {
    color: "#241125",
    fontSize: 11,
    fontFamily: "Tajawal-Bold",
    marginTop: 4,
  },
  swiperStyle: {
    borderRadius: 12,
  },
  imageSlider: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-end",
  },
  sliderContent: {
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  sliderTitle: {
    fontFamily: "Tajawal-Bold",
    fontSize: 16,
    color: "#FFFFFF",
  },
  sliderTitle_ar: {
    fontFamily: "Tajawal-Bold",
    fontSize: 16,
    color: "#FFFFFF",
    textAlign: 'right'
  },
  sliderText: {
    fontFamily: "Tajawal-Light",
    fontSize: 12,
    color: "#CCCCCC",
  },
  sliderText_ar: {
    fontFamily: "Tajawal-Light",
    fontSize: 12,
    color: "#CCCCCC",
  },
  searchInputHead: {
    width: "100%",
    height: 42,
    marginTop: 8,
    flexDirection: "row",
    borderColor: "#241125",
    borderWidth: 1,
    borderRadius: 4,
    overflow: "hidden",
  },
  searchInputHead_ar: {
    width: "100%",
    height: 42,
    marginTop: 8,
    flexDirection: "row-reverse",
    borderColor: "#241125",
    borderWidth: 1,
    borderRadius: 4,
    overflow: "hidden",
  },
  searchInput: {
    width: "88%",
    height: 42,
    paddingHorizontal: 10,
    justifyContent: "center",
  },
  searchText: {
    fontFamily: "Tajawal-Light",
    fontSize: 14,
    color: '#4F4F4F'
  },
  searchText_ar: {
    fontFamily: "Tajawal-Light",
    fontSize: 14,
    textAlign: "right",
    color: '#4F4F4F'
  },
  srcIconHead: {
    width: "12%",
    backgroundColor: "#241125",
    justifyContent: "center",
  },
  searchIcon: {
    width: 17.5,
    height: 17.5,
    alignSelf: "center",
  },
  sliderShift: {
    paddingRight: 3.5,
    paddingLeft: 3.5 
  },
  slider: {
    height: 140,
    width: "100%",
  },
  sliderWrap: {
    height: 140,
    width: "100%",
  },
  padd300 : {
    paddingBottom: 320
  },
  padd80 : {
    paddingBottom: 80
  },
  activityIndicator: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
};