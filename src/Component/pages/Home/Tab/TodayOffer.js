import React, { Component } from "react";
import { Text, View, Image, AsyncStorage } from "react-native";
//import styles from "../CatagoryView/ProductBox/ProductStyle.js";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import I18n from "../../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { SafeAreaView } from "react-native-safe-area-context";
import { productImagePath, HomeImagePath } from "../../../../config/imageConst"
import commonStyle from "../../../../../commonStyle.js";
import Product from "../../../partial/ProductBox/Product"
import styles from "./style"
import { connect } from "react-redux";
import apiClient from "../../../../services/api.client";
import { ActivityIndicator } from "react-native-paper";
import { withNavigation } from 'react-navigation';

const baseUri = "https://stagewrapper.ocstaging.net/bitaqatywrapper";
class TodayOffer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Today Offer",
      locale: "",
      language: "",
      productList: [],
      isLoading: true,
    };
    this._getOffersFromApi();
  }
  _getOffersFromApi() {
    apiClient.getRequest('/offers')
      .then(productList => this.setState({ productList, isLoading: false }))
      .catch(err => this.setState({ productList: [], isLoading: false }))
  }

  static getDerivedStateFromProps(props, state) {


    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  goToCartPage = (cart) => {
    this.props.navigation.navigate('Cart')
  }

  goToLoginPage = (login) => {
    this.props.navigation.navigate("Login");
  }

  checknet = net => {
    if (net == false) {
      this.props.navigation.navigate("HomeScreen");
    }
  }

  render() {
    return (
      <View style={styles.innerBodyTab}>


        {this.state.isLoading && <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A" />}
        {!this.state.isLoading && this.state.productList && <ScrollView>

          {
            this.state.productList && this.state.productList.length > 0 ?
              this.state.productList.map((item, key) => {
                return item.discountPercentage > 0
                  ?
                  <Product
                    key={key}
                    item={item}
                    outOfStock={!item.avaiable}
                    productImage={item.backImagePath}
                    productTitle={this.state.locale === "eng" ? item.nameEn : item.nameAr}
                    productStore={this.state.locale === "eng" ? item.merchantNameEn : item.merchantNameAr}
                    before={I18n.t("Before")} productBeforePrice={item.individualPrice}
                    beforeUnit={I18n.t("SAR")}
                    percentage={productImagePath.percent} productPrice={item.individualPriceAfter}
                    productCurrency={I18n.t('SAR')}
                    goToCart={value => this.goToCartPage(value)}
                    goToLogin={value => this.goToLoginPage(value)}
                    netcheck={value => this.checknet(value)}
                    doNotNavigate={true}

                  />
                  :
                  <Product
                    key={key}
                    item={item}
                    outOfStock={!item.avaiable}
                    productImage={item.backImagePath}
                    productTitle={this.state.locale === "eng" ? item.nameEn : item.nameAr}
                    productStore={this.state.locale === "eng" ? item.merchantNameEn : item.merchantNameAr}
                    productPrice={item.individualPrice}
                    productCurrency={I18n.t('SAR')}
                    goToCart={value => this.goToCartPage(value)}
                    goToLogin={value => this.goToLoginPage(value)}
                    netcheck={value => this.checknet(value)}
                    doNotNavigate={true}
                  />

              }) :
              <View style={styles.emptyHead}>
                <Text style={styles.emptyTextHead}>{I18n.t("Offers Of The Day")}</Text>

                <View style={{ width: "100%", alignItems: "center" }}>
                  <Image
                    source={HomeImagePath.offerEmpty}
                    style={{ marginBottom: 10 }}
                  />
                  <Text style={styles.emptyText}>
                    {I18n.t("Offers and discounts are not available at this time")}
                  </Text>
                  <Text style={styles.emptyText}>
                    {I18n.t("Stay tuned for great deals soon")}
                  </Text>
                </View>
              </View>
          }
        </ScrollView>}
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(withNavigation(TodayOffer));