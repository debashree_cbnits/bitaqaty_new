import React from "react";
import { Text, View, Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import I18n from "../../../../i18n/index";
import { productImagePath, HomeImagePath } from "../../../../config/imageConst";
import Product from "../../../partial/ProductBox/Product";
import styles from "./style";
import { connect } from "react-redux";
import apiClient from "../../../../services/api.client";
import { ActivityIndicator } from "react-native-paper";
import { withNavigation } from 'react-navigation';

const baseUri = "https://stagewrapper.ocstaging.net/bitaqatywrapper";
class NewCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "New Cards",
      locale: "",
      language: "",
      productList: [],
      isLoading: true,
    };
    this._getNewCardsFromApi();
  }

  _getNewCardsFromApi() {
    apiClient
      .getRequest("/new-cards")
      .then((productList) =>
        this.setState({ productList, isLoading: false }))
      .catch((err) => this.setState({ productList: [], isLoading: false }));
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  goToCartPage = (cart) => {
    this.props.navigation.navigate('Cart')
  }
  goToLoginPage = (login) => {
    this.props.navigation.navigate("Login");
  }

  checknet = net => {
    if (net == false) {
      this.props.navigation.navigate("HomeScreen");
    }
  }

  render() {
    return (
      <View style={styles.innerBodyTab}>
        {this.state.isLoading && <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A"/>}
        {!this.state.isLoading && this.state.productList && (
          <ScrollView>
            {
              this.state.productList && this.state.productList.length > 0 ?
                this.state.productList.map((item, key) => {
                  return item.discountPercentage > 0 ? (
                    <Product
                      item={item}
                      key={key}
                      outOfStock={!item.avaiable}
                      productImage={item.backImagePath}
                      productTitle={
                        this.state.locale === "eng" ? item.nameEn : item.nameAr
                      }
                      productStore={
                        this.state.locale === "eng"
                          ? item.merchantNameEn
                          : item.merchantNameAr
                      }
                      before={I18n.t("Before")}
                      productBeforePrice={item.individualPrice}
                      beforeUnit={I18n.t("SAR")}
                      percentage={productImagePath.percent}
                      productPrice={item.individualPriceAfter}
                      productCurrency={I18n.t('SAR')}
                      goToCart={value => this.goToCartPage(value)}
                      goToLogin={value => this.goToLoginPage(value)}
                      netcheck={value => this.checknet(value)}
                      navigation={this.props.navigation}
                      doNotNavigate={false}

                    />
                  ) : (
                      <Product
                        key={key}
                        item={item}
                        outOfStock={!item.avaiable}
                        productImage={item.backImagePath}
                        productTitle={
                          this.state.locale === "eng" ? item.nameEn : item.nameAr
                        }
                        productStore={
                          this.state.locale === "eng"
                            ? item.merchantNameEn
                            : item.merchantNameAr
                        }
                        productPrice={item.individualPrice}
                        productCurrency={I18n.t('SAR')}
                        goToCart={value => this.goToCartPage(value)}
                        goToLogin={value => this.goToLoginPage(value)}
                        netcheck={value => this.checknet(value)}
                        navigation={this.props.navigation}
                        doNotNavigate={false}
                      />
                    );
                })
                :
                <View style={styles.emptyHead}>
                  <Text style={styles.emptyTextHead}>{I18n.t("New Cards")}</Text>

                  <View style={{ width: "100%", alignItems: "center" }}>
                    <Image
                      source={HomeImagePath.newCardEmpty}
                      style={{ marginBottom: 10 }}
                    />
                    <Text style={styles.emptyText}>
                      {I18n.t("New cards are coming soon!")}
                    </Text>
                    <Text style={styles.emptyText}>
                      {I18n.t("Please try again later")}
                    </Text>
                  </View>
                </View>
            }
          </ScrollView>
        )}
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(withNavigation(NewCard));
