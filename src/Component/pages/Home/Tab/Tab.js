import React, { Component } from "react";
import { Text, View, Dimensions } from "react-native";
import styles from "./style.js";
import NewCard from "./NewCard";
import TodayOffer from "./TodayOffer";
import BestSelling from "./BestSelling";
import HeaderArrow from "../../Header/Header_logo_name/HeaderArrow";
import I18n from "../../../../i18n/index";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import commonStyle from "../../../../../commonStyle.js";
import { connect } from "react-redux";
import network from "../../Network/check";

class Tab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      locale: "",
      language: "",
      index: this.props.navigation.state.params.index
        ? this.props.navigation.state.params.index
        : 0,
      routes: []
    };
    this.checknet();
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          routes: [
            { key: "first", title: I18n.t("New Cards") },
            { key: "second", title: I18n.t("Best Selling") },
            { key: "third", title: I18n.t("Offers Of The Day") }
          ]
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          routes: [
            { key: "first", title: I18n.t("Offers Of The Day") },
            { key: "second", title: I18n.t("Best Selling") },
            { key: "third", title: I18n.t("New Cards") }
          ]
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        routes: [
          { key: "first", title: I18n.t("New Cards") },
          { key: "second", title: I18n.t("Best Selling") },
          { key: "third", title: I18n.t("Offers Of The Day") }
        ]
      };
    }
  }

  componentDidMount() {
    this.props.navigation.addListener("didFocus", () => {
      this.setState({
        index: this.props.navigation.state.params.index
          ? this.props.navigation.state.params.index
          : 0,
      })
    });
  }

  checknet = () => {
    this.props.navigation.addListener("willFocus", () => {
      const net = network();
      if (net == false) {
        this.props.navigation.navigate("HomeScreen");
      }
    });
    const net = network();

    if (net == false) {
      this.props.navigation.navigate("HomeScreen");
    }
  }

  NewCard = () => {
    if (this.state.locale == "eng") {
      this.setState({ screen: 1 });
    } else {
      this.setState({ screen: 3 });
    }
  };

  BestSelling = () => {
    this.setState({ screen: 2 });
  };

  TodayOffer = () => {
    if (this.state.locale == "eng") {
      this.setState({ screen: 3 });
    } else {
      this.setState({ screen: 1 });
    }
  };
  backValueReceive = value => {
    if (value == "back") {
      this.props.navigation.navigate("HomeScreen");
    }
  };
  render() {
    return (
      <View style={commonStyle.mainBody}>
        <HeaderArrow
          language={this.state.language}
          locale={this.state.locale}
          backPage={value => this.backValueReceive(value)}
        />
        {this.state.locale == "eng" ? (
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              first: NewCard,
              second: BestSelling,
              third: TodayOffer
            })}
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ width: Dimensions.get("window").width }}
            style={{ justifyContent: "center" }}
            renderTabBar={props => {
              return (
                <TabBar
                  {...props}
                  renderLabel={({ route, focused, color }) => (
                    <Text
                      style={
                        focused
                          ? {
                            ...styles.tabLabel,
                            fontSize: 13,
                            fontFamily: "Tajawal-Bold"
                          }
                          : styles.tabLabel
                      }
                    >
                      {route.title}
                    </Text>
                  )}
                  style={styles.tabWrap}
                  indicatorStyle={styles.tabIndicator}
                />
              );
            }}
          />
        ) : (
            <TabView
              navigationState={this.state}
              renderScene={SceneMap({
                first: TodayOffer,
                second: BestSelling,
                third: NewCard
              })}
              onIndexChange={index => this.setState({ index })}
              initialLayout={{ width: Dimensions.get("window").width }}
              style={{ justifyContent: "center" }}
              renderTabBar={props => {
                return (
                  <TabBar
                    {...props}
                    renderLabel={({ route, focused, color }) => (
                      <Text
                        style={
                          focused
                            ? { ...styles.tabLabel, fontFamily: "Tajawal-Bold" }
                            : styles.tabLabel
                        }
                      >
                        {route.title}
                      </Text>
                    )}
                    style={styles.tabWrap}
                    indicatorStyle={styles.tabIndicator}
                  />
                );
              }}
            />
          )}
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(Tab);
