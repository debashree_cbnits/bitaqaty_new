const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  catagoryStyle: {
    //borderBottomWidth: 4,
    //borderBottomColor: "#eeeeee",
    //borderStyle: "solid",
    width: "33.33%",
    justifyContent: "center",
    alignItems: "center",
  },

  NewCardSelect: {
    width: 68,
    height: 20,
    borderBottomWidth: 4,
    borderBottomColor: "#241125",
    borderStyle: "solid",
  },

  NewCardUnSelect: {
    width: 68,
    height: 20,
    // borderBottomWidth: 4,
    // borderBottomColor: "#ffff",
    // borderStyle: "solid"
  },

  // BestWishSelect: {
  //   width: 68,
  //   height: 20,
  //   borderBottomWidth: 4,
  //   borderBottomColor: "#000000",
  //   borderStyle: "solid"
  // },

  // BestWishUnSelect: {
  //   width: 68,
  //   height: 20,
  //   borderBottomWidth: 4,
  //   borderBottomColor: "#ffff",
  //   borderStyle: "solid"
  // },

  OfferSelect: {
    width: 82,
    height: 20,
    borderBottomWidth: 4,
    borderBottomColor: "#241125",
    borderStyle: "solid",
  },

  OfferUnSelect: {
    width: 82,
    height: 20,
    // borderBottomWidth: 4,
    // borderBottomColor: "#ffff",
    // borderStyle: "solid"
  },
  scene: {
    flex: 1,
    height: 500,
  },
  tabWrap: {
    backgroundColor: "#fff",
    elevation: 0,
    marginVertical: 10,
    marginHorizontal: 12,
  },
  tabLabel: {
    width: 120,
    color: "#1A1A1A",
    fontFamily: "Tajawal-Light",
    textTransform: "capitalize",
    fontSize: 13,
    textAlign: "center",
  },
  tabIndicator: {
    backgroundColor: "#241125",
    height: 2,
  },
  innerBodyTab: {
    height: deviceHeight - 210,
    justifyContent: "center",
  },
  emptyHead: {
    flex: 1,
    justifyContent: "flex-start",
    paddingHorizontal: "3%",
  },
  emptyTextHead: {
    marginBottom: 35,
    fontSize: 20,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
  },
  emptyText: {
    textAlign: "center",
    fontSize: 14,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
  },
  directEng : {
    flexDirection: 'row'
  },
  directAr : {
    flexDirection: 'row-reverse'
  },
};

