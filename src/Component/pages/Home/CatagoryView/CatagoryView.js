import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  Dimensions,
  ScrollView,
  Alert, BackHandler
} from "react-native";
import styles from "./CateHeadStyle.js";
import I18n from "../../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import Product from "../../../partial/ProductBox/Product";
import ProductHorizontal from "../../../partial/ProductBox/ProductHorizontal";
import HeaderArrow from "../../Header/Header_logo_name/HeaderArrow";
import { productImagePath } from "../../../../config/imageConst";
import { connect } from "react-redux";
import apiClient from "../../../../services/api.client.js";
import { ActivityIndicator } from "react-native-paper";
import {WebView} from 'react-native-webview';
const baseUri = "https://stagewrapper.ocstaging.net/bitaqatywrapper";
const deviceHeight = Dimensions.get("window").height;

import network from "../../Network/check";
import Internet from "../../Network/Internet";
import imageUrl from '../../../../services/config';


class CatagoryView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      show: false,
      isLoading: true,
      productData: null,
      checkNetworkConnection: "",
      isLoadingRelatedData: true,
      relatedData: null,
      merchantData: null,
      categoryModel: null,
      back: this.props.navigation.state.params.back
        ? this.props.navigation.state.params.back
        : "",
      fromWhere: this.props.navigation.state.params.fromWhere
        ? this.props.navigation.state.params.fromWhere
        : "",
      totalRelatedData: []
    };


  }
  _getMerchantProductsFromApi() {
    let merchantId = ''    
    if (this.props.navigation.state.params.fromWhere == "tabs") {
      merchantId = this.props.navigation.state.params.merchantInfo.merchantId
    } else {
      merchantId = this.props.navigation.state.params.merchantInfo.id
    }

    let data = {
      merchantId: merchantId,
      categoryId: this.props.navigation.state.params.categoryId
    }    
    apiClient
      .postRequest("/merchant/Products", data)
      .then(res => {
        this.setState({
          isLoading: false,
          productData: res.seoList.listData,
          merchantData: res.localMerchantResponseDTO,
          categoryModel: res.categoryModel ? res.categoryModel : null
        });
      })
      .catch(err =>
        this.setState({
          isLoading: false,
          productData: null,
          merchantData: null,
          categoryModel: null
        })
      );
  }


  _getRelatedProductFromApi() {
    let merchantId = ''

    if (this.props.navigation.state.params.fromWhere == "tabs") {
      merchantId = this.props.navigation.state.params.merchantInfo.merchantId
    } else {
      merchantId = this.props.navigation.state.params.merchantInfo.id
    }

    apiClient
      .postRequest("/related-products", {
        merchantId: merchantId
      })
      .then(res => {
        this.setState({ totalRelatedData: res })
        this.setState({ isLoadingRelatedData: false, relatedData: res });
      })
      .catch(err =>
        this.setState({ isLoadingRelatedData: false, relatedData: null }, () =>
          console.log("err>>>>>", err)
        )
      );
  }

  seeMoreRelated = () => {
    this.setState({ relatedData: this.state.totalRelatedData })
  }

  componentDidMount() {
    this._getMerchantProductsFromApi();
    this._getRelatedProductFromApi();

    BackHandler.addEventListener('hardwareBackPress', function () {
      if (this.props.navigation.state.routeName === 'CatagoryView') {
        if (this.props.navigation.state.params.fromWhere == "tabs") {
          this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere, item: this.props.navigation.state.params.productApiResponse.categoryModel });
        } else {
          this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere });
        }
        return true;
      } else {
        this.props.navigation.goBack(null);
        return true;
      }

    }.bind(this));

    this.props.navigation.addListener("didFocus", () => {
      this.setState({ isLoading: true })
      this._getMerchantProductsFromApi();
      this._getRelatedProductFromApi();
      const net = network();
      this.setState({
        checkNetworkConnection: net, fromWhere: this.props.navigation.state.params.fromWhere
          ? this.props.navigation.state.params.fromWhere
          : ""
      });
    });


  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }

  backValueReceive = value => {
    if (this.state.back == "Home") {
      this.props.navigation.navigate("HomeScreen");
    } else {
      if (this.props.navigation.state.params.fromWhere == "tabs") {
        this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere, item: this.props.navigation.state.params.productApiResponse.categoryModel });
      } else {
        this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere });
      }
    }
  };

  goToCartPage = cart => {
    this.props.navigation.navigate("Cart");
  };

  back = () => {
    this.setState({ show: false });
    if (this.state.back == "Home") {
      // this.props.navigation.navigate("HomeScreen");
      this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere,item: this.props.navigation.state.params.productApiResponse.categoryModel  });
    } else {
      if (this.props.navigation.state.params.fromWhere == "tabs") {
        this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere, item: this.props.navigation.state.params.productApiResponse.categoryModel });
      } else {
        this.props.navigation.navigate("Merchants", { fromWhere: this.props.navigation.state.params.fromWhere });
      }
    }
  };

  netConnectReload = value => {
    this.setState({ checkNetworkConnection: value });
  };

  goToLoginPage = login => {
    this.props.navigation.navigate("Login");
  };

  checknet = value => {
    this.setState({ checkNetworkConnection: value });
  }

  renderHorizontalProduct = () => (
    <>
      <View
        style={this.state.locale == "eng" ? styles.secHead : styles.secHead_ar}
      >
        <Text style={styles.BoldText}>{I18n.t("You May Also Like")}</Text>
      </View>
      <ScrollView style={{ paddingBottom: 270 }} nestedScrollEnabled horizontal>
        {this.state.relatedData.map((item, key) => {

          return item.discountPercentage > 0 ? (
            <ProductHorizontal
              key={key}
              item={item}
              outOfStock={!item.avaiable}
              productImagePath={item.backImagePath}
              productHzTitle={
                this.state.locale === "eng" ? item.nameEn : item.nameAr
              }
              productHzStore={
                this.state.locale === "eng"
                  ? item.merchantNameEn
                  : item.merchantNameAr
              }
              productHzPriceH={I18n.t("Price")}
              productHzPrice={item.individualPriceAfter}
              productHzTag={I18n.t('SAR')}
              prodWish={productImagePath.heart}
              prodVarPar={productImagePath.vartiParcentage}
              beforeHz={I18n.t("Before")}
              beforeHzPrice={item.individualPrice}
              beforeHzUnit={I18n.t('SAR')}
              goToCart={value => this.goToCartPage(value)}
              goToLogin={value => this.goToLoginPage(value)}
              netcheck={value => this.checknet(value)}
              navigation={this.props.navigation}
              doNotNavigate={false}
            />
          ) : (
              <ProductHorizontal
                key={key}
                item={item}
                outOfStock={!item.avaiable}
                productImagePath={item.backImagePath}
                productHzTitle={
                  this.state.locale === "eng" ? item.nameEn : item.nameAr
                }
                productHzStore={
                  this.state.locale === "eng"
                    ? item.merchantNameEn
                    : item.merchantNameAr
                }
                productHzPriceH={I18n.t("Price")}
                productHzPrice={item.individualPrice}
                productHzTag={I18n.t('SAR')}
                prodWish={productImagePath.heart}
                goToCart={value => this.goToCartPage(value)}
                goToLogin={value => this.goToLoginPage(value)}
                netcheck={value => this.checknet(value)}
                navigation={this.props.navigation}
                doNotNavigate={false}
              />
            );
        })}
      </ScrollView>
    </>
  );



  render() {
    const regex = /(<([^>]+)>)/ig;
    const companyDescription = this.state.merchantData
      ? this.state.locale == "eng"
        ? this.state.merchantData.companyDescriptionEn &&
          this.state.merchantData.companyDescriptionEn !== ""
          ? this.state.merchantData.companyDescriptionEn
          : null
        : this.state.merchantData.companyDescriptionAr &&
          this.state.merchantData.companyDescriptionAr !== ""
          ? this.state.merchantData.companyDescriptionAr
          : null
      : null;


    const howToUse = this.state.merchantData
      ? this.state.locale == "eng"
        ? this.state.merchantData.howToUseEn &&
          this.state.merchantData.howToUseEn !== ""
          ? this.state.merchantData.howToUseEn
          : null
        : this.state.merchantData.howToUseAr &&
          this.state.merchantData.howToUseAr !== ""
          ? this.state.merchantData.howToUseAr
          : null
      : null;
    var net = network();
    return (
      <>
        <Modal transparent={true} visible={this.state.show}>
          <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
            <View
              style={{ backgroundColor: "#fff", }}
            >
              <View
                style={styles.header}
              >
                <RTLView locale={this.state.locale}>
                  <View style={{ width: "12%", zIndex: 1 }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ show: false });
                      }}
                    >
                      <Image
                        style={
                          this.state.locale === "eng"
                            ? styles.arrow_en
                            : styles.arrow_ar
                        }
                        source={
                          this.state.locale === "eng"
                            ? require("../../../assets/image/arrow1.png")
                            : require("../../../assets/image/arrow2.png")
                        }
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={styles.headerLogo}
                  >
                    <Image
                      style={{ height: 29, width: 24 }}
                      source={require("../../../assets/image/logo.png")}
                    />
                    <RTLText
                      style={styles.headerText}
                    >
                      {I18n.t("BITAQATY")}
                    </RTLText>
                  </View>
                  <View style={{ width: "10%" }} />
                </RTLView>
              </View>


              <View style={styles.topSection2}>
                <Image
                  style={[styles.imageBox, { resizeMode: 'stretch' }]}
                  source={{
                    uri: this.props.navigation.state.params.fromWhere == "tabs" ?
                      imageUrl.productImageUrl + this.props.navigation.state.params.productApiResponse.localMerchantResponseDTO.logoPath :
                      imageUrl.productImageUrl + this.props.navigation.state.params.productApiResponse.localMerchantResponseDTO.logoPath
                  }}
                />
                <Text style={styles.headText}>
                  {this.state.locale === "eng"
                    ? this.props.navigation.state.params.merchantInfo
                      .companyNameEn
                    : this.props.navigation.state.params.merchantInfo
                      .companyNameAr}
                </Text>
                <TouchableOpacity onPress={this.back}>
                  <Text style={styles.blueText}>
                    {this.state.locale === "eng"
                      ? this.props.navigation.state.params.productApiResponse
                        .categoryModel.nameEn
                      : this.props.navigation.state.params.productApiResponse
                        .categoryModel.nameAr}
                  </Text>
                </TouchableOpacity>

              </View>
              <ScrollView>
                <View>
                  <View style={{ width: '100%', paddingHorizontal: '4%', }}>
                    {this.state.isLoading && (
                      <ActivityIndicator style={styles.activityIndicator}
                      size="small"
                      color="#1A1A1A"/>
                    )}
                    {!this.state.isLoading && this.state.merchantData ? (
                      <>
                        {companyDescription && (
                          <Text style={{ ...styles.light12, paddingTop: 12 }}>
                            {companyDescription}
                          </Text>
                        )}

                        {howToUse && (
                          <View style={{ paddingTop: 10 }}>
                            <Text style={styles.bold16}>
                              {I18n.t("How To Use")}
                            </Text>
                            {/* <View style={{height: 100,}}>
                            <WebView
                            style={{backgroundColor: 'red', width: 300, justifyContent: 'flex-end'}}
                              automaticallyAdjustContentInsets={false}
                              scalesPageToFit={false}
                              javaScriptEnabled={true}
                              domStorageEnabled={true}
                              source={{ html: howToUse}}
                            /> 
                            </View> */}
                            <ScrollView nestedScrollEnabled style={{height: 100,}}>
                              <Text style={styles.light12}>{howToUse.replace(regex, '')}</Text>                              
                            </ScrollView>
                          </View>
                        )}
                      </>
                    ) : (
                        <></>
                      )}
                  </View>

                </View>
              </ScrollView>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ show: false });
                }}
                style={styles.drop}
              >
                <RTLView locale={this.state.locale}>
                  <Text style={styles.dropText}>
                    {I18n.t("How To Use")}
                  </Text>
                  <Image
                    style={styles.dropRotate}
                    source={require("../../../assets/image/downarrow.png")}
                  />
                </RTLView>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View style={styles.mainSection}>
          {net ? (
            <View>
              <HeaderArrow
                language={this.state.language}
                locale={this.state.locale}
                backPage={value => this.backValueReceive(value)}
              />
              <View style={styles.topSection}>
                <Image
                  style={[styles.imageBox, { resizeMode: 'stretch' }]}
                  source={{
                    uri: this.props.navigation.state.params.fromWhere == "tabs" ?
                      imageUrl.productImageUrl + this.props.navigation.state.params.productApiResponse.localMerchantResponseDTO.logoPath :
                      imageUrl.productImageUrl + this.props.navigation.state.params.productApiResponse.localMerchantResponseDTO.logoPath
                  }}
                />
                <Text>
                  {this.state.locale === "eng"
                    ? this.props.navigation.state.params.productApiResponse
                      .localMerchantResponseDTO.companyNameEn
                    : this.props.navigation.state.params.productApiResponse
                      .localMerchantResponseDTO.companyNameAr
                  }
                </Text>
                <TouchableOpacity onPress={this.back}>
                  <Text style={styles.blueText}>
                    {this.state.locale === "eng"
                      ? this.props.navigation.state.params.productApiResponse
                        .categoryModel.nameEn
                      : this.props.navigation.state.params.productApiResponse
                        .categoryModel.nameAr}
                  </Text>
                </TouchableOpacity>

                {howToUse && howToUse.length || companyDescription && companyDescription.length > 0 ? (
                  <TouchableOpacity
                    style={styles.drop}
                    onPress={() => {
                      this.setState({ show: true });
                    }}
                  >
                    <View locale={this.state.locale} style={{ flexDirection: 'row' }}>
                      <Text style={styles.dropText}>{I18n.t("How To Use")}</Text>
                      <Image
                        style={{ marginLeft: 7, marginTop: 25 }}
                        source={require("../../../assets/image/downarrow.png")}
                      />
                    </View>
                  </TouchableOpacity>
                ) : null}
                {/* </Collapse> */}
              </View>
              {this.state.isLoading && <ActivityIndicator size="small" />}
              {!this.state.isLoading && this.state.productData && (
                <ScrollView >
                  <ScrollView nestedScrollEnabled style={{ paddingBottom: this.state.relatedData && this.state.relatedData ? 40 : 300 }}>
                    {this.state.productData.map((item, index) => {
                      return item.discountPercentage > 0 ? (
                        <View key={index} style={{ paddingVertical: 0 }}>
                          <Product
                            item={item}
                            outOfStock={!item.avaiable}
                            productImage={item.backImagePath}
                            productTitle={
                              this.state.locale === "eng"
                                ? item.nameEn
                                : item.nameAr
                            }
                            productStore={
                              this.state.locale === "eng"
                                ? item.merchantNameEn
                                : item.merchantNameAr
                            }
                            before={I18n.t("Before")}
                            productBeforePrice={item.individualPrice}
                            beforeUnit={I18n.t('SAR')}
                            percentage={productImagePath.percent}
                            productPrice={item.individualPriceAfter}
                            productCurrency={I18n.t('SAR')}
                            goToCart={value => this.goToCartPage(value)}
                            goToLogin={value => this.goToLoginPage(value)}
                            netcheck={value => this.checknet(value)}
                            navigation={this.props.navigation}
                            doNotNavigate={false}
                          />
                        </View>
                      ) : (
                          <View key={index} style={{ paddingVertical: 0 }}>
                            <Product
                              outOfStock={!item.avaiable}
                              item={item}
                              productImage={item.backImagePath}
                              productTitle={
                                this.state.locale === "eng"
                                  ? item.nameEn
                                  : item.nameAr
                              }
                              pointsCard={I18n.t("Points Card")}
                              productStore={
                                this.state.locale === "eng"
                                  ? item.merchantNameEn
                                  : item.merchantNameAr
                              }
                              productPrice={item.individualPrice}
                              productCurrency={I18n.t('SAR')}
                              goToCart={value => this.goToCartPage(value)}
                              goToLogin={value => this.goToLoginPage(value)}
                              netcheck={value => this.checknet(value)}
                              navigation={this.props.navigation}
                              doNotNavigate={false}
                            />
                          </View>
                        );
                    })}
                  </ScrollView>
                  {/* {this.renderHorizontalProduct()} */}
                  {!this.state.isLoadingRelatedData &&
                    this.state.relatedData &&
                    this.renderHorizontalProduct()}
                </ScrollView>
              )}
            </View>
          ) : (
              <Internet netConnectReload={value => this.netConnectReload(value)} />
            )}

        </View>
      </>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(CatagoryView);
