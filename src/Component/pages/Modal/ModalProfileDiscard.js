import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { loginScreen } from "../../../config/imageConst";

class ModalProfileDiscard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  disCardChange = () => {
    let responseEditProfile = "discard";
    this.props.goToEditProfile(responseEditProfile);
  };

  contineueEditProfile = () => {
    let responseEditProfile = "continueEdIt";
    this.props.goToEditProfile(responseEditProfile);
  };

  render() {
    return (
      <View style={commonStyle.modalHead}>
        <View style={{ alignItems: "center", marginTop: "20%" }}>
          <Image source={loginScreen.loginError} />
          <Text style={styles.dupLoginBoldMsg}>
            {I18n.t("You have unsaved changes")}
          </Text>
          <Text style={styles.dupLoginMsg}>
            {I18n.t("Do you want to discard them?")}
          </Text>
        </View>

        <TouchableOpacity
          style={
            this.state.locale == "eng"
              ? [styles.discardBtn, { marginTop: 50 }]
              : [styles.discardBtn_ar, { marginTop: 50 }]
          }
          onPress={this.disCardChange}
        >
          <Text style={styles.whiteBtnText}>{I18n.t("Discard")}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            this.state.locale == "eng"
              ? styles.discardBtn
              : styles.discardBtn_ar
          }
          onPress={this.contineueEditProfile}
        >
          <Text style={styles.whiteBtnText}>{I18n.t("Continue Editing")}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ModalProfileDiscard);