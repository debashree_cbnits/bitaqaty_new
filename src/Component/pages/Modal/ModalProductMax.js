import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import styles from "./style";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { loginScreen } from "../../../config/imageConst";


class ModalProductMax extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  render() {
    return (
      <View style={styles.modalHead}>
        <View
          style={
            this.state.locale == "eng"
              ? styles.successSec
              : styles.successSec_ar
          }
        >
          <Image source={loginScreen.error} style={styles.profileImg} />
          <Text style={{ ...styles.dupLoginBoldMsg, width: '85%' }}>{I18n.t("Maximum cards per day reached")}</Text>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ModalProductMax);
