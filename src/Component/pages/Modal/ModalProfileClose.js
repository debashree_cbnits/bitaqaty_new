import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import {
  loginScreen,
} from "../../../config/imageConst";

class ModalProfileClose extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  saveEditChanges = () => {
    let editChangeValue = "saveEditChanges";
    this.props.backPageEdit(editChangeValue);
  }
  dontSave = () => {
    let editChangeValue = "dontSave";
    this.props.backPageEdit(editChangeValue);
  }
  continueEdit = () => {
    let editChangeValue = "continueEdit";
    this.props.backPageEdit(editChangeValue);
  }

  render() {
    return (
      <View style={commonStyle.modalHead}>
        <View style={{ alignItems: "center", marginTop: "20%" }}>
          <Image source={loginScreen.loginError} />
          <Text style={styles.dupLoginBoldMsg}>
            {I18n.t(
              "Do you want to save changes to your personal data before leaving the page?"
            )}
          </Text>
          <Text style={styles.dupLoginMsg}>
            {I18n.t("Changes will be lost, if you do not save them")}
          </Text>
        </View>
        <View
          style={
            this.state.locale == "eng"
              ? styles.paddingRow
              : styles.paddingRow_ar
          }
        >
          <TouchableOpacity style={styles.blackBtn}
            onPress={this.saveEditChanges}>
            <Text style={styles.blackBtnText}>
              {I18n.t("Save Changes")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.whiteButton}
            onPress={this.dontSave}>
            <Text style={styles.whiteBtnText}>
              {I18n.t("Don't Save")}
            </Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity onPress={this.continueEdit}>
          <Text style={styles.dupLoginBoldMsg}>{I18n.t("Continue Editing")}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ModalProfileClose);