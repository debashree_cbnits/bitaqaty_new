import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, TextInput } from "react-native";
import styles from "../orderHistory/style";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { othersImage } from "../../../config/imageConst";

class ModalOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  render() {
    return (
      <View style={styles.modalDatePicker}>
        <View style={styles.modalTop}>
          <Text style={styles.modalTopText}>{I18n.t("Order History")}</Text>
          <View
            style={
              this.state.locale === "eng"
                ? styles.modalDate
                : styles.modalDate_ar
            }
          >
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.modalTopDate}>Jan 15</Text>
              <Text style={styles.modalTopDate}> - </Text>
              <Text style={styles.modalTopDate}>Dec 10</Text>
            </View>
            <TouchableOpacity>
              <Image source={othersImage.calendarSm} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.modalBottom}>
          <View
            style={
              this.state.locale === "eng"
                ? styles.modalDateBox
                : styles.modalDateBox_ar
            }
          >
            <View style={styles.DateBox}>
              <Text
                style={
                  this.state.locale === "eng" ? styles.bText1 : styles.bText1_ar
                }
              >
                {I18n.t("From")}
              </Text>
              <TextInput
                style={
                  this.state.locale === "eng" ? styles.bText2 : styles.bText2_ar
                }
                placeholder="15/1/2019"
                placeholderTextColor="#000"
                keyboardType="numeric"
              />
            </View>
            <View style={styles.DateBox}>
              <Text
                style={
                  this.state.locale === "eng" ? styles.bText1 : styles.bText1_ar
                }
              >
                {I18n.t("To")}
              </Text>
              <TextInput
                style={
                  this.state.locale === "eng" ? styles.bText2 : styles.bText2_ar
                }
                placeholder="10/12/2019"
                placeholderTextColor="#000"
                keyboardType="numeric"
              />
            </View>
          </View>
          <View
            style={
              this.state.locale === "eng"
                ? styles.saveAlign
                : styles.saveAlign_ar
            }
          >
            <View
              style={
                this.state.locale === "eng" ? styles.btnArea : styles.btnArea_ar
              }
            >
              <TouchableOpacity
              // onPress={() => {
              //   this.setState({ show: false });
              // }}
              >
                <Text style={styles.saveText}>{I18n.t("Cancel")}</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={styles.saveText}>{I18n.t("Save")}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ModalOrder);
