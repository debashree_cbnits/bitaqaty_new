import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import styles from "./style";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";

class ModalProfileSave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  render() {
    return (
      <View
        style={
          this.state.locale == "eng" ? styles.editProf : styles.editProf_ar
        }
      >
        <TouchableOpacity style={styles.blackBtn}>
          <Text style={styles.blackBtnText}>{I18n.t("Save Changes")}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.whiteButton}
        >
          <Text style={styles.whiteBtnText}>{I18n.t("Cancel")}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ModalProfileSave);