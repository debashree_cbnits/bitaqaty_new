import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import styles from "./style";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { othersImage } from "../../../config/imageConst";


class ModalProfileUpdate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  render() {
    return (
      <View style={styles.modalHead}>
        <View
          style={
            this.state.locale == "eng"
              ? styles.successSec
              : styles.successSec_ar
          }
        >
          <Image source={othersImage.rightTick} style={styles.profileImg} />
          <Text style={{ ...styles.dupLoginBoldMsg, width: '85%', height: '50%' }}>{I18n.t("Changes saved successfully")}</Text>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ModalProfileUpdate);