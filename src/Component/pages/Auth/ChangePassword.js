import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import apiClient from "../../../services/api.client";
// import { forFade } from "react-navigation-stack/lib/typescript/src/vendor/TransitionConfigs/HeaderStyleInterpolators";
import Toast from "react-native-tiny-toast";
import { TapGestureHandler } from "react-native-gesture-handler";

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      confirmPassword: "",
      confirmPasswordError: false,
      newPassword: "",
      passError: false,
      oldPassword: "",
      oldPasswordError: false,
      oldPasswordApiError:false,
      passMatchError: false,
      showOldPass: true,
      showNewPass: true,
      // showConfirmPass: true,
      userToken: "",
      error1: false,
      error2: false,
      error3: false,
      email: "",
      isRegistering: false,
      isPasswordIncorrect: false,
      passError3: false,
      passError2: false,
      passError4: false,
      passError5: false,
      passError6: false,
      passError1: false,
      passError: false,
      newpassError: false,
      backmsg : this.props.navigation.state.params && this.props.navigation.state.params.backmsg ? this.props.navigation.state.params.backmsg : "",
    };
    this.getUserToken();
  }

  async getUserToken() {
    try {
      const value = await AsyncStorage.getItem("userToken");
      if (value !== null) {
        this.setState({ userToken: value });
      }
    } catch (error) {}
    try {
      const value = await AsyncStorage.getItem("loginEmailId");
      if (value !== null) {        
        this.setState({ email: value });
      }
    } catch (error) {}
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }

  // backValueReceive = value => {
  //   if (value == "back") {
  //     this.props.navigation.navigate("Menu");
  //   }
  // };

  backValueReceive = (value) => {
    if (value == "back") {
      if(this.state.backmsg == "profile"){
      let edit = "non";
      this.props.navigation.navigate("ProfileDetails", {edit : edit});
      }
      else{
      this.props.navigation.navigate("Menu");
      }
    }
  };

  newPassFocus = (value)=>{
    if (value == ""){     
      this.setState({ newpassError: false, passError: true, errorText: '', passError1: true, passError2: true, passError3: true, passError4: true, passError5: true, passError6: true });
      }
  }
  validation = (value, fieldName) => {
    //field validation
    if (fieldName == "oldPassword") {
      if (value == "") {
        this.setState({ oldPasswordError: true,oldPasswordApiError: false, newpassMatchError: false });
      } else {
        this.setState({ oldPassword: value, oldPasswordError: false, oldPasswordApiError: false, newpassMatchError: false });
      }
    } else if (fieldName == "newPassword") {
      if (value == ""){
        this.setState({
          newpassError: true, passError1: false})
      } else{
        this.setState({newpassError: false, newpassMatchError: false})
        const rg2 = new RegExp(/^.{8,}$/);
        const rg3 = new RegExp(/(?=.*[a-z])/);
        const rg4 = new RegExp(/(?=.*[A-Z])/);
        const rg5 = new RegExp(/(?=.*\d)/);
        
        rg2.test(value)
          ? this.setState({ passError2: false })
          : this.setState({ passError2: true });
        rg3.test(value)
          ? this.setState({ passError3: false })
          : this.setState({ passError3: true });
        rg4.test(value)
          ? this.setState({ passError4: false })
          : this.setState({ passError4: true });
        rg5.test(value)
          ? this.setState({ passError5: false })
          : this.setState({ passError5: true });
        rg4.test(value) || rg3.test(value)
        ? this.setState({ passError6: false })
        : this.setState({ passError6: true });
        if (
          rg2.test(value) &&
          rg3.test(value) &&
          rg4.test(value) &&
          rg5.test(value)
        ) {
          this.setState({ passError1: false });
        } else {
          this.setState({ passError1: true });
        }
      }
    } else if (fieldName == "confirmPassword") {
      if (value == "") {
        this.setState({ confirmPasswordError: true, newpassMatchError: false , passMatchError:false}, ()=>{
          if (this.state.oldPassword == this.state.newPassword) {
            this.setState({
              newpassMatchError: true,
              error2: true,
              isRegistering: false
            });
          }
        });
      } else {
        this.setState(
          { confirmPassword: value, confirmPasswordError: false, newpassMatchError: false},
          () => {            
            this.checkPass();
          }
        );
      }
    }
  };
  checkPass = () => {
    //pass matched function
    if (this.state.newPassword == this.state.confirmPassword) {
      this.setState({ passMatchError: false });
    } else {
      this.setState({ passMatchError: true, error3: true });
    }
  };

  changePassWord = () => {
    
    this.setState({ isRegistering: true });
    if (this.state.oldPassword == "" && this.state.newPassword == "" && this.state.confirmPassword == "") {
      this.setState({
        oldPasswordError: true, newpassError: true, confirmPasswordError: true, isRegistering: false})
    }
    else if (this.state.oldPassword == "") {
      this.setState({
        oldPasswordError: true,
        error1: true,
        isRegistering: false
      });
    } else if (this.state.newPassword == "") {
      this.setState({
        newpassError: true,
        error2: true,
        isRegistering: false
      });
    } else if (this.state.confirmPassword == "") {
      this.setState({
        confirmPasswordError: true,
        error3: true,
        isRegistering: false
      });
    } else  if (this.state.oldPassword == this.state.newPassword) {      

      this.setState({
        newpassMatchError: true,
        error2: true,
        isRegistering: false
      });
    } else if (this.state.passError1){
     this.setState({isRegistering: false})
    }
    else if (this.state.newPassword != this.state.confirmPassword) {
      

      this.setState({
        passMatchError: true,
        error3: true,
        isRegistering: false
      });
    } 
    else {
    
      let changePassData = {
        oldPassword: this.state.oldPassword,
        newPassword: this.state.newPassword,
        confirmPassword: this.state.confirmPassword
      };      
      apiClient
        .postRequest("/changePassword", changePassData, this.state.userToken)        
        .then(changePassDataRes => {          
          this.setState({ isRegistering: false });
          if (changePassDataRes.success === 200) {
            this.props.navigation.navigate("LoginWlcm", {
              email: this.state.email,
              passWord: this.state.newPassword
            });
          } else if (changePassDataRes.oldPassword == 213) {
            this.setState({ oldPasswordApiError: true, error1: true, oldPassword: "" });
          }
        })
        .catch(err => console.log("err", err));
    }
  };

  render() {
    const {
      passError1,
      passError2,
      passError3,
      passError4,
      passError5,
      passError6,
    } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainSection}>
          <HeaderArrow
            locale={this.state.locale}
            language={this.state.language}
            backPage={value => this.backValueReceive(value)}
          />
          <ScrollView >
            <View style={styles.container}>
              <View style={styles.top}>
                <Text style={styles.pageTitle}>
                  {I18n.t("Change password")}
                </Text>
                <RTLView locale={this.state.locale}>
                  <Image
                    source={
                      this.state.locale == "eng"
                        ? loginScreen.changePassword2
                        : loginScreen.changePassword2Y
                    }
                    resizeMode={"contain"}
                    style={styles.stepsImg}
                  />
                </RTLView>
                <Text style={[styles.orText, { marginBottom: 17 }]}>
                  {I18n.t("Waiting for new password registration")}
                </Text>

                <View style={[styles.formWrap, { marginTop: 0 }]}>
                  <View
                    style={[
                      styles.formGroup,
                      { marginTop: 8, marginBottom: 30 }
                    ]}
                  >
                    <Text style={styles.fromText}>
                      {I18n.t("Current Password")}
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <RTLView locale={this.state.locale}>
                        <TextInput
                          //style={[styles.formControl, { width: "85%" }]}
                          // style={
                          //   this.state.locale == "eng"
                          //     ? this.state.error1
                          //       ? styles.formPassword_en_error
                          //       : styles.formPassword_en
                          //     : this.state.error
                          //     ? styles.formPassword_ar_error
                          //     : styles.formPassword_ar
                          // }
                          style={
                            [this.state.locale == "eng"
                              ? styles.formPassword_en
                              : styles.formPassword_ar,
                              (
                                this.state.oldPasswordError || this.state.newpassMatchError || this.state.oldPasswordApiError
                                ) && {
                                borderColor: "#ff0000"
                              }]
                          }
                          placeholder={I18n.t(
                            "Please enter your current password"
                          )}
                          onChangeText={text => this.setState({oldPassword: text}, ()=>{
                            this.validation(this.state.oldPassword, "oldPassword")
                          })                           
                          }
                          placeholderTextColor="#CCCCCC"
                          returnKeyType="go"
                          value={this.state.oldPassword}
                          secureTextEntry={this.state.showOldPass}
                          password={true}
                          autoCorrect={false}
                        />
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.passIcon_en
                              : styles.passIcon_ar
                          }
                        >
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                showOldPass: !this.state.showOldPass
                              })
                            }
                          >
                            <Image
                              source={this.state.showOldPass ? loginScreen.password : loginScreen.passwordOff }
                              style={
                                this.state.locale == "eng"
                                  ? styles.passImg
                                  : styles.passImg2
                              }
                            />
                          </TouchableOpacity>
                        </View>
                      </RTLView>
                    </View>
                    {this.state.oldPasswordError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("This field is required")}
                      </Text>
                    ) : null}
                    {this.state.oldPasswordApiError  ? <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("Incorrect password")}
                      </Text>
                    : null}
                  </View>

                  <View style={styles.formGroup}>
                    <Text style={styles.fromText}>
                      {I18n.t("New Password")}
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <RTLView locale={this.state.locale}>
                        <TextInput
                          //style={[styles.formControl, { width: "85%" }]}
                          // style={
                          //   this.state.locale == "eng"
                          //     ? this.state.error2
                          //       ? styles.formPassword_en_error
                          //       : styles.formPassword_en
                          //     : this.state.error
                          //     ? styles.formPassword_ar_error
                          //     : styles.formPassword_ar
                          // }
                          style={
                            [this.state.locale == "eng"
                              ? styles.formPassword_en
                              : styles.formPassword_ar,
                              (
                                this.state.newpassError || this.state.newpassMatchError
                                ) && {
                                borderColor: "#ff0000"
                              }]
                          }
                          onChangeText={text => this.setState({newPassword: text}, ()=>{
                            this.validation(text, "newPassword")
                          })                            
                          }
                          value={this.state.newPassword}
                          onFocus={(text)=> this.newPassFocus(this.state.newPassword)}
                          placeholder={I18n.t("Please enter your new password")}
                          placeholderTextColor="#CCCCCC"
                          returnKeyType="go"
                          secureTextEntry={this.state.showNewPass}
                          password={true}
                          autoCorrect={false}
                        />
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.passIcon_en
                              : styles.passIcon_ar
                          }
                        >
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                showNewPass: !this.state.showNewPass
                              })
                            }
                          >
                            <Image
                              source={this.state.showNewPass ? loginScreen.password : loginScreen.passwordOff }
                              style={
                                this.state.locale == "eng"
                                  ? styles.passImg
                                  : styles.passImg2
                              }
                            />
                          </TouchableOpacity>
                        </View>
                      </RTLView>
                    </View>
                    {this.state.newpassError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("This field is required")}
                      </Text>
                    ) : null}

                    {this.state.passError &&
                      this.state.errorText !== "SignUpSuccess" &&
                      passError1 &&
                      <View
                        style={
                          this.state.locale == "eng"
                            ? styles.resErHead
                            : styles.resErHead_ar
                        }
                      >
                          
                        <Text style={styles.resErBold}>{I18n.t("Strong Password should:")} </Text>
                        {passError6 ? 
                        <Text style={styles.resErBold}>{I18n.t("Be written in English")} </Text> : null}
                        {passError2 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least 8 characters")} </Text> : null}
                        {passError3 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least one small letter")} </Text> : null}
                        {passError4 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least one capital letter")} </Text> : null}
                        {passError5 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least one number")} </Text> : null}
                    
                    </View>
                    }

                    {this.state.newpassMatchError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t(
                          "New password shouldn't be as current password"
                        )}{" "}
                      </Text>
                    ) : null}
                  </View>

                  <View style={styles.formGroup}>
                    <Text style={styles.fromText}>
                      {I18n.t("Confirm Password")}
                    </Text>
                    <View
                      style={
                        this.state.locale == "eng" ? styles.none : styles.aiEnd
                      }
                    >
                      <RTLView locale={this.state.locale}>
                        <TextInput
                          //style={[styles.formControl, { width: "85%" }]}
                          // style={
                          //   this.state.locale == "eng"
                          //     ? this.state.error3
                          //       ? styles.formPassword_en_error
                          //       : styles.formPassword_en
                          //     : this.state.error
                          //     ? styles.formPassword_ar_error
                          //     : styles.formPassword_ar
                          // }
                          style={
                           [ this.state.locale == "eng"
                              ? styles.formPassword_en
                              : styles.formPassword_ar,
                              (
                                this.state.confirmPasswordError
                                ) && {
                                borderColor: "#ff0000"
                              }]
                          }
                          onChangeText={text =>
                            this.validation(text, "confirmPassword")
                          }
                          placeholder={I18n.t("Re-enter new Password")}
                          placeholderTextColor="#CCCCCC"
                          secureTextEntry={true}
                          returnKeyType="go"
                          password={true}
                          autoCorrect={false}
                          secureTextEntry={true}

                        />
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.passIcon_en
                              : styles.passIcon_ar
                          }
                        >
                          {/* <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                showConfirmPass: !this.state.showConfirmPass
                              })
                            }
                          >
                            <Image                              
                              source={this.state.showConfirmPass ? loginScreen.passwordOff : loginScreen.password }
                              style={
                                this.state.locale == "eng"
                                  ? styles.passImg
                                  : styles.passImg2
                              }
                            />
                          </TouchableOpacity> */}
                        </View>
                      </RTLView>
                    </View>
                    {this.state.confirmPasswordError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("This field is required")}
                      </Text>
                    ) : null}
                    {this.state.passMatchError ? (
                      <Text style={{ color: "red" }}>
                        {I18n.t(
                          "Passwords don't match"
                        )}
                      </Text>
                    ) : null}
                  </View>

                  <View style={styles.formGroup}>
                    <TouchableOpacity
                      activeOpacity={0.7}
                      disabled={this.state.isRegistering}
                      onPress={() => this.changePassWord()}
                      style={styles.commonButton}
                    >
                      <Text style={styles.buttonText}>
                        {I18n.t("Save Changes")}
                      </Text>
                      {this.state.isRegistering && (
                        <ActivityIndicator
                          size="small"
                          color="#fff"
                          style={{
                            position: "absolute",
                            right: 30
                          }}
                        />
                      )}
                    </TouchableOpacity>
                  </View>
                  
                  <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                    <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                    <TouchableOpacity onPress ={()=> this.props.navigation.navigate("ContactUs")}>
                      <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>  
                    </TouchableOpacity>
                  </View>
                  
                </View>
              </View>

              <View
                style={
                  this.state.locale == "eng"
                    ? [
                        commonStyle.rights,
                        commonStyle.rowSec,
                        { marginTop: 50 }
                      ]
                    : [
                        commonStyle.rights,
                        commonStyle.rowSec_ar,
                        { marginTop: 50 }
                      ]
                }
              >
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                </Text>
                <Text
                  style={[
                    commonStyle.screenText,
                    { fontSize: 12, fontFamily: "Tajawal-Bold" }
                  ]}
                >
                  {I18n.t(" Bitaqaty ")}
                </Text>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("@2019")}
                </Text>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(ChangePassword);
