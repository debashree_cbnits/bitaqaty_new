import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  Switch,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen, menuScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
import apiClient from "../../../services/api.client";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import AsyncStorage from "@react-native-community/async-storage";

class LoginFirst extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      passwordImage: true,
      password: "",
      showPassword: true,
      locale: "",
      language: "",
      email: this.props.navigation.state.params.email
        ? this.props.navigation.state.params.email
        : "",
      password: this.props.navigation.state.params.password
        ? this.props.navigation.state.params.password
        : "",
      errorText: "",
      // email: "riham.mohamed@onecard.net",
      // password: "Aa12345678",
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  onClickBtn = () => {
    this.props.navigation.navigate("ForgotPassword");
  };

  _login = () => {
    let loginData = {
      username: this.state.email,
      password: this.state.password,
    };
    apiClient
      .postRequest("/login", loginData)
      .then((loginRes) => {
        if (loginRes.userToken) {
          this.props.navigation.navigate("HomeScreen");
        } else {
          this.setState({ errorText: "error" });
        }
      })
      .catch((err) => {
        this.setState({ errorText: "error" });
      });
  };
  backArrowMenu = () => {
    this.props.navigation.navigate("Menu");
  };

  changePwdType = () => {
    let newState;
    if (this.state.showPassword) {
      newState = {
        passwordImage: false,
        showPassword: false,
        password: this.state.password,
      };
    } else {
      newState = {
        passwordImage: true,
        showPassword: true,
        password: this.state.password,
      };
    }
    this.setState(newState);
  };
  handlePassword = (password) => {
    let newState = {
      passwordImage: this.state.passwordImage,
      showPassword: this.state.showPassword,
      password: password,
    };
    this.setState(newState);
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainSection}>
          <View style={styles.headerWrap}>
            <RTLView locale={this.state.locale}>
              <View style={styles.headerLeftIcon}>
                <TouchableOpacity onPress={this.backArrowMenu}>
                  <Image
                    style={
                      this.state.locale == "en"
                        ? styles.arrow_en
                        : styles.arrow_ar
                    }
                    source={
                      this.state.locale == "en"
                        ? menuScreen.close
                        : menuScreen.close
                    }
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.headerLogo}>
                <Image
                  style={styles.logoImage}
                  source={menuScreen.headerLogo}
                />
                <RTLText style={styles.logoText}>{I18n.t("BITAQATY")}</RTLText>
              </View>
            </RTLView>
          </View>
          {this.state.errorText !== "" &&
            this.state.errorText !== "LoginSuccess" && (
              <View style={styles.loginErrorDisplayOuterView}>
                <RTLView
                  style={[
                    styles.loginErrorDisplayInnerView,
                    this.state.errorText === "SignUpSuccess" && {
                      borderColor: "#008000",
                    },
                  ]}
                  locale={this.state.locale}
                >
                  <Ionicons
                    name={Platform.OS === "android" ? "md-alert" : "ios-alert"}
                    size={25}
                    color="#FF0000"
                  />
                  <Text style={styles.viewErrorText}>
                    {I18n.t(this.state.errorText)}
                  </Text>
                </RTLView>
              </View>
            )}
          <ScrollView>
            <View style={styles.container}>
              <View>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.pageTitle}>
                    {I18n.t("Welcome")}
                  </RTLText>
                </RTLView>
              </View>
              <View>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.loginText}>
                    {I18n.t("You can log in now")}
                  </RTLText>
                </RTLView>
              </View>
              <Image
                style={styles.stepsImg2}
                resizeMode={"contain"}
                source={
                  this.state.locale == "eng"
                    ? loginScreen.createAccount6
                    : loginScreen.createAccount6Y
                }
              />
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.loginText}>
                    {I18n.t("Waiting for mobile number verification")}
                  </RTLText>
                </RTLView>
              </View>

              <View style={[styles.formWrap, { marginTop: 0 }]}>
                <View style={[styles.formGroup, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.fromText}>
                        {I18n.t("Email")}
                      </RTLText>
                    </RTLView>
                  </View>
                  <TextInput
                    onChangeText={(text) => this.setState({ email: text })}
                    //style={styles.formControl}
                    style={
                      this.state.locale == "eng"
                        ? styles.formControlE_en
                        : styles.formControlE_ar
                    }
                    placeholder="example@example.com"
                    placeholderTextColor="#CCCCCC"
                    value={this.state.email}
                  />
                </View>

                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.fromText}>
                        {I18n.t("Password")}
                      </RTLText>
                    </RTLView>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <RTLView locale={this.state.locale}>
                      <TextInput
                        style={
                          this.state.locale == "eng"
                            ? styles.formPassword_en
                            : styles.formPassword_ar
                        }
                        onChangeText={(text) =>
                          this.setState({ password: text })
                        }
                        // style={[styles.formControl, { width: "85%" }]}
                        placeholder="************"
                        placeholderTextColor="#CCCCCC"
                        returnKeyType="go"
                        password={true}
                        autoCorrect={false}
                        value={this.state.password}
                        onChangeText={this.handlePassword}
                        secureTextEntry={this.state.showPassword}
                      />
                      <View
                        style={
                          this.state.locale == "eng"
                            ? styles.passIcon_en
                            : styles.passIcon_ar
                        }
                        //style={{ width: "15%", justifyContent: "center" }}
                      >
                        <TouchableOpacity onPress={this.changePwdType}>
                          {this.state.passwordImage ? (
                            <Image
                              source={loginScreen.password}
                              style={
                                this.state.locale == "eng"
                                  ? styles.passImg
                                  : styles.passImg2
                              }
                            />
                          ) : (
                            <Image
                              source={loginScreen.passwordOff}
                              style={
                                this.state.locale == "eng"
                                  ? styles.passImg
                                  : styles.passImg2
                              }
                            />
                          )}
                        </TouchableOpacity>
                      </View>
                    </RTLView>
                  </View>
                </View>

                <TouchableOpacity onPress={this.onClickBtn}>
                  <Text style={styles.forgotText}>
                    {I18n.t("Forgot password?")}
                  </Text>
                </TouchableOpacity>

                <View style={styles.formGroup}>
                  <TouchableOpacity
                    style={styles.commonButton}
                    onPress={this._login}
                  >
                    <Text style={styles.buttonText}>{I18n.t("Login")}</Text>
                  </TouchableOpacity>
                </View>

                <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                  <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                  <TouchableOpacity onPress ={()=> this.props.navigation.navigate("ContactUs")}>
                    <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>  
                  </TouchableOpacity>
                </View>

              </View>
              <View style={commonStyle.rights}>
                <RTLView locale={this.state.locale}>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                  </Text>
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, fontFamily: "Tajawal-Bold" },
                    ]}
                  >
                    {I18n.t(" Bitaqaty ")}
                  </Text>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("@2019")}
                  </Text>
                </RTLView>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(mapStateToProps, {})(LoginFirst);
