import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { othersImage } from "../../../config/imageConst";
import { connect } from "react-redux";

const deviceHeight = Dimensions.get("window").height;

class ForgotError extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  // openMobile = () => {
  //   this.props.navigation.navigate("LoginSuccess");
  // };

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={commonStyle.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        <View style={{ ...styles.container, justifyContent: "space-between" }}>
          <View style={{ width: "100%" }}>
            <View>
              <RTLView locale={this.state.locale}>
                <RTLText style={styles.pageTitle}>
                  {I18n.t("Something went wrong!")}
                </RTLText>
              </RTLView>
            </View>
            <View style={{ alignItems: "center", marginTop: "10%" }}>
              <Image source={othersImage.forgotError} />
            </View>

            <View style={{ alignItems: "center" }}>
              <Text style={{ ...commonStyle.bold14, marginTop: 22 }}>
                {I18n.t("The link has expired")}
              </Text>
              <Text style={{...commonStyle.light14, textAlign: 'center'}}>
                {I18n.t("Please re-enter your email to reset your password")}
              </Text>
            </View>
          </View>

          <View style={{ width: "100%" }}>
            <View style={[styles.formGroup, { marginBottom: 0 }]}>
              <TouchableOpacity
                onPress={()=> this.props.navigation.navigate('ForgotPassword')}
                style={styles.commonButton}
              >
                <Text style={styles.buttonText}>{I18n.t("Try Again")}</Text>
              </TouchableOpacity>
            </View>

            <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
              <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
              <TouchableOpacity onPress ={()=> this.props.navigation.navigate("ContactUs")}>
                <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>  
              </TouchableOpacity>
            </View>

            {/* <TouchableOpacity onPress={this.openMobile}><Text style={{fontSize: 20}}>Login Suc</Text></TouchableOpacity> */}


            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ForgotError);
