import React, { Component } from "react";
import { Text, View, ScrollView, Image, TouchableOpacity, TextInput, SafeAreaView, Modal } from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { connect } from "react-redux";
import { loginScreen, menuScreen } from "../../../config/imageConst";
import apiClient from "../../../services/api.client";
import AsyncStorage from "@react-native-community/async-storage";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import { log } from "react-native-reanimated";
import Toast from "react-native-tiny-toast";


class facebookSignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      showEmailOverley: false,
      showMobileOverley: false,
      socialmobileInput: "",
      socialMobileError: false,
      mobileNumberNumericError: false,
      socialemail: this.props.navigation.state.params && this.props.navigation.state.params.email ? this.props.navigation.state.params.email : "",
      socialemailerror: false,
      socialfullname: this.props.navigation.state.params && this.props.navigation.state.params.fullname ? this.props.navigation.state.params.fullname : "",
      socialemailnull: false,
      socialMobileNull: false,
      providerId: this.props.navigation.state.params && this.props.navigation.state.params.providerid ? this.props.navigation.state.params.providerid : "",
      provider: "FACEBOOK",
      hasmail: true
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          lang: "en"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          lang: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        lang: "en"
      };
    }
  }


  socialConnectApi = async () => {    
    let regex2 = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    let numRegex = /^[0-9]*$/;

    if (this.state.socialemail == "") {
      this.setState({ socialemailnull: true });
    }
    else if (!regex2.test(this.state.socialemail)) {
      this.setState({ socialemailerror: true });
    }
    else if (this.state.socialmobileInput == "") {
      this.setState({ socialMobileNull: true })
    }
    else if (this.state.socialmobileInput.length != 9) {
      this.setState({ socialMobileError: true })
    }

    else if (!numRegex.test(this.state.socialmobileInput)) {
      this.setState({ mobileNumberNumericError: true })
    }

    else {

      await apiClient
        .postWithoutToken("/social-connect", {
          fullName: this.state.socialfullname,
          username: this.state.socialemail,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: this.state.providerId,
          provider: this.state.provider,
          isLogin: true,
        })
        .then(async FBregisterSuccess => {
          if (FBregisterSuccess.userNameExists == 3002) {
            this.setState({
              showEmailOverley: true
            });
          }
          else if (FBregisterSuccess.userToken) {
            this.setState({
              showEmailOverley: true
            });
          }
          else if (FBregisterSuccess.mailInvalid == 4002) {
            Toast.show(I18n.t("emailNotValid"));
          }
          else if (FBregisterSuccess.error == "SECURITY_USER_EMAIL_NOT_VERIFIED") {
            AsyncStorage.setItem("mobileInput", this.state.socialmobileInput);
            AsyncStorage.setItem("email", this.state.socialemail);
            this.props.navigation.navigate("Email");
          }
          else if (FBregisterSuccess.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
            AsyncStorage.setItem("userName", FBregisterSuccess.userName);
            AsyncStorage.setItem("mobileInput", FBregisterSuccess.mobileNumber);
            AsyncStorage.setItem("email", this.state.socialemail);
            this.props.navigation.navigate("Mobile", { social: "social" })
          }
          else if (FBregisterSuccess.error == "SECURITY_USER_EMAIL_NOT_VERIFIED") {
            this.socialRegisterApi();
          }
          else if (FBregisterSuccess.username == 200) {
            this.socialRegisterApi();
          }
        })
        .catch(err => {
          console.log("FBregister err", err);
        });
    }
  }

  socialEmailVerifyApi = async () => {

    await apiClient
      .postWithoutToken("/validate-email", {
        fullName: this.state.socialfullname ? this.state.socialfullname : "",
        username: this.state.socialemail ? this.state.socialemail : "",
        lang: this.state.lang,
        application_name: "Bitaqaty",
        providerId: this.state.providerId,
        provider: this.state.provider,
        isLogin: false,
      })
      .then(async validateemail => {
        if (validateemail.userNameExists == 3002) {
          this.setState({
            showEmailOverley: true
          });
          Toast.show(I18n.t("emailexist"));
        } else if (validateemail.mailInvalid == 4002) {
          Toast.show(I18n.t("emailNotValid"));
        }
        else if (validateemail.username == 200) {
          this.socialRegisterApi();
        }
      })
      .catch(err => {
        console.log("socialemail err", err);
      });
  }

  socialRegisterApi = async () => {
    await apiClient
      .postWithoutToken("/register", {
        fullName: this.state.socialfullname,
        username: this.state.socialemail,
        mobileNumber: this.state.socialmobileInput,
        selectedCountry: '2',
        phoneCode: '00966',
        lang: this.state.lang,
        application_name: "Bitaqaty",
        providerId: this.state.providerId,
        provider: this.state.provider,
        isLogin: false,
        hasMail: this.state.hasmail
      })
      .then(async socialregister => {
        if (socialregister.userNameExists == 3002) {
          Toast.show(I18n.t("emailexist"));
          this.setState({
            showEmailOverley: true
          });
        } else if (socialregister.mobileNumber == 3006) {
          this.setState({
            showMobileOverley: true
          });
        } else if (socialregister.mailInvalid == 4002) {
          Toast.show(I18n.t("emailNotValid"));
        } else if (socialregister.fullName == 3008) {
          Toast.show(I18n.t("noName"));
        }
        else if (socialregister.error == "SECURITY_USER_EMAIL_NOT_VERIFIED") {
          AsyncStorage.setItem("mobileInput", this.state.socialmobileInput);
          AsyncStorage.setItem("email", this.state.socialemail);
          this.props.navigation.navigate("Email");
        }
        else if (socialregister.userNameEncrypted) {
          AsyncStorage.setItem("userName", socialregister.userNameEncrypted);
          AsyncStorage.setItem("email", this.state.socialemail);
          AsyncStorage.setItem("mobileInput", socialregister.mobileNumber);
          this.props.navigation.navigate("Mobile", { social: "social" })
        }
        else if (socialregister.userName) {
          AsyncStorage.setItem("userName", socialregister.userName);
          AsyncStorage.setItem("email", this.state.socialemail);
          AsyncStorage.setItem("mobileInput", socialregister.mobileNumber);
          this.props.navigation.navigate("Mobile", { social: "social" })
        }
      })
      .catch(err => {
        console.log("socialregister err", err);
      })
  }

  backArrowLogin = () => {
    this.props.navigation.navigate("SignUp");
  };

  goToSignup1 = () => {
    this.setState({ showMobileOverley: false });
    this.props.navigation.navigate("SignUp");
  };

  goToSignup = () => {
    this.setState({ showEmailOverley: false });
    this.props.navigation.navigate("SignUp");
  };

  goToForgetpassword = () => {
    this.props.navigation.navigate("ForgotPassword");
    this.setState({ showEmailOverley: false });
  };

  _goToLogin = () => {
    this.setState({ showMobileOverley: false, showEmailOverley: false })
    this.props.navigation.navigate("Login");
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainSection}>
          <View style={styles.headerWrap}>
            <RTLView locale={this.state.locale}>
              <View style={styles.headerLeftIcon}>
                <TouchableOpacity onPress={this.backArrowLogin}>
                  <Image
                    style={
                      this.state.locale == "en"
                        ? styles.arrow_en
                        : styles.arrow_ar
                    }
                    source={
                      this.state.locale == "en"
                        ? menuScreen.close
                        : menuScreen.close
                    }
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.headerLogo}>
                <Image
                  style={styles.logoImage}
                  source={menuScreen.headerLogo}
                />
                <RTLText style={styles.logoText}>{I18n.t("BITAQATY")}</RTLText>
              </View>
            </RTLView>
          </View>
          <View style={{ flex: 1 }}>
            <Modal transparent={true} visible={this.state.showEmailOverley}>
              <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
                <View style={styles.dupEmailSec}>
                  <View style={{ alignItems: "center", marginTop: "20%" }}>
                    <Image source={loginScreen.loginError} />
                    <Text style={styles.dupLoginBoldMsg}>
                      {I18n.t("Email address already in use")}
                    </Text>
                    <Text style={styles.dupLoginMsg}>
                      {I18n.t("This account already exists with the e-mail")}
                    </Text>
                    <Text style={styles.dupLoginEmail}>
                      {this.state.socialemail}
                    </Text>
                  </View>
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.dupText, commonStyle.none]
                      : [styles.dupText, commonStyle.rightAlign]
                  }>
                    {I18n.t("Are you a returning customer?")}
                  </Text>

                  <TouchableOpacity
                    style={styles.dupLoginBtn}
                    onPress={this._goToLogin}
                  >
                    <Text style={styles.dupLoginText}>{I18n.t("Login")}</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.dupLoginBtn}
                    onPress={this.goToForgetpassword}
                  >
                    <Text style={styles.dupLoginText}>
                      {I18n.t("Forgot your password?")}
                    </Text>
                  </TouchableOpacity>
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.dupText, commonStyle.none]
                      : [styles.dupText, commonStyle.rightAlign]
                  }>
                    {I18n.t("Your First time to visit?")}
                  </Text>

                  <TouchableOpacity
                    style={styles.dupLoginBtn}
                    onPress={this.goToSignup}
                  >
                    <Text style={styles.dupLoginText}>
                      {I18n.t("Try another email")}
                    </Text>
                  </TouchableOpacity>
                  <View style={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                    <Text
                      style={styles.modal1}
                    >
                      {I18n.t("For help, you can contact our")}{" "} </Text>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("ContactUs")}
                    >
                      <Text style={{
                        color: "#2700EB",
                        fontFamily: "Tajawal-Bold",
                        fontSize: 14
                      }}> {I18n.t("customer service")}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>

          <View style={{ flex: 1 }}>
            <Modal transparent={true} visible={this.state.showMobileOverley}>
              <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
                <View style={styles.dupEmailSec}>
                  <View style={{ alignItems: "center", marginTop: "20%" }}>
                    <Image source={loginScreen.loginError} />
                    <Text style={styles.dupLoginBoldMsg}>
                      {I18n.t("Mobile number already in use")}
                    </Text>
                    <Text style={styles.dupLoginMsg}>
                      {I18n.t(
                        "This account already exists with the mobile number"
                      )}
                    </Text>
                    <Text style={styles.dupLoginEmail}>
                      {this.state.socialmobileInput}
                    </Text>
                  </View>
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.dupText, commonStyle.none]
                      : [styles.dupText, commonStyle.rightAlign]
                  }>
                    {I18n.t("Are you a returning customer?")}
                  </Text>
                  <TouchableOpacity
                    style={styles.dupLoginBtn}
                    onPress={this._goToLogin}
                  >
                    <Text style={styles.dupLoginText}>{I18n.t("Login")}</Text>
                  </TouchableOpacity>
                  <Text
                    style={
                      this.state.locale == "eng"
                        ? [styles.dupText, commonStyle.none]
                        : [styles.dupText, commonStyle.rightAlign]
                    }>
                    {I18n.t("Your First time to visit?")}
                  </Text>
                  <TouchableOpacity
                    style={styles.dupLoginBtn}
                    onPress={this.goToSignup1}
                  >
                    <Text style={styles.dupLoginText}>
                      {I18n.t("Try another email")}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>

          <ScrollView>
            <View>
              <RTLView locale={this.state.locale}>
                <RTLText style={[styles.pageTitle, { marginLeft: 16, marginBottom: 16, marginTop: 16, marginRight: 16 }]}>
                  {I18n.t("Create New Account")}
                </RTLText>
              </RTLView>
            </View>
            <Text style={
                this.state.locale == "eng"
                  ? [styles.socialSubHead, commonStyle.none]
                  : [styles.socialSubHead, commonStyle.rightAlign]
              }>
                {I18n.t("please enter your email and mobile number, as an extra security step to secure your account")}
            </Text>
            <View style={[styles.container,{marginTop : 30}]}>

              <View style={[styles.formWrap, { marginTop: 0 }]}>
                <View style={[styles.formGroup1, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>{I18n.t("Name")}</Text>
                    </RTLView>
                  </View>
                  <TextInput
                    style={
                      this.state.locale == "eng"
                        ? styles.formControl_en
                        : styles.formControl_ar
                    }
                    value={this.state.socialfullname}
                    placeholder={I18n.t("Name")}
                    placeholderTextColor="#CCCCCC"
                    autoCapitalize='none'
                    editable={false}
                  />
                </View>

                <View style={[styles.formGroup1, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>{I18n.t("Email")}</Text>
                    </RTLView>
                  </View>
                  <TextInput
                    style={[
                      this.state.locale == "eng"
                        ? styles.formControl_en
                        : styles.formControl_ar,
                      (
                        this.state.socialemailnull === true || this.state.socialemailerror == true
                      ) && {
                        borderColor: "#ff0000"
                      }
                    ]}
                    value={this.state.socialemail}
                    onChangeText={socialemail =>
                      this.setState({
                        socialemail: socialemail, socialemailnull: false,
                        socialemailerror: false, hasmail: false
                      })
                    }
                    placeholder="example@example.com"
                    placeholderTextColor="#CCCCCC"
                    autoCapitalize='none'
                  />
                </View>
                {this.state.socialemailnull ? (
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.loginError]
                      : [styles.loginError, commonStyle.rightAlign]}>
                    {I18n.t("This field is required")}
                  </Text>
                ) : null}
                {this.state.socialemailerror ? (
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.loginError]
                      : [styles.loginError, commonStyle.rightAlign]}>
                    {I18n.t("mailFormat")}
                  </Text>
                ) : null}

                <View style={[styles.formGroup1, { marginTop: 20 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={[styles.fromText]}>
                        {I18n.t("Mobile Number")}
                        <Text style={styles.countryText}>
                          {I18n.t(" (Saudi) ")}
                        </Text>
                      </Text>
                    </RTLView>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? [commonStyle.rowSec, { marginBottom: 8 }]
                        : [commonStyle.rowSec_ar, { marginBottom: 8 }]
                    }
                  >
                    <View
                      style={[
                        this.state.locale == "eng"
                          ? styles.mobileInput
                          : styles.mobileInput_ar,
                        (
                          this.state.socialMobileNull === true || this.state.socialMobileError == true
                          || this.state.mobileNumberNumericError == true
                        ) && {
                          borderColor: "#ff0000"
                        }
                      ]}
                    >
                      <View style={styles.countryCode}>
                        <Text style={styles.countryCodeText}>966</Text>
                      </View>
                      <View></View>
                      <TextInput
                        onChangeText={socialmobileInput =>
                          this.setState({
                            socialmobileInput: socialmobileInput, socialMobileNull: false,
                            socialMobileError: false, mobileNumberNumericError: false
                          })
                        }
                        style={
                          this.state.locale == "eng"
                            ? styles.mobileTextInput
                            : styles.mobileTextInput_ar
                        }
                        placeholder=" 0000000000"
                        keyboardType="numeric"
                        underlineColorAndroid="transparent"
                        contextMenuHidden={true}
                      />
                    </View>
                    <View style={{ width: "15%", justifyContent: "center" }}>
                      <Image
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.flagImage
                            : commonStyle.flagImage_ar
                        }
                        source={menuScreen.flag2}
                      />
                    </View>
                  </View>
                  {this.state.socialMobileNull ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.loginError]
                        : [styles.loginError, commonStyle.rightAlign]}>
                      {I18n.t("This field is required")}
                    </Text>
                  ) : null}
                  {this.state.socialMobileError ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error1]
                        : [styles.error1, commonStyle.rightAlign]}>
                      {I18n.t("Length should be 9 numbers")}
                    </Text>
                  ) : null}
                  {this.state.mobileNumberNumericError ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t("Please enter numbers only")}
                    </Text>
                  ) : null}
                </View>

                {/* <View style={[styles.formGroup, { marginTop: 20 }]}>
                  <TouchableOpacity
                    onPress={() => this.socialConnectApi()}
                    style={styles.commonButton}
                  >
                    <Text style={styles.buttonText}>
                      {I18n.t("Verify")}
                    </Text>
                  </TouchableOpacity>
                </View> */}
                <View style={[styles.formGroup,{marginTop : 16}]}>
                    <TouchableOpacity
                      style={styles.commonButton}
                      onPress={() => this.socialConnectApi()}
                    >
                      <Text style={styles.buttonText}>{I18n.t("Verify")}</Text>
                      {/* {this.state.isLoading && (
                        <ActivityIndicator
                          size="small"
                          color="#fff"
                          style={{
                            position: "absolute",
                            right: 30
                          }}
                        />
                      )} */}
                    </TouchableOpacity>
                  </View>
                  <View
                style={this.state.locale == "eng" ? styles.row : styles.row_ar}
              >
                <Text style={styles.cusText}>
                  {I18n.t("By continuing, you agree to Bitaqaty")}
                </Text>
                <TouchableOpacity>
                  <Text style={styles.cusSrvc}> {I18n.t("Terms of use")} </Text>
                </TouchableOpacity>
                <Text style={styles.cusText}>{I18n.t("and")}</Text>
                <TouchableOpacity>
                  <Text style={styles.cusSrvc}>
                    {" "}
                    {I18n.t("Privacy policy")}{" "}
                  </Text>
                </TouchableOpacity>
              </View>
              </View>
              <View style={commonStyle.rights}>
                <RTLView locale={this.state.locale}>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                  </Text>
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, fontFamily: "Tajawal-Bold" }
                    ]}
                  >
                    {I18n.t(" Bitaqaty ")}
                  </Text>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("@2019")}
                  </Text>
                </RTLView>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(facebookSignUp);
