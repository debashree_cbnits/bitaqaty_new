import React from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert,
  NativeModules,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen, menuScreen } from "../../../config/imageConst";
import { connect } from "react-redux";

import apiClient from "../../../services/api.client";
import { GoogleSignin, GoogleSigninButton, statusCodes } from "@react-native-community/google-signin";
import Toast from "react-native-tiny-toast";
// import { FBLoginManager } from "react-native-facebook-login";
import {
  LoginButton,
  AccessToken,
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';


const { RNTwitterSignIn } = NativeModules
// GoogleSignin.configure({
//   //scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
//   //iosClientId: '343711898192-h76ifijr02836bojvho2r7n81qovq8io.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
//   iosClientId: '126768512754-fcuklbg8p85jl7cav27nejan2ohb5oel.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
//   webClientId: '126768512754-oji1ep986i77s1p2lqalsblo9tt0p4sn.apps.googleusercontent.com',
//   // webClientId: "33602206675-c24ec81npqpn719opgpp6dt6b6sl168o.apps.googleusercontent.com",
//   offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
//   hostedDomain: '', // specifies a hosted domain restriction
//   loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
//   forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
//   accountName: '', // [Android] specifies an account name on the device that should be used
// });

GoogleSignin.configure({
  scopes: ["https://www.googleapis.com/auth/drive.readonly"], 
  webClientId:
    "33602206675-sk89db5rf09lbu4en1cm51u7497ii98c.apps.googleusercontent.com", 
  offlineAccess: true, 
  forceCodeForRefreshToken: true 
});

const twitterConstants = {
    //Dev Parse keys
    TWITTER_COMSUMER_KEY: "appPEVCRTg9eKm5OfT3xvEal1",
    TWITTER_CONSUMER_SECRET: "X2di9pGHUMUCAjLBQS5xZW71ylVZ0qpuKtJv6EnrfwhtKwVYce"
  }

class SocialLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      fbAccessId: "",
      picture: "",
      google_signup_id: ""
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  componentDidMount(){
    RNTwitterSignIn.init(twitterConstants.TWITTER_COMSUMER_KEY, twitterConstants.TWITTER_CONSUMER_SECRET)
  }

  twitterLogin = () => {    
    RNTwitterSignIn.logIn()
      .then(loginData => {
        console.log(loginData)
        const { authToken, authTokenSecret, email, name, userID, userName } = loginData;
        this.props.goToHome({
          social_name: name, 
          social_email: email, 
          social: "twitter", 
          userID, 
          userName,
          authToken, 
          authTokenSecret });
      })
      .catch(error => {
        console.log(error)
      }
    )
  }

  faceBookLogin = async () => {
    LoginManager.logOut();
    try {
      const userInfo = await LoginManager.logInWithPermissions(["public_profile", "email"]);
      if (userInfo.isCancelled) {
      } else {
        AccessToken.getCurrentAccessToken().then(
          (data) => {
            let accessToken = data.accessToken.toString();
            if (accessToken) {
              fetch('https://graph.facebook.com/v2.5/me?fields=email,name,picture,friends&access_token=' + accessToken)
                .then((response) => response.json())
                .then((json) => {
                  let email = json.email;
                  let name = json.name;
                  let fbAccessId = json.id;
                  let picture = json.picture.data.url;

                  this.fbsucess(email, name, fbAccessId, picture);
                })
                .catch(() => {
                  reject('ERROR GETTING DATA FROM FACEBOOK')
                })
            }
          }
        )
      }
    } catch (error) {
      console.log("signin error", error);
    }
  }

  fbsucess = (email, name, fbAccessId, picture) => {
    this.setState({ email, name, fbAccessId, picture })
    let backvalue = { social_name: name, social_email: email, social_Id: fbAccessId, social: "facebook" };
    this.props.goToHome(backvalue);
  }

  isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    console.log(isSignedIn, 'isSignedIn=====')

    if (isSignedIn == true) {
      this.signInSilent();
    }
    else {
      this.signInFirst();
    }
  };

  signInSilent = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      let name = userInfo.user.name;
      let first_name = userInfo.user.givenName;
      let last_name = userInfo.user.familyName;
      let email = userInfo.user.email;
      let picture = userInfo.user.photo;
      let google_signup_id = userInfo.user.id;

      this.signUpSuccess(email, name, google_signup_id, picture);
    } catch (error) {
      console.log("err", error);
    }

  };

  signInFirst = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo, 'userInfo')
      var google_data = JSON.stringify(userInfo);
      let name = userInfo.user.name;
      let first_name = userInfo.user.givenName;
      let last_name = userInfo.user.familyName;
      let email = userInfo.user.email;
      let picture = userInfo.user.photo;
      let google_signup_id = userInfo.user.id;

      this.signUpSuccess(email, name, google_signup_id, picture);

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log("SIGN_IN_CANCELLED-error", error);
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.log("IN_PROGRESS-error", error);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log("PLAY_SERVICES_NOT_AVAILABLE-error", error);
      } else {
        // some other error happened
        console.log("other-error", error);
      }
    }
  }

  signUpSuccess = (email, name, google_signup_id, picture) => {
    this.setState({ email, name, google_signup_id, picture })
    let backvalue = { social_name: name, social_email: email, social_Id: google_signup_id, social: "google" };
    this.props.goToHome(backvalue);
  }

  render() {
    return (
      <View>
        <TouchableOpacity
          onPress={this.signInFirst}
          style={
            this.state.locale == "eng"
              ? styles.socialLogin_en
              : styles.socialLogin_ar
          }
        >
          <RTLView locale={this.state.locale}>
            <Image
              source={loginScreen.googleIcon}
              style={styles.socialImg}
            />
            <View style={{ justifyContent: "center" }}>
              <Text style={styles.socialText}>
                {I18n.t(this.props.socialTextName)}{" "}
                <Text style={styles.socialBoldText}>
                  {I18n.t("Google")}
                </Text>
              </Text>
            </View>
          </RTLView>
        </TouchableOpacity>

        <TouchableOpacity
          style={
            this.state.locale == "eng"
              ? styles.socialLogin_en
              : styles.socialLogin_ar
          }
          onPress={this.faceBookLogin}
        >
          <RTLView locale={this.state.locale}>
            <Image
              source={loginScreen.facebookIcon}
              style={styles.socialImg}
            />
            <View style={{ justifyContent: "center" }}>
              <Text style={styles.socialText}>
                {I18n.t(this.props.socialTextName)}{" "}{" "}
                <Text style={styles.socialBoldText}>
                  {I18n.t("Facebook")}
                </Text>
              </Text>
            </View>
          </RTLView>
        </TouchableOpacity>

        <TouchableOpacity onPress={ ()=>this.twitterLogin ()}
          style={
            this.state.locale == "eng"
              ? styles.socialLogin_en
              : styles.socialLogin_ar
          }
        >
          <RTLView locale={this.state.locale}>
            <Image
              source={loginScreen.twitterIcon}
              style={styles.socialImg}
            />
            <View  style={{ justifyContent: "center" }}>
              <Text style={styles.socialText}>
                {I18n.t(this.props.socialTextName)}{" "}{" "}
                <Text style={styles.socialBoldText}>
                  {I18n.t("Twitter")}
                </Text>
              </Text>
            </View >
          </RTLView>
        </TouchableOpacity>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(SocialLogin);