import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { connect } from "react-redux";

class MobileVerification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: ""
    };
  }

  static getDerivedStateFromProps(props, state) {    

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }

  // openEmail = () => {
  //   this.props.navigation.navigate("EmailVerification");
  // };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainSection}>
          <View style={styles.logoHead}>
            <Image
              style={{ height: 30, width: 24.5 }}
              source={require("../../assets/image/logo.png")}
            />
          </View>
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.top}>
                <Text style={styles.pageTitle}>
                  {I18n.t("Mobile Number Verification")}
                </Text>

                <View style={[styles.formWrap, { marginTop: 10 }]}>
                  <Text style={styles.linkLight}>
                    {I18n.t("Your verification code has been sent to")}
                  </Text>
                  <Text
                    style={
                      this.state.locale == "eng"
                        ? styles.mobNumber
                        : styles.mobNumber_ar
                    }
                  >
                    9661234567
                  </Text>
                  <Text style={styles.linkLight}>
                    {I18n.t("You'll find the code inside your mobile messages")}
                  </Text>
                  <Text style={styles.linkLight}>
                    {I18n.t("Please enter the 6 digit verification code")}
                  </Text>

                  <View style={[styles.formGroup, { marginTop: 30 }]}>
                    <Text style={styles.fromText}>
                      {I18n.t("Verification Code")}
                    </Text>
                    <TextInput
                      style={styles.otpField}
                      placeholder="0 0 0 0 0 0"
                      keyboardType="numeric"
                      maxLength={6}
                      placeholderTextColor="#CCCCCC"
                    />
                  </View>

                  <View style={styles.formGroup}>
                    <TouchableOpacity
                      onPress={this.onClickBtn}
                      style={styles.commonButton}
                    >
                      <Text style={styles.buttonText}>
                        {I18n.t("Activate the Account")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  
                    <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                      <Text style={styles.cusText}>{I18n.t("If you do not receive the message, you can")}</Text>
                      <TouchableOpacity>
                        <Text style={styles.cusSrvc}>{" "}{I18n.t("Resend verification code")}{" "}</Text>  
                      </TouchableOpacity>
                    </View>
                  

                </View>
              </View>
              {/* <TouchableOpacity onPress={this.openEmail}>
                <Text>Email</Text>
              </TouchableOpacity> */}

              <View style={commonStyle.rights}>
                <RTLView locale={this.state.locale}>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                    <Text
                      style={[
                        commonStyle.screenText,
                        { fontSize: 12, fontFamily: "Tajawal-Bold" }
                      ]}
                    >
                      {I18n.t(" Bitaqaty ")}
                    </Text>
                    <RTLText style={[commonStyle.screenText, { fontSize: 12 }]}>
                      {I18n.t("@2019")}
                    </RTLText>
                  </Text>
                </RTLView>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(
  mapStateToProps,
  {}
)(MobileVerification);
