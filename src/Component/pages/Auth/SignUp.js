import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Modal
} from "react-native";
import styles from "./style";
import { Button } from "react-native-elements";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen, menuScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
import apiClient from "../../../services/api.client";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import AsyncStorage from "@react-native-community/async-storage";
import SocialLogin from "./SocialLogin";
import Toast from "react-native-tiny-toast";



class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      isRegistering: false,
      errorText: "",
      mobileInput: "",
      passwordImage: true,
      phoneCode: "",
      currency: "",
      countryCode: 0,
      confirmPasswordImage: true,
      newPassword: "",
      showPassword: true,
      showConfirmPassword: true,
      confirmPassword: "",
      locale: "",
      language: "",
      show: false,
      passError: "",
      socialTextName: "Sign up with",
      passError1: false,
      passError2: false,
      passError3: false,
      passError4: false,
      passError5: false,
      passError6: false,
      newPassValidation: false,
      nameError: false,
      nameError1: false,
      emailError: false,
      emailError1: false,
      emailError2: false,
      passwordError: false,
      confirmPasswordError: false,
      mobileNumberError: false,
      mobileNumberNumericError: false,
      socialinfo: "",
      duplicatemobileerror: false,
      duplicateemailerror: false,
      showEmailOverley: false,
      showMobileOverley: false,
      isLoading: false
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          lang: "en"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          lang: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        lang: "en"
      };
    }
  }

  backArrowLogin = () => {
    this.props.navigation.navigate("Login");
  };

  _goToLogin = () => {
    this.setState({ showMobileOverley: false, showEmailOverley: false })
    this.props.navigation.navigate("Login");
  };

  goToForgetpassword = () => {
    this.setState({ showEmailOverley: false });
    this.props.navigation.navigate("ForgotPassword");
  };


  custonerSerVice = () => {
    this.setState({ showMobileOverley: false, showEmailOverley: false })
    this.props.navigation.navigate("ContactUs");
  }

  goToSignup = () => {
    this.setState({ showEmailOverley: false });
  };

  goToSignup1 = () => {
    this.setState({ showMobileOverley: false });
  };

  async componentDidMount() {
    await apiClient
      .getRequest("/bitaqatySettings")
      .then(async currency => {
        await this.setState({
          currency: currency.currency.symbol
        });

        await apiClient
          .postRequest("/lookup", {
            currencyStandardSymbol: this.state.currency,
            discriminatorValue: 1
          })
          .then(async phoneCountry => {
            await this.setState({
              phoneCode: phoneCountry.phoneCode,
              countryCode: phoneCountry.id
            });
          })
          .catch(async err => {
            // this.setState({ errorText: "error" });
          });
      })
      .catch(async err => {
        // this.setState({ errorText: "error" });
      });

      this.props.navigation.addListener("willFocus", () => {
        this.setState({
        nameError : false,
        nameError1 : false,
        emailError :  false,
        emailError1 : false,
        emailError2 : false,
        passwordError : false,
        confirmPasswordError : false,
        mobileNumberError : false,
        mobileNumberNumericError: false,
        name: "",
        email: "",
        errorText: "",
        mobileInput: "",
        newPassword: "",
        confirmPassword: "",
        })
      });
  }

  emptyvalidation = () => {
    if(this.state.name === ""){
      this.setState({ nameError : true, })
    }
    if(this.state.email === ""){
      this.setState({ emailError : true, emailError1 : false})
    }
    if(this.state.newPassword === ""){
      this.setState({ passwordError : true, passError1: false, passError2:  false,
        passError3:  false,
        passError4:  false,
        passError5:  false,
        passError6: false, passError : false})
    }
    if(this.state.confirmPassword === ""){
      this.setState({ confirmPasswordError : true, })
    }
    if(this.state.mobileInput === ""){
      this.setState({ mobileNumberError : true, })
    }
  }

  _signUp = async () => {
    this.emptyvalidation();
    if (this.state.name === "" && this.state.email === "" && this.state.newPassword === "" && this.state.confirmPassword === ""
      && this.state.mobileInput === "") {
      this.setState({ nameError: true, emailError: true, passwordError: true, confirmPasswordError: true, mobileNumberError: true });
      return;
    }
    if (this.state.name === "") {
      this.setState({ nameError: true, nameError1: false });
      return;
    }

    let regex1 = /^[a-zA-Z ]*$/;
    let numRegex = /^[0-9]*$/;

    if (!regex1.test(this.state.name)) {
      this.setState({ nameError1: true, nameError: false });
      return
    }

    if (this.state.email === "") {
      this.setState({ emailError: true, emailError1: false, emailError2: "" });
      return;
    }

    let regex2 = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (!regex2.test(this.state.email)) {
      this.setState({ emailError1: true, emailError: false, emailError2: false });
      return
    }

    if (this.state.newPassword === "") {
      this.setState({ passError: true, newPassValidation: false })
      return;
    }

    if (this.state.confirmPassword === "") {
      this.setState({ confirmPasswordError: true, confirmPasswordError1: false })
      return;
    }

    if (this.state.newPassword !== this.state.confirmPassword && this.state.passError == "") {
      this.setState({ confirmPasswordError1: true, confirmPasswordError: false })
      return;
    }

    if (this.state.mobileInput === "") {
      this.setState({ mobileNumberError: true, mobileNumberError1: false, mobileNumberError2: false, mobileNumberNumericError: false })
      return;
    }

    if (!numRegex.test(this.state.mobileInput)) {
      this.setState({ mobileNumberNumericError: true, mobileNumberError1: false, mobileNumberError: false, mobileNumberError2: false })
      return
    }

    if (this.state.mobileInput.length != 9) {
      this.setState({ mobileNumberError1: true, mobileNumberError: false, mobileNumberError2: false, mobileNumberNumericError: false, })
      return
    }

    if (this.state.emailError2 == false && this.state.newPassValidation == false) {
      this.setState({
        isRegistering: true
      });
      AsyncStorage.setItem("email", this.state.email);
      AsyncStorage.setItem("password", this.state.password);
      // AsyncStorage.setItem("mobileInput", this.state.mobileInput);
      AsyncStorage.setItem("mobileInput", 966+this.state.mobileInput);
      AsyncStorage.removeItem('userName');
      AsyncStorage.removeItem('activationCode');
      let signUpData = {
        fullName: this.state.name,
        username: this.state.email,
        password: this.state.newPassword,
        confirmPassword: this.state.confirmPassword,
        selectedCountry: 2,
        mobileNumber: this.state.mobileInput,
        phoneCode: "00966",
        receiveNewsLetter: null,
        captcha: "",
        application_name: "Bitaqaty",
        lang: this.state.lang
      }


      await apiClient
        .postRequest("/register",
          signUpData)
        .then(async registerSuccess => {

          this.setState({ isRegistering: false });
          if (registerSuccess.mobileNumber == 220) {
            this.setState({
              isRegistering: false,
              mobileNumberError2: true
            });
          } else if (registerSuccess.mobileNumber == 3006) {
            this.setState({
              duplicatemobileerror: true
            });
          } else if (registerSuccess.userNameExists == 3002) {
            this.setState({
              duplicateemailerror: true
            });
          } else if (registerSuccess.mailInvalid == 4002) {
            this.setState({ emailError2: true, isRegistering: false });
          } else if (registerSuccess.password !== undefined) {
            this.setState({ isRegistering: false, confirmPasswordError1: true });
          } else if (registerSuccess.confirmPassword !== undefined) {
            this.setState({ confirmPasswordError1: true, isRegistering: false });
          } else if (registerSuccess.success == 200) {
            this.setState({ errorText: "SignUpSuccess", isRegistering: false });
            this.props.navigation.navigate("Email");
            // this.props.navigation.navigate("Mobile",{social : "non"});
          }
        })
        .catch(err => {
          this.setState({ errorText: "error", isRegistering: false });
        });
    }
  };

  changePwdType = type => {
    if (type === "password") {
      this.setState({
        passwordImage: !this.state.passwordImage,
        showPassword: !this.state.showPassword
      });
    } else {
      this.setState({
        confirmPasswordImage: !this.state.confirmPasswordImage,
        showConfirmPassword: !this.state.showConfirmPassword
      });
    }
  };

  nameValidCheck = () => {
    let nameRegex = /^[a-zA-Z\u0621-\u064A ]*$/
    let value = this.state.name;
    if (!nameRegex.test(value)) {
      this.setState({ nameError1: true, nameError: false });
    }
  }

  confirmPassCheck = () => {
    if (this.state.newPassword !== this.state.confirmPassword) {
      this.setState({ confirmPasswordError1: true, confirmPasswordError: false })
    }
  }

  passValidation = (value) => {
    const rg2 = new RegExp(/^.{8,}$/);
    const rg3 = new RegExp(/(?=.*[a-z])/);
    const rg4 = new RegExp(/(?=.*[A-Z])/);
    const rg5 = new RegExp(/(?=.*\d)/);
    rg2.test(value)
      ? this.setState({ passError2: false, newPassValidation: false, error2: false })
      : this.setState({ newPassValidation: true, passError2: true, passError: "errorpass", error2: true });
    rg3.test(value)
      ? this.setState({ passError3: false, newPassValidation: false, error2: false })
      : this.setState({ newPassValidation: true, passError3: true, passError: "errorpass", error2: true });
    rg4.test(value)
      ? this.setState({ passError4: false, newPassValidation: false, error2: false })
      : this.setState({ newPassValidation: true, passError4: true, passError: "errorpass", error2: true });
    rg5.test(value)
      ? this.setState({ passError5: false, newPassValidation: false, error2: false })
      : this.setState({ newPassValidation: true, passError5: true, passError: "errorpass", error2: true });
      rg4.test(value) || rg3.test(value)
        ? this.setState({ passError6: false, newPassValidation: false, error2: false })
        : this.setState({ passError6: true, newPassValidation: true, passError: "errorpass", error2: true  });
    if (
      rg2.test(value) &&
      rg3.test(value) &&
      rg4.test(value) &&
      rg5.test(value)
    ) {
      this.setState({ passError1: false, passError: "" });
    } else {
      this.setState({ newPassValidation: true, passError1: true, error2: true });
    }

  }

  passFocus = (value) => {
    this.setState({ passwordError: false })
    let emailRegex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/
    let email = this.state.email;
    if (email.length > 0 && !emailRegex.test(email)) {
      this.setState({ emailError1: true, emailError: false, emailError2: false });
    }
    if (this.state.newPassword.length == 0) {
      this.setState({
        newPasswordError: false, newPassValidation: true, errorText: '', passError1: true, passError2: true,
        passError3: true, passError4: true, passError5: true, error2: true, passError6: true
      });
    }
    else {
      this.passValidation(this.state.newPassword)
    }
  }

  goToHomePage = async (value) => {
    // this.setState({isLoading: true})
    if (value) {
      this.setState({
        socialinfo: value
      })
      this.socialSignUp();
    }
  }

  validation = (value, fieldName) => {
    let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    //let regex = /^\w([\.-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (fieldName == "socialmobile") {
      if (value.length != 9) {
        this.setState({ socialMobileError: true })
      }
      else {
        this.setState({ socialMobileError: false })
      }
    }
    else if (fieldName == "socialemail") {
      let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
      if (!regex.test(value)) {
        this.setState({ socialemailerror: true });
      } else {
        this.setState({ socialemailerror: false });
      }
    }
  };

  socialSignUp = async () => {
    let value = this.state.socialinfo;
    this.setState({ socialemail: value.social_email })
    if (value.social == "facebook") {
      let providername = 'FACEBOOK';
      await apiClient
        .postWithoutToken("/validate-email", {
          fullName: value.social_name,
          username: value.social_email,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: value.social_Id,
          provider: providername,
          isLogin: false,
        })
        .then(async response => {
          this.setState({isLoading:false})
          if (response.username && response.username == 200) {
            this.props.navigation.navigate("facebookSignUp", {
              fullname: value.social_name ? value.social_name : "",
              email: value.social_email ? value.social_email : "",
              providerid: value.social_Id ? value.social_Id : ""
            })
          }
          else if (response.userNameExists && response.userNameExists == 3002) {
            if (response.userIdExist && response.userIdExist == 7000) {
              this.socialConnectApi(value);
            }
            else {
              this.setState({
                showEmailOverley: true
              });
            }
          }
          else if (response.userIdExist && response.userIdExist == 7000) {
            this.socialConnectApi(value);
          }
          else if (response.mailInvalid && response.mailInvalid == 4002) {
            this.props.navigation.navigate("facebookSignUp", {
              fullname: value.social_name ? value.social_name : "",
              email: value.social_email ? value.social_email : "",
              providerid: value.social_Id ? value.social_Id : ""
            })
          }
          else if (response.emptyEmail && response.emptyEmail == 2002) {
            if (response.userIdExist && response.userIdExist == 7000) {
              this.socialConnectApi(value);
            }
            else {
              this.props.navigation.navigate("facebookSignUp", {
                fullname: value.social_name ? value.social_name : "",
                email: value.social_email ? value.social_email : "",
                providerid: value.social_Id ? value.social_Id : ""
              })
            }
          }
        })
        .catch(err => {
          this.setState({isLoading: false})
        });
    }
    else if (value.social == "google") {
      let providername = 'GMAIL';
      await apiClient
        .postWithoutToken("/validate-email", {
          fullName: value.social_name,
          username: value.social_email,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: value.social_Id,
          provider: providername,
          isLogin: false,
        })
        .then(async response => {
          if (response.username && response.username == 200) {
            this.props.navigation.navigate("googleSignUp", {
              fullname: value.social_name ? value.social_name : "",
              email: value.social_email ? value.social_email : "",
              providerid: value.social_Id ? value.social_Id : ""
            })
          }
          else if (response.userNameExists && response.userNameExists == 3002) {
            if (response.userIdExist && response.userIdExist == 7000) {
              this.socialConnectApi(value);
            }
            else {
              this.setState({
                showEmailOverley: true
              });
            }
          }
          else if (response.mailInvalid && response.mailInvalid == 4002) {
            this.props.navigation.navigate("googleSignUp", {
              fullname: value.social_name ? value.social_name : "",
              email: value.social_email ? value.social_email : "",
              providerid: value.social_Id ? value.social_Id : ""
            })
          }
          else if (response.emptyEmail && response.emptyEmail == 2002) {
            if (response.userIdExist && response.userIdExist == 7000) {
              this.socialConnectApi(value);
            }
            else {
              this.props.navigation.navigate("googleSignUp", {
                fullname: value.social_name ? value.social_name : "",
                email: value.social_email ? value.social_email : "",
                providerid: value.social_Id ? value.social_Id : ""
              })
            }
          }
        })
        .catch(err => {
        });
    }
  }
  socialConnectApi = async (value) => {
    if (value.social == "facebook") {
      await apiClient
        .postWithoutToken("/social-connect", {
          fullName: value.social_name,
          username: value.social_email,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: value.social_Id,
          provider: "FACEBOOK",
          isLogin: true
        })
        .then(async FBregisterSuccess => {
          if (FBregisterSuccess.userToken) {
            this.setState({ userToken: FBregisterSuccess.userToken, isLoading: false });
            AsyncStorage.setItem("userToken", FBregisterSuccess.userToken);
            AsyncStorage.setItem("loginEmailId", value.social_email);
            AsyncStorage.setItem("loginuserName", FBregisterSuccess.fullName);
            AsyncStorage.setItem("userDetails", JSON.stringify(FBregisterSuccess));
            let sociallogintype = "socialloginfb";
            AsyncStorage.setItem("socialLoginType", sociallogintype);
            let logintype = "sociallogin";
            AsyncStorage.setItem("loginTypeInfo", logintype);

            this.props.navigation.navigate("LoginSuccessFB");
          }
          else if (FBregisterSuccess.userNameExists == 3002) {
            this.setState({
              showEmailOverley: true
            });
          }
          else if (FBregisterSuccess.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
            AsyncStorage.setItem("userName", FBregisterSuccess.userName);
            AsyncStorage.setItem("mobileInput", FBregisterSuccess.mobileNumber);
            AsyncStorage.setItem("email", value.social_email);
            this.props.navigation.navigate("Mobile", { social: "social" })
          }
        })
        .catch(err => {
        });
    }

    else if (value.social == "google") {
      await apiClient
        .postWithoutToken("/social-connect", {
          fullName: value.social_name,
          username: value.social_email,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: value.social_Id,
          provider: "GMAIL",
          isLogin: true
        })
        .then(async GMAILregisterSuccess => {
          if (GMAILregisterSuccess.userToken) {
            this.setState({ userToken: GMAILregisterSuccess.userToken, isLoading: false });
            AsyncStorage.setItem("userToken", GMAILregisterSuccess.userToken);
            AsyncStorage.setItem("loginEmailId", value.social_email);
            AsyncStorage.setItem("loginuserName", GMAILregisterSuccess.fullName);
            AsyncStorage.setItem("userDetails", JSON.stringify(GMAILregisterSuccess));
            let sociallogintype = "sociallogingmail";
            AsyncStorage.setItem("socialLoginType", sociallogintype);
            let logintype = "sociallogin";
            AsyncStorage.setItem("loginTypeInfo", logintype);
            this.props.navigation.navigate("LoginSuccess");
          }
          else if (GMAILregisterSuccess.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
            AsyncStorage.setItem("userName", GMAILregisterSuccess.username);
            AsyncStorage.setItem("mobileInput", GMAILregisterSuccess.mobileNumber);
            AsyncStorage.setItem("email", value.social_email);
            this.props.navigation.navigate("Mobile", { social: "social" })
          }
          else if (GMAILregisterSuccess.userNameExists == 3002) {
            this.setState({
              showEmailOverley: true
            });
          }
        })
        .catch(err => {
        });
    }
  }


  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.headerWrap}>
          <RTLView locale={this.state.locale}>
            <View style={styles.headerLeftIcon}>
              <TouchableOpacity onPress={this.backArrowLogin}>
                <Image
                  style={
                    this.state.locale == "eng"
                      ? styles.arrow_en
                      : styles.arrow_ar
                  }
                  source={
                    this.state.locale == "eng"
                      ? menuScreen.close
                      : menuScreen.close
                  }
                />
              </TouchableOpacity>
            </View>
            <View style={styles.headerLogo}>
              <Image style={styles.logoImage} source={menuScreen.headerLogo} />
              <RTLText style={styles.logoText}>{I18n.t("BITAQATY")}</RTLText>
            </View>
          </RTLView>
        </View>
        
        <View style={{ flex: 1 }}>
          <Modal transparent={true} visible={this.state.showEmailOverley}>
            <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
              <View style={styles.dupEmailSec}>
                <View style={{ alignItems: "center", marginTop: "20%" }}>
                  <Image source={loginScreen.loginError} />
                  <Text style={styles.dupLoginBoldMsg}>
                    {I18n.t("Email address already in use")}
                  </Text>
                  <Text style={styles.dupLoginMsg}>
                    {I18n.t("This account already exists with the e-mail")}
                  </Text>
                  <Text style={styles.dupLoginEmail}>
                    {this.state.socialemail}
                  </Text>
                </View>
                <Text style={
                  this.state.locale == "eng"
                    ? [styles.dupText, commonStyle.none]
                    : [styles.dupText, commonStyle.rightAlign]
                }>
                  {I18n.t("Are you a returning customer?")}
                </Text>

                <TouchableOpacity
                  style={styles.dupLoginBtn}
                  onPress={this._goToLogin}
                >
                  <Text style={styles.dupLoginText}>{I18n.t("Login")}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.dupLoginBtn}
                  onPress={this.goToForgetpassword}
                >
                  <Text style={styles.dupLoginText}>
                    {I18n.t("Forgot your password?")}
                  </Text>
                </TouchableOpacity>
                <Text style={
                  this.state.locale == "eng"
                    ? [styles.dupText, commonStyle.none]
                    : [styles.dupText, commonStyle.rightAlign]
                }>
                  {I18n.t("Your First time to visit?")}
                </Text>

                <TouchableOpacity
                  style={styles.dupLoginBtn}
                  onPress={this.goToSignup}
                >
                  <Text style={styles.dupLoginText}>
                    {I18n.t("Try another email")}
                  </Text>
                </TouchableOpacity>
                <View style={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                  <Text
                    style={styles.modal1}
                  >
                    {I18n.t("For help, you can contact our")}{" "} </Text>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("ContactUs")}
                  >
                    <Text style={{
                      color: "#2700EB",
                      fontFamily: "Tajawal-Bold",
                      fontSize: 14
                    }}> {I18n.t("customer service")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
        <SafeAreaView>
          {this.state.errorText !== "" &&
            this.state.errorText !== "SignUpSuccess" && (
              <View style={styles.loginErrorDisplayOuterView}>
                <RTLView
                  style={[
                    styles.loginErrorDisplayInnerView,
                    this.state.errorText === "SignUpSuccess" && {
                      borderColor: "#008000"
                    }
                  ]}
                  locale={this.state.locale}
                >
                  <Ionicons
                    name={Platform.OS === "android" ? "md-alert" : "ios-alert"}
                    size={25}
                    color="#FF0000"
                  />
                  <Text style={styles.viewErrorText}>
                    {I18n.t(this.state.errorText)}
                  </Text>
                </RTLView>
              </View>
            )}
            {this.state.isLoading ? (
            <ActivityIndicator size="small" />
            ) : (
          <ScrollView>
            <View style={styles.container}>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={styles.pageTitle}>{I18n.t("Welcome")}</Text>
                </RTLView>
              </View>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={styles.loginText}>
                    {I18n.t("Create new Bitaqaty account")}
                  </Text>
                </RTLView>
              </View>

              <SocialLogin
                socialTextName={this.state.socialTextName}
                goToHome={value => this.goToHomePage(value)}
              />

              <Text style={styles.orText}>{I18n.t("Or")}</Text>
              <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                <Image
                  style={styles.stepsImg22}
                  resizeMode={"contain"}
                  source={
                    this.state.locale == "eng"
                      ? loginScreen.sighnupscreenImage
                      : loginScreen.createAccount1_Y
                  }
                />
              </View>

              <View style={[styles.formWrap, { marginTop: 0 }]}>
                <View style={[styles.formGroup, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>{I18n.t("Name")}</Text>
                    </RTLView>
                  </View>

                  <TextInput
                    style={[
                      this.state.locale == "eng"
                        ? styles.formControl
                        : [styles.formControl, commonStyle.align_ar],
                      (
                        this.state.nameError == true || this.state.nameError1 == true
                      ) && {
                        borderColor: "#ff0000"
                      }
                    ]}
                    placeholder={I18n.t("Full Name")}
                    value={this.state.name}
                    onChangeText={(text) => this.setState({ name: text, nameError: "", nameError1: "" })}
                    placeholderTextColor="#CCCCCC"
                  />
                  {this.state.nameError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("This field is required")}
                    </Text>
                  ) : null}
                  {this.state.nameError1 ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t("namevalidation")}
                    </Text>
                  ) : null}

                </View>
                <View style={[styles.formGroup, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>{I18n.t("Email")}</Text>
                    </RTLView>
                  </View>
                  <TextInput
                    style={[
                      this.state.locale == "eng"
                        ? styles.formControl
                        : [styles.formControl, commonStyle.align_ar],
                      (
                        this.state.emailError == true || this.state.emailError1 == true
                        || this.state.emailError2 == true || this.state.duplicateemailerror) && {
                        borderColor: "#ff0000"
                      }
                    ]}
                    onFocus={this.nameValidCheck}
                    value={this.state.email}
                    onChangeText={(text) => this.setState({ email: text, emailError: false, emailError1: false, emailError2: false, duplicateemailerror: false })}
                    placeholder="example@example.com"
                    placeholderTextColor="#CCCCCC"
                    autoCapitalize='none'
                  />
                  {this.state.emailError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("This field is required")}
                    </Text>
                  ) : null}

                  {this.state.emailError1 ? (
                    <View
                      style={
                        this.state.locale == "eng"
                          ? [styles.loginError]
                          : [styles.loginError, commonStyle.rightAlign]
                      }
                    >
                      <View style={
                          this.state.locale == "eng"
                            ? styles.mailExm_en
                            : styles.mailExm_ar
                        }>
                        <Text style={{ color: "red", flexDirection: 'row-reverse', fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("Mail should be like")}
                        <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}> example@example.com </Text>
                      </Text>
                      </View>
                    </View>
                  ) : null}

                  {this.state.emailError2 ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t("emailNotValid")}
                    </Text>
                  ) : null}
                  {this.state.duplicateemailerror ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t("The email already registered")}
                    </Text>
                  ) : null}
                </View>

                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>{I18n.t("Password")}</Text>
                    </RTLView>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? styles.rowEnglish
                        : styles.rowArabic
                    }
                  >
                    <TextInput
                      style={[
                        this.state.locale == "eng"
                          ? styles.formControlPasswordField
                          : [
                            styles.formControlPasswordField,
                            { textAlign: "right" }
                          ],
                        (
                          this.state.passwordError == true || this.state.newPassValidation == true) && {
                          borderColor: "#ff0000"
                        }
                      ]}

                      onChangeText={newPassword =>
                        this.setState({ newPassword }, () => {
                          this.passValidation(newPassword);
                        })
                      }

                      onFocus={text => this.passFocus(text)}
                      placeholder="************"
                      placeholderTextColor="#CCCCCC"
                      returnKeyType="go"
                      password={true}
                      autoCorrect={false}
                      value={this.state.password}
                      secureTextEntry={this.state.showPassword}
                    />
                    <View style={{ width: "15%", justifyContent: "center" }}>
                      <TouchableOpacity
                        onPress={() => this.changePwdType("password")}
                      >
                        {this.state.passwordImage ? (
                          <Image
                            source={loginScreen.password}
                            style={
                              this.state.locale == "eng"
                                ? { alignSelf: "flex-end" }
                                : { alignSelf: "flex-start" }
                            }
                          />
                        ) : (
                            <Image
                              source={loginScreen.passwordOff}
                              style={
                                this.state.locale == "eng"
                                  ? { alignSelf: "flex-end" }
                                  : { alignSelf: "flex-start" }
                              }
                            />
                          )}
                      </TouchableOpacity>
                    </View>
                  </View>
                  {this.state.passwordError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("This field is required")}
                    </Text>
                  ) : null}
                  {this.state.newPassValidation ? (
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.resErHead
                          : styles.resErHead_ar
                      }
                    >
                      {this.state.passError1 ?
                        <View>
                          <Text
                            style={
                              this.state.locale == "eng"
                                ? [styles.resErBold]
                                : [styles.resErBold, commonStyle.rightAlign]}>{I18n.t("Strong Password should:")}
                          </Text>
                          {this.state.passError6 ?
                          <Text
                            style={
                              this.state.locale == "eng"
                                ? [styles.resErBold]
                                : [styles.resErBold, commonStyle.rightAlign]}>{I18n.t("Be written in English")}
                          </Text> : null }
                        </View> : null}
                      {this.state.passError2 ?
                        <Text
                          style={
                            this.state.locale == "eng"
                              ? [styles.resErLight]
                              : [styles.resErLight, commonStyle.rightAlign]}>{I18n.t("Contain at least 8 characters")}</Text> : null}
                      {this.state.passError3 ?
                        <Text style={
                          this.state.locale == "eng"
                            ? [styles.resErLight]
                            : [styles.resErLight, commonStyle.rightAlign]}>{I18n.t("Contain at least one small letter")}</Text> : null}
                      {this.state.passError4 ?
                        <Text style={
                          this.state.locale == "eng"
                            ? [styles.resErLight]
                            : [styles.resErLight, commonStyle.rightAlign]}>{I18n.t("Contain at least one capital letter")}</Text> : null}
                      {this.state.passError5 ?
                        <Text style={
                          this.state.locale == "eng"
                            ? [styles.resErLight]
                            : [styles.resErLight, commonStyle.rightAlign]}>{I18n.t("Contain at least one number")}</Text> : null}
                    </View>
                  ) : null}
                </View>


                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>
                        {I18n.t("Confirm Password")}
                      </Text>
                    </RTLView>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? styles.rowEnglish
                        : styles.rowArabic
                    }
                  >
                    <TextInput
                      onChangeText={text =>
                        this.setState({ confirmPassword: text })
                      }
                      style={[
                        this.state.locale == "eng"
                          ? styles.formControlPasswordField
                          : [
                            styles.formControlPasswordField,
                            { textAlign: "right" }
                          ],
                        (
                          this.state.confirmPasswordError == true || this.state.confirmPasswordError1 == true) && {
                          borderColor: "#ff0000"
                        }
                      ]}
                      placeholder="************"
                      placeholderTextColor="#CCCCCC"
                      returnKeyType="go"
                      password={true}
                      autoCorrect={false}
                      value={this.state.confirmPassword}
                      onChangeText={(text) => this.setState({ confirmPassword: text, confirmPasswordError: "", confirmPasswordError1: "" })}
                      secureTextEntry={this.state.showConfirmPassword}
                    />
                    <View style={{ width: "15%", justifyContent: "center" }}>
                      <TouchableOpacity
                        onPress={() => this.changePwdType("confirmPassword")}
                      >
                      </TouchableOpacity>
                    </View>
                  </View>
                  {this.state.confirmPasswordError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("This field is required")}
                    </Text>
                  ) : null}
                  {this.state.confirmPasswordError1 ? (
                    <Text style={
                      this.state.locale == "eng"
                        ? [styles.error]
                        : [styles.error, commonStyle.rightAlign]}>
                      {I18n.t("passwordNoMatch")}
                    </Text>
                  ) : null}
                </View>

                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={[styles.fromText]}>
                        {I18n.t("Mobile Number")}
                        <Text style={styles.countryText}>
                          {I18n.t(" (Saudi) ")}
                        </Text>
                      </Text>
                    </RTLView>
                  </View>

                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <View
                      style={[
                        this.state.locale == "eng"
                          ? styles.mobileInput
                          : styles.mobileInput_ar,
                        (
                          this.state.mobileNumberError == true
                          || this.state.mobileNumberError1 == true
                          || this.state.mobileNumberError2 == true
                          || this.state.mobileNumberNumericError
                          || this.state.duplicatemobileerror == true) && {
                          borderColor: "#ff0000"
                        }
                      ]}
                    >
                      <View style={styles.countryCode}>
                        <Text style={styles.countryCodeText}>966</Text>
                      </View>
                      <View></View>
                      <TextInput
                        onChangeText={(text) => this.setState({
                          mobileInput: text,
                          mobileNumberError: false,
                          mobileNumberError1: false,
                          mobileNumberError2: false,
                          mobileNumberNumericError: false,
                          duplicatemobileerror: false
                        })}
                        onFocus={this.confirmPassCheck}
                        style={
                          this.state.locale == "eng"
                            ? styles.mobileTextInput
                            : styles.mobileTextInput_ar
                        }
                        placeholder=" 0000000000"
                        keyboardType="numeric"
                        underlineColorAndroid="transparent"
                        contextMenuHidden={true}
                      />
                    </View>

                    <View style={{ width: "15%", justifyContent: "center" }}>
                      <Image
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.flagImage
                            : commonStyle.flagImage_ar
                        }
                        source={menuScreen.flag2}
                      />
                    </View>
                  </View>
                </View>
                {this.state.mobileNumberError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("This field is required")}
                  </Text>
                ) : null}
                {this.state.mobileNumberNumericError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("Please enter numbers only")}
                  </Text>
                ) : null}

                {this.state.mobileNumberError1 ? (
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.error]
                      : [styles.error, commonStyle.rightAlign]}>
                    {I18n.t("mobileError")}
                  </Text>
                ) : null}
                {this.state.mobileNumberError2 ? (
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.error]
                      : [styles.error, commonStyle.rightAlign]}>
                    {I18n.t("MobileNumberExistsOrInvalid")}
                  </Text>
                ) : null}
                {this.state.duplicatemobileerror ? (
                  <Text style={
                    this.state.locale == "eng"
                      ? [styles.error]
                      : [styles.error, commonStyle.rightAlign]}>
                    {I18n.t("Mobile is already used")}
                  </Text>
                ) : null}


                <View style={{ ...styles.formGroup, marginTop: 20 }}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    disabled={this.state.isRegistering}
                    onPress={() => this._signUp()}
                    style={styles.commonButton}
                  >
                    <Text style={styles.buttonText}>
                      {this.state.isRegistering
                        ? I18n.t("Creating Account")
                        : I18n.t("Create Account")}
                    </Text>
                    {this.state.isRegistering && (
                      <ActivityIndicator
                        size="small"
                        color="#fff"
                        style={{
                          position: "absolute",
                          right: 30
                        }}
                      />
                    )}
                  </TouchableOpacity>
                </View>
                <View>
                  <RTLView locale={this.state.locale}>
                    <Text style={styles.dontText}>
                      {I18n.t("Already have an account?")}
                    </Text>
                  </RTLView>
                </View>

                <View style={styles.formGroup}>
                  <TouchableOpacity
                    onPress={this._goToLogin}
                    style={styles.whiteButton}
                  >
                    <Text style={styles.whiteButtonText}>
                      {I18n.t("Login")}
                    </Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flexDirection: this.state.locale == "eng" ? 'row' : 'row-reverse', flexWrap: 'wrap' }}>
                  <Text style={{ ...styles.bottomText, marginTop: 0 }}>
                    {I18n.t("By continuing, you agree to Bitaqaty")}
                  </Text>

                  <TouchableOpacity onPress={() => this.props.navigation.navigate("Terms", { index: 3 })}>
                    <Text style={styles.blueText}> {I18n.t("Terms of use")}</Text>
                  </TouchableOpacity>

                  <Text> {I18n.t("and")} </Text>

                  <TouchableOpacity onPress={() => this.props.navigation.navigate("Privacy", { index: 2 })}>
                    <Text style={styles.blueText}>{I18n.t("Policies")}</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={commonStyle.rights}>
                <RTLView locale={this.state.locale}>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                  </Text>
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, fontFamily: "Tajawal-Bold" }
                    ]}
                  >
                    {I18n.t(" Bitaqaty ")}
                  </Text>
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, marginBottom: 60 }
                    ]}
                  >
                    {I18n.t("@2019")}
                  </Text>
                </RTLView>
              </View>
            </View>
          </ScrollView>        
          )}
            </SafeAreaView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(SignUp);