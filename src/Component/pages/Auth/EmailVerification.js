import React, { Component } from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { connect } from "react-redux";

class EmailVerification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: ""
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
    // return null;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainSection}>
          <View style={styles.logoHead}>
            <Image
              style={{ height: 30, width: 24.5 }}
              source={require("../../assets/image/logo.png")}
            />
          </View>

          <ScrollView>
            <View style={styles.container}>
              <View style={styles.top}>
                <Text style={styles.pageTitle}>
                  {I18n.t("Email Verification")}
                </Text>

                <View style={[styles.formWrap, { marginTop: 10 }]}>
                  <Text style={styles.linkLight}>
                    {I18n.t("An email has been sent to")}
                  </Text>
                  <Text
                    style={
                      this.state.locale == "eng"
                        ? styles.linkBold
                        : styles.linkBold_ar
                    }
                  >
                    ali@mail.net
                  </Text>
                  <Text style={styles.linkLight}>
                    {I18n.t("To complete the email verification process")}
                  </Text>
                  <Text style={styles.linkMedium}>
                    {I18n.t(
                      "Please click on the verification link from Bitaqaty via email"
                    )}
                  </Text>
                  <Text style={styles.linkTextLight}>
                    {I18n.t(
                      "Be sure to check all email files if you cant find the verification message in your inbox"
                    )}
                  </Text>

                  <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                    <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                    <TouchableOpacity onPress ={()=> this.props.navigation.navigate("ContactUs")}>
                      <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>  
                    </TouchableOpacity>
                  </View>

                  <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                  <Text style={styles.cusText}>{I18n.t("If you do not receive the message, you can")}</Text>
                  <TouchableOpacity>
                    <Text style={styles.cusSrvc}>{" "}{I18n.t("Resend link")}{" "}</Text>  
                  </TouchableOpacity>
                </View>
                </View>
              </View>

              <View style={commonStyle.rights}>
                <RTLView locale={this.state.locale}>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                    <Text
                      style={[
                        commonStyle.screenText,
                        { fontSize: 12, fontFamily: "Tajawal-Bold" }
                      ]}
                    >
                      {I18n.t(" Bitaqaty ")}
                    </Text>
                    <RTLText style={[commonStyle.screenText, { fontSize: 12 }]}>
                      {I18n.t("@2019")}
                    </RTLText>
                  </Text>
                </RTLView>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(
  mapStateToProps,
  {}
)(EmailVerification);
