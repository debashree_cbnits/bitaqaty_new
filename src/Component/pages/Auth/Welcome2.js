import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ImageBackground,
  Animated,
  Easing,
  TouchableOpacity,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { othersImage } from "../../../config/imageConst";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import { cos } from "react-native-reanimated";

class Welcome1 extends React.Component {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.state = {
      locale: "",
      language: "",
      // name: ""
      // status: this.props.navigation.state.params.status
      //   ? this.props.navigation.state.params.status
      //   : "",
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  handleAnimation = () => {
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 2000,
      easing: Easing.ease,
    }).start();
  };

  componentDidMount() {
    this.getUserDetails()
    setTimeout(() => {
      this.props.navigation.navigate("HomeScreen");
    }, 1500);
  }
  getUserDetails (){
    this.handleAnimation()
    AsyncStorage.getItem("userToken")
    .then((token) => {
      if (token){
      AsyncStorage.getItem("userDetails")
      .then((results) => {        
        let user = JSON.parse(results)
        this.setState({ name: user.fullName })
      })
      } else {
        this.setState({name : ''})
      }
    })
  }

  render() {
    return (
      <View style={styles.mainSection}>
        <ImageBackground
          source={require("../../assets/image/BG.png")}
          style={styles.image1}
        >
          <View style={{width: '100%', height: '100%',  position: 'absolute',}}>
          <View style={commonStyle.logoHead}>
            <Image
              style={{ height: 30, width: 24.5 }}
              source={require("../../assets/image/logo.png")}
            />
          </View>
          {/* <TouchableOpacity onPress={this.handleAnimation}>
            <Text>Welcome Animation</Text>
          </TouchableOpacity> */}
          <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rowSec, {paddingHorizontal: '4%', height: '10%',}]
                  : [commonStyle.rowSec_ar, {paddingHorizontal: '4%', height: '10%',}]
              }
            >
            {this.state.name && this.state.name  ? <Text style={styles.helloMed16}>{I18n.t("Hello")}</Text> : null}
              <Text style={styles.pageTitle}>{" "}{this.state.name && this.state.name ? this.state.name  :''}{" "}</Text>
            </View>
          {/* <View
            style={{
              ...styles.container,
              justifyContent: "space-between",
              alignItems: "center",
            }}
          > */}
            <View
              style={{
                width: "100%",
                height: '70%',
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Animated.Image
                source={othersImage.welcomeHeart}
                resizeMode="contain"
                style={{
                  alignItems: "center",
                  height: 40,
                  width: 15.5,
                  transform: [
                    {
                      scaleX: this.animatedValue.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1, 12],
                      }),
                    },
                    {
                      scaleY: this.animatedValue.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1, 12],
                      }),
                    },
                  ],
                }}
              />

              <Image
                source={othersImage.smile}
                style={{
                  marginTop: "-6%",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              />

              <Text
                style={{
                  ...commonStyle.bold14,
                  marginTop: 22,
                  textAlign: "center",
                }}
              >
                {I18n.t("Welcome Back to Bitaqaty")}
              </Text>

              
            </View>

            <View style={{ width: "100%" }}>
              <View
                style={
                  this.state.locale == "eng"
                    ? [commonStyle.rights, commonStyle.rowSec]
                    : [commonStyle.rights, commonStyle.rowSec_ar]
                }
              >
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                </Text>
                <Text
                  style={[
                    commonStyle.screenText,
                    { fontSize: 12, fontFamily: "Tajawal-Bold" },
                  ]}
                >
                  {I18n.t(" Bitaqaty ")}
                </Text>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("@2019")}
                </Text>
              </View>
            </View>
          {/* </View> */}
          </View>
        </ImageBackground>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Welcome1);