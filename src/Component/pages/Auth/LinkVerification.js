import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
const deviceHeight = Dimensions.get("window").height;
import apiClient from "../../../services/api.client";
import Toast from "react-native-tiny-toast";


class LinkVerification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      email: this.props.navigation.state.params.email
        ? this.props.navigation.state.params.email
        : "",
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          lang: 'en'
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          lang: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        lang: 'en'
      };
    }
  }

  resentForgetPassLink = () => {

    let loginData = {
      userName: this.state.email,
      lang: this.state.lang
    };
    apiClient
      .postRequest("/forgetPassword", loginData)
      .then(forgetRes => {
        Toast.show(I18n.t("resendLinkSuccess"))
      })
      .catch(err => console.log("err", err));
  };

  render() {

    return (
      <View style={styles.mainSection}>
        <View style={styles.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={{ width: "100%" }}>
            <View>
              <RTLView locale={this.state.locale}>
                <RTLText style={styles.pageTitle}>
                  {I18n.t("Link Verification")}
                </RTLText>
              </RTLView>
            </View>
            <Image
              source={
                this.state.locale == "eng"
                  ? loginScreen.linkVerification
                  : loginScreen.linkVerificationY
              }
              resizeMode={"contain"}
              style={{ width: "100%", height: 30, marginTop: 5 }}
            />

            <Text style={[styles.orText, { marginBottom: 17 }]}>
              {I18n.t("Password reset link has been sent successfully")}
            </Text>

            <View style={[styles.formWrap, { marginTop: 0 }]}>
              <View>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.linkLight}>
                    {I18n.t("An email has been sent to")}
                  </RTLText>
                </RTLView>
              </View>

              <Text
                style={
                  this.state.locale == "eng"
                    ? styles.linkBold
                    : styles.linkBold_ar
                }
              >
                {this.state.email}
              </Text>
              <View>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.linkLight}>
                    {I18n.t("To reset your password")}
                  </RTLText>
                </RTLView>
              </View>
              <View>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.linkMedium}>
                    {I18n.t(
                      "Please click on the verification link from Bitaqaty via email"
                    )}
                  </RTLText>
                </RTLView>
              </View>
              <View>
                <RTLView locale={this.state.locale}>
                  <RTLText style={styles.linkTextLight}>
                    {I18n.t(
                      "Be sure to check all email files if you cant find the verification message in your inbox"
                    )}
                  </RTLText>
                </RTLView>
              </View>

              <View style={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("ContactUs")}>
                  <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>
                </TouchableOpacity>
              </View>

              <View style={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                <Text style={styles.cusText}>{I18n.t("If you do not receive the message, you can")}</Text>
                <TouchableOpacity onPress={this.resentForgetPassLink}>
                  <Text style={styles.cusSrvc}>{" "}{I18n.t("Resend link")}{" "}</Text>
                </TouchableOpacity>
              </View>

            </View>

          </View>

          <View style={{ width: "100%" }}>
            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" }
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(LinkVerification);