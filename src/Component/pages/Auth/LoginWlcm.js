import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import apiClient from "../../../services/api.client";
import AsyncStorage from "@react-native-community/async-storage";

// import { forFade } from "react-navigation-stack/lib/typescript/src/vendor/TransitionConfigs/HeaderStyleInterpolators";

class LoginWlcm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      email: this.props.navigation.state.params.email
        ? this.props.navigation.state.params.email
        : "",
      password: this.props.navigation.state.params.passWord
        ? this.props.navigation.state.params.passWord
        : "",
      showOldPass: false,
      isRegistering: false
    };
    this.getUserEmail();
  }

  async getUserEmail() {
    try {
      const value = await AsyncStorage.getItem("loginEmailId");
      if (value !== null) {
        this.setState({ email: value });
      }
    } catch (error) { }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  backValueReceive = value => {
    if (value == "back") {
      this.props.navigation.navigate("ChangePassword");
    }
  };

  onClickBtn = () => {
    this.props.navigation.navigate("ForgotPassword");
  };

  login = () => {
    if (this.state.email === "") {
      this.setState({ emailError: true });
    } else if (this.state.password === "") {
      this.setState({ passwordError: true });
    } else {
      this.setState({ isRegistering: true });
      let loginData = {
        username: this.state.email,
        password: this.state.password
      };
      apiClient
        .postRequest("/login", loginData)
        .then(loginRes => {
          this.setState({ isRegistering: false });
          if (loginRes.userToken) {
            AsyncStorage.setItem("userToken", loginRes.userToken);
            AsyncStorage.setItem("loginEmailId", this.state.email);
            AsyncStorage.setItem("loginuserName", loginRes.fullName);
            AsyncStorage.setItem("userDetails", JSON.stringify(loginRes));

            let logintype = "normal";
            AsyncStorage.setItem("loginTypeInfo", logintype);

            this.props.navigation.navigate("Welcome2");
          } else if (loginRes.error == "401") {
            this.setState({ email: "", password: "" });
            Toast.show("Please enter valid credentials");
          } else if (loginRes.statusCode == "401") {
            this.setState({ email: "", password: "" });
            Toast.show("Please enter valid credentials");
          }
          else if (loginRes.error == "SECURITY_USER_EMAIL_NOT_VERIFIED") {
            this.setState({ isLoading: false });
            AsyncStorage.setItem("email", this.state.email);
            this.props.navigation.navigate("Email");
          }
          else if (loginRes.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
            this.setState({ isLoading: false });
            this.props.navigation.navigate("Mobile", { social: this.state.socialdata });
          }
        })
        .catch(err => console.log(err));
    }
  };


  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={value => this.backValueReceive(value)}
        />
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.top}>
              <Text style={styles.pageTitle}>{I18n.t("Welcome Back")}</Text>
              <RTLView locale={this.state.locale}>
                <Image
                  source={
                    this.state.locale == "eng"
                      ? loginScreen.changePasswordWlcm
                      : loginScreen.changePasswordWlcm_Y
                  }
                  resizeMode={"contain"}
                  style={styles.stepsImg}
                />
              </RTLView>
              <Text style={[styles.orText, { marginBottom: -9 }]}>
                {I18n.t("new password has been set successfully,")}
              </Text>
              <Text style={[styles.orText, { marginBottom: 17 }]}>
                {I18n.t("You can now login with the new password")}
              </Text>

              <View style={[styles.formWrap, { marginTop: 0 }]}>
                <View style={[styles.formGroup, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.fromText}>
                        {I18n.t("Email")}
                      </RTLText>
                    </RTLView>
                  </View>
                  <TextInput
                    style={
                      this.state.locale == "eng"
                        ? styles.formControl_en
                        : styles.formControl_ar
                    }
                    placeholder="example@example.com"
                    placeholderTextColor="#CCCCCC"
                    value={this.state.email}
                  />
                </View>

                <View style={[styles.formGroup, { marginTop: 8 }]}>
                  <Text style={styles.fromText}>
                    {I18n.t("Password")}
                  </Text>
                  <View style={{ flexDirection: "row" }}>
                    <RTLView locale={this.state.locale}>
                      <TextInput
                        style={
                          this.state.locale == "eng"
                            ? styles.formPassword_en
                            : styles.formPassword_ar
                        }
                        placeholder={I18n.t(
                          "Please enter your current password"
                        )}
                        placeholderTextColor="#CCCCCC"
                        returnKeyType="go"
                        secureTextEntry={!this.state.showOldPass}
                        password={true}
                        autoCorrect={false}
                        value={this.state.password}
                      />
                      <View
                        style={
                          this.state.locale == "eng"
                            ? styles.passIcon_en
                            : styles.passIcon_ar
                        }
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.setState({
                              showOldPass: !this.state.showOldPass
                            })
                          }
                        >
                          <Image
                            source={this.state.showOldPass ? loginScreen.password : loginScreen.passwordOff}
                            style={
                              this.state.locale == "eng"
                                ? styles.passImg
                                : styles.passImg2
                            }
                          />
                        </TouchableOpacity>
                      </View>
                    </RTLView>
                  </View>
                </View>

                <TouchableOpacity onPress={this.onClickBtn}>
                  <Text style={styles.forgotText}>
                    {I18n.t("Forgot password?")}
                  </Text>
                </TouchableOpacity>

                <View style={styles.formGroup}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    disabled={this.state.isRegistering}
                    onPress={() => this.login()}
                    style={styles.commonButton}
                  >
                    <Text style={styles.buttonText}>
                      {I18n.t("Login")}
                    </Text>
                    {this.state.isRegistering && (
                      <ActivityIndicator
                        size="small"
                        color="#fff"
                        style={{
                          position: "absolute",
                          right: 30
                        }}
                      />
                    )}
                  </TouchableOpacity>
                </View>

                <View style={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                  <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate("ContactUs")}>
                    <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View>
              <View style={commonStyle.rights}>
                <RTLView locale={this.state.locale}>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                    <Text
                      style={[
                        commonStyle.screenText,
                        { fontSize: 12, fontFamily: "Tajawal-Bold" }
                      ]}
                    >
                      {I18n.t(" Bitaqaty ")}
                    </Text>
                    <RTLText style={[commonStyle.screenText, { fontSize: 12 }]}>
                      {I18n.t("@2019")}
                    </RTLText>
                  </Text>
                </RTLView>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(LoginWlcm);
