import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { connect } from "react-redux";
import { loginScreen } from "../../../config/imageConst";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import apiClient from "../../../services/api.client";
import AsyncStorage from "@react-native-community/async-storage";
import RNFetchBlob from 'rn-fetch-blob';


class Mobile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      code: "",
      errorText: "",
      imagecolor: false,
      userName: "",
      mobile: "",
      email: "",
      password: "",
      social: this.props.navigation.state.params && this.props.navigation.state.params.social
        ? this.props.navigation.state.params.social
        : "",
      activationCode: "",
      maxMobileVerificationMsg : "",
      maxResendAttems : true,
      maxMsgSent : 1,
      blanktext : false
    };
    // this.urlDataRetrive();
  }

  componentDidMount = () =>{
    this.urlDataRetrive();
    this.props.navigation.addListener('didFocus', () => { 
      this.urlDataRetrive();
    })
    // this.props.navigation.addListener('didBlur', () => { 
    //   this.urlDataRetrive();
    // }) 
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  async urlDataRetrive() {
    try {
      const value = await AsyncStorage.getItem("userName");
      if (value !== null) {
        this.resendCodeFirst(value);
        this.resendStatusCheck(value);
        this.setState({ userName: value });
      }
    } catch (error) {}
    try {
      const value = await AsyncStorage.getItem("mobileInput");
      if (value !== null) {
        this.setState({ mobile: value });
      }
    } catch (error) {}
    try {
      const value = await AsyncStorage.getItem("email");
      if (value !== null) {
        this.setState({ email: value });
      }
    } catch (error) {}
    try {
      const value = await AsyncStorage.getItem("password");
      if (value !== null) {
        this.setState({ password: value });
      }
    } catch (error) {}
    try {
      const value = await AsyncStorage.getItem("activationCode");
      if (value !== null) {
        this.setState({ activationCode: value });
      }
    } catch (error) {}
    if(this.state.social == ""){
      this.email_verify();
     }
  }

  resendCodeFirst = async(username) => {
    try {
      const maxnum = await AsyncStorage.getItem("maxMobileVerificationMsg");
      if (maxnum !== null) {
        this.setState({ maxMobileVerificationMsg: maxnum });
        let data = {"userName": username, "messagesSent": true}
    const fetchClient = RNFetchBlob.config({
      trusty: true
    })
    const headers = {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        "app-name": "Bitaqaty",
        country: "2",
        username: "Bitaqaty",
      };
    fetchClient
      .fetch("POST", 'https://stagewrapper.ocstaging.net/bitaqatywrapper/resendActivation', headers, JSON.stringify(data))
     // .fetch("POST", 'https://downloads.netader.com/resendActivation', headers, JSON.stringify(data))
      .then(res => {
      if(res.respInfo.status == 200){
        let resdata = JSON.parse(res.data);
        if(resdata.mobileVerificationMessagesSent == 0){
          this.setState({ maxMsgSent : 1})
        }
        else if(resdata.mobileVerificationMessagesSent >= maxnum ){
          this.setState({ maxResendAttems : false})
        }
        else{
          this.setState({ maxMsgSent : resdata.mobileVerificationMessagesSent})
        }
        // if(resdata.error){
        //   this.setState({ errorText: "error" });
        // }
      }
      })
      .catch(err => { });
      }
    } catch (error) {}
  }

  resendStatusCheck = async (userName) =>{
        let statusCheckPayload = {
          userName: userName,
        };
        await apiClient
          .postRequest("/checkStatus",statusCheckPayload)
          .then(async resendStatusRes => {
            this.setState({mobileVerficationAttems : resendStatusRes.mobileVerificationMessagesSent})
            if (resendStatusRes === 'ACTIVE') {
              this.props.navigation.navigate("Login")
            }
        })
  }
  email_verify = () => {
    if (this.state.userName != "") {
      let emailLinkData = {
        userName: this.state.userName,
        activationCode: this.state.activationCode
      };
      apiClient
        .postRequest("/mailActivation", emailLinkData)
        .then(linkVerifyRes => {
          if(linkVerifyRes.mobileNumber){
            this.setState({mobile : linkVerifyRes.mobileNumber})
            }
            else{
              this.props.navigation.navigate("Email");
            }
        })
        .catch(err => {
          this.setState({ errorText: "error", isRegistering: false });
        });
    }
  };


  codeVerify = async () => {
    if (this.state.code === "") {
      this.setState({ blanktext : true });
      return;
    }
    this.setState({
      errorText: ""
    });
    let data = { "userName": this.state.userName, "verficationCode": this.state.code }
    const fetchClient = RNFetchBlob.config({
      trusty: true
    })
    const headers = {
      "Content-Type": "application/json",
      Accept: "application/json, text/plain, */*",
      "app-name": "Bitaqaty",
      country: "2",
      username: "Bitaqaty",
    };
    fetchClient
      .fetch("POST", 'https://stagewrapper.ocstaging.net/bitaqatywrapper/newActivation', headers, JSON.stringify(data))
      .then(res => {
        if (res.respInfo.status == 200) {
          let resdata = JSON.parse(res.data);
          if (resdata.error) {
            this.setState({ errorText: "Invalid code"  });
          }
          else {
            if (this.state.social == "") {
              this.props.navigation.navigate("LoginFirst", {
                email: this.state.email,
                password: this.state.password
              });
            }
            else {
              this.props.navigation.navigate("Login")
            }
          }
        }
      })
      .catch(err => {
      });
  };

  resendMobileCode = async () => {
    
    if(this.state.maxMsgSent+1 > Number(this.state.maxMobileVerificationMsg)){
      this.setState({maxResendAttems : false})
    }
    else{
      let resend = this.state.maxMsgSent;
      resend = resend+1;
      this.setState({maxMsgSent : resend})
    }

    this.setState({ errorText: "", code: "" })
    let username = this.state.userName;
    let data = { "userName": this.state.userName, "newActivation": true }

    const fetchClient = RNFetchBlob.config({
      trusty: true
    })
    const headers = {
      "Content-Type": "application/json",
      Accept: "application/json, text/plain, */*",
      "app-name": "Bitaqaty",
      country: "2",
      username: "Bitaqaty",
    };
    //https://wrapper.bitaqaty.com
    //https://stagewrapper.ocstaging.net/bitaqatywrapper/resendActivation
    fetchClient
      .fetch("POST", 'https://stagewrapper.ocstaging.net/bitaqatywrapper/resendActivation', headers, JSON.stringify(data))
      .then(res => {

        if (res.respInfo.status == 200) {
          let resdata = JSON.parse(res.data);
          this.setState({ maxMsgSent : resdata.mobileVerificationMessagesSent})
          if (resdata.error) {
            this.setState({ errorText: "error" });
          }
          else {
          }
        }
      })
      .catch(err => {
      });
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        {this.state.errorText !== "" &&
          this.state.errorText !== "codeVerifySuccess" && (
            <View style={styles.loginErrorDisplayOuterView}>
              <RTLView
                style={[
                  styles.loginErrorDisplayInnerView,
                  this.state.errorText === "SignUpSuccess" && {
                    borderColor: "#008000"
                  }
                ]}
                locale={this.state.locale}
              >
                <Ionicons
                  name={Platform.OS === "android" ? "md-alert" : "ios-alert"}
                  size={25}
                  color="#FF0000"
                />
                <Text style={styles.viewErrorText}>
                  {I18n.t(this.state.errorText)}
                </Text>
              </RTLView>
            </View>
          )}
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.top}>
              <Text style={
                this.state.locale == "eng"
                  ? [styles.pageTitle, commonStyle.none]
                  : [styles.pageTitle, commonStyle.rightAlign]
              }>
                {I18n.t("Create new Bitaqaty account")}
              </Text>

              <Text style={
                this.state.locale == "eng"
                  ? [styles.fromText, commonStyle.none]
                  : [styles.fromText, commonStyle.rightAlign]
              }>
                {I18n.t("Waiting for mobile number verification")}
              </Text>
              {this.state.imagecolor == false ? (
                <Image
                  style={styles.stepsImg2}
                  resizeMode={"contain"}
                  source={
                    this.state.locale == "eng"
                      ? loginScreen.createAccount4
                      : loginScreen.createAccount4Y
                  }
                />
              ) : (
                  <Image
                    style={styles.stepsImg2}
                    resizeMode={"contain"}
                    source={
                      this.state.locale == "eng"
                        ? loginScreen.createAccount5
                        : loginScreen.createAccount5Y
                    }
                  />
                )}

              {this.state.imagecolor == false ? (
                <Text style={{ ...styles.fromText, textAlign: "center" }}>
                  {I18n.t("email verified successfully")}
                </Text>
              ) : (
                  <Text style={{ ...styles.fromText, textAlign: "center" }}>
                    {I18n.t("Waiting for mobile number verification")}
                  </Text>
                )}

              <View style={[styles.formWrap, { marginTop: 10 }]}>
                <Text
                  style={
                    this.state.locale == "eng"
                      ? [styles.linkLight, commonStyle.none]
                      : [styles.linkLight, commonStyle.rightAlign]
                  }>
                  {I18n.t("Your verification code has been sent to")}
                </Text>
                <Text
                  style={
                    this.state.locale == "eng"
                      ? styles.mobNumber
                      : styles.mobNumber_ar
                  }
                >
                  {this.state.mobile}
                </Text>
                <Text
                  style={
                    this.state.locale == "eng"
                      ? [styles.linkLight, commonStyle.none]
                      : [styles.linkLight, commonStyle.rightAlign]
                  }>
                  {I18n.t("You'll find the code inside your mobile messages")}
                </Text>
                <Text style={
                  this.state.locale == "eng"
                    ? [styles.linkLight, commonStyle.none]
                    : [styles.linkLight, commonStyle.rightAlign]
                }>
                  {I18n.t("Please enter the 6 digit verification code")}
                </Text>

                <View style={[styles.formGroup, { marginTop: 30 }]}>
                  <Text
                    style={
                      this.state.locale == "eng"
                        ? [styles.fromText, commonStyle.none]
                        : [styles.fromText, commonStyle.rightAlign]
                    }>
                    {I18n.t("Verification Code")}
                  </Text>
                  <TextInput
                    style={[styles.otpField,(
                      this.state.blanktext == true
                      ) && {
                      borderColor: "#ff0000"
                    }]}
                    placeholder="0 0 0 0 0 0"
                    keyboardType="numeric"
                    maxLength={6}
                    placeholderTextColor="#CCCCCC"
                    onChangeText={text =>
                      this.setState({ code: text, imagecolor: true, errorText : "", blanktext: false })
                    }
                  />
                  {this.state.blanktext ? (
                      <Text  style={
                        this.state.locale == "eng"
                          ? [styles.error]
                          : [styles.error, commonStyle.rightAlign]}>
                        {I18n.t("This field is required")}
                      </Text>
                ) : null}
                </View>

                <View style={styles.formGroup}>
                  <TouchableOpacity
                    disabled = {!this.state.maxResendAttems}
                    onPress={() => this.codeVerify()}
                    style={[styles.commonButton, {backgroundColor : this.state.maxResendAttems ? "#241125" : "#CCCCCC" }]}
                  >
                    <Text style={styles.buttonText}>
                      {I18n.t("Activate the Account")}
                    </Text>
                  </TouchableOpacity>
                </View>

                {this.state.maxResendAttems ?
                <>
                {this.state.maxMsgSent != "" ?
                <Text style={{ ...styles.fromText, textAlign: "center" }}>
                  {this.state.maxMsgSent}/{this.state.maxMobileVerificationMsg}
                </Text>
                : null}
                <View style={{ flexDirection:this.state.locale == "eng" ?  'row' : 'row-reverse', flexWrap:'wrap'}}>
                <Text style={
                  this.state.locale == "eng"
                    ? [styles.linkLight, commonStyle.none]
                    : [styles.linkLight, commonStyle.rightAlign]
                }>
                  {I18n.t("If you do not receive the message,")}
                  <Text style={
                  this.state.locale == "eng"
                      ? [styles.linkLight, commonStyle.none]
                      : [styles.linkLight, commonStyle.rightAlign]
                  }>
                    {" "}{I18n.t("you can")}{" "}</Text>
                </Text>
                <TouchableOpacity onPress={this.resendMobileCode}>
                 
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? [styles.linkLightBlue, commonStyle.none]
                          : [styles.linkLightBlue, commonStyle.rightAlign]
                      }>
                      {I18n.t("Resend verification code")}
                  </Text>
                </TouchableOpacity>
                </View>
                </>
                 : 
                <>
                <Text style={
                  this.state.locale == "eng"
                    ? [styles.linkLight, commonStyle.none]
                    : [styles.linkLight, commonStyle.rightAlign]
                }>
                  {I18n.t("Maximum attempts to send verification code has been reached:")}
                  {' '}{this.state.maxMobileVerificationMsg}/{this.state.maxMobileVerificationMsg}
                </Text>
                </> 
              } 
                
              </View>
            </View>

            <View style={commonStyle.rights}>
              <RTLView locale={this.state.locale}>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, fontFamily: "Tajawal-Bold" }
                    ]}
                  >
                    {I18n.t(" Bitaqaty ")}
                  </Text>
                  <RTLText style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("@2019")}
                  </RTLText>
                </Text>
              </RTLView>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(Mobile);