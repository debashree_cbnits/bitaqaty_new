import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import AsyncStorage from "@react-native-community/async-storage";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen, productImagePath } from "../../../config/imageConst";
import Toast from "react-native-tiny-toast";
import apiClient from "../../../services/api.client";
import Ionicons from "react-native-vector-icons/dist/Ionicons";
import { connect } from "react-redux";

const deviceHeight = Dimensions.get("window").height;

class SetNewPass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      requestDate: "",
      requestID: "",
      emailForgetPassWord: "",
      userName: "",
      status: "",
      password: "",
      comfirmPassWord: "",
      errorText: "",
      showPassword: true,
      showConfirmPassword: true,
      // passError:"",
      newPassError: false,
      confirmPassError: false,
      passValidError : false,
     
      passError1: false,
      passError2: false,
      passError3: false,
      passError4: false,
      passError5: false,
      passMatchError: false,
      
    };
    this.urlDataRetrive();
  }

  componentDidMount() {    
    this.props.navigation.addListener('willFocus', () => {      
      this.setState({ password: "", comfirmPassWord: "" })
      this.urlDataRetrive();
    });
  }

  async urlDataRetrive() {
    try {
      const value = await AsyncStorage.getItem("emailForgetPassWord");
      if (value !== null) {        
        this.setState({ emailForgetPassWord: value });
      }
    } catch (error) {}

    try {
      const value = await AsyncStorage.getItem("userName");
      if (value !== null) {
        this.setState({ userName: value });
      }
    } catch (error) {}

    try {
      const value = await AsyncStorage.getItem("requestDate");
      if (value !== null) {        
        this.setState({ requestDate: value });
      }
    } catch (error) {}

    try {
      const value = await AsyncStorage.getItem("requestID");
      if (value !== null) {        
        this.setState({ requestID: value });
        this.link_verify();
      }
    } catch (error) {}
  }

  link_verify = () => {    
    console.log('link_verify')         
    if (this.state.requestDate != "" && this.state.requestID != "") {      

      let forgetPassLinkkData = {
        requestDate: this.state.requestDate,
        action: "checkExpired",
        requestID: this.state.requestID
      };

      apiClient
        .postRequest("/resetPassword", forgetPassLinkkData)
        .then(forgetPassLinkRes => { 
          console.log(forgetPassLinkRes, 'forgetPassLinkRes')         
          if (forgetPassLinkRes.status == "expired") {
            this.props.navigation.navigate('ForgotError');
            this.setState({ errorText: "The link has expired" });
          } else if (forgetPassLinkRes.status == "valid") {
            this.setState({ status: "valid" });
          }
        })
        .catch(err => {
          console.log("err", err);
        });
    } else {
      // Toast.show(I18n.t("Something went wrong!"));
    }
  };

  static getDerivedStateFromProps(props, state) {    

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  passValidation = (value, fieldName) => {
    let passwordRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/;
    if (fieldName == "newPassword"){
    if (value == "") {
      // this.setState({ errorText: "passReq", passError: "" });
      this.setState({newPassError: true})
    } else {
      this.setState({newPassError: false, confirmPassError: false})
      const rg2 = new RegExp(/^.{8,}$/);
      const rg3 = new RegExp(/(?=.*[a-z])/);
      const rg4 = new RegExp(/(?=.*[A-Z])/);
      const rg5 = new RegExp(/(?=.*\d)/);
      rg2.test(value)
        ? this.setState({ passError2: false })
        : this.setState({ passError2: true });
      rg3.test(value)
        ? this.setState({ passError3: false })
        : this.setState({ passError3: true });
      rg4.test(value)
        ? this.setState({ passError4: false })
        : this.setState({ passError4: true });
      rg5.test(value)
        ? this.setState({ passError5: false })
        : this.setState({ passError5: true });
      if (
        rg2.test(value) &&
        rg3.test(value) &&
        rg4.test(value) &&
        rg5.test(value)
      ) {
        this.setState({ passError1: false });
      } else {
        this.setState({ passError1: true });
      }
    }
    //  else if (!passwordRegex.test(value)){
    //   this.setState({ newPassError: false, passValidError: true,confirmPassError: false, errorText: "", Password: value})
    // } else {
    //   this.setState({ Password: value, passValidError:false, newPassError: false,});
    // }
  } else if (fieldName == "confirmPassword"){
    if (value == ""){
      this.setState({confirmPassError: true, })
    } else {
      this.setState({comfirmPassWord: value, confirmPassError: false, passMatchError: false})
    }
  }

  }
  resetPassWord = () => {
    if (this.state.password === "" && this.state.comfirmPassWord === "") {
      this.setState({ newPassError: true, confirmPassError: true });
      return;
    }
    if (this.state.password === "") {
      // this.setState({ errorText: "passReq" });
      this.setState({newPassError: true})
      return;
    }
    if (this.state.passError1){
      return
    }

    if (this.state.comfirmPassWord === "") {
      // this.setState({ errorText: "confirmpassReq" });
      this.setState({confirmPassError: true})
      return;
    }
    if (this.state.password !== this.state.comfirmPassWord) {
      this.setState({ passMatchError: true });
      return;
    }
    if (this.state.errorText !=""){
      return 
    } 
    if (this.state.status == "valid") {
      let resetPassData = {
        newPassword: this.state.password,
        confirmPassword: this.state.comfirmPassWord,
        userName: this.state.userName,
        requestDate: this.state.requestDate,
        requestID: this.state.requestID,
        action: "reset"
      };      

      apiClient
        .postRequest("/resetPassword", resetPassData)
        .then(resetPassRes => {          
          if (resetPassRes.success == 200) {
            this.props.navigation.navigate("LoginWlcm", {
              email: this.state.emailForgetPassWord,
              passWord: this.state.password
            });
          }
          else if(resetPassRes.newPassword == 211){
            this.setState({errorText: "New password shouldn’t be as current password"})
          }
        })
        .catch(err => {
          console.log("err", err);
        });
    } else {
      // Toast.show(I18n.t("Something went wrong!"));
    }
  };

  changePwdType = type => {
    if (type === "password") {
      this.setState({
        passwordImage: !this.state.passwordImage,
        showPassword: !this.state.showPassword
      });
    } else {
      this.setState({
        confirmPasswordImage: !this.state.confirmPasswordImage,
        showConfirmPassword: !this.state.showConfirmPassword
      });
    }
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        {
          this.state.errorText != "" && (
            <View style={styles.loginErrorDisplayOuterView}>
              <RTLView
                style={[
                  styles.loginErrorDisplayInnerView                  
                ]}
                locale={this.state.locale}
              >
                <Ionicons
                  name={Platform.OS === "android" ? "md-alert" : "ios-alert"}
                  size={25}
                  color="#FF0000"
                />
                <Text style={styles.viewErrorText}>
                  {I18n.t(this.state.errorText)}
                </Text>
              </RTLView>
            </View>
          )}
        <ScrollView >
          <View style={styles.container}>
          <View style={{ width: "100%" }}>
            <Text style={styles.pageTitle}>
              {I18n.t("Set your new password")}
            </Text>
            <Image
              source={
                this.state.locale == "eng"
                  ? loginScreen.setPass
                  : loginScreen.setPass_Y
              }
              resizeMode={"contain"}
              style={{ width: "100%", height: 30, marginTop: 5 }}
            />

            <Text style={[styles.orText, { marginBottom: 17 }]}>
              {I18n.t("Waiting for new password registration")}
            </Text>

            <View style={[styles.formWrap, { marginTop: 0 }]}>
              <View style={styles.formGroup}>
                <Text style={styles.fromText}>{I18n.t("New Password")}</Text>
                <View style={{ flexDirection: "row" }}>
                  <RTLView locale={this.state.locale}>
                    <TextInput
                      style={
                      [  this.state.locale == "eng"
                          ? styles.formPassword_en
                          : styles.formPassword_ar, (
                            this.state.newPassError) && {
                            borderColor: "#ff0000"
                          }]
                      }
                      placeholder={I18n.t("Please enter your new password")}
                      placeholderTextColor="#CCCCCC"
                      returnKeyType="go"
                      password={true}
                      autoCorrect={false}
                      value={this.state.password}
                      onChangeText={password => this.setState({ password, errorText: ""}, ()=>{
                        this.passValidation (password, 'newPassword')
                      })}
                      secureTextEntry={this.state.showPassword}
                    />
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.passIcon_en
                          : styles.passIcon_ar
                      }
                    >
                      <TouchableOpacity
                        onPress={() => this.changePwdType("password")}
                      >
                        {this.state.passwordImage ? (
                          <Image
                            source={loginScreen.passwordOff}
                            style={
                              this.state.locale == "eng"
                                ? { alignSelf: "flex-end" }
                                : { alignSelf: "flex-start" }
                            }
                          />
                        ) : (
                          <Image
                            source={loginScreen.password}
                            style={
                              this.state.locale == "eng"
                                ? { alignSelf: "flex-end" }
                                : { alignSelf: "flex-start" }
                            }
                          />
                        )}
                      </TouchableOpacity>
                    </View>
                  </RTLView>
                </View>
                  {!this.state.newPassError &&                    
                    this.state.passError1 &&
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.resErHead
                          : styles.resErHead_ar
                      }
                    >
                        
                        <Text style={styles.resErBold}>{I18n.t("Strong Password should:")} </Text>
                        <Text style={styles.resErBold}>{I18n.t("Be written in English")} </Text> 
                        {this.state.passError2 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least 8 characters")} </Text> : null}
                        {this.state.passError3 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least one small letter")} </Text> : null}
                        {this.state.passError4 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least one capital letter")} </Text> : null}
                        {this.state.passError5 ? 
                        <Text style={styles.resErLight}>{I18n.t("Contain at least one number")} </Text> : null}
                  
                  </View>
                  }

                  {this.state.newPassError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("This field is required")}
                    </Text>
                  ) : null}
              </View>

              <View style={styles.formGroup}>
                <Text style={styles.fromText}>
                  {I18n.t("Confirm Password")}
                </Text>
                <View style={{ flexDirection: "row" }}>
                  <RTLView locale={this.state.locale}>
                    <TextInput
                      style={
                        [this.state.locale == "eng"
                          ? styles.formPassword_en
                          : styles.formPassword_ar, (
                           this.state.confirmPassError)  || this.state.passMatchError && {
                            borderColor: "#ff0000"
                          } ]
                      }
                      placeholder={I18n.t("Re-enter new Password")}
                      placeholderTextColor="#CCCCCC"
                      returnKeyType="go"
                      password={true}
                      autoCorrect={false}
                      value={this.state.comfirmPassWord}
                      onChangeText={comfirmPassWord =>
                        this.setState({ comfirmPassWord, errorText: "" }, ()=>{
                          this.passValidation(this.state.comfirmPassWord, 'confirmPassword')
                        })
                      }
                      secureTextEntry = {true}
                      // secureTextEntry={this.state.showConfirmPassword}
                    />
                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.passIcon_en
                          : styles.passIcon_ar
                      }
                    >
                    </View>
                  </RTLView>
                </View>
                {this.state.confirmPassError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("This field is required")}
                    </Text>
                ) : null}
                {this.state.passMatchError ? (
                    <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("passwordNoMatch")}
                    </Text>
                ) : null}
                

              </View>

              <View style={styles.formGroup}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  disabled={this.state.isRegistering}
                  onPress={() => this.resetPassWord()}
                  style={styles.commonButton}
                >
                  <Text style={styles.buttonText}>{I18n.t("Save")}</Text>
                  {this.state.isRegistering && (
                    <ActivityIndicator
                      size="small"
                      color="#fff"
                      style={{
                        position: "absolute",
                        right: 30
                      }}
                    />
                  )}
                </TouchableOpacity>
              </View>

              <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                  <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                  <TouchableOpacity onPress ={()=> this.props.navigation.navigate("ContactUs")}>
                    <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>  
                  </TouchableOpacity>
                </View>
            </View>
          </View>

          {/* <TouchableOpacity onPress={this.openMobile}><Text style={{fontSize: 20}}>Forgot error</Text></TouchableOpacity> */}

          <View style={{ width: "100%" }}>
            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" }
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View> 
          </View>         
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(SetNewPass);
