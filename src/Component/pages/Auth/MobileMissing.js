import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { HomeImagePath, menuScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
// import { ScrollView } from "react-native-gesture-handler";

class MobileMissing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.logoArea}>
          <Image
            source={
              this.state.locale == "eng"
                ? HomeImagePath.logoEn
                : HomeImagePath.logoar
            }
          />
        </View>
        {/* <ScrollView style={{ flex: 1 }}> */}
        <View style={styles.container}>
          <View style={styles.top}>
            <Text style={styles.pageTitle}>{I18n.t("Create New Account")}</Text>
            <Text style={styles.loginText}>
              {I18n.t("Please enter your email and mobile number")}
            </Text>

            <View style={styles.formWrap}>
              <View style={styles.formGroup}>
                <Text style={styles.fromText}>{I18n.t("Name")}</Text>
                <TextInput
                  style={
                    this.state.locale == "eng"
                      ? styles.loginName
                      : styles.loginName_ar
                  }
                  placeholder="Ali"
                  placeholderTextColor="#1A1A1A"
                />
              </View>

              <View style={styles.formGroup}>
                <Text style={styles.fromText}>{I18n.t("Email")}</Text>
                <TextInput
                  style={
                    this.state.locale == "eng"
                      ? styles.loginName
                      : styles.loginName_ar
                  }
                  placeholder="ali@mail.com"
                  placeholderTextColor="#1A1A1A"
                />
              </View>

              <View style={styles.formGroup}>
                <Text style={[styles.fromText]}>
                  {I18n.t("Mobile Number")}
                  <Text style={styles.countryText}> {I18n.t("(Saudi)")} </Text>
                </Text>

                <View
                  style={
                    this.state.locale == "eng"
                      ? commonStyle.rowSec
                      : commonStyle.rowSec_ar
                  }
                >
                  <View
                    style={
                      this.state.locale == "eng"
                        ? styles.mobileInput
                        : styles.mobileInput_ar
                    }
                  >
                    <View style={styles.countryCode}>
                      <Text style={styles.countryCodeText}>966</Text>
                    </View>
                    <TextInput
                      style={
                        this.state.locale == "eng"
                          ? styles.mobileTextInput
                          : styles.mobileTextInput_ar
                      }
                      placeholder=" 0000000000"
                      keyboardType="numeric"
                      underlineColorAndroid="transparent"
                    />
                  </View>

                  <View style={{ width: "12%", justifyContent: "center" }}>
                    <Image
                      style={
                        this.state.locale == "eng"
                          ? commonStyle.flagImage
                          : commonStyle.flagImage_ar
                      }
                      source={menuScreen.flag2}
                    />
                  </View>
                </View>
              </View>

              <View style={styles.formGroup}>
                <TouchableOpacity style={styles.commonButton}>
                  <Text style={styles.buttonText}>{I18n.t("Verify")}</Text>
                </TouchableOpacity>
              </View>

              <View
                style={this.state.locale == "eng" ? styles.row : styles.row_ar}
              >
                <Text style={styles.cusText}>
                  {I18n.t("By continuing, you agree to Bitaqaty")}
                </Text>
                <TouchableOpacity>
                  <Text style={styles.cusSrvc}> {I18n.t("Terms of use")} </Text>
                </TouchableOpacity>
                <Text style={styles.cusText}>{I18n.t("and")}</Text>
                <TouchableOpacity>
                  <Text style={styles.cusSrvc}>
                    {" "}
                    {I18n.t("Privacy policy")}{" "}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={{ width: "100%" }}>
            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </View>
        {/* </ScrollView> */}
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(MobileMissing);
