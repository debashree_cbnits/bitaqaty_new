import React from "react";
import {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { loginScreen, menuScreen } from "../../../config/imageConst";
import { connect } from "react-redux";
import apiClient from "../../../services/api.client";
import Toast from "react-native-tiny-toast";
import AsyncStorage from "@react-native-community/async-storage";
import SocialLogin from "./SocialLogin";
import ConfirmGoogleCaptcha from 'react-native-google-recaptcha-v2';


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      passwordImage: true,
      email: "",
      password: "",
      showPassword: true,
      locale: "",
      language: "",
      emailError: false,
      passwordError: false,
      allError: false,
      userToken: "",
      errorText: "",
      isLoading: false,
      lang: "",
      code: "",
      socialTextName: "Continue with",
      loginAttempts: "",
      noOfAttems: 0,
      emailformatError: false,
      redBorderError: false,
      isLoading: false
    };
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          lang: 'en'
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          lang: 'ar'
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        lang: 'en'
      };
    }
  }

  async componentDidMount() {
    let fromCart = this.props.navigation.state.params ? this.props.navigation.state.params.cartWithoutLogin : 'no'
    this.setState({ ifFromCart: fromCart })
    this.props.navigation.addListener("willFocus", () => {
      this.setState({
        ifFromCart: fromCart,
        emailError: false,
        emailformatError: false,
        allError: false,
        passwordError: false,
        email: "",
        password: ""
      })
      if (this.state.userToken) {
        this.props.navigation.navigate("Menu");
      }
    });

    await apiClient
      .getRequest("/bitaqatySettings")
      .then(async appsettings => {
        if (appsettings.applicationSettings && appsettings.applicationSettings.length) {
          let appSettingArray = appsettings.applicationSettings;
          for (let i = 0; i < appSettingArray.length; i++) {
            if (appSettingArray[i].propertyName == "loginAttempts") {
              this.setState({ loginAttempts: appSettingArray[i].propertyValue })
            }
            else if (appSettingArray[i].propertyName == "maxMobileVerficationMessage") {
              this.setState({ maxMobileVerificationMsg: appSettingArray[i].propertyValue })
              AsyncStorage.setItem("maxMobileVerificationMsg", appSettingArray[i].propertyValue);
            }
          }
        }
      })
      .catch(async err => {
        this.setState({ errorText: "error" });
      });
  }
  goToForgotPassword = () => {
    //this.props.navigation.navigate("ForgotPassword");
    this.props.navigation.navigate("facebookSignUp");
  };
  // goToForgotPassword = () => {
  //   this.props.navigation.navigate("ForgotPassword");
  //   // this.props.navigation.navigate("SetNewPass");
  // };
  _createAccount = () => {
    this.props.navigation.navigate("SignUp");
  };

  backArrowMenu = () => {
    this.props.navigation.navigate("Menu");
  };

  changePwdType = () => {
    let newState;
    if (this.state.showPassword) {
      newState = {
        passwordImage: false,
        showPassword: false,
        password: this.state.password
      };
    } else {
      newState = {
        passwordImage: true,
        showPassword: true,
        password: this.state.password
      };
    }
    this.setState(newState);
  };

  // validation = (value, fieldName) => {
  //   let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
  //   this.setState({ allError: false, redBorderError: false })
  //   if (fieldName == "email") {
  //     if (value === "") {
  //       this.setState({ emailError: true, emailformatError: false });
  //     } else {
  //       if (!regex.test(value)) {
  //         this.setState({ emailError: false, emailformatError: true });
  //       } else {
  //         this.setState({ emailError: false, email: value, emailformatError: false });
  //       }
  //     }
  //   } else if (fieldName == "password") {
  //     if (value === "") {
  //       this.setState({ passwordError: true });
  //     } else {
  //       this.setState({ passwordError: false, password: value });
  //     }
  //   }
  // };
  validation = (value, fieldName) => {
    let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    this.setState({ allError: false })
    if (fieldName == "email") {
      if (value === "") {
        this.setState({ emailError: true });
      } else {
        this.setState({ emailError: false, email: value });
      }
    } else if (fieldName == "password") {
      if (!regex.test(this.state.email)) {
        this.setState({ emailformatError: true });
      }
      if (value === "") {
        this.setState({ passwordError: true });
      } else {
        this.setState({ passwordError: false, password: value });
      }
    }
  };

  login = () => {
    this.setState({ allError: false, redBorderError: false })
    let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (this.state.email == "" && this.state.password == "") {
      this.setState({ emailError: true, passwordError: true })
    }
    else if (this.state.email === "") {
      this.setState({ emailError: true });
    }
    else if (this.state.email.trim() === "") {
      this.setState({ emailError: true });
    }
    else if (!regex.test(this.state.email)) {
      this.setState({ emailformatError: true })
    }
    else if (this.state.password === "") {
      this.setState({ passwordError: true });
    }
    else if (this.state.password.trim() === "") {
      this.setState({ passwordError: true });
    }
    else if (this.state.noOfAttems < this.state.loginAttempts) {
      this.loginApiCall();
    }
    else {
      this.captchaForm.show();
    }
  };

  loginApiCall = async () => {
    this.setState({ isLoading: true })
    let loginData = {
      username: this.state.email,
      password: this.state.password,
      captcha: this.state.code
    };
    apiClient
      .postRequest("/login", loginData)
      .then(loginRes => {

        if (loginRes.userToken) {
          this.setState({ userToken: loginRes.userToken, isLoading: false });

          AsyncStorage.setItem("userToken", loginRes.userToken);
          AsyncStorage.setItem("loginEmailId", this.state.email);
          AsyncStorage.setItem("loginuserName", loginRes.fullName);
          AsyncStorage.setItem("userDetails", JSON.stringify(loginRes));

          let logintype = "normal";
          AsyncStorage.setItem("loginTypeInfo", logintype);

          if (this.state.ifFromCart == "yes") {
            this.props.navigation.navigate("Cart");
          } else {
            this.props.navigation.navigate("Welcome2");
          }

        } else if (loginRes.error == "401") {
          this.setState({
            isLoading: false,
            allError: true, emailError: false, passwordError: false, emailformatError: false, redBorderError: true
          });
          let attemno = this.state.noOfAttems;
          attemno = attemno + 1;
          this.setState({ noOfAttems: attemno })
        }
        else if (loginRes.statusCode == "401") {
          this.setState({ isLoading: false, allError: true, email: "", password: "", emailError: false, passwordError: false, redBorderError: true });
          let attemno = this.state.noOfAttems;
          attemno = attemno + 1;
          this.setState({ noOfAttems: attemno })
        }
        else if (loginRes.error == "SECURITY_USER_EMAIL_NOT_VERIFIED") {
          this.setState({ isLoading: false });
          AsyncStorage.setItem("email", this.state.email);
          this.props.navigation.navigate("Email");
        }
        else if (loginRes.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
          this.setState({ isLoading: false });
          AsyncStorage.setItem("userName", loginRes.userName);
          AsyncStorage.setItem("mobileInput", loginRes.mobileNumber);
          AsyncStorage.setItem("email", this.state.email);
          this.props.navigation.navigate("Mobile", { social: this.state.socialdata });
        }
      })
      .catch(err => console.log(err));
  }

  goToHomePage = async (value) => {
    this.setState({ isLoading: true })
    if (value.social == "facebook") {
      await apiClient
        .postWithoutToken("/social-connect", {
          fullName: value.social_name,
          username: value.social_email,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: value.social_Id,
          provider: "FACEBOOK",
          isLogin: true
        })
        .then(async FBregisterSuccess => {
          this.setState({ isLoading: false })

          if (FBregisterSuccess.userToken) {
            this.setState({ userToken: FBregisterSuccess.userToken, isLoading: false });
            AsyncStorage.setItem("userToken", FBregisterSuccess.userToken);
            AsyncStorage.setItem("loginEmailId", value.social_email);
            AsyncStorage.setItem("loginuserName", FBregisterSuccess.fullName);
            AsyncStorage.setItem("userDetails", JSON.stringify(FBregisterSuccess));
            let sociallogintype = "socialloginfb";
            AsyncStorage.setItem("socialLoginType", sociallogintype);
            let logintype = "sociallogin";
            AsyncStorage.setItem("loginTypeInfo", logintype);

            if (this.state.ifFromCart == "yes") {
              this.props.navigation.navigate("Cart");
            } else {
              this.props.navigation.navigate("LoginSuccessFB");
            }
          }
          else if (FBregisterSuccess.userNameExists == 3002) {
            Toast.show(I18n.t("emailexist"));
          }
          else if (FBregisterSuccess.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
            AsyncStorage.setItem("userName", FBregisterSuccess.userName);
            AsyncStorage.setItem("mobileInput", FBregisterSuccess.mobileNumber);
            AsyncStorage.setItem("email", value.social_email);
            this.props.navigation.navigate("Mobile", { social: "social" })
          }
          else if (FBregisterSuccess.username == 200) {
            Toast.show(I18n.t("Please Signup First"));
            this.props.navigation.navigate("SignUp");
          }
        })
        .catch(err => {
          this.setState({ isLoading: false })
          console.log("FBregister err", err);
        });
    }

    else if (value.social == "google") {
      await apiClient
        .postWithoutToken("/social-connect", {
          fullName: value.social_name,
          username: value.social_email,
          lang: this.state.lang,
          application_name: "Bitaqaty",
          providerId: value.social_Id,
          provider: "GMAIL",
          isLogin: true
        })
        .then(async GMAILregisterSuccess => {

          if (GMAILregisterSuccess.userToken) {
            this.setState({ userToken: GMAILregisterSuccess.userToken, isLoading: false });
            AsyncStorage.setItem("userToken", GMAILregisterSuccess.userToken);
            AsyncStorage.setItem("loginEmailId", value.social_email);
            AsyncStorage.setItem("loginuserName", GMAILregisterSuccess.fullName);
            AsyncStorage.setItem("userDetails", JSON.stringify(GMAILregisterSuccess));
            let sociallogintype = "sociallogingmail";
            AsyncStorage.setItem("socialLoginType", sociallogintype);
            let logintype = "sociallogin";
            AsyncStorage.setItem("loginTypeInfo", logintype);

            if (this.state.ifFromCart == "yes") {
              this.props.navigation.navigate("Cart");
            } else {
              this.props.navigation.navigate("LoginSuccess");
            }
          }
          else if (GMAILregisterSuccess.error == "SECURITY_USER_MOBILE_NOT_VERIFIED") {
            AsyncStorage.setItem("userName", GMAILregisterSuccess.username);
            AsyncStorage.setItem("mobileInput", GMAILregisterSuccess.mobileNumber);
            AsyncStorage.setItem("email", value.social_email);
            this.props.navigation.navigate("Mobile", { social: "social" })
          }
          else if (GMAILregisterSuccess.userNameExists == 3002) {
            Toast.show(I18n.t("emailexist"));
          }
          else if (GMAILregisterSuccess.username == 200) {
            Toast.show(I18n.t("Please Signup First"));
            this.props.navigation.navigate("SignUp");
          }
        })
        .catch(err => {
          console.log("GMAILregister err", err);
        });
    }
  }

  //capcha//
  onMessage = event => {
    if (event && event.nativeEvent.data) {
      if (['cancel', 'error', 'expired'].includes(event.nativeEvent.data)) {
        this.captchaForm.hide();
        return;
      } else {
        this.setState({ code: event.nativeEvent.data });

        setTimeout(() => {
          this.captchaForm.hide();
          this.loginApiCall();
        }, 3000);

      }
    }
  };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.mainSection}>
          <View style={styles.headerWrap}>
            <RTLView locale={this.state.locale}>
              <View style={styles.headerLeftIcon}>
                <TouchableOpacity onPress={this.backArrowMenu}>
                  <Image
                    style={
                      this.state.locale == "en"
                        ? styles.arrow_en
                        : styles.arrow_ar
                    }
                    source={
                      this.state.locale == "en"
                        ? menuScreen.close
                        : menuScreen.close
                    }
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.headerLogo}>
                <Image
                  style={styles.logoImage}
                  source={menuScreen.headerLogo}
                />
                <RTLText style={styles.logoText}>{I18n.t("BITAQATY")}</RTLText>
              </View>
            </RTLView>
          </View>

          {this.state.allError ? (
            <View
              style={
                this.state.locale == "eng"
                  ? commonStyle.errorMsg
                  : commonStyle.errorMsg_ar
              }
            >
              <Image source={loginScreen.error} />
              <Text style={commonStyle.errorMsgText}>
                {I18n.t("Incorrect email or password")}
              </Text>
            </View>
          ) : null}
          {this.state.isLoading ? (
            <ActivityIndicator size="small" />
          ) : (
              <ScrollView>
                <View style={styles.container}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.pageTitle}>
                        {I18n.t("Welcome")}
                      </RTLText>
                    </RTLView>
                  </View>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.loginText}>{I18n.t("Login")}</RTLText>
                    </RTLView>
                  </View>

                  <SocialLogin
                    socialTextName={this.state.socialTextName}
                    goToHome={value => this.goToHomePage(value)}
                  />

                  <Text style={styles.orText}>{I18n.t("Or")}</Text>

                  <View style={[styles.formWrap, { marginTop: 0 }]}>
                    <View style={[styles.formGroup, { marginTop: 8 }]}>
                      <View>
                        <RTLView locale={this.state.locale}>
                          <RTLText style={styles.fromText}>
                            {I18n.t("Email")}
                          </RTLText>
                        </RTLView>
                      </View>
                      <TextInput
                        style={[
                          this.state.locale == "eng"
                            ? styles.formControl_en
                            : styles.formControl_ar,
                          (
                            this.state.emailError || this.state.redBorderError || this.state.emailformatError
                          ) && {
                            borderColor: "#ff0000"
                          }
                        ]}
                        value={this.state.email}
                        onChangeText={email =>
                          this.setState({ email }, () => {
                            this.validation(email, "email");
                          })
                        }
                        placeholder="example@example.com"
                        placeholderTextColor="#CCCCCC"
                      />
                    </View>
                    {this.state.emailError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("This field is required")}
                      </Text>
                    ) : null}

                    {this.state.emailformatError ? (
                      <View
                        style={
                          this.state.locale == "eng"
                            ? [styles.loginError]
                            : [styles.loginError, commonStyle.rightAlign]
                        }
                      >
                        <View style={
                          this.state.locale == "eng"
                            ? styles.mailExm_en
                            : styles.mailExm_ar
                        }>
                          <Text style={{ color: "red", flexDirection: 'row-reverse', fontFamily: "Tajawal-Medium" }}>
                            {I18n.t("Mail should be like")}
                            <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}> example@example.com </Text>
                          </Text>
                        </View>
                      </View>
                    ) : null}

                    <View style={styles.formGroup}>
                      <View>
                        <RTLView locale={this.state.locale}>
                          <RTLText style={styles.fromText}>
                            {I18n.t("Password")}
                          </RTLText>
                        </RTLView>
                      </View>
                      <View style={{ flexDirection: "row" }}>
                        <RTLView locale={this.state.locale}>
                          <TextInput
                            style={[
                              this.state.locale == "eng"
                                ? styles.formPassword_en
                                : styles.formPassword_ar,
                              (
                                this.state.passwordError || this.state.redBorderError
                              ) && {
                                borderColor: "#ff0000"
                              }
                            ]}
                            placeholder="************"
                            placeholderTextColor="#CCCCCC"
                            returnKeyType="go"
                            password={true}
                            autoCorrect={false}
                            value={this.state.password}
                            onChangeText={password =>
                              this.setState({ password }, () => {
                                this.validation(password, "password");
                              })
                            }
                            secureTextEntry={this.state.showPassword}
                          />
                          <View
                            style={
                              this.state.locale == "eng"
                                ? styles.passIcon_en
                                : styles.passIcon_ar
                            }
                          >
                            <TouchableOpacity
                              onPress={() =>
                                this.setState({
                                  showPassword: !this.state.showPassword
                                })
                              }
                            >
                              <Image
                                source={this.state.showPassword ? loginScreen.password : loginScreen.passwordOff}
                                style={
                                  this.state.locale == "eng"
                                    ? styles.passImg
                                    : styles.passImg2
                                }
                              />
                            </TouchableOpacity>
                          </View>
                        </RTLView>
                      </View>
                    </View>
                    {this.state.passwordError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("This field is required")}
                      </Text>
                    ) : null}

                    {/* //Capcha// */}
                    <View style={{ backgroundColor: '#CCCCCC' }}>
                      <ConfirmGoogleCaptcha
                        ref={_ref => this.captchaForm = _ref}
                        siteKey='6LecW_8UAAAAAJO5lSzqg0YDZoSPhafV140Pp3_G'
                        baseUrl='https://google.com'
                        languageCode='en'
                        onMessage={(e) => this.onMessage(e)}
                      />
                    </View>
                    {/* //Capcha// */}

                    <TouchableOpacity onPress={this.goToForgotPassword}>
                      <Text style={styles.forgotText}>
                        {I18n.t("Forgot password?")}
                      </Text>
                    </TouchableOpacity>

                    <View style={styles.formGroup}>
                      <TouchableOpacity
                        style={styles.commonButton}
                        onPress={this.login}
                      >
                        <Text style={styles.buttonText}>{I18n.t("Login")}</Text>
                        {this.state.isLoading && (
                          <ActivityIndicator
                            size="small"
                            color="#fff"
                            style={{
                              position: "absolute",
                              right: 30
                            }}
                          />
                        )}
                      </TouchableOpacity>
                    </View>
                    <View>
                      <RTLView locale={this.state.locale}>
                        <RTLText style={styles.dontText}>
                          {I18n.t("Dont have Bitaqaty account?")}
                        </RTLText>
                      </RTLView>
                    </View>

                    <View style={[styles.formGroup, { marginBottom: 0 }]}>
                      <TouchableOpacity
                        onPress={this._createAccount}
                        style={styles.whiteButton}
                      >
                        <Text style={styles.whiteButtonText}>
                          {I18n.t("Create New Account")}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View style={commonStyle.rights}>
                    <RTLView locale={this.state.locale}>
                      <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                        {I18n.t("All rights reserved for ")}
                      </Text>
                      <Text
                        style={[
                          commonStyle.screenText,
                          { fontSize: 12, fontFamily: "Tajawal-Bold" }
                        ]}
                      >
                        {I18n.t(" Bitaqaty ")}
                      </Text>
                      <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                        {I18n.t("@2019")}
                      </Text>
                    </RTLView>
                  </View>
                </View>
              </ScrollView>

            )}
        </View>
      </SafeAreaView>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(Login);