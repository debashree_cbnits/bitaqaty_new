import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  Dimensions
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import apiClient from "../../../services/api.client";
import Toast from "react-native-tiny-toast";
import { loginScreen, productImagePath } from "../../../config/imageConst";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
const deviceHeight = Dimensions.get("window").height;

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      //emailError: false,
      email: "",
      errorText: "",
      emailError: false,
      emailFormatError: false,
      wrongEmail: false
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
          lang: "en"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
          lang: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
        lang: "en"
      };
    }
    // return null;
  }

  backValueReceive = value => {
    if (value == "back") {
      this.props.navigation.navigate("Login");
    }
  };

  emailValidation = emailValue => {
    // let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    
    if (emailValue === ""){
        this.setState({emailError : true, wrongEmail: false, emailFormatError: false})
    } else{
      this.setState({ email: emailValue, emailError: false, wrongEmail: false,  emailFormatError: false })
    }
    
  };
  forgotPasswordFunc = () => {
    let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;

    if (this.state.email === "") {
      this.setState({emailError: true})
    }
    else if (!regex.test(this.state.email)) {
      this.setState({emailFormatError: true });
    } else if (this.state.lang != "") {
      let loginData = {
        userName: this.state.email,
        lang: this.state.lang
      };
      apiClient
        .postRequest("/forgetPassword", loginData)
        .then(forgetRes => {          
          if (forgetRes.success === 200) {
            AsyncStorage.setItem("emailForgetPassWord", this.state.email);
            this.props.navigation.navigate("LinkVerification", {
              email: this.state.email
            });
          } else if (forgetRes.error === 13){
            this.setState({ errorText: "we were unable to process your request", email: "", wrongEmail: true, });
          }else {
            this.setState({email: "" });
            Toast.show(I18n.t ('we were unable to process your request'))
          }
        }).catch(err => console.log("err", err));
    }
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={value => this.backValueReceive(value)}
        />        
        <ScrollView contentContainerStyle={styles.container}>         
          <View style={{ width: "100%" }}>
            <Text style={styles.pageTitle}>
              {I18n.t("Forgot your password?")}
            </Text>
            <Image
              source={
                this.state.locale == "eng"
                  ? loginScreen.forgotImage
                  : loginScreen.forgotImageY
              }
              resizeMode={"contain"}
              style={styles.stepsImg}
            />
            <Text style={[styles.orText, { marginBottom: 17 }]}>
              {I18n.t(
                "Please enter your email address, to send Password Reset link"
              )}
            </Text>

            <View style={[styles.formWrap, { marginTop: 0 }]}>
              <View style={[styles.formGroup, { marginTop: 8 }]}>
                <View>
                  <Text style={styles.fromText}>{I18n.t("Email")}</Text>
                </View>
                <TextInput
                  style={[
                    this.state.locale == "eng"
                      ? styles.formControl_en
                      : styles.formControl_ar,
                      (
                        this.state.emailError || this.state.emailFormatError || this.state.wrongEmail 
                        ) && {
                        borderColor: "#ff0000"
                      }
                  ]}
                  onChangeText={text =>this.setState({email: text}, ()=>{
                    this.emailValidation(text)
                  }) }
                  autoCapitalize='none'
                  value={this.state.email}
                  placeholder="example@example.com"
                  placeholderTextColor="#CCCCCC"
                />

                    {this.state.emailError ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                         {I18n.t("This field is required")}
                      </Text>
                    ) : null}
                     {this.state.emailFormatError ? (
                       <View style={
                        this.state.locale == "eng"
                          ? {flexDirection: 'row', justifyContent :'flex-start'}
                          : {flexDirection: 'row', justifyContent :'flex-end'}
                          }>
                      <View style={
                          this.state.locale == "eng"
                            ? styles.mailExm_en
                            : styles.mailExm_ar
                        }><Text style={{ color: "red", flexDirection: 'row-reverse', fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("Mail should be like")}                
                        <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}> example@example.com </Text>
                        </Text>
                      </View>
                      </View>
                    ) : null}
                     {this.state.wrongEmail ? (
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                         {I18n.t("we were unable to process your request")}
                      </Text>
                    ) : null}
              </View>

              <View style={styles.formGroup}>
                <TouchableOpacity
                  onPress={this.forgotPasswordFunc}
                  style={styles.commonButton}
                >
                  <Text style={styles.buttonText}>{I18n.t("Continue")}</Text>
                </TouchableOpacity>
              </View>

                  <View style ={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
                    <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
                    <TouchableOpacity onPress ={()=> this.props.navigation.navigate("ContactUs")}>
                      <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>  
                    </TouchableOpacity>
                  </View>
            </View>
          </View>

          <View style={{ width: "100%" }}>
            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" }
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
            {/* </View> */}
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(mapStateToProps, {})(ForgotPassword);