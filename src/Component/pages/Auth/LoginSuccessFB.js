import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { othersImage } from "../../../config/imageConst";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";

const deviceHeight = Dimensions.get("window").height;

class LoginSucessFB extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }
  componentDidMount(){
    this.getUserDetails()
  }
  getUserDetails (){
      AsyncStorage.getItem("loginuserName")
      .then((results) => {
        if (results){
          this.setState({ name: results })
        }else {
          this.setState({name : ''})
        }
      })
  } 

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={commonStyle.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        <View style={{ ...styles.container, justifyContent: "space-between" }}>
          <View style={{ width: "100%" }}>
            <View locale={this.state.locale}>
              <Text style={styles.pageTitle}>{this.state.name && this.state.name ? this.state.name  :''}</Text>
            </View>
            <View style={{ alignItems: "center", marginTop: "10%" }}>
              <Image source={othersImage.facebookBig} />
            </View>

            <View style={{ alignItems: "center" }}>
              <Text style={{ ...commonStyle.bold14, marginTop: 22 }}>
                {I18n.t("You are now connected to")}
              </Text>
              <Text style={commonStyle.light14}>{I18n.t("Facebook")}</Text>
            </View>
          </View>

          <View style={{ width: "100%" }}>
            <View style={[styles.formGroup, { marginBottom: 0 }]}>
              <TouchableOpacity
              
                onPress={() => this.props.navigation.navigate("Welcome2") }
                style={styles.commonButton}
              >
                <Text style={styles.buttonText}>
                  {I18n.t("Start Shopping")}
                </Text>
              </TouchableOpacity>
            </View>

            {/* <TouchableOpacity onPress={this.openMobile}><Text style={{fontSize: 20}}>Welcome</Text></TouchableOpacity> */}

            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(LoginSucessFB);
