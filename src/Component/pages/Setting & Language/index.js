import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import I18n from "../../../i18n/index";
import styles from "./styles";
import commonStyle from "../../../../commonStyle";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { HomeImagePath } from "../../../config/imageConst";
import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import Toast from 'react-native-tiny-toast'
import { setSelectedLanguage } from "../../../redux/language/action";
import AsyncStorage from '@react-native-community/async-storage';

class LangSelection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: ""
    };
  }

  componentDidMount() {
    this.state.locale === 'eng' ? this._selectEnglish() : this._selectArabic();
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  componentDidMount() {

    this.state.locale === 'eng' ? this._selectEnglish() : this._selectArabic();
  }

  setSelectLanguage = (language) => {
    if (language === this.state.language) {
      Toast.show(I18n.t("SameLanguage"));
      return;
    }

    if (language === "english") {
      AsyncStorage.setItem("language", JSON.stringify({ language: "english", locale: "eng" }));
      this.setState({ language: "english", locale: "eng" }, () =>
        this.props.setSelectedLanguage({ language: "english", locale: "eng" })
      );
      I18n.locale = "eng";
    } else if (language == "arabic") {
      AsyncStorage.setItem("language", JSON.stringify({ language: "arabic", locale: "ar" }));
      this.setState({ language: "arabic", locale: "ar" }, () =>
        this.props.setSelectedLanguage({ language: "arabic", locale: "ar" })
      );
      I18n.locale = "ar";
    } else {
      AsyncStorage.setItem("language", JSON.stringify({
        language: this.props.setlanguage.language,
        locale: this.props.setlanguage.locale
      }));
      this.setState({ language: this.props.setlanguage.language, locale: this.props.setlanguage.locale }, () =>
        this.props.setSelectedLanguage({ language: this.props.setlanguage.language, locale: this.props.setlanguage.locale })
      );
      I18n.locale = this.state.locale;
    }
    this.props.navigation.navigate("Welcome1");
  };


  _selectArabic = () => {
    this.arabicTouchable.props.style.borderColor = "#FFC300";
    this.arabicTouchable.props.style.borderWidth = 2;
    delete this.englishTouchable.props.style.borderColor;
    delete this.englishTouchable.props.style.borderWidth;
    this.button.language = "arabic";
    this.setState({ locale: 'ar' })
  }

  _selectEnglish = () => {
    this.englishTouchable.props.style.borderColor = "#FFC300";
    this.englishTouchable.props.style.borderWidth = 2;
    delete this.arabicTouchable.props.style.borderColor;
    delete this.arabicTouchable.props.style.borderWidth;
    this.button.language = "english";
    this.setState({ locale: 'eng' })
  }

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.logoArea}>
          <Image
            source={
              this.state.locale == "eng"
                ? HomeImagePath.logoEn
                : HomeImagePath.logoar
            }
          />
        </View>

        <View style={[styles.container, { marginTop: 12 }]}>
          <View style={styles.top}>
            <Text style={{ fontSize: 20, fontFamily: "Tajawal-Bold" }}>
              {I18n.t("Setting & Language")}
            </Text>
            <Text style={{ fontSize: 14, fontFamily: "Tajawal-Medium" }}>
              {I18n.t("Choose Language")}
            </Text>

            <TouchableOpacity
              style={
                this.state.locale == "eng" ? styles.arBtn : styles.arBtn_ar
              }
              ref={ref => this.arabicTouchable = ref}
              onPress={() => this._selectArabic()}
            >
              <Text
                style={{ fontSize: 14, fontFamily: "Tajawal-Bold" }}>
                العربية
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={
                this.state.locale == "eng" ? styles.enBtn : styles.enBtn_ar
              }
              ref={ref => this.englishTouchable = ref}
              onPress={() => this._selectEnglish()}
            >
              <Text style={{ fontSize: 14, fontFamily: "Tajawal-Bold" }}>
                English
              </Text>
            </TouchableOpacity>

            <View style={styles.selectCountry}>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={[styles.screenText, { fontSize: 14 }]}>
                    {I18n.t("Bitaqaty Service available in")}
                  </Text>
                </RTLView>
              </View>
              <View
                style={
                  this.state.locale == "eng"
                    ? styles.greyBtn_en
                    : styles.greyBtn_ar
                }
              >
                <Image
                  style={{ height: 26, width: 38, borderRadius: 4 }}
                  source={require("../../assets/image/flag2.png")}
                />
                <Text
                  style={[styles.rightsGreyText, { fontSize: 14, margin: 8 }]}
                >
                  {I18n.t("Saudi Arabia")}
                </Text>
              </View>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={[styles.screenText2, { fontSize: 12 }]}>
                    {I18n.t("More countries will be available soon")}
                  </Text>
                </RTLView>
              </View>
            </View>
          </View>

          <View style={styles.btnArea}>
            <TouchableOpacity
              ref={ref => this.button = ref}
              onPress={() => this.setSelectLanguage(this.button.language)}
              style={styles.blackBtn}
            >
              <Text
                style={styles.saveText}
              >
                {I18n.t("Save")}
              </Text>
            </TouchableOpacity>

            <View style={commonStyle.rights}>
              <RTLView locale={this.state.locale}>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, fontFamily: "Tajawal-Bold" },
                    ]}
                  >
                    {I18n.t(" Bitaqaty ")}
                  </Text>
                  <RTLText style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("@2019")}
                  </RTLText>
                </Text>
              </RTLView>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {
    setSelectedLanguage,
  }
)(LangSelection);