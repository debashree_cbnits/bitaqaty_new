const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    backgroundColor: "#fafafa",
  },
  container: {
    flex: 1,
    width: "92%",
    marginHorizontal: "4%",
    justifyContent: "space-between",
  },
  top: {
    flex: 1,
  },
  topSec: {
    width: "100%",
    height: 144,
    paddingHorizontal: "4%",
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: "#241125",
    justifyContent: "space-between",
  },
  pageTitle: {
    fontSize: 20,
    color: "#1A1A1A",
    marginBottom: 5,
    fontFamily: "Tajawal-Bold",
  },
  linkMedium: {
    fontFamily: "Tajawal-Medium",
    fontSize: 14,
    color: "#1A1A1A",
    marginBottom: 20,
  },
  mainContent: {
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  chatWithUsText: {
    fontSize: 20,
    fontFamily: "Tajawal-Bold",
  },
  requestDetails:  {
    fontFamily: "Tajawal-Regular",
    fontSize: 13,
  },
  formWrap: {
    borderStyle: "solid",
    borderColor: "#CCCCCC",
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
    marginTop: 15,
    backgroundColor: "#fff",
  },
  formGroup: {
    marginTop: 10,
    marginBottom: 10,
  },
  commonButton: {
    backgroundColor: "#241125",
    //padding: 10,
    borderRadius: 5,
    marginBottom: 10,
    height: 48,
    justifyContent: "center",
  },
  buttonTex: {
    color: "#fff",
    textAlign: "center",
    fontFamily: "Tajawal-Bold",
    fontSize: 14,
  },

  fromText: {
    marginBottom: 5,
    color: "#1A1A1A",
    fontSize: 14,
    fontFamily: "Tajawal-Medium",
  },
  formControl: {
    height: 48,
    borderStyle: "solid",
    borderColor: "#CCCCCC",
    borderWidth: 1,
    padding: 10,
    borderRadius: 4,
  },

  formControl_en: {
    height: 48,
    borderStyle: "solid",
    borderColor: "#CCCCCC",
    borderWidth: 1,
    padding: 10,
    borderRadius: 4,
    fontFamily: "Tajawal-Light",
    color: "#4F4F4F",
  },
  formControl_ar: {
    height: 48,
    borderStyle: "solid",
    borderColor: "#CCCCCC",
    borderWidth: 1,
    padding: 10,
    borderRadius: 4,
    fontFamily: "Tajawal-Light",
    color: "#4F4F4F",
    textAlign: "right",
  },
  mobileImageStyle: { width: "12%", justifyContent: "center" },
  // ///// phone input
  mobileInputHead: {
    flexDirection: "row",
    //justifyContent: "space-between"
  },
  countryCode: {
    width: "18%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F2F0F0",
  },
  countryCodeText: {
    color: "#4F4F4F",
    fontFamily: "Tajawal-Bold",
    fontSize: 16,
  },
  mobileTextInput: {
    width: "70%",
    fontFamily: "Tajawal-Light",
    fontSize: 14,
    flexDirection: "row-reverse",
    height: 48,
  },
  mobileTextInput_ar: {
    width: "70%",
    fontFamily: "Tajawal-Light",
    fontSize: 14,
    textAlign: "right",
  },
  mobileInput: {
    flexDirection: "row",
    borderColor: "#CCCCCC",
    borderWidth: 1,
    borderRadius: 5,
    overflow: "hidden",
  },
  mobileInput_ar: {
    flexDirection: "row-reverse",
    borderColor: "#CCCCCC",
    borderWidth: 1,
    borderRadius: 5,
    overflow: "hidden",
  },
  countryText: {
    color: "#4F4F4F",
    fontFamily: "Tajawal-Light",
  },
  flagImage: {
    height: 25,
    width: 30,
    borderRadius: 4,
    alignSelf: "flex-end",
  },
  phoneStyle: {
    borderColor: "#CCCCCC",
    borderWidth: 1,
    width: "88%",
    flexDirection: "row",
    borderRadius: 5,
  },
  mobileFormStyle: { justifyContent: "space-between", backgroundColor: "red" },
  mobileHeaderStyle: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "red",
  },
  buttonStyle: {
    alignItems: "center",
    backgroundColor: "#30302F",
    borderRadius: 8,
    marginTop: 15,
    marginBottom: 5,
    paddingVertical: 10,
  },
  startChatStyle: {
    color: "#fff",
    fontFamily: "Tajawal-Bold",
    fontSize: 15,
  },
  closeIcon: {
    marginRight: 30,
  },
  closeIcon_ar: {
    marginLeft: 30,
  },
  chatHead: {
    marginBottom: "1%",
    color: "#fff",
    fontSize: 16,
    fontFamily: "Tajawal-Bold",
  },
  //live chat styles
  chatBubbleStyle: { 
    width: 60, 
    height: 60, 
    borderRadius: 60 / 2, 
    borderWidth: 1,
    borderColor: '#ececec',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center' 
  },
  //


  agentName: {
    marginHorizontal: 10,
    color: "#CCCCCC",
    fontFamily: "Tajawal-Light",
    fontSize: 14,
  },
  rate: {
    color: "#FFFFFF",
    fontFamily: "Tajawal-Light",
    fontSize: 12,
  },
  rate_ar: {
    color: "#FFFFFF",
    fontFamily: "Tajawal-Light",
    fontSize: 12,
  },
  recvChat: {
    width: "88%",
    backgroundColor: "#fff",
    padding: 15,
    borderRadius: 8,
    borderTopLeftRadius: 0,
  },
  recvChatText: {
    color: "#1A1A1A",
    fontFamily: "Tajawal-Medium",
    fontSize: 14,
  },
  userIcon2: {
    height: 25,
    width: 25,
    borderRadius: 13,
  },
  sendMsg: {
    width: "100%",
    backgroundColor: "#241125",
    padding: 15,
    borderRadius: 8,
    borderTopRightRadius: 0,
  },
  send: {
    marginTop: 20,
  },
  sendChatText: {
    color: "#fff",
    fontFamily: "Tajawal-Medium",
    fontSize: 14,
  },
  chatTime: {
    marginTop: 5,
    textAlign: "right",
    color: "#4F4F4F",
    fontFamily: "Tajawal-Light",
    fontSize: 12,
  },
  chatTime_ar: {
    marginTop: 5,
    textAlign: "left",
    color: "#4F4F4F",
    fontFamily: "Tajawal-Light",
    fontSize: 12,
  },
  chatTypingArea: {
    width: "100%",
    paddingHorizontal: "4%",
    height: 72,
    position: "absolute",
    bottom: 0,
    backgroundColor: "#FAFAFA",
    elevation: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  editViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    paddingHorizontal: 5,
    borderRadius: 4
  },
  editTextStyle: {
    fontSize: 14,
    fontFamily: 'Tajawal-Regular',
    paddingLeft: 5
  },
  agentTypingText: {
    color: '#cccccc',
    fontFamily: 'Tajawal-Regular',
    fontSize: 12,
    paddingLeft: 10
  },
  leftArchivedStyle: {
    borderWidth: 1,
    borderColor: "#ececec",
    marginHorizontal: 20,
    marginTop: 10
  },
  rateViewStyle: {
    marginHorizontal: 80,
    borderRadius: 15,
    marginTop: 10
  },
  rateTextStyle: {
    textAlign: 'center',
    fontFamily: 'Tajawal-Regular',
    padding: 5,
    borderRadius: 15,
    backgroundColor: '#ececec'
  },
  errorTextStyle: {
    textAlign: "center",
    marginTop: 30,
    fontFamily: "Tajawal-Regular",
    fontSize: 16
  }
};