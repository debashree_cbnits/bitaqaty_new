import React, { Component } from "react";
import {View,
} from "react-native";
import styles from "./styles";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import WebView from "react-native-webview";


class ChatWithUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      name: "",
      email: "",
      accessToken: "",
      mobileInput: "",
      language: "",
      errorText: "",
      nameEditableStatus: true,
      emailEditableStatus: true,
      isLoading: false,
      noToken: false
    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }    
  }

  backValueReceive = value => {    
    if (value == "back") {      
      this.props.navigation.navigate("Menu");
    }
  };
  
  render() {    
    return(
      <View style={styles.mainSection}>
        <WebView
          source={{
            uri: 'https://secure.livechatinc.com/licence/8646654/v2/open_chat.cgi',
          }} />
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(
  mapStateToProps,
  {}
)(ChatWithUs);