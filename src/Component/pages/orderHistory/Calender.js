import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, Modal } from "react-native";
import styles from "./style";
import I18n from "../../../i18n/index";
import { chatScreen, orderHistoryImage } from "../../../config/imageConst";
import { connect } from "react-redux";
import CalendarPicker from "react-native-calendar-picker";
import ModalOrder from "../Modal/ModalOrder";
import moment from 'moment'

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      selectedStartDate: null,
      selectedEndDate: null,
      orderHistoryModal: false,
    };
  }

  onDateChange = (date, type) => {
    if (type === "END_DATE") {
      this.setState({
        selectedEndDate: date,
      });
    } else {
      this.setState({
        selectedStartDate: date,
        selectedEndDate: null,
      });
    }
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  saveDate = () => {
    const startDate = this.state.selectedStartDate ? this.state.selectedStartDate.toString() : "";
    const endDate = this.state.selectedEndDate ? this.state.selectedEndDate.toString() : "";
    this.props.navigation.navigate('OrderHistory', { "startDate": moment(startDate).format('DD/MM/YYYY'), "endDate": moment(endDate).format('DD/MM/YYYY') })
  }

  render() {
    const { selectedStartDate, selectedEndDate } = this.state;    
    const startDate = selectedStartDate ? selectedStartDate.toString() : "";
    const endDate = selectedEndDate ? selectedEndDate.toString() : "";

    var currentYear = (new Date).getFullYear();
    // var currentMonth = (new Date).getMonth();
    var currentDay = (new Date).getDate();

    return (
      <View style={styles.mainSection}>
        <Modal transparent={true} visible={this.state.orderHistoryModal}>
          <View style={{ backgroundColor: "#0000008A", flex: 1 }}>
            <ModalOrder />
          </View>
        </Modal>
        <View style={styles.calendarTop}>
          <View
            style={
              this.state.locale === "eng"
                ? styles.closeSave
                : styles.closeSave_ar
            }
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("OrderHistory")}
            >
              <Image source={chatScreen.chatClose} />
            </TouchableOpacity>

            <TouchableOpacity style={startDate && endDate ? styles.saveBtn : styles.disableSave} onPress={() => this.saveDate()}>
              <Text style={startDate && endDate ? styles.saveBtnText : styles.disableSaveTxt}>{I18n.t("Save")}</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.range}>
            <Text style={styles.modalTopText}>{I18n.t("Order History")}</Text>
            <View
              style={
                this.state.locale === "eng"
                  ? styles.orderDate
                  : styles.orderDate_ar
              }
            >
              <View
                style={
                  this.state.locale === "eng"
                    ? styles.dateRange
                    : styles.dateRange_ar
                }
              >
                <TouchableOpacity style={{ flexDirection: 'row' }}>
                  <Text style={styles.modalTopDate}>{startDate ? this.state.locale == "eng" ? moment(startDate).format('MMMM') : moment(startDate).format('Do') : I18n.t("From Date")}</Text>

                  <Text style={styles.modalTopDate}>{" "}{startDate ? this.state.locale == "eng" ? moment(startDate).format('Do') : I18n.t(moment(startDate).format('MMMM')) : I18n.t("")}{" "}</Text>
                </TouchableOpacity>
                <Text style={{ color: "#fff" }}> - </Text>
                <TouchableOpacity style={{ flexDirection: 'row' }}>
                  <Text style={styles.modalTopDate}>{endDate ? this.state.locale == "eng" ? moment(endDate).format('MMMM') : moment(endDate).format('Do') : I18n.t("To Date")}</Text>

                  <Text style={styles.modalTopDate}>{" "}{endDate ? this.state.locale == "eng" ? moment(endDate).format('Do') : I18n.t(moment(endDate).format('MMMM')) : I18n.t("")}{" "}</Text>
                </TouchableOpacity>
              </View>

              {/* <TouchableOpacity
                onPress={() => {
                  this.setState({ orderHistoryModal: true });
                }}
                style={{ marginTop: 15 }}
              >
                <Image source={orderHistoryImage.editIconWhite} />
              </TouchableOpacity> */}
            </View>
          </View>
        </View>

        <View
          style={
            this.state.locale === "eng"
              ? styles.calender_en
              : styles.calender_ar
          }
        >
          <CalendarPicker
            startFromMonday={false}
            allowRangeSelection={true}
            allowBackwardRangeSelect={true}
            selectedRangeStartStyle={styles.rangeStart}
            selectedRangeEndStyle={styles.rangeEnd}
            selectedRangeStyle={styles.rangeColor}
            maxDate ={new Date()}          
            // minDate={minDate}
            // maxDate={maxDate}
            weekdays={[
              I18n.t("Su"), I18n.t("Mo"), I18n.t("Tu"), I18n.t("We"), I18n.t("Th"), I18n.t("Fr"), I18n.t("Sa")
            ]}
            months={[
              I18n.t("January"), I18n.t("February"), I18n.t("March"), I18n.t("April"), I18n.t("May"), I18n.t("June"), I18n.t("July"), I18n.t("August"), I18n.t("September"), I18n.t("October"), I18n.t("November"), I18n.t("December"),
            ]}
            todayBackgroundColor="#0000"
            selectedDayColor="#FFC300"
            selectedDayTextColor="#1A1A1A"
            selectedDayStyle={styles.selectedDay}
            scaleFactor={350}
            previousTitle=" "
            nextTitle=" "
            dayShape="square"
            dayLabelsWrapper={
              this.state.locale === "eng"
                ? styles.dayLables_en
                : styles.dayLables_ar
            }
            monthYearHeaderWrapperStyle={
              this.state.locale === "eng"
                ? styles.monthCalendar
                : styles.monthCalendar_ar
            }
            textStyle={
              this.state.locale === "eng"
                ? styles.calendarText_en
                : styles.calendarText_ar
            }
            onDateChange={this.onDateChange}
          />
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Calendar);
