import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  Modal, ActivityIndicator
} from "react-native";
import {
  orderHistoryImage,
  othersImage,
  menuScreen,
} from "../../../config/imageConst";
import styles from "./style";
import I18n from "../../../i18n/index";
import { connect, connectAdvanced } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";
import moment from 'moment'
import { or } from "react-native-reanimated";
import imageUrl from '../../../services/config';
import { RTLView, RTLText } from "react-native-rtl-layout";


const deviceHeight = Dimensions.get('window').height;

class OrderHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      show: false,
      showOrderDetails: false,
      isLoading: true,
      orderListArray: [],
      orderDetails: {},
      startDate: this.props.navigation.state.params && this.props.navigation.state.params.startDate ? this.props.navigation.state.params.startDate : "",
      endDate: this.props.navigation.state.params && this.props.navigation.state.params.endDate ? this.props.navigation.state.params.endDate : "",
      backmsg: this.props.navigation.state.params && this.props.navigation.state.params.backmsg ? this.props.navigation.state.params.backmsg : "",
      filterArrayExists: false,
      testarray: [], 
      totalNumberOfQuantity: 0,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  componentDidMount() {
    this.getOrderList()
    this.setState({
      startDate: this.props.navigation.state.params && this.props.navigation.state.params.startDate ? this.props.navigation.state.params.startDate : "",
      endDate: this.props.navigation.state.params && this.props.navigation.state.params.endDate ? this.props.navigation.state.params.endDate : ""
    }, () => {
      if (this.state.startDate) {
        this.getfilteredData()
      }
    })
    this.props.navigation.addListener("didFocus", () => {
      this.setState({
        startDate: this.props.navigation.state.params && this.props.navigation.state.params.startDate ? this.props.navigation.state.params.startDate : "",
        endDate: this.props.navigation.state.params && this.props.navigation.state.params.endDate ? this.props.navigation.state.params.endDate : ""
      }, () => {
        if (this.state.startDate) {
          this.getfilteredData()
        }
      })
    });

  }

  getOrderList() {    
    AsyncStorage.getItem("userToken").then((results) => {
      if (results) {
        let data =
          { "pageSize": 2000, "pageNumber": 1 }
        this.setState({ isLoading: true });
        apiClient
          .postRequest("/orderList", data, results)
          .then((orderListRes) => {            
            if (orderListRes) {
              this.setState({ orderListArray: orderListRes, isLoading: false, testarray: orderListRes})
            } else {
              this.setState({ orderListArray: [], filterArrayExists: false })
            }
          })
          .catch((err) => this.setState({ isLoading: false }));
      } else {
        this.setState({ isLoading: false });
      }
    });
  }
  getfilteredData() {

    this.setState({ isLoading: true })
    let start = this.state.startDate
    let end = this.state.endDate
    let result = this.state.testarray.filter(d => {
      var date = moment(d.orderDate).format('L');
      return (date >= start && date <= end);
    });
    if (result) {
      this.setState({ orderListArray: result, isLoading: false, filterArrayExists: true })
    } else {
      this.setState({ isLoading: false })
    }
  }

  backValueReceive = (value) => {
    if (value == "back") {
      if (this.state.backmsg == "profile") {
        let edit = "non";
        this.props.navigation.navigate("ProfileDetails", { edit: edit });
      }
      else {
        this.props.navigation.navigate("Menu");
      }
    }
  };

  openCalendar() {
    this.props.navigation.navigate("Calender")
  }


  onReceipt = () => {
    this.props.navigation.navigate("Receipt");
  };

  getOrderDetails = (data) => {
    let totalQuantity = 0;
    this.setState({ showOrderDetails: !this.state.showOrderDetails, orderDetails: data }, ()=>{      
      for (let i = 0; i< this.state.orderDetails.bulkPurchaseModelList.length; i++){
        totalQuantity += this.state.orderDetails.bulkPurchaseModelList[i].numberOfItems;              
        this.setState({totalNumberOfQuantity : totalQuantity}) 
      } 
    })
  }

  render() {
    const imagePathUrl = "https://www.bitaqaty.com/assets/images/paymentMethods/";
    const { orderListArray, orderDetails, filterArrayExists } = this.state
    return (
      <>

        {/* Recipt Details----start */}
        <Modal transparent={true} visible={this.state.showOrderDetails}>
          <View style={{ flex: 1, backgroundColor: "#0000008a" }}>
            <View style={{ backgroundColor: "#fff", height: deviceHeight / 2 }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showOrderDetails: false });
                }}
                style={[styles.receiptSec, { flexDirection: this.state.locale == "eng" ? "row" : "row-reverse" }]}
              >
                <Text style={styles.receiptText}>{I18n.t("Receipt Details")}</Text>
                <Image source={menuScreen.downarrow} />
              </TouchableOpacity>

              <View
                style={
                  this.state.locale == "eng"
                    ? [styles.totalPrice, styles.paymentSec]
                    : [styles.totalPrice_ar, styles.paymentSec]
                }
              >
                <Text style={styles.medLightText}>
                  {I18n.t("Total Payment")}{" "}
                </Text>
                <Text style={styles.totalCartPrice}>{(orderDetails.totalPayment + orderDetails.totalVat + orderDetails.fee).toFixed(2)} </Text>
                <Text style={styles.tagSym}>{I18n.t("SAR")} </Text>
              </View>

              <ScrollView>

                <View style={styles.vatDetails}>
                  <Text style={styles.lightText}>
                    {I18n.t("Number of Cards")}{" "}
                    <Text style={styles.medText}>{this.state.totalNumberOfQuantity && this.state.totalNumberOfQuantity}</Text>
                  </Text>
                  <Text style={styles.lightText}>
                    {I18n.t("Total Price")}{" "}
                    <Text style={styles.medText}>{orderDetails.totalPayment}</Text> {I18n.t("SAR")}
                  </Text>
                  <Text style={styles.lightText}>
                    {I18n.t("Total VAT")}{" "}
                    <Text style={styles.medText}>{orderDetails.totalVat}</Text> {I18n.t("SAR")}
                  </Text>
                  <Text style={styles.lightText}>
                    {I18n.t("Extra fees")}{" "}
                    <Text style={styles.medText}>{orderDetails.fee}</Text> {I18n.t("SAR")}
                  </Text>
                </View>

                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.paymentMode
                      : styles.paymentMode_ar
                  }
                >
                  <View style={styles.wrap100}>
                    <Text style={styles.head}>{I18n.t("Receipt Number")}</Text>
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? styles.info2
                          : styles.info2_ar
                      }
                    >
                      {orderDetails.orderReferenceNo}
                    </Text>
                  </View>
                  <View style={styles.wrap50}>
                    <Text style={styles.head}>{I18n.t("Payment Method")}</Text>
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? styles.info2
                          : styles.info2_ar
                      }
                    >
                      {orderDetails.paymentMethod}
                    </Text>
                  </View>

                  <View
                    style={
                      this.state.locale == "eng"
                        ? styles.imgWrap
                        : styles.imgWrap_ar
                    }
                  >
                    <Image
                      // source={cartImage.visaPayment}
                      source={{
                        uri: imagePathUrl + orderDetails.paymentMethod + ".png"
                      }}
                      style={{ width: '100%', height: 80, marginBottom: 5, }}
                      style={styles.recImg}
                    />
                  </View>
                  <View style={styles.wrap50}>
                    <Text style={styles.head}>
                      {I18n.t("Date of Purchase")}
                    </Text>
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? styles.info2
                          : styles.info2_ar
                      }
                    >
                      {moment(orderDetails.orderDate).format('DD/MM/YYYY')}
                    </Text>
                  </View>
                  <View style={styles.wrap50}>
                    <Text style={styles.head}>{I18n.t("Time")}</Text>
                    <Text
                      style={
                        this.state.locale == "eng"
                          ? styles.info2
                          : styles.info2_ar
                      }
                    >
                      {moment(orderDetails.orderDate).format('HH:mm')}
                    </Text>
                  </View>
                  {orderDetails.bulkPurchaseModelList && orderDetails.bulkPurchaseModelList.map((data => {                    
                    return (
                      <>
                        <View style={styles.orderImage}>
                          <Image
                            source={{ uri: imageUrl.productImageUrl + data.product.backImagePath }}
                            style={styles.cartProdImg}
                          />
                        </View>
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.orderDetails
                              : styles.orderDetails_ar
                          }
                        >
                          <View>
                            <RTLView locale={this.state.locale}>
                              <RTLText style={styles.medText}>
                                {this.state.locale == "eng" ? data.product.nameEn : data.product.nameAr}
                              </RTLText>
                            </RTLView>
                          </View>

                          <View>
                            <RTLView locale={this.state.locale}>
                              <RTLText style={styles.medLightText}>
                                {I18n.t("Price")}{" "}
                                <Text style={styles.BoldText}>
                                  {data.product.discountPercentage > 0 ? data.product.individualPriceAfter.toFixed(2) : data.product.individualPrice.toFixed(2)}</Text>{" "}
                                {I18n.t("SAR")}
                              </RTLText>
                            </RTLView>
                          </View>

                          <View>
                            <RTLView locale={this.state.locale}>
                              <RTLText style={styles.medLightText}>
                                {I18n.t("VAT")}{" "}
                                <Text style={styles.BoldText}>{data.product.vat}</Text>{" "}
                                {I18n.t("SAR")}
                              </RTLText>
                            </RTLView>
                          </View>
                        </View>
                        {data.productItem.map((item1, index) => {
                          return (
                            <View
                              style={
                                this.state.locale == "eng"
                                  ? styles.detailsSec
                                  : styles.detailsSec_ar
                              }
                            >
                              <View style={styles.detailsInfo}>

                                {item1.username &&
                                  <>
                                    <Text style={styles.head}>{I18n.t("User Name")}</Text>
                                    <Text
                                      style={
                                        this.state.locale == "eng" ? styles.info : styles.info_ar
                                      }
                                    >
                                      {item1.username}
                                    </Text>
                                  </>
                                }
                                <Text style={styles.head}>{I18n.t("Password")}</Text>
                                <Text
                                  style={
                                    this.state.locale == "eng" ? styles.info : styles.info_ar
                                  }
                                >
                                  {item1.secret}
                                </Text>
                                <Text style={styles.head}>{I18n.t("Serial Number")}</Text>
                                <Text
                                  style={
                                    this.state.locale == "eng" ? styles.info : styles.info_ar
                                  }
                                >
                                  {item1.serial}
                                </Text>
                              </View>
                              <View style={styles.detailsNo}>
                                <Text style={styles.detailsNoText}>{index + 1}/{data.productItem && data.productItem.length}</Text>
                              </View>
                            </View>
                          )
                        })}
                      </>
                    )
                  }))}



                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
        {/* Recipt details ---end */}

        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />

        <View style={styles.mainSection} >
          {orderListArray && orderListArray.length > 0 || filterArrayExists && orderListArray.length == 0 ?
            <View style={styles.container}>
              <View style={styles.calenderWrap}>
                <View>
                  <Text style={styles.textLable}>
                    {I18n.t("Choose Date Range")}
                  </Text>
                </View>
                <View
                  style={[
                    this.state.locale === "eng"
                      ? styles.calenderArea
                      : [styles.calenderArea, styles.calenderArea_ar],
                  ]}
                >
                  <View
                    style={[
                      this.state.locale === "eng"
                        ? styles.calenderInputArea
                        : [styles.calenderInputArea, styles.calenderInputArea_ar],
                    ]}
                  >
                    <View style={
                      this.state.locale === "eng"
                        ? { flexDirection: 'row' }
                        : { flexDirection: 'row-reverse' }
                    }>
                      <Text style={styles.textInput}>

                        {this.state.startDate ? this.state.startDate : I18n.t("From Day")}
                      </Text>
                      <Text>{" "}-{" "}</Text>
                      <Text style={styles.textInput}>
                        {this.state.endDate ? this.state.endDate : I18n.t("To Day")}
                      </Text>
                    </View>

                    <Image
                      style={styles.textInputArrow}
                      source={orderHistoryImage.roundArrowDown}
                    />
                  </View>
                  <TouchableOpacity
                    style={{ width: "10%", }}
                    onPress={() => this.openCalendar()}
                  >
                    <Image
                      source={orderHistoryImage.roundCalendar}
                      style={
                        this.state.locale === "eng"
                          ? styles.calenderImage2
                          : [styles.calenderImage2, styles.calenderImage2_ar]
                      }
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <Text style={styles.textTitle}>{I18n.t("Order History")}</Text>
            </View>
            : null}

          <ScrollView>
            {this.state.isLoading ? (
              <ActivityIndicator 
              size="small"
              color="#1A1A1A" />
            ) : (
                orderListArray && orderListArray.length > 0 ?
                  <View style={styles.cardWrap}>
                    {orderListArray.map((data, index) => {
                      return (
                        <TouchableOpacity key={data.id} onPress={() => this.getOrderDetails(data)}
                          style={[
                            this.state.locale === "eng"
                              ? styles.cardArea
                              : [styles.cardArea, styles.cardArea_ar],
                          ]}
                        >
                          <Text style={styles.textLable}>{data.orderReferenceNo}</Text>
                          <View
                            style={[
                              this.state.locale === "eng"
                                ? styles.productWrap
                                : [styles.productWrap, styles.productWrap_ar],
                            ]}
                          >
                            <View
                              style={
                                this.state.locale === "eng"
                                  ? styles.productImageArea
                                  : styles.productImageArea_ar
                              }
                            >
                              {data.bulkPurchaseModelList && data.bulkPurchaseModelList.map((item, index) => {
                                return (
                                  <Image
                                    style={styles.productImage}
                                    source={{ uri: imageUrl.productImageUrl + item.product.backImagePath }}
                                  />
                                )
                              })}
                            </View>
                            <Text style={styles.dateText}>{moment(data.orderDate).format('DD/MM/YYYY')}</Text>
                          </View>
                          <View
                            style={[
                              this.state.locale === "eng"
                                ? styles.productPrize
                                : [styles.productPrize, styles.productPrize_ar],
                            ]}
                          >
                            <Text style={styles.priceText}>{I18n.t("Total")}</Text>
                            <Text style={[styles.priceText, styles.priceTextBold]}>
                              {(data.totalPayment + data.totalVat + data.fee).toFixed(2)}
                            </Text>
                            <Text style={styles.priceText}>{I18n.t("SAR")}</Text>
                          </View>
                        </TouchableOpacity>
                      )
                    })}
                  </View>
                  : !filterArrayExists && orderListArray.length == 0 ?
                    <View style={[styles.emptyHeadnull]}>
                      <Text style={
                          this.state.locale == "eng"
                            ? styles.textTitle
                            : styles.textTitle2
                        }>{I18n.t("Order History")}</Text>
                      <View style={{ width: "100%", marginTop: '15%', alignItems: "center" }}>
                        <Image
                          source={othersImage.orderHB}
                          style={{ marginBottom: 20 }}
                        />
                        <Text style={styles.emptyText}>
                          {I18n.t("You haven't purchased any cards yet")}
                        </Text>
                      </View>
                    </View> :
                    filterArrayExists && orderListArray.length == 0 &&
                    <View style={styles.emptyHead}>
                      <View style={{ width: "100%", alignItems: "center" }}>
                        <Image
                          source={othersImage.calendarGrey}
                          style={{ marginBottom: 20 }}
                        />
                        <Text style={styles.emptyText}>
                          {I18n.t("You have not purchased any cards on this date")}
                        </Text>
                      </View>
                    </View>
              )}
          </ScrollView>
        </View>

        {/* @@@@@@@@@@##### empty history after order #####@@@ */}
      </>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(OrderHistory);
