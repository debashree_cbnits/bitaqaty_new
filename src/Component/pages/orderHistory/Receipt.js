import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Modal, ActivityIndicator
} from "react-native";
import styles from "./style";
import {
  HomeImagePath,
  menuScreen,
  othersImage,
  CatagoryImagePath,
} from "../../../config/imageConst";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";
import moment from 'moment';
import imageUrl from '../../../services/config';

class Receipt extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      show: false,
      howToUse: false,
      isLoading: false,
      receiptDetails: {},
      totalNumberOfQuantity : 0,
      orderId: this.props.navigation.state.params ? this.props.navigation.state.params.orderId : '',
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  componentDidMount() {
    this.gerReceiptDetails()
  }

  gerReceiptDetails() {
    let totalQuantity = 0;
    this.setState({ isLoading: true })    
    AsyncStorage.getItem("userToken")
      .then((token) => {
        let data = {          
          "id": this.state.orderId
        }
        apiClient.postRequest('/orderReceipt', data, token).then((orderReceiptRes) => {          
          this.setState({ isLoading: false, receiptDetails: orderReceiptRes }, ()=>{
            for (let i = 0; i< this.state.receiptDetails.bulkPurchaseModelList.length; i++){
              totalQuantity += this.state.receiptDetails.bulkPurchaseModelList[i].numberOfItems;              
              this.setState({totalNumberOfQuantity : totalQuantity}) 
            }            
          })
        }).catch(err => {
          this.setState({ isLoading: false })
        })
      }).catch(err => { this.setState({ isLoading: false }) })

  }
  render() {
    const imagePathUrl = "https://www.bitaqaty.com/assets/images/paymentMethods/";    
    return (
      <>
        {this.state.isLoading ? (
          <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A" />
        ) : (
            <View style={styles.mainSection}>
              <View style={styles.logoArea}>
                <Image
                  source={
                    this.state.locale == "eng"
                      ? HomeImagePath.logoEn
                      : HomeImagePath.logoar
                  }
                />
              </View>

              <TouchableOpacity
                onPress={() => {
                  this.setState({ show: true });
                }}
                style={[styles.receiptSec, { flexDirection: this.state.locale == "eng" ? "row" : "row-reverse" }]}
              >
                <Text style={styles.receiptText}>{I18n.t("Receipt Details")}</Text>
                <Image source={menuScreen.downarrow} />
              </TouchableOpacity>

              {/* @@@@@@@@@@ receipt modal section @@@@@@@@@ */}
              <Modal transparent={true} visible={this.state.show}>
                <View style={{ flex: 1, backgroundColor: "#0000008a" }}>
                  <View style={{ backgroundColor: "#fff" }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ show: false });
                      }}
                      style={[styles.receiptSec, { flexDirection: this.state.locale == "eng" ? "row" : "row-reverse" }]}
                    >
                      <Text style={styles.receiptText}>{I18n.t("Receipt Details")}</Text>
                      <Image source={menuScreen.downarrow} />
                    </TouchableOpacity>

                    <View
                      style={
                        this.state.locale == "eng"
                          ? [styles.totalPrice, styles.paymentSec]
                          : [styles.totalPrice_ar, styles.paymentSec]
                      }
                    >
                      <Text style={styles.medLightText}>
                        {I18n.t("Total Payment")}{" "}
                      </Text>
                      <Text style={styles.totalCartPrice}>{(this.state.receiptDetails.totalPayment + this.state.receiptDetails.totalVat + this.state.receiptDetails.fee).toFixed(2)} </Text>
                      <Text style={styles.tagSym}>{I18n.t("SAR")} </Text>
                    </View>

                    <View style={styles.vatDetails}>
                      <Text style={styles.lightText}>
                        {I18n.t("Number of Cards")}{" "}
                        <Text style={styles.medText}>{this.state.totalNumberOfQuantity && this.state.totalNumberOfQuantity}</Text>
                      </Text>
                      <Text style={styles.lightText}>
                        {I18n.t("Total Price")}{" "}
                        <Text style={styles.medText}>{this.state.receiptDetails.totalPayment && (this.state.receiptDetails.totalPayment).toFixed(2)}</Text> {I18n.t("SAR")}
                      </Text>
                      <Text style={styles.lightText}>
                        {I18n.t("Total VAT")}{" "}
                        <Text style={styles.medText}>{this.state.receiptDetails.totalVat ? (this.state.receiptDetails.totalVat).toFixed(2) : 0}</Text> {I18n.t("SAR")}
                      </Text>
                      <Text style={styles.lightText}>
                        {I18n.t("Extra fees")}{" "}
                        <Text style={styles.medText}>{this.state.receiptDetails.fee}</Text> {I18n.t("SAR")}
                      </Text>
                    </View>

                    <View
                      style={
                        this.state.locale == "eng"
                          ? styles.paymentMode
                          : styles.paymentMode_ar
                      }
                    >
                      <View style={styles.wrap100}>
                        <Text style={styles.head}>{I18n.t("Receipt Number")}</Text>
                        <Text
                          style={
                            this.state.locale == "eng"
                              ? styles.info2
                              : styles.info2_ar
                          }
                        >
                          {this.state.receiptDetails.orderReferenceNo}
                        </Text>
                      </View>
                      <View style={styles.wrap50}>
                        <Text style={styles.head}>{I18n.t("Payment Method")}</Text>
                        <Text
                          style={
                            this.state.locale == "eng"
                              ? styles.info2
                              : styles.info2_ar
                          }
                        >
                          {this.state.receiptDetails.paymentMethod}
                        </Text>
                      </View>

                      <View
                        style={
                          this.state.locale == "eng"
                            ? styles.imgWrap
                            : styles.imgWrap_ar
                        }
                      >
                        <Image
                          source={{
                            uri: imagePathUrl + this.state.receiptDetails.paymentMethod + ".png"
                          }}
                          style={{ width: '100%', height: 80, marginBottom: 5, }}
                          style={styles.recImg}
                        />
                      </View>
                      <View style={styles.wrap50}>
                        <Text style={styles.head}>
                          {I18n.t("Date of Purchase")}
                        </Text>
                        <Text
                          style={
                            this.state.locale == "eng"
                              ? styles.info2
                              : styles.info2_ar
                          }
                        >
                          {moment(this.state.receiptDetails.orderDate).format('L')}
                        </Text>
                      </View>
                      <View style={styles.wrap50}>
                        <Text style={styles.head}>{I18n.t("Time")}</Text>
                        <Text
                          style={
                            this.state.locale == "eng"
                              ? styles.info2
                              : styles.info2_ar
                          }
                        >
                          {moment(this.state.receiptDetails.orderDate).format('HH:mm')}

                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </Modal>

              {/* @@@@@@@@@@ how to use modal section @@@@@@@@@ */}

              <Modal transparent={true} visible={this.state.howToUse}>
                <View style={{ flex: 1, backgroundColor: "#0000008a" }}>
                  <View
                    style={styles.howModalHead}
                  >
                    <Image source={menuScreen.about80} />
                    <Text
                      style={styles.howModalText}
                    >
                      {I18n.t("How To Use")}
                    </Text>
                    <View>
                      <Text style={styles.light12}>{I18n.t("step1")}</Text>
                      <Text style={styles.light12}>{I18n.t("step2")}</Text>
                      <Text style={styles.light12}>{I18n.t("step3")}</Text>
                      <Text style={styles.light12}>{I18n.t("step4")}</Text>
                      <Text style={styles.light12}>{I18n.t("step5")}</Text>
                      <Text style={styles.light12}>{I18n.t("step6")}</Text>
                    </View>
                    <TouchableOpacity onPress={() => {
                      this.setState({ howToUse: false });
                    }}>
                      <Image
                        source={CatagoryImagePath.rightArrow}
                        style={styles.rotateImage}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>

              <ScrollView>
                {/* @@@@@@@@@@ top success msg @@@@@@@@@ */}
                <TouchableNativeFeedback
                  style={
                    this.state.locale == "eng"
                      ? commonStyle.successMsg
                      : commonStyle.successMsg_ar
                  }
                  onPress={() => this.props.navigation.navigate("OrderHistory")}
                >
                  <Image source={othersImage.rightTick} style={styles.tickImg} />
                  <Text style={commonStyle.errorMsgText}>
                    {I18n.t(
                      "You can see the latest receipt and all receipts on your"
                    )}
                    <Text style={commonStyle.emptyBlueMsg}>
                      {I18n.t(" Order History ")}
                    </Text>
                    <Text>
                      <Text>{I18n.t("Page")}</Text>
                    </Text>
                  </Text>
                </TouchableNativeFeedback>

                {this.state.receiptDetails.bulkPurchaseModelList && this.state.receiptDetails.bulkPurchaseModelList.map((data => {                  
                  return (

                    <>
                      <View style={styles.cartMain}>
                        <View style={styles.cartOrder}>
                          <RTLView locale={this.state.locale}>
                            <View style={styles.orderImage}>
                              <Image
                                source={{ uri: imageUrl.productImageUrl + data.product.backImagePath }}
                                style={styles.cartProdImg}
                              />
                            </View>

                            <View
                              style={
                                this.state.locale == "eng"
                                  ? styles.orderDetails
                                  : styles.orderDetails_ar
                              }
                            >
                              <View>
                                <RTLView locale={this.state.locale}>
                                  <RTLText style={styles.medText}>
                                    {this.state.locale == "eng" ? data.product.nameEn : data.product.nameAr}
                                  </RTLText>
                                </RTLView>
                              </View>

                              <View>
                                <RTLView locale={this.state.locale}>
                                  <RTLText style={styles.medLightText}>
                                    {I18n.t("Price")}{" "}
                                    <Text style={styles.BoldText}>
                                      {data.product.discountPercentage > 0 ? (data.product.individualPriceAfter && (data.product.individualPriceAfter).toFixed(2)) : (data.product.individualPrice && (data.product.individualPrice).toFixed(2))}</Text>{" "}
                                    {I18n.t("SAR")}
                                  </RTLText>
                                </RTLView>
                              </View>

                              <View>
                                <RTLView locale={this.state.locale}>
                                  <RTLText style={styles.medLightText}>
                                    {I18n.t("VAT")}{" "}
                                    <Text style={styles.BoldText}>{data.product.vat}</Text>{" "}
                                    {I18n.t("SAR")}
                                  </RTLText>
                                </RTLView>
                              </View>
                            </View>
                          </RTLView>
                        </View>

                        <View style={{ flexDirection: "row" }}>
                          <RTLView locale={this.state.locale}>
                            <View style={{ ...styles.remove, justifyContent: 'center' }}>
                              <Text style={styles.priceText2}>
                                {I18n.t("Number of Cards")}
                              </Text>
                            </View>
                            <View style={{ ...styles.odrQuantity, justifyContent: 'center' }}>
                              <Text style={{ marginBottom: 7 }}>
                                {data.numberOfItems}
                              </Text>
                            </View>
                            <View style={{ ...styles.price, justifyContent: 'center' }}>
                              <Text style={styles.priceText2}>{data.product.discountPercentage > 0 ? ((data.numberOfItems ? data.numberOfItems : 1 )*(data.product.individualPriceAfter + data.product.vat)).toFixed(2) : ((data.numberOfItems ? data.numberOfItems : 1 )*(data.product.individualPrice + data.product.vat)).toFixed(2)}</Text>
                            </View>
                          </RTLView>
                        </View>
                      </View>
                      {data.productItem.map((item, index) => {
                        return (
                          
                          <View
                            style={
                              this.state.locale == "eng"
                                ? styles.detailsSec
                                : styles.detailsSec_ar
                            }
                          >
                            <View style={styles.detailsInfo}>
                              {item.username &&
                                <>
                                <Text style={styles.head}>{I18n.t("User Name")}</Text>
                                <Text
                                  style={
                                    this.state.locale == "eng" ? styles.info : styles.info_ar
                                  }
                                >
                                  {item.username}
                                </Text>
                                </>
                              }
                              
                              <Text style={styles.head}>{I18n.t("Password")}</Text>
                              <Text
                                style={
                                  this.state.locale == "eng" ? styles.info : styles.info_ar
                                }
                              >
                                {item.secret}
                              </Text>
                              <Text style={styles.head}>{I18n.t("Serial Number")}</Text>
                              <Text
                                style={
                                  this.state.locale == "eng" ? styles.info : styles.info_ar
                                }
                              >
                                {item.serial}
                              </Text>
                            </View>
                            <View style={styles.detailsNo}>
                              <Text style={styles.detailsNoText}>{index+1}/{data.productItem && data.productItem.length}</Text>
                            </View>
                          </View>
                        )
                      })}
                    </>
                  )
                }))}
              </ScrollView>
            </View>
          )}
      </>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Receipt);
