import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
//import { RadioButton } from "react-native-paper";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import { othersImage } from "../../../config/imageConst";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import { RTLView, RTLText } from "react-native-rtl-layout";


class ContactSuccess extends React.Component {
  // const [value, onChangeText] = React.useState('Useless Multiline Placeholder');
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  onCntcSubmit = () => {
    this.props.navigation.navigate("ContactSuccess");
  };

  onHomeScreen = () => {
    this.props.navigation.navigate("Home");
  };

  onFAQ = () => {
    this.props.navigation.navigate("Faq");
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={commonStyle.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        <View style={{ ...styles.container, justifyContent: "space-between" }}>
          <View style={{ width: "100%" }}>
          <View>
          <RTLView locale={this.state.locale}>
            <RTLText style={styles.pageTitle}>
              {I18n.t("Thank you for contacting us!")}
            </RTLText>
            </RTLView>
            </View>
            <View style={{ alignItems: "center", marginTop: "10%" }}>
              <Image source={othersImage.rightTick} />
            </View>

            <View style={{ alignItems: "center" }}>
              <Text style={{ ...commonStyle.bold14, marginVertical: 22 }}>
              {I18n.t("Will be replied within an hour")}
              </Text>
              <Text style={commonStyle.light14}>
              {I18n.t("Please note that the opening hours are")}
              </Text>
              <Text style={commonStyle.light14}>
              {I18n.t("from")} <Text style={commonStyle.bold14}>9</Text> {I18n.t("am to")}{" "}
                <Text style={commonStyle.bold14}>2</Text> {I18n.t("am")}
              </Text>
            </View>
          </View>

          <View style={{ width: "100%" }}>
            <View style={[styles.formGroup, { marginBottom: 0 }]}>
              <TouchableOpacity
                onPress={this.onFAQ}
                style={styles.commonButton}
              >
                <Text style={styles.buttonTex}>{I18n.t("FAQ")}</Text>
              </TouchableOpacity>
            </View>

            <View style={[styles.formGroup, { marginBottom: 0 }]}>
              <TouchableOpacity
                onPress={this.onHomeScreen}
                style={commonStyle.whiteButton}
              >
                <Text style={commonStyle.whiteBtnText}>{I18n.t("Home Page")}</Text>
              </TouchableOpacity>
            </View>
            

            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ContactSuccess);
