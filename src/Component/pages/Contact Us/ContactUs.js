import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image, ActivityIndicator, Alert,
} from "react-native";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from "react-native-simple-radio-button";
import { menuScreen } from "../../../config/imageConst";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";

class ContactUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      selectedRadioValue: 4,
      radioValue: 'others',
      reason: [
        { label: I18n.t("Suggestion"), value: 'suggestion' },
        { label: I18n.t("Question"), value: 'question' },
        { label: I18n.t("outofstockinContactUs"), value: "out of stock" },
        { label: I18n.t("Complain"), value: "complain" },
        { label: I18n.t("Others"), value: "others" },
      ],
      fullName: "",
      fullNameError: false,
      email: "",
      emailError: false,
      mobileNumber: "",
      mobileNumberError: false,
      description: "",
      descriptionError: false,
      reasonError: false,
      reasonValue: "",
      emailError2: false,
      userDetails: {},
      isLoading: false,
      loggedUser: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  componentDidMount() {
    this.getUserDetails()
  }

  getUserDetails() {
    AsyncStorage.getItem("userToken")
      .then((token) => {
        if (token) {
          AsyncStorage.getItem("userDetails")
            .then((results) => {
              this.setState({ userDetails: JSON.parse(results) }, () => {
                this.setState({
                  loggedUser: true,
                  fullName: this.state.userDetails.fullName,
                  email: this.state.userDetails.userName,
                  mobileNumber: this.state.userDetails.mobileNumber.length > 0 && this.state.userDetails.mobileNumber.slice(5)
                })
              })
            })
        } else {
          this.setState({ userDetails: {} }, () => {
            this.setState({
              loggedUser: false,
              fullName: '',
              email: '',
              mobileNumber: ''
            })
          })
        }
      })
  }
  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Menu");
    }
  };

  validation = (value, fieldName) => {
    let emailregex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;

    //field validation
    if (fieldName == "fullName") {
      if (value == "") {
        this.setState({ fullNameError: true });
      } else {
        this.setState({ fullName: value, fullNameError: false });
      }
    } else if (fieldName == "email") {
      let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;

      if (value == "") {
        this.setState({ emailError: true, emailError2: false });
      }
      else {
        this.setState({ emailError: false, email: value }, () => {
          if (regex.test(this.state.email)) {
            this.setState({ emailError2: false, emailError: false, });
          }
        }
        );
      }
    } else if (fieldName == "mobileNumber") {
      let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;

      if (value == "") {
        this.setState({ mobileNumberError: true, mobileNumberFormatError: false }, () => {
          if (this.state.email != ""){
            if (!emailregex.test(this.state.email)) {
              this.setState({ emailError2: true });
            } else {
              this.setState({ emailError2: false });
            }
          } else {
            this.setState({ emailError2: false, emailError: true });
          }
        });
      } else if (value.length != 9) {
        this.setState({ mobileNumberFormatError: true, mobileNumberError: false, })
      } else {
        this.setState({ mobileNumber: value, mobileNumberError: false, mobileNumberFormatError: false }, () => {
          if (this.state.email != ""){
            if (!emailregex.test(this.state.email)) {
              this.setState({ emailError2: true });
            } else {
              this.setState({ emailError2: false });
            }
          } else {
            this.setState({ emailError2: false, emailError: true });
          }
        });
      }
    } else if (fieldName == "description") {
      let regex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;

      if (value == "") {
        this.setState({ descriptionError: true }, () => {
          if (!regex.test(this.state.email)) {
            this.setState({ emailError2: true });
          } else {
            this.setState({ emailError2: false });
          }
        });
      } else if (value.trim() === "") {
        this.setState({ descriptionError: true })
      } else {
        this.setState({ description: value, descriptionError: false }, () => {
          if (!regex.test(this.state.email)) {
            this.setState({ emailError2: true });
          } else {
            this.setState({ emailError2: false });
          }
        });
      }
    }
  };

  contactUsSubmit = () => {
    let regex = /^\w([\.-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    let emailregex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (this.state.fullName === "" && this.state.email === "" && this.state.mobileNumber === "" && this.state.description === "") {
      this.setState({ fullNameError: true, emailError: true, mobileNumberError: true, descriptionError: true });
    } else if (this.state.fullName === "") {
      this.setState({ fullNameError: true});
    } else if (this.state.email === "") {
      this.setState({ emailError: true});
    } else if (!emailregex.test(this.state.email)) {
      this.setState({ emailError2: true });
    } else if (this.state.mobileNumber === "") {
      this.setState({ mobileNumberError: true});
    } else if (this.state.description === "") {
      this.setState({ descriptionError: true});
    } else if (this.state.mobileNumber.length != 9) {
      this.setState({ mobileNumberFormatError: true })
    } else if (this.state.description.trim() === "") {
      this.setState({ descriptionError: true })
    } else {
      this.setState({ isLoading: true })
      let contactUsData = {
        fullName: this.state.fullName,
        email: this.state.email,
        mobileNumber: this.state.mobileNumber,
        type: this.state.radioValue,
        comment: this.state.description
      }
      apiClient
        .postRequest("/freshDeskCreateTicket ", contactUsData)
        .then(contactusRes => {
          this.setState({ isLoading: false })
          this.props.navigation.navigate("ContactSuccess");
        }).catch(err => {
          this.setState({ isLoading: false })
          console.log(err)
        })
    }
  };

  nameOnFocus = (fullName) => {
    if (fullName.length >= 1) {
      this.setState({ fullNameError: false })
    }
    else {
      this.setState({ fullNameError: true })
    }
  }

  emailOnfocus = (email) => {
    
    let emailregex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (this.state.emailError2 == false) {
      if (email.length >= 1) {
        if (!emailregex.test(this.state.email)) {
          this.setState({ emailError2: true, emailError: false, });
        } else {
          this.setState({ emailError2: false, emailError: false });
        }
      }
      else {
        this.setState({ emailError: true })
      }
    }
  }


  mobileOnFocus = (mobile) => {
    let emailregex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;

    if (this.state.email != ""){
      if (!emailregex.test(this.state.email)) {
        this.setState({ emailError2: true });
      } else {
        this.setState({ emailError2: false });
      }
    } else {
      this.setState({ emailError2: false, emailError: true });
    }
    
    if (mobile.length >= 1) {
      this.setState({ mobileNumberError: false })
    }
    else {
      this.setState({ mobileNumberError: true })
    }
  }

  desOnFocus = (description) => {
    let emailregex = /^\w([\.+-]?\w)*@\w([\.-]?\w)*(\.\w{2,3})$/;
    if (this.state.email != ""){
      if (!emailregex.test(this.state.email)) {
        this.setState({ emailError2: true });
      } else {
        this.setState({ emailError2: false });
      }
    } else {
      this.setState({ emailError2: false, emailError: true });
    }

    if (description.length >= 1) {
      this.setState({ descriptionError: false })
    }
    else {
      this.setState({ descriptionError: true })
    }
  }
  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        {this.state.isLoading ? <ActivityIndicator
          style={styles.activityIndicator}
          size="small"
          color="#1A1A1A"
        /> :
          <ScrollView>
            <View style={styles.container}>
              <View>
                <RTLView locale={this.state.locale}>
                  <Text style={styles.pageTitle}>{I18n.t("Contact Us")}</Text>
                </RTLView>
              </View>

              <View style={styles.formWrap}>
                <View style={[styles.formGroup, { marginTop: 8 }]}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.fromText}>{I18n.t("Name")}</RTLText>
                    </RTLView>
                  </View>
                  <TextInput
                    style={
                      this.state.locale == "eng"
                        ? [styles.formControl, commonStyle.none, { borderColor: this.state.fullNameError ? "#ff0000" : null }]
                        : [styles.formControl, commonStyle.rightAlign, { borderColor: this.state.fullNameError ? "#ff0000" : null }]

                    }
                    value={this.state.fullName}
                    onChangeText={(text) => this.setState({ fullName: text }, () => {
                      this.validation(text, "fullName")
                    })
                    }
                    editable={!this.state.loggedUser}
                    placeholder={I18n.t("Full Name")}
                    underlineColorAndroid="transparent"
                    onBlur={(text) => {
                      this.nameOnFocus(this.state.fullName)
                    }}
                  />
                </View>
                {this.state.fullNameError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("This field is required")}
                  </Text>
                ) : null}
                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <RTLText style={styles.fromText}>{I18n.t("Email")}</RTLText>
                    </RTLView>
                  </View>
                  <TextInput
                    style={
                      this.state.locale == "eng"
                        ? [styles.formControl, commonStyle.none, { borderColor: this.state.emailError ||  this.state.emailError2  ? "#ff0000" : null }]
                        : [styles.formControl, commonStyle.rightAlign, { borderColor: this.state.emailError || this.state.emailError2 ? "#ff0000" : null }]
                    }
                    value={this.state.email}
                    onChangeText={(text) => this.setState({ email: text }, () => {
                      this.validation(text, "email")
                    })
                    }
                    editable={!this.state.loggedUser}
                    autoCapitalize='none'
                    placeholder="example@example.com"
                    underlineColorAndroid="transparent"
                    onBlur={(text) => {
                      this.emailOnfocus(this.state.email)
                    }}
                  />
                </View>
                {this.state.emailError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("This field is required")}
                  </Text>
                ) : null}
                {this.state.emailError2 ?
                  <View style={
                    this.state.locale == "eng"
                      ? { flexDirection: 'row', }
                      : { flexDirection: 'row', }
                  }>
                    <View style={
                          this.state.locale == "eng"
                            ? styles.mailExm_en
                            : styles.mailExm_ar
                        }><Text style={{ color: "red", flexDirection: 'row-reverse', fontFamily: "Tajawal-Medium" }}>
                      {I18n.t("Mail should be like")}
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}> example@example.com </Text>
                    </Text>
                    </View>
                  </View>
                  : null}
                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={[styles.fromText]}>
                        {I18n.t("Mobile Number")}
                        <Text style={styles.countryText}>
                          {I18n.t(" (Saudi) ")}
                        </Text>
                      </Text>
                    </RTLView>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <View
                      style={[
                        this.state.locale == "eng"
                          ? styles.mobileInput
                          : styles.mobileInput_ar,
                        (
                          this.state.mobileNumberError === true ||
                          this.state.mobileNumberError1 === true ||
                          this.state.mobileNumberFormatError
                        ) && {
                          borderColor: "#ff0000"
                        }
                      ]}
                    >
                      <View style={styles.countryCode}>
                        <Text style={styles.countryCodeText}>966</Text>
                      </View>
                      <TextInput
                        style={
                          this.state.locale == "eng"
                            ? [styles.mobileTextInput, { borderColor: this.state.mobileNumberError || this.state.mobileNumberFormatError ? "#ff0000" : null }]
                            : [styles.mobileTextInput_ar, { borderColor: this.state.mobileNumberError ? "#ff0000" : null }]

                        }
                        editable={!this.state.loggedUser}
                        value={this.state.mobileNumber}
                        placeholder="0000000000"
                        underlineColorAndroid="transparent"
                        keyboardType={"numeric"}
                        onChangeText={(text) => this.setState({ mobileNumber: text }, () => {
                          this.validation(text, "mobileNumber")
                        })

                        }
                        onBlur={(text) => {
                          this.mobileOnFocus(this.state.mobileNumber)
                        }}

                      />
                    </View>

                    <View style={{ width: "12%", justifyContent: "center" }}>
                      <Image
                        style={
                          this.state.locale == "eng"
                            ? commonStyle.flagImage
                            : commonStyle.flagImage_ar
                        }
                        source={menuScreen.flag2}
                      />
                    </View>
                  </View>
                </View>
                {this.state.mobileNumberError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("This field is required")}
                  </Text>
                ) : null}
                {this.state.mobileNumberFormatError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("mobileError")}
                  </Text>
                ) : null}

                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>
                        {I18n.t("Reason for the message")}
                      </Text>
                    </RTLView>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? { alignItems: "flex-start" }
                        : {
                          justifyContent: "flex-end",
                          transform: [{ rotateY: "180deg" }],
                        }
                    }
                  >
                    <RadioForm
                      animation={true}
                      disabled={false}
                      formHorizontal={false}
                    >
                      {this.state.reason.map((obj, i) => {
                        var onPress = (value, index) => {
                          this.setState({
                            radioValue: value,
                            selectedRadioValue: index
                          })
                        }
                        return (
                          <RadioButton key={i}>
                            <RadioButtonInput
                              obj={obj}
                              index={i}
                              isSelected={this.state.selectedRadioValue === i}
                              onPress={onPress}
                              buttonInnerColor={"#241125"}
                              buttonOuterColor={
                                this.state.selectedRadioValue === i
                                  ? "#241125"
                                  : "#241125"
                              }
                              buttonSize={10}
                              buttonOuterSize={20}
                            />
                            <RadioButtonLabel
                              obj={obj}
                              index={i}
                              labelHorizontal={true}
                              onPress={onPress}
                              labelStyle={
                                this.state.locale == "eng" &&
                                  this.state.selectedRadioValue === i
                                  ? {
                                    fontSize: 16,
                                    fontFamily: "Tajawal-Bold",
                                    color: "#1A1A1A",
                                  }
                                  : this.state.locale == "ar" &&
                                    this.state.selectedRadioValue === i
                                    ? {
                                      transform: [{ rotateY: "180deg" }],
                                      marginHorizontal: 10,
                                      fontSize: 16,
                                      fontFamily: "Tajawal-Bold",
                                      color: "#1A1A1A",
                                    }
                                    : this.state.locale == "ar"
                                      ? {
                                        transform: [{ rotateY: "180deg" }],
                                        color: "red",
                                        marginHorizontal: 10,
                                        fontSize: 14,
                                        fontFamily: "Tajawal-Light",
                                        color: "#4F4F4F",
                                      }
                                      : {
                                        fontSize: 14,
                                        fontFamily: "Tajawal-Light",
                                        color: "#4F4F4F",
                                      }
                              }
                            />
                          </RadioButton>
                        )
                      })}
                    </RadioForm>
                  </View>
                </View>

                <View style={styles.formGroup}>
                  <View>
                    <RTLView locale={this.state.locale}>
                      <Text style={styles.fromText}>
                        {I18n.t("Describe the problem")}
                      </Text>
                    </RTLView>
                  </View>
                  <TextInput
                    style={[styles.formTextArea, { borderColor: this.state.descriptionError ? "#ff0000" : null }]}
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ description: text }, () => {
                      this.validation(text, "description")
                    })}
                    numberOfLines={10}
                    multiline={true}
                    value={this.state.description}
                    onBlur={(text) => {
                      this.desOnFocus(this.state.description)
                    }}
                  />
                </View>
                {this.state.descriptionError ? (
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("This field is required")}
                  </Text>
                ) : null}
                <View style={[styles.formGroup, { marginBottom: 0 }]}>
                  <TouchableOpacity
                    onPress={this.contactUsSubmit}
                    style={styles.commonButton}
                  >
                    <Text style={styles.buttonTex}>{I18n.t("Send")}</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View
                style={
                  this.state.locale == "eng"
                    ? [commonStyle.rights, commonStyle.rowSec]
                    : [commonStyle.rights, commonStyle.rowSec_ar]
                }
              >
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("All rights reserved for ")}
                </Text>
                <Text
                  style={[
                    commonStyle.screenText,
                    { fontSize: 12, fontFamily: "Tajawal-Bold" },
                  ]}
                >
                  {I18n.t(" Bitaqaty ")}
                </Text>
                <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                  {I18n.t("@2019")}
                </Text>
              </View>
            </View>
          </ScrollView>
        }
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ContactUs);
