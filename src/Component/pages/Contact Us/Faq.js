import React, { Component } from "react";
import { Text, View, Image, ScrollView } from "react-native";
//import { RadioButton } from "react-native-paper";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import { othersImage } from "../../../config/imageConst";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";
import { menuScreen } from "../../../config/imageConst";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";

class Faq extends React.Component {
  // const [value, onChangeText] = React.useState('Useless Multiline Placeholder');
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      showArrowIcon1: false,
      showArrowIcon2: false,
      showArrowIcon3: false,
      showArrowIcon4: false,
      showArrowIcon5: false,
      showArrowIcon6: false,
      showArrowIcon7: false,
      showArrowIcon8: false,
      
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("Menu");
    }
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        <ScrollView>
          <View style={{ ...styles.containerForPrivacy, paddingVertical: "5%" }}>
            <View
              style={
                this.state.locale == "eng" ? styles.faqHead : styles.faqHead_ar
              }
            >
              <Text style={styles.pageTitle}>{I18n.t("FAQ")}</Text>
              {/* <Text style={styles.smallText}>
                {I18n.t("Or-Terms of use-Or-Privacy Policy")}
              </Text> */}
            </View>

            <Collapse onToggle = {()=>this.setState({showArrowIcon1: !this.state.showArrowIcon1})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar
                }
              >
                <Text style={{...styles.dropText2, ...styles.headWidthPrcnt}}>{I18n.t("F1st Title")}</Text> 
                <Image source={menuScreen.downarrow} style={{ transform: this.state.showArrowIcon1 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}}/>
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                { this.state.locale == "eng" ? 
                <>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ1st para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ1st para2")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ1st para3")}</Text>
                </>
                :
                <>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ1st para1")}</Text>
                <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("FAQ1st Para11")}</Text></View>
                <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("FAQ1st Para12")}</Text></View>
                <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("FAQ1st Para13")}</Text></View>
                <View style={this.state.locale == "eng" ? styles.paraRow : styles.paraRowRev}><Text style={styles.textB}>{'\u2022' + " "}</Text><Text style={styles.termsText}>{I18n.t("FAQ1st Para14")}</Text></View>
                </>
              }

              </CollapseBody>
            </Collapse>

            <Collapse onToggle = {()=>this.setState({showArrowIcon2: !this.state.showArrowIcon2})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar
                }
              >
                <Text 
                style= {this.state.locale == 'eng' ? 
                [commonStyle.align_en, styles.dropText2, styles.headWidthPrcnt] : 
                [commonStyle.align_ar, styles.dropText2, styles.headWidthPrcnt]
                }>
                  {I18n.t(
                    "F2nd Title"
                  )}
                </Text>
                <Image source={menuScreen.downarrow} style={{transform: this.state.showArrowIcon2 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}}/>
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ2nd Para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ2nd Para2")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle = {()=>this.setState({showArrowIcon3: !this.state.showArrowIcon3})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >                  
                  <Text style={{...styles.dropText, ...styles.headWidthPrcnt}}>{I18n.t("F3rd Title")}</Text>

                <Image style={{marginLeft: 'auto', transform: this.state.showArrowIcon3 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
              <Text style={styles.dropTextLight2}>{I18n.t("FAQ3rd Para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ3rd Para2")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ3rd Para3")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle = {()=>this.setState({showArrowIcon4: !this.state.showArrowIcon4})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >
                                  
                  <Text style={{...styles.dropText, ...styles.headWidthPrcnt}}>{I18n.t("F4th Title")}</Text>
                <View style={{width:'5%'}}><Image style={{marginLeft: 'auto',  transform: this.state.showArrowIcon4 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}} source={menuScreen.downarrow} /></View>
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
              <Text style={styles.dropTextLight2}>{I18n.t("FAQ4th Para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ4th Para2")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle = {()=>this.setState({showArrowIcon5: !this.state.showArrowIcon5})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >                  
                  <Text style={{...styles.dropText, ...styles.headWidthPrcnt}}>{I18n.t("F5th Title")}</Text>
                <Image style={{marginLeft: 'auto',  transform: this.state.showArrowIcon5 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQFAQ5th Para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQFAQ5th Para2")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle = {()=>this.setState({showArrowIcon6: !this.state.showArrowIcon6})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >                  
                  <Text style={{...styles.dropText, ...styles.headWidthPrcnt}}>{I18n.t("F6th Title")}</Text>
                <Image style={{marginLeft: 'auto', transform: this.state.showArrowIcon6 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
              <Text style={styles.dropTextLight2}>{I18n.t("FAQ6th Para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ5th Para2")}</Text>
              </CollapseBody>
            </Collapse>

            <Collapse onToggle = {()=>this.setState({showArrowIcon7: !this.state.showArrowIcon7})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >                  
                  <Text style={{...styles.dropText, ...styles.headWidthPrcnt}}>{I18n.t("F7th Title")}</Text>
                <Image style={{marginLeft: 'auto',  transform: this.state.showArrowIcon7 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ7th Para")}</Text>               
              </CollapseBody>
            </Collapse>
            <Collapse onToggle = {()=>this.setState({showArrowIcon8: !this.state.showArrowIcon8})}>
              <CollapseHeader
                style={
                  this.state.locale == "eng"
                    ? styles.infoSec
                    : styles.infoSec_ar2
                }
              >                  
                  <Text style={{...styles.dropText, ...styles.headWidthPrcnt}}>{I18n.t("F8th Title")}</Text>
                <Image style={{marginLeft: 'auto',  transform: this.state.showArrowIcon8 ?  [{ rotate: '180deg'}] :  [{ rotate: '0deg'}]}} source={menuScreen.downarrow} />
              </CollapseHeader>
              <CollapseBody style={styles.collapseBody}>
              <Text style={styles.dropTextLight2}>{I18n.t("FAQ8th Para1")}</Text>
                <Text style={styles.dropTextLight2}>{I18n.t("FAQ8th Para2")}</Text>
              </CollapseBody>
            </Collapse>
          </View>
        </ScrollView>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(Faq);
