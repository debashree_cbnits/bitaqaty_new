import React, { Component } from "react";
import {
  Text,
  View,
  ActivityIndicator,
  Image,
  Dimensions,
  ImageBackground, BackHandler
} from "react-native";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import styles from "./style.js";
import I18n from "../../../../i18n/index";
import { MarchantImagePath } from "../../../../config/imageConst";
import HeaderArrow from "../../Header/Header_logo_name/HeaderArrow";
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import apiClient from "../../../../services/api.client";
import imageUrl from '../../../../services/config';
import { WebView } from 'react-native-webview';

const deviceHeight = Dimensions.get('window').height;

const baseUri = "https://stagewrapper.ocstaging.net/bitaqatywrapper";


class Merchants extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Merchants",
      isLoading: true,
      MarchantList: null,
      CatagoryDesc: null,
      locale: "",
      language: ""
    };
    this._getMerchantsFromApi();
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', function () {
      if (this.props.navigation.state.routeName === 'Merchants') {
        this.props.navigation.navigate('Catagory');
        return true;
      } else {
        this.props.navigation.goBack(null);
        return true;
      }

    }.bind(this));

    this.props.navigation.addListener("didFocus", () => {
      this._getMerchantsFromApi();
      BackHandler.addEventListener('hardwareBackPress', function () {
        if (this.props.navigation.state.routeName === 'Merchants') {
          this.props.navigation.navigate('Catagory');
          return true;
        } else {
          this.props.navigation.goBack(null);
          return true;
        }

      }.bind(this));
    });

    this.props.navigation.addListener("didBlur", () => {
      this._getMerchantsFromApi();
    });
  }

  _getMerchantsFromApi() {
    this.setState({ isLoading: true, })
    apiClient
      .postRequest("/merchants-full-data", {
        categoryId:
          this.props.navigation.state.params.fromWhere != "tabs"
            ? this.props.navigation.state.params.item.id :
            this.props.navigation.state.params.fromWhere == "tabs" ?
              this.props.navigation.state.params.item.id
              : this.props.navigation.state.params.item.categoryId
      })
      .then(res => {
        this.setState({
          MarchantList: res.seoListDTO.listData,
          isLoading: false,
          CatagoryDesc: res.categoryModel
        });
      })
      .catch(err => {
        this.setState({
          MarchantList: null,
          isLoading: false,
          CatagoryDesc: null
        })
      });
  }

  backValueReceive = value => {
    if (value == "back") {
      this.props.navigation.navigate("Catagory");
    }
  };

  categogyDetails = (merchantInfo, categoryId) => {
    apiClient
      .postRequest("/merchant/Products", {
        merchantId: merchantInfo.id,
        categoryId: categoryId,
      })
      .then((productApiResponse) => {
        this.props.navigation.navigate("CatagoryView",
          {
            merchantInfo: productApiResponse.localMerchantResponseDTO,
            categoryId, productApiResponse, back: "Marchent", fromWhere: ""
          });
      })
      .catch((err) => {
        console.log("err", err);
      }
      );

    apiClient
      .postRequest("/related-products", {
        merchantId: merchantInfo.id,
      })
      .then((relatedProductResponse) => {
      })
      .catch((err) => { console.log("err", err); }

      );
  };

  render() {

    return (
      <View style={styles.mainBody}>
        <View>
          <HeaderArrow
            locale={this.state.locale}
            language={this.state.language}
            backPage={value => this.backValueReceive(value)}
          />

          <View style={styles.container}>
            <View>
              <RTLView locale={this.state.locale}>
                <Text style={styles.pageTitle}>
                  {this.state.locale === "eng"
                    ? this.props.navigation.state.params.item.nameEn
                    : this.props.navigation.state.params.item.nameAr}
                </Text>
              </RTLView>
            </View>

            {!this.state.isLoading && this.state.CatagoryDesc && this.state.CatagoryDesc !== "" &&
              <View style={styles.toImageBackgroundpBanner}>
                <ImageBackground
                  style={styles.bannerImage}
                  source={MarchantImagePath.marchantBanner}
                  source={{ uri: imageUrl.productImageUrl + this.state.CatagoryDesc.backImagePath }}
                >
                  <LinearGradient
                    style={styles.sliderGradient}
                    start={{ x: 0.1, y: 0.6 }}
                    end={{ x: 0.1, y: 0 }}
                    colors={["rgba(0,0,0,0.7)", "#54545400"]}
                  >
                  </LinearGradient>
                </ImageBackground>
              </View>
            }
          </View>
        </View>
        <View style={styles.productWrap}>
          {this.state.isLoading && <ActivityIndicator
            style={styles.activityIndicator}
            size="small"
            color="#1A1A1A"
          />}
          {!this.state.isLoading && this.state.MarchantList && <ScrollView>
            <View style={styles.marchantBox}>
              {this.state.MarchantList.map((item, key) => (
                <View key={key} style={styles.listBox}>
                  <TouchableOpacity style={{ width: '100%' }}
                    onPress={() => this.categogyDetails(item, this.props.navigation.state.params.item.id)}>
                    <Image
                      style={styles.marchantIcon}
                      source={{ uri: imageUrl.productImageUrl + item.logoPath }}
                    />
                    <RTLText style={styles.marchantName}>
                      {this.state.locale === "eng"
                        ? item.companyNameEn
                        : item.companyNameAr}
                    </RTLText>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
            {this.state.locale === "ar" && !this.state.isLoading && this.state.CatagoryDesc && this.state.CatagoryDesc.metaDescriptionAr !== "" &&
              <View style={styles.container}>
                <Text style={styles.aboutTitle}>{I18n.t("aboutcategoty")}</Text>
                <Text style={styles.aboutText}>{this.state.CatagoryDesc.metaDescriptionAr}</Text>
              </View>}

            {this.state.locale === "eng" && !this.state.isLoading && this.state.CatagoryDesc && this.state.CatagoryDesc.metaDescriptionEn !== "" &&
              <View style={{...styles.container, marginBottom: 20}}>
                <Text style={styles.aboutTitle}>{I18n.t("aboutcategoty")}</Text>

                <View style={{height: 200}}>
                <WebView
                    automaticallyAdjustContentInsets={false}
                    scalesPageToFit={false}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={{ html: this.state.CatagoryDesc.metaDescriptionEn }}
                  /> 
                  </View>              
                {/* <Text style={styles.aboutText}>{this.state.CatagoryDesc.metaDescriptionEn}</Text> */}
              </View>}
          </ScrollView>}
        </View>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(
  mapStateToProps,
  {}
)(Merchants);