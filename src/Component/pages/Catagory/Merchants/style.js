const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    mainBody:{
        flex: 1
    },
    container: {
        //flex: 1,
        //width: "92%",
        paddingHorizontal: 15,
        paddingTop: 15,
        backgroundColor:'#fff'
    },
    pageTitle: {
        fontSize: 20,
        color: "#1A1A1A",
        paddingBottom: 10,
        fontFamily:'Tajawal-Bold'
    },
    topBanner: {
        position: "relative",        
    },
    bannerText: {
        // position: "absolute",
        // bottom: 20,
        width: "100%",
        //padding: 20,        
    },
    subTitle: {
        fontSize: 16,
        color: "#fff",
        fontWeight: "bold"
    },
    commonText: {
        color: "#CCCCCC",
        fontSize: 12,
    },
    bannerImage: {
        height: 110,
        marginBottom: 20,
        borderRadius: 12,
        resizeMode: "cover",
        overflow: "hidden",
        width: "100%",
        position: 'relative',
    },
    productWrap:{
        height:deviceHeight - 310,
        backgroundColor:'#fff',
        paddingBottom:20
    },
    marchantBox: {
        //flex: 1,
        //height:deviceHeight - 300,
        flexDirection:"row",
        flexWrap: "wrap",
        //justifyContent: "space-evenly",
        //paddingBottom:100        
    },
    marchantIcon: {
        width: 90,
        height: 90,
        borderRadius: 4,
        marginBottom: 5,
        resizeMode: "contain"
    },
    marchantName: {
        width: 90,
        fontSize: 12,
        fontFamily:'Tajawal-Bold',
        textAlign:'center',
    },
    listBox: {
        alignItems: "center",
        width:'33.33%',
        marginBottom:20,
    },
    aboutTitle:{
        fontSize: 16,
        fontFamily:'Tajawal-Bold', 
        marginBottom:10
    },
    aboutText:{
        fontSize: 14,
        fontFamily:'Tajawal-Light',
        color:'#4F4F4F'
    },
    activityIndicator: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
      },
      sliderGradient: {
        width: "100%",
        position: "absolute",
        bottom: 0,
        paddingHorizontal: 20,
        paddingBottom: 12
      }

}
