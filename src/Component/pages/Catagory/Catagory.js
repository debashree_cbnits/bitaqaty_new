import React, { Component } from "react";
import {
  Text,
  View,
  Button,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  BackHandler
} from "react-native";
import { RTLView, RTLText } from "react-native-rtl-layout";
import styles from "./style.js";
import I18n from "../../../i18n/index";
import { CatagoryImagePath } from "../../../config/imageConst";
import { connect } from "react-redux";
import HeaderArrow from "../../pages/Header/Header_logo_name/HeaderArrow";
import apiClient from "../../../services/api.client"

import network from "../Network/check";
import Internet from "../Network/Internet";

class Catagory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      isLoading: true,
      checkNetworkConnection: "",
      CategoryList: null,
    };
    this._getCatagoriesFromApi();
  }
  _getCatagoriesFromApi() {
    apiClient
      .getRequest("/categories")
      .then(CategoryList => this.setState({ CategoryList, isLoading: false }))
      .catch(err => this.setState({ CategoryList: null, isLoading: false }));
  }
  static getDerivedStateFromProps(props, state) {    
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "eng",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "ar",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "eng",
        locale: "eng"
      };
    }
    // return null;
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", () => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        if (this.props.navigation.state.routeName === 'Merchants') {
          this.props.navigation.navigate('Catagory');
          return true;
        } else {
          this.props.navigation.goBack(null);
          return true;
        }
  
      }.bind(this));
      const net = network();
      this.setState({ checkNetworkConnection: net });      
    });
    const net = network();

    this.setState({ checkNetworkConnection: net });
    BackHandler.addEventListener('hardwareBackPress', function () {      
      if (this.props.navigation.state.routeName === 'Catagory') {
        this.props.navigation.navigate('HomeScreen');
        return true;
      } else {
        this.props.navigation.goBack(null);
        return true;
      }

    }.bind(this));
  }

  netConnectReload = value => {    
    this.setState({ checkNetworkConnection: value });
  };



  backValueReceive = value => {    
    if (value == "back") {      
      this.props.navigation.navigate("HomeScreen");
    }
  };

  categoryPage = item => {    
    this.props.navigation.navigate("Merchants", { item , fromWhere: 'cate'});
  };

  render() {      
    return (
      <View style={styles.mainBody}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={value => this.backValueReceive(value)}
        />
        <View style={styles.container}>
          <View>
          <RTLView locale={this.state.locale}>
          <RTLText style={styles.pageTitle}>{I18n.t("Shopping Categories")}</RTLText>
          </RTLView>
          </View>
          {this.state.isLoading && <ActivityIndicator
            style={styles.activityIndicator}
            size="small"
            color="#1A1A1A"
          />}
          {!this.state.isLoading && this.state.CategoryList && <ScrollView>
            <View style={styles.wrap}>
              {this.state.CategoryList.map((item, key) => (
                <TouchableOpacity
                  onPress={() => {
                    this.categoryPage(item);
                  }}
                  style={styles.categoryList}
                  key={key}
                >
                  <RTLView locale={this.state.locale}>
                    <View style={{ width: "95%" }}>
                      <RTLView locale={this.state.locale}>
                        <RTLText style={styles.listName}>
                          {this.state.locale == "eng" ? item.nameEn : item.nameAr}
                        </RTLText>
                      </RTLView>
                    </View>
                    <Image                      
                      style={
                        this.state.locale == "eng"
                          ? styles.listIcon_en
                          : styles.listIcon_ar
                      }
                      source={CatagoryImagePath.rightArrow}
                    />
                  </RTLView>
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>}
        </View>
      </View>
    );
  }
}
mapStateToProps = state => ({
  setlanguage: state.Language.setlanguage
});
export default connect(
  mapStateToProps,
  {}
)(Catagory);
