const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
    categoryList: {
        flex: 1,
        borderBottomColor: "#F2F0F0",
        borderBottomWidth: 1,
        borderStyle: "solid",
        height: 48,
        paddingTop: 15,
    },
    mainBody: {
        flex: 1,
        backgroundColor: '#fff',
    },
    container: {
        flex: 1,
        marginTop: "5%",
    },
    wrap: {
        paddingHorizontal: 15
    },
    pageTitle: {
        fontSize: 20,
        color: "#1A1A1A",
        marginBottom: 20,
        fontFamily: 'Tajawal-Bold',
        paddingHorizontal: 15

    },
    listName: {
        color: '#1A1A1A',
        fontSize: 14,
        fontFamily: 'Tajawal-Bold'
    },
    listIcon_en: {
        width: 16,
    },
    listIcon_ar: {
        width: 16,
        transform: [{ rotate: '180deg' }]
    },
    activityIndicator: {
        flex: 1,
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: "center",
        alignItems: "center",
    }
}
