import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";

class GatewayLoading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      checkoutData: '',
      isLoading: false,
      token: ""

    };
  }

  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  componentDidMount() {
    AsyncStorage.removeItem("paymentstart")
    AsyncStorage.getItem("userToken")
      .then((token) => {
        AsyncStorage.getItem("checkoutData").then((res) => {
          this.setState({ checkoutData: JSON.parse(res), token: token }, () => {
            this.fetchPaymentStatus();
          })
        }).catch(err => console.log(err))
      })
      this.props.navigation.addListener("didFocus", () => {
        AsyncStorage.removeItem("paymentstart")    
      });
  
      this.props.navigation.addListener("didBlur", () => {
        AsyncStorage.removeItem("paymentstart")
      });
  }

  onGatewayFail = () => {
    this.props.navigation.navigate("GatewayFail");
  };

  fetchPaymentStatus = () => {
    this.setState({ isLoading: true })
    try {
      
      apiClient.paymentpostRequest('/checkPayment', this.state.checkoutData, this.state.token).then((checkPaymentRes) => {
        this.setState({ isLoading: false })         
             
        if (checkPaymentRes.orderID){          
          this.setState({ isLoading: false })   
          let orderId = checkPaymentRes.orderID;
           if(orderId){
             setTimeout(() => {
               this.props.navigation.navigate("GatewaySuccess", {orderId: orderId})
             }, 2000);
          }
          else{
            setTimeout(() => {
              this.props.navigation.navigate("GatewayFail")
            }, 2000);
          }
        } else {
          this.setState({ isLoading: false })   
          setTimeout(() => {
            this.props.navigation.navigate("GatewayFail")
          }, 2000);
        }
      })
    } catch (err) {
      this.setState({ isLoading: false })
      this.props.navigation.navigate("GatewayFail")
      return err;
    }
  };

  render() {
    return (
      <>
        {this.state.isLoading ?
          <ActivityIndicator
            style={styles.activityIndicator}
            size="small"
            color="#1A1A1A"
          /> :
          <View style={styles.container2}>
            <ImageBackground
              source={require("../../assets/image/BG.png")}
              style={styles.image1}
            >
              <TouchableOpacity onPress={this.onGatewayFail}>
                <Image
                  style={{ height: 168, width: 140 }}
                  source={require("../../assets/image/LOGOSPLASH.png")}
                />
              </TouchableOpacity>
            </ImageBackground>
            <View style={styles.loadingHead}>
              <Text style={styles.boldText}>
                {I18n.t("Payment is in progress")}
              </Text>
              <Text style={styles.light14Text}>
                {I18n.t("Please wait for a moment")}
              </Text>

              <View style={{ width: "100%" }}>
                <View
                  style={
                    this.state.locale == "eng"
                      ? [commonStyle.rights, commonStyle.rowSec]
                      : [commonStyle.rights, commonStyle.rowSec_ar]
                  }
                >
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("All rights reserved for ")}
                  </Text>
                  <Text
                    style={[
                      commonStyle.screenText,
                      { fontSize: 12, fontFamily: "Tajawal-Bold" },
                    ]}
                  >
                    {I18n.t(" Bitaqaty ")}
                  </Text>
                  <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                    {I18n.t("@2019")}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        }
      </>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(GatewayLoading);