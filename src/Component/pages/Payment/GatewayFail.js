import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { othersImage } from "../../../config/imageConst";
import { connect } from "react-redux";

class GatewayFail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  onGatewaySuccess = () => {
    this.props.navigation.navigate("Home");
  };

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        <View style={styles.container}>
          <View>
            <Text style={styles.headText}>
              {I18n.t("Something went wrong!")}
            </Text>
            <Image source={othersImage.forgotError} style={styles.marginAuto} />
            <Text style={styles.errorBold}>
              {I18n.t("Your payment failed!")}
            </Text>
            <Text style={styles.errorLight}>
              {I18n.t("Please try again or contact customer service for help")}
            </Text>
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity
              onPress={this.onGatewaySuccess}
              style={commonStyle.blackBtn}
            >
              <Text style={commonStyle.blackBtnText}>
                {I18n.t("Try Again")}
              </Text>
            </TouchableOpacity>
            <View style={this.state.locale == 'eng' ? styles.row : styles.row_ar}>
              <Text style={styles.cusText}>{I18n.t("For help, you can contact our")}</Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate("ContactUs")}>
                <Text style={styles.cusSrvc}>{" "}{I18n.t("customer service")}{" "}</Text>
              </TouchableOpacity>
            </View>

            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(GatewayFail);
