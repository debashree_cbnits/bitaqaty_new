import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { othersImage } from "../../../config/imageConst";
import { connect } from "react-redux";

class GatewaySuccess extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      orderId: this.props.navigation.state.params ? this.props.navigation.state.params.orderId : '',

    };
  }
  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
    // return null;
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate("Receipt", { orderId: this.state.orderId });
    }, 2000);
  }

  render() {
    return (
      <View style={styles.mainSection}>
        <View style={styles.logoHead}>
          <Image
            style={{ height: 30, width: 24.5 }}
            source={require("../../assets/image/logo.png")}
          />
        </View>
        <View style={styles.container}>
          <View>
            {/* <Text style={styles.headText}>{I18n.t("Awesome")}</Text> */}

            <Image source={othersImage.rightTick} style={styles.marginAuto} />
            <Text style={commonStyle.emptymsg}>
              {I18n.t("Now the cards are in your pocket")}
            </Text>
            <Text style={commonStyle.emptymsg2}>
              {I18n.t("The receipt will appear soon")}
            </Text>
          </View>
          <View style={styles.bottom}>
            <View
              style={
                this.state.locale == "eng"
                  ? [commonStyle.rights, commonStyle.rowSec]
                  : [commonStyle.rights, commonStyle.rowSec_ar]
              }
            >
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("All rights reserved for ")}
              </Text>
              <Text
                style={[
                  commonStyle.screenText,
                  { fontSize: 12, fontFamily: "Tajawal-Bold" },
                ]}
              >
                {I18n.t(" Bitaqaty ")}
              </Text>
              <Text style={[commonStyle.screenText, { fontSize: 12 }]}>
                {I18n.t("@2019")}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(GatewaySuccess);
