const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  mainSection: {
    flex: 1,
    backgroundColor: "#FAFAFA",
  },
  container: {
    flex: 1,
    paddingHorizontal: "4%",
    justifyContent: "space-between",
  },
  headText: {
    marginVertical: 15,
    fontFamily: "Tajawal-Bold",
    color: "#1A1A1A",
    fontSize: 20,
  },
  bottom: {
    paddingVertical: 10,
  },
  cardDetails_en: {
    width: '100%',
    height: 44,
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#FFC300",
    flexDirection: "row",
    justifyContent: 'space-between'
  },
  cardDetails_ar: {
    width: '100%',
    height: 44,
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#FFC300",
    flexDirection: "row-reverse",
    justifyContent: 'space-between'
  },
  cardInput_en: {
    width: '79%',
    height: 44,
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Tajawal-Bold',
    justifyContent: 'flex-end',
  },
  cardInput_ar: {
    width: '79%',
    height: 44,
    textAlign: 'right',
    fontSize: 16,
    fontFamily: 'Tajawal-Bold',
    justifyContent: 'flex-end',
  },
  cardImg: {
    borderRadius: 4,
    marginRight: 10,
    borderColor: '#CFCFCF',
    borderWidth: 1,
    // widthL:20,
    // height:32
  },
  cardHolder_en: {
    width: '100%',
    height: 44,
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Tajawal-Bold',
    borderBottomWidth: 2,
    borderBottomColor: "#FFC300",
  },
  cardHolder_ar: {
    width: '100%',
    height: 44,
    textAlign: 'right',
    fontSize: 16,
    fontFamily: 'Tajawal-Bold',
    borderBottomWidth: 2,
    borderBottomColor: "#FFC300",
  },
  medText: {
    marginTop: 10,
    fontFamily: 'Tajawal-Medium',
    color: '#1A1A1A',
    fontSize: 14
  },
  lightText: {
    marginTop: 5,
    fontFamily: 'Tajawal-Light',
    color: '#4F4F4F',
    fontSize: 12
  },
  container2: {
    flex: 1,
    flexDirection: "column"
  },
  image1: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    alignItems: 'center'
  },
  boldText: {
    fontFamily: 'Tajawal-Bold',
    color: '#1A1A1A',
    fontSize: 16
  },
  errorBold: {
    marginTop: 15,
    fontFamily: 'Tajawal-Bold',
    color: '#1A1A1A',
    fontSize: 14,    
    textAlign: "center",
  },
  errorLight: {
    fontFamily: 'Tajawal-Light',
    color: '#1A1A1A',
    fontSize: 14,    
    textAlign: "center",
  },
  light14Text: {
    fontFamily: 'Tajawal-Light',
    color: '#4F4F4F',
    fontSize: 14
  },
  loadingHead: {
    position: "absolute",
    width: "100%",
    bottom: 15,
    alignItems: 'center'
  },
  logoHead: {
    height: 50,
    paddingVertical: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  marginAuto: {
    marginTop: 20,
    marginLeft: "auto",
    marginRight: "auto",
  },
  blueText: {
    color: "#2700EB",
    fontFamily: "Tajawal-Bold",
    fontSize: 14,
  },
  lightText: {
    marginTop: 10,
    color: "#4F4F4F",
    fontFamily: "Tajawal-Light",
    fontSize: 14,
  },
  row: {
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center",
    flexWrap: 'wrap'
  },
  row_ar: {
    marginBottom: 10,
    flexDirection: "row-reverse",
    alignItems: "center",
    flexWrap: 'wrap'
  },
  cusSrvc: {
    color: "#2700EB",
    fontFamily: "Tajawal-Bold",
    fontSize: 14,
  },
  activityIndicator: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
};
