import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions,
  DeviceEventEmitter,
  NativeModules,
  Linking,
  KeyboardAvoidingView,
  RefreshControlBase,
} from "react-native";
import styles from "./style";
import commonStyle from "../../../../commonStyle.js";
import I18n from "../../../i18n/index";
import { cartImage } from "../../../config/imageConst";
import { connect } from "react-redux";
import HeaderArrow from "../Header/Header_logo_name/HeaderArrow";
import axios from "axios";
import apiClient from "../../../services/api.client";
import AsyncStorage from "@react-native-community/async-storage";
import Toast from "react-native-tiny-toast";
import store from "../../../redux/store";
import { setCartArrayLength } from "../../../redux/storage/action";

const deviceHeight = Dimensions.get("window").height;

class BankDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      checkoutId: "",
      redirectUrl: '',
      cartDataArray: this.props.navigation.state.params ? this.props.navigation.state.params.cartDataArray : [],
      totalPayment: this.props.navigation.state.params ? this.props.navigation.state.params.totalPayment : '',
      cardname: this.props.navigation.state.params ? this.props.navigation.state.params.cardname : '',
      cardLogo: this.props.navigation.state.params ? this.props.navigation.state.params.cardLogo : '',
      fee: this.props.navigation.state.params ? this.props.navigation.state.params.fee : '',
      paymentBrand: "",
      cardNumber: "",
      cardNumberError: false,
      holderName: "",
      name: "",
      nameError: false,
      expiryMonth: "",
      expiryYear: "",
      cvv: "",
      cvvError: false,
      cardExpiry:'',
      cardExpiryError: false,
    };
  }
  static getDerivedStateFromProps(props, state) {

    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  backValueReceive = (value) => {
    if (value == "back") {
      this.props.navigation.navigate("PaymentOption");
    }
  };
  componentDidMount() {
    this.getAllPaymentDetails()
    this.props.navigation.addListener("didFocus", () => {
      this.getAllPaymentDetails();
    });
  }

  getAllPaymentDetails() {
    AsyncStorage.getItem("userToken")
      .then((token) => {
        this.setState({ token: token }, () => {
          this.getCheckoutId(token);
        })
      }).catch(err => console.log(err))
    Linking.getInitialURL().then((url) => {
      if (url) {
        let regex = /[?&]([^=#]+)=([^&#]*)/g,
          params = {},
          match;
        while ((match = regex.exec(url))) {
          params[match[1]] = match[2];
        }
        const { id, resourcePath } = params;
      } else {
      }
    });
    DeviceEventEmitter.addListener('transactionStatus', this.onSessionConnect);
  }
  getCheckoutId = () => {
    let data = {
      "orderProducts": this.state.cartDataArray,
      "paymentMethod": this.state.cardname,
      "paymentFees": this.state.fee,
      "channel": 1
    }    
    apiClient.postRequest('/preparePayment', data, this.state.token).then((preparePaymentRes) => {      
      this.setState({ checkoutId: preparePaymentRes.id }, () => {
        let resourcePath = "/v1/checkouts/" + preparePaymentRes.id + "/payment";
        let array = [];
        for (let i = 0; i < this.state.cartDataArray.length; i++) {
          array.push({ "productId": this.state.cartDataArray[i].id, "quantity": this.state.cartDataArray[i].prodQuantity })
        }

        let checkPaymentData = {
          "orderProducts": array,
          "resourcePath": resourcePath,
          "requestId": preparePaymentRes.requestId,
          "checkoutId": preparePaymentRes.id,
          "amount": this.state.totalPayment,
          "local": "ar",
          "applicationName": "Bitaqaty"
        }

        AsyncStorage.setItem("checkoutData", JSON.stringify(checkPaymentData))
      })
    }).catch(err => {
      console.log(err)
    })
  }

  onSessionConnect = event => {
    try {
      Linking.openURL(event.redirectUrl);
    } catch (err) {
      Toast.show(I18n.t("Something went wrong!"));
      this.props.navigation.navigate("HomeScreen");
    }
  };

  fetchPaymentStatus = async (resPath) => {
    try {
      const response = await axios({
        method: "post",
        url: "http://saib.gate2play.com/hussam/payment.php",
        headers: {},
        data: {
          method: "check_payment",
          resourcePath: resPath,
        },
      });
      const checkoutId = response.data.checkoutId;
      this.setState({ checkoutId });
    } catch (err) {
      console.log(err);
      return err;
    }
  };

  validation = (value, fieldName) => {
      if (fieldName == "cardnumber"){
      if (value == ""){
        this.setState({cardNumberError : true})
      } else {
        this.setState({
          cardNumber: value
            .replace(/\s?/g, "")
            .replace(/(\d{4})/g, "$1 ")
            .trim(), cardNumberError: false
        }, ()=>{
          if (value.length === 19){        
            this.setState({validCardError : false})
          }
        });
      }
    } else if (fieldName == "name"){
      if (value == ""){
        this.setState({nameError : true})
      } else {
        this.setState({name : value, nameError : false});
      }
    } else if (fieldName == "cvv"){
      if (value == ""){
        this.setState({cvvError : true})
      } else {
        this.setState({cvv : value, cvvError : false, hyperPayError : false});
      }
    }
  }

  onGatewayLoading = async () => {

    let name = this.state.name;
    let cardnumber = this.state.cardNumber;
    let expity = this.state.cardExpiry;
    let cvv = this.state.cvv;    

    if (cardnumber== "" && name == "" && cvv == "" && expity == "") {
      this.setState({ cardNumberError: true, nameError: true, cvvError: true, cardExpiryError: true })
    }
    else if (cardnumber.trim() == "") {
      this.setState({ cardNumberError: true })
    }
    else if (cardnumber.trim().length < 19) {
      this.setState({ cardNumberError: true })
    }
    else if (name.trim() == "") {
      this.setState({ nameError: true })
    }
    else if (cvv.trim() == "") {
      this.setState({ cvvError: true })
    }
    else if (cvv.trim().length < 3) {
      this.setState({ cvvError: true })
    }
    else if (expity.trim() == "") {
      this.setState({ cardExpiryError: true })
    } else if (this.state.cardNumberError == false && this.state.nameError == false && this.state.cvvError == false && this.state.cardExpiryError == false){
      var card = this.state.cardNumber.replace(/\s/g, '');              
      var card = this.state.cardExpiry;
      var expiry = card.split('/');
      var mnth = expiry[0];
      var yr = expiry[1];

      let data = {
        checkoutID: this.state.checkoutId,
        paymentBrand: this.state.cardname,
        cardNumber: this.state.cardNumber.replace(/\s/g, ''),
        holderName: this.state.name,
        expiryMonth: mnth,
        expiryYear: yr,
        cvv: this.state.cvv,
      }
      AsyncStorage.removeItem("CartItemList")
      store.dispatch(setCartArrayLength(0));
      try {
        AsyncStorage.setItem("paymentstart", "paystart")
        AsyncStorage.removeItem("CartItemList");
        store.dispatch(setCartArrayLength(0));        
        NativeModules.Hyperpay.transactionPayment(data);
        setTimeout(() => {
          this.props.navigation.navigate("GatewayLoading");
        }, 5000);
      } catch (error) {
        console.log("error", error);
        this.setState({ hyperPayError: true, cvv: "", cardExpiry: "" })
        if (error) {

          console.log("error", JSON.stringify(error));
        }
      }
    }

  };
  _handlingCardNumber(number) {
    this.setState({
      cardNumberError : false,
      cardNumber: number
        .replace(/\s?/g, "")
        .replace(/(\d{4})/g, "$1 ")
        .trim(),
    });
  }

  cardNumberValidation = () => {    
    let value = this.state.cardNumber;
    if(value.trim() == ""){
      this.setState({ cardNumberError : true })
    }
    else if (this.state.cardNumber.length <19){
      this.setState({ cardNumberError : true })
    }
  }

  namevalidation = () => {    
    let value = this.state.name;
   if(value.trim() == ""){
     this.setState({ nameError : true })
   }
   else if(value.trim().length < 3){
    this.setState({ nameError : true })
   }
  }

  cVVvalidation = ( ) =>{
    let value = this.state.cvv;
    if(value.trim() == ""){
      this.setState({ cvvError : true })
    }
    else if(value.trim().length < 3){
      this.setState({ cvvError : true })
    }
  }

  _handlingCardExpiry(text) {
    if (text.indexOf(".") >= 0 || text.length > 7) {
      return;
    }

    if (text.length === 2 && this.state.cardExpiry.length === 1) {
      text += "/";
    }
    this.setState({
      cardExpiry: text, cardExpiryError : false
    });
  }

  expiryvalidation = () => {
    let value = this.state.cardExpiry;
    if(value.trim() == ""){
      this.setState({ cardExpiryError : true})
    }
  }


  render() {
    const imagePathUrl = "https://www.bitaqaty.com/assets/images/paymentMethods/";
    return (
      <View style={styles.mainSection}>
        <HeaderArrow
          locale={this.state.locale}
          language={this.state.language}
          backPage={(value) => this.backValueReceive(value)}
        />
        <View style={styles.container}>
          <View style={{ width: "100%" }}>
            <KeyboardAvoidingView behavior="padding">
            <ScrollView >

              <Text style={styles.headText}>
                {I18n.t("Your Bank Card Information")}
              </Text>

              <View>
                <Text>{I18n.t("Credit Card Number")}</Text>
                <View
                  style={[
                    this.state.locale == "eng"
                      ? styles.cardDetails_en
                      : styles.cardDetails_ar,
                      (
                        this.state.cardNumberError
                        ) && {
                        borderBottomColor: "red",
                      }
                  ]}
                >
                  <Image
                    source={this.state.cardname == "AMEX" ? cartImage.americanExp : {
                      uri: imagePathUrl + this.state.cardLogo
                    }}
                    style={[styles.cardImg, { width: '20%', height: 30, marginBottom: 5 }]}
                  />
                  <TextInput
                    onChangeText={(text) => this.setState({ cardNumber: text }, () => {
                      this.validation(text, 'cardnumber')
                    })}
                    style={[
                      this.state.locale == "eng"
                        ? styles.cardInput_en
                        : styles.cardInput_ar                       
                    ]}
                    maxLength={19}
                    placeholder="1234 5678 9009 8745"
                    placeholderTextColor="#CCCCCC"
                    keyboardType="numeric"
                    value={this.state.cardNumber}
                    onBlur = {this.cardNumberValidation}
                  />
                  <Image source={cartImage.cameraPayment} />
                </View>
                <Text style={styles.lightText}>
                  {I18n.t("Long serial number on the face of the card")}
                </Text>
                {this.state.cardNumberError ?
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("Invalid card number or brand")}
                  </Text>
                  : null}

                <Text style={styles.medText}>
                  {I18n.t("Name Of Card Holder")}
                </Text>
                <TextInput
                  style={[
                    this.state.locale == "eng"
                      ? styles.cardHolder_en
                      : styles.cardHolder_ar,
                      (
                        this.state.nameError
                        ) && {
                          borderBottomColor: "red"
                      }
                  ]}
                  value={this.state.name}
                  placeholder="Ali"
                  placeholderTextColor="#CCCCCC"
                  onChangeText={(text) => this.setState({ name: text }, () => {
                    this.validation(text, 'name')
                  })}
                  onBlur= {this.namevalidation}
                />
                {this.state.nameError ?
                  <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                    {I18n.t("Invalid card holder")}
                  </Text>
                  : null}

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <View style={{ width: "48%" }}>
                    <Text style={styles.medText}>
                      {I18n.t("Verification Code (CVV)")}
                    </Text>
                    <TextInput
                      style={[
                        this.state.locale == "eng"
                          ? styles.cardHolder_en
                          : styles.cardHolder_ar,
                          (
                            this.state.cvvError
                            ) && {
                              borderBottomColor: "red",
                          }
                      ]}
                      value={this.state.cvv}
                      onChangeText={(text) => this.setState({ cvv: text }, () => {
                        this.validation(text, 'cvv')
                      })}
                      placeholder="485"
                      placeholderTextColor="#CCCCCC"
                      keyboardType="numeric"
                      // style={{flex: 1}}
                      secureTextEntry={true}
                      autoCorrect={false}
                      maxLength={3}
                      onBlur={this.cVVvalidation}
                    />
                    {this.state.cvvError ?
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                         {I18n.t("Invalid CVV")}
                      </Text>
                      : null}
                  </View>
                  <View style={{ width: "48%" }}>
                    <Text style={styles.medText}>{I18n.t("Expiry Date")}</Text>
                    <TextInput
                      onChangeText={(exdt) => this._handlingCardExpiry(exdt)}
                      style={[
                        this.state.locale == "eng"
                          ? styles.cardHolder_en
                          : styles.cardHolder_ar,
                          (
                            this.state.cardExpiryError
                            ) && {
                              borderBottomColor: "red"
                          }
                      ]}
                      placeholder="MM / YYYY"
                      placeholderTextColor="#CCCCCC"
                      keyboardType="numeric"
                      maxLength={7}
                      value={this.state.cardExpiry}
                      onBlur={this.expiryvalidation}
                    />
                    {this.state.cardExpiryError ?
                      <Text style={{ color: "red", fontFamily: "Tajawal-Medium" }}>
                        {I18n.t("Invalid expiry date")}
                      </Text>
                      : null}
                  </View>
                </View>
              </View>
            </ScrollView>
            </KeyboardAvoidingView>
          </View>

          <View style={styles.bottom}>
            <TouchableOpacity              
              onPress={this.onGatewayLoading}
              style={commonStyle.yellowBtn}
            >
              <Text style={commonStyle.whiteBtnText}>
                {I18n.t("Confirm Payment")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(BankDetails);
