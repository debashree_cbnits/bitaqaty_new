import React, { Component } from "react";
import { Text, View, Image, Modal, TouchableOpacity, ActivityIndicator, Dimensions, Alert } from "react-native";
import { Button } from "react-native-elements";
import styles from "./style";
import I18n from "../../../i18n/index";
import { RTLView, RTLText } from "react-native-rtl-layout";
import { productImagePath, othersImage, loginScreen } from "../../../config/imageConst";
import commonStyle from "../../../../commonStyle";
import { connect } from "react-redux";
import StorageFunc from "../../../services/Storage";
import Toast from "react-native-tiny-toast";
import apiClient from "../../../services/api.client";
import AsyncStorage from "@react-native-community/async-storage";
import network from "../../pages/Network/check";
import store from '../../../redux/store';
import { setCartArrayChanged, setCartArrayLength } from '../../../redux/storage/action';
import { color } from "react-native-reanimated";
import { LearnMoreLinks } from "react-native/Libraries/NewAppScreen";
const deviceHeight = Dimensions.get("window").height;
import imageUrl from '../../../services/config';
import { TouchableHighlight } from "react-native-gesture-handler";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      cartActionSheet: false,
      show: false,
      fav: false,
      maxLimit: true,
      maxcartProducts: 0,
      cartArrayChanged: false,
      cartlength: 0,
      array: [],
      alreadyExists: false,
      showMaxLimitAlert: '',
      cartReached: false,
      favListRes: [],
      loggedInUser: false,
      isLoading: false,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng"
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar"
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng"
      };
    }
  }

  componentDidMount() {    
    this.getFavouriteList();  
    this.getCartFromSettings();  
    AsyncStorage.getItem("CartItemList").then(cartArray => {
      this.setState({ array: JSON.parse(cartArray) });
    })
    store.subscribe(() => this.forceUpdate());
  }

  getFavouriteList = () => {
    AsyncStorage.getItem("userToken")
      .then((results) => {
        if (results) {
          let data = {
            data: ''
          }
          this.setState({ loggedInUser: true, })          
          apiClient
            .postRequest("/get-favorites", data, results)
            .then(favListRes => {
              this.setState({ favListRes: favListRes, isLoading: false }, () => {
                const exists = this.state.favListRes.some(item => (item.id === this.props.item.id))
                this.setState({ fav: exists })
              })

            }).catch(err => console.log(err));
        }
        else {
          this.setState({ isLoading: false, loggedInUser: false })
        }
      })
  }

  getCartFromSettings() {
    // this.setState({ isLoading: true })
    AsyncStorage.getItem("userToken")
      .then((token) => {
        apiClient.getRequest('/bitaqatySettings', token).then((bitaqatySettingsRes) => {   
          this.setState({ applicationSettings: bitaqatySettingsRes.applicationSettings }, () => {
            for (let i = 0; i < this.state.applicationSettings.length; i++) {
              if (this.state.applicationSettings[i].propertyName == "maxcartProducts") {
                this.setState({ 
                  maxcartProducts: Number(this.state.applicationSettings[i].propertyValue),
                  // isLoading: false 
                })
              }
            }
          })
        }).catch(err => {
          this.setState({ isLoading: false })
        })
      }).catch(err => {
        this.setState({ isLoading: false })
      })
  }

  showAddToCartAction = async data => {    
    this.setState({ showMaxLimitAlert: '', cartReached: false })
    const net = network();
    let cartlength = store.getState().Storage.cartArrayChanged ?
      store.getState().Storage.cartArrayLength :
      (this.state.array !== null ? this.state.array.length : 0)
    let length = this.state.maxcartProducts - 1;
    console.log(this.state.maxcartProducts, 'length')
    if (length > 0) {
      if (cartlength <= length) {
        this.setState({ cartActionSheet: true, showMaxLimitAlert: '2', cartReached: false })
        await AsyncStorage.getItem("CartItemList").then(alreadyExists => {
          if (alreadyExists !== null) {
            JSON.parse(alreadyExists).map((element, index) => {
              if (data.item.id == element.id) {
                this.setState({ testarray: JSON.parse(alreadyExists) }, () => {
                  let newArray = [...this.state.testarray];
                  let quantity = element.prodQuantity ? (element.prodQuantity) + 1 : (element.quantity) + 1;
                  newArray[index].prodQuantity = quantity;
                  AsyncStorage.setItem("CartItemList", JSON.stringify(newArray));
                  this.setState({ alreadyExists: true });
                })

              }
            });
          }
        });
        if (net == false) {
          this.props.netcheck(net);
        } else {
          store.dispatch(setCartArrayChanged(true));
          StorageFunc.addToCart(JSON.stringify(this.props.item));
          AsyncStorage.getItem("CartItemList").then(cartArray => {
            if (
              cartArray === null ||
              (cartArray !== null && JSON.parse(cartArray).length == 0)
            ) {
              store.dispatch(setCartArrayLength(1));
            } else {
              let totalQuantity = 0;
              this.setState({ cartarray: JSON.parse(cartArray) }, () => {
                for (let i = 0; i < this.state.cartarray.length; i++) {
                  this.state.cartarray[i].prodQuantity = this.state.cartarray[i].prodQuantity
                    ? this.state.cartarray[i].prodQuantity : 1;
                  totalQuantity += this.state.cartarray[i].prodQuantity
                }
              })
              store.dispatch(
                setCartArrayLength(
                  this.state.alreadyExists
                    ? totalQuantity
                    : totalQuantity + 1
                )
              );
            }
            this.setState({ alreadyExists: false })
            this.forceUpdate();
          });
        }
      }
      else {
        this.setState({ showMaxLimitAlert: '1', cartReached: true })
      }
    }
  };


  goToCartPage = () => {
    this.setState({ cartActionSheet: false });
    let goToCart = "Cart";    
    this.props.goToCart(goToCart);
  };

  addtoFav = data => {

    const net = network();
    if (net == false) {
      this.props.netcheck(net);
    } else {
      AsyncStorage.getItem("userToken")
        .then((results) => {
          if (results) {
            let favData = {
              "productId": data.item.id,
              "action": "add"
            }
            apiClient
              .postRequest("change-favorites", favData, results)
              .then(addFavRes => {
                this.setState({ fav: true })
                Toast.show(I18n.t("Succesfully added to favourite list"))
              }).catch(err => console.log(err));
          } else {
            Toast.show(I18n.t("Please login first"))
            let goToLogin = "Login";
            this.props.goToLogin(goToLogin);
          }
        })
    }
  };

  deleteFromFav = data => {
    const net = network();
    if (net == false) {
      this.props.netcheck(net);
    } else {
      this.setState({ isLoading: true })
      AsyncStorage.getItem("userToken")
        .then((results) => {
          if (results) {
            let favData = {
              "productId": data.item.id,
              "action": "delete"
            }
            apiClient
              .postRequest("change-favorites", favData, results)
              .then(deletedFromFav => {
                this.setState({ isLoading: false })
                this.setState({ fav: false })
                if (this.props.fromFav) {
                  this.props.goToFav('Favourites');
                }
                Toast.show(I18n.t("Succesfully deleted from favourite list"))
              }).catch(err => console.log(err));
          } else {
            this.setState({ isLoading: false })
            Toast.show(I18n.t("Please login first"))
            let goToLogin = "Login";
            this.props.goToLogin(goToLogin);
          }
        })
    }
  }

  _getMerchants = async () => {
    this.setState({ isLoading: true })
    apiClient
      .postRequest("/merchant/Products", {
        merchantId: this.props.item.merchantId,
        categoryId: this.props.item.categoryId,
      })
      .then((productApiResponse) => {
        this.setState({ isLoading: false })
        this.props.navigation.navigate("CatagoryView",
          { merchantInfo: this.props.item, categoryId: productApiResponse.categoryModel.id, productApiResponse, back: "Marchent", fromWhere: 'tabs' });
      })
      .catch((err) => {
        this.setState({ isLoading: false })
        console.log("err", err);
      }
      );
  };


  render() {

    return (
      <>
        <View style={{ flex: 1 }}>

          {/* add to cart modal */}
          {this.state.showMaxLimitAlert == "2" ?
            <Modal animationType={"fade"} transparent={true} visible={this.state.cartActionSheet}>
              <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
                <View style={styles.addCartMsgCnt}>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? [
                          commonStyle.rowSec,
                          { marginBottom: 15, alignItems: "center" }
                        ]
                        : [
                          commonStyle.rowSec_ar,
                          { marginBottom: 15, alignItems: "center" }
                        ]
                    }
                  >
                    <Image
                      source={othersImage.rightTick}
                      style={{ width: 20, height: 20 }}
                    />
                    <Text style={{ marginHorizontal: 10 }}>
                      {I18n.t("Added to cart")}
                    </Text>
                  </View>

                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <View style={styles.productImg}>
                      <Image
                        source={{ uri: imageUrl.productImageUrl + this.props.productImage }}
                        style={styles.imageItem}
                      />
                    </View>
                    <View style={{ width: "80%", paddingHorizontal: "3%" }}>
                      <Text
                        style={
                          this.state.locale == "eng"
                            ? [styles.prodText, commonStyle.align_en]
                            : [styles.prodText, commonStyle.align_ar]
                        }
                      >
                        {this.props.productTitle}
                      </Text>
                      <Text
                        style={
                          this.state.locale == "eng"
                            ? [styles.storeText, commonStyle.align_en]
                            : [styles.storeText, commonStyle.align_ar]
                        }
                      >
                        {this.props.productStore}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? styles.addCartBtnCntn
                        : styles.addCartBtnCntn_ar
                    }
                  >
                    <TouchableOpacity
                      style={styles.shopCartBtn}
                      onPress={this.goToCartPage}
                    >
                      <Text style={styles.shopCartText}>
                        {I18n.t("Go To Shopping Cart")}
                      </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={styles.addMoreBtn}
                      onPress={() => {                        
                        this.setState({ cartActionSheet: false });
                      }}
                    >
                      <Text style={styles.addMoreText}>{I18n.t("Add More")}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
            :
            this.state.showMaxLimitAlert == "1" ?
              <Modal transparent={true} visible={this.state.cartReached}>
                <View style={{ backgroundColor: '#00000aa', flex: 1 }}>
                  <View style={{ width: '90%', padding: 10, marginHorizontal: '5%', borderColor: 'red', borderWidth: 2, borderRadius: 4, backgroundColor: '#fff', position: 'absolute', bottom: '11%', alignItems: 'center' }}>

                    <Image source={othersImage.forgotError} style={{ width: 40, height: 40 }} />
                    <Text style={{ ...styles.dupLoginBoldMsg, marginVertical: 10 }}>
                      {I18n.t("Max")} {" "}{this.state.maxcartProducts}{" "}{I18n.t("products")}
                    </Text>

                    <TouchableOpacity
                      style={{ ...styles.shopCartBtn, width: '100%', marginTop: 10 }}
                      onPress={() => this.setState({ cartReached: false })}
                    >
                      <Text style={styles.shopCartText}>
                        {I18n.t("Close")}
                      </Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </Modal> : null}

        </View>
        {this.state.isLoading ? (
          <ActivityIndicator style={styles.activityIndicator}
          size="small"
          color="#1A1A1A" />
        ) : (
            <View style={{ width: "100%", paddingTop: 30, paddingBottom: 10 }}>
              <View
                style={
                  this.state.locale == "eng"
                    ? styles.productWrap
                    : styles.productWrap_ar
                }
              >
                <View
                  style={{
                    ...styles.productImg,
                    opacity: this.props.outOfStock ? 0.75 : 1
                  }}
                >
                  <Image
                    style={styles.imageItem}
                    source={{ uri: imageUrl.productImageUrl + this.props.productImage }}
                  />
                </View>

                <View
                  style={
                    this.state.locale == "eng"
                      ? styles.productDetails
                      : styles.productDetails_ar
                  }
                >
                  <View>
                    <View style={styles.productTop}>
                      <RTLView locale={this.state.locale}>
                        <View style={styles.topText}>
                          <View>
                            <RTLView locale={this.state.locale}>
                              <Text
                                style={
                                  this.state.locale == "eng"
                                    ? [styles.prodText, commonStyle.align_en]
                                    : [styles.prodText, commonStyle.align_ar]
                                }
                              >
                                {this.props.productTitle}
                              </Text>
                            </RTLView>
                          </View>
                        </View>
                        {this.state.loggedInUser ?
                          <View
                            style={
                              this.state.locale == "eng"
                                ? styles.likeImgArea
                                : [styles.likeImgArea, styles.likeImgAreaAb]
                            }
                          >
                            <TouchableOpacity
                              onPress={() => this.props.fromFav || this.state.fav ?
                                this.deleteFromFav(this.props) :
                                this.addtoFav(this.props)}
                              disabled={this.state.outOfStock}
                            >
                              <Image
                                style={styles.productLike}
                                source={
                                  this.state.fav || this.props.fromFav
                                    ? productImagePath.heartY
                                    : productImagePath.heart
                                }
                              />
                            </TouchableOpacity>
                          </View> :
                          null}
                      </RTLView>
                    </View>

                    <View style={styles.productTop}>
                      <RTLView locale={this.state.locale}>
                        <View style={styles.topText}>
                          <View>
                            <TouchableOpacity
                              activeOpacity={0.7}
                              disabled={this.props.doNotNavigate ? true : false}
                              onPress={() => this._getMerchants()}
                            >
                              <RTLView locale={this.state.locale}>
                                <RTLText style={styles.storeText}>
                                  {this.props.productStore}
                                </RTLText>
                              </RTLView>
                            </TouchableOpacity>
                          </View>
                          <View
                            style={
                              this.state.locale == "eng"
                                ? [commonStyle.rowSec, commonStyle.align_en]
                                : [commonStyle.rowSec_ar, commonStyle.align_ar]
                            }
                          >
                            <Text style={styles.beforeText}>
                              {this.props.before}
                            </Text>
                            <Text style={styles.beforePrice}>
                              {this.props.productBeforePrice}
                            </Text>
                            <Text style={styles.beforeText}>
                              {this.props.beforeUnit}
                            </Text>
                          </View>
                        </View>
                        <View
                          style={
                            this.state.locale == "eng"
                              ? styles.likeImgAreaPar
                              : [styles.likeImgAreaPar, styles.likeImgAreaAb]
                          }
                        >
                          <Image
                            style={
                              this.state.locale == "eng"
                                ? styles.percentageImg
                                : [styles.percentageImg, styles.percentageImgAb]
                            }
                            source={this.props.percentage}
                          />
                        </View>
                      </RTLView>
                    </View>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.showAddToCartAction(this.props)}
                    style={{
                      ...styles.addCart,
                      opacity: this.props.outOfStock ? 0.6 : 1
                    }}
                    disabled={this.props.outOfStock}
                  >
                    <RTLView locale={this.state.locale}>
                      <View>
                        <RTLView locale={this.state.locale}>
                          {!this.props.outOfStock && (
                            <Image
                              style={{ width: 24, height: 24 }}
                              source={productImagePath.whiteCart}
                            />
                          )}
                          <Text
                            style={
                              this.props.outOfStock
                                ? [
                                  styles.cartText,
                                  { width: "94%", textAlign: "center" }
                                ]
                                : [styles.cartText, { paddingTop: 0 }]
                            }
                          >
                            {this.props.outOfStock
                              ? I18n.t("Out Of Stock")
                              : I18n.t("Add To Cart")}
                          </Text>
                        </RTLView>
                      </View>

                      <View style={commonStyle.mlAuto}>
                        <RTLView locale={this.state.locale}>
                          <Text style={[styles.prodPrice, { paddingTop: 2 }]}>
                            {this.props.productPrice}
                          </Text>
                          <Text style={[styles.prodTag, { paddingTop: 2.5 }]}>
                            {this.props.productCurrency}
                          </Text>
                        </RTLView>
                      </View>
                    </RTLView>
                  </TouchableOpacity>
                </View>
              </View>
            </View>)}
      </>
    );
  }
}
const mapStateToProps = state => (
  {
    setlanguage: state.Language.setlanguage,
    cartLength: state.Storage.cartArrayLength,
    cartArrayChanged: state.Storage.cartArrayChanged

  });
export default connect(mapStateToProps, {})(Product);
