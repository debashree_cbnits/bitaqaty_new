import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, Modal, Alert } from "react-native";
import styles from "../../pages/Home/CatagoryView/CateHeadStyle";
import commonStyle from "../../../../commonStyle.js";
import {
  productImagePath,
  othersImage,
  loginScreen,
} from "../../../config/imageConst";
import I18n from "../../../i18n/index";
import { connect } from "react-redux";
import StorageFunc from "../../../services/Storage";
import Toast from "react-native-tiny-toast";
import network from "../../pages/Network/check";
import AsyncStorage from "@react-native-community/async-storage";
import apiClient from "../../../services/api.client";
import store from "../../../redux/store";
import {
  setCartArrayChanged,
  setCartArrayLength,
} from "../../../redux/storage/action";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import imageUrl from '../../../services/config';


class ProductHorizontal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: "",
      language: "",
      show: false,
      cartActionSheet: false,
      maxLimit: false,
      maxcartProducts: 0,
      cartArrayChanged: false,
      cartlength: 0,
      array: [],
      alreadyExists: false,
      showMaxLimitAlert: '',
      cartReached: false,
      loggedInUser: false,

    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.setlanguage && Object.keys(props.setlanguage).length) {
      if (props.setlanguage.language == "english") {
        I18n.locale = "eng";
        return {
          language: "english",
          locale: "eng",
        };
      } else {
        I18n.locale = "ar";
        return {
          language: "arabic",
          locale: "ar",
        };
      }
    } else {
      I18n.locale = "eng";
      return {
        language: "english",
        locale: "eng",
      };
    }
  }

  componentDidMount() {
    this.getCartFromSettings();
    this.getFavouriteList();
    AsyncStorage.getItem("CartItemList").then((cartArray) => {
      this.setState({ array: JSON.parse(cartArray) });
    });
    store.subscribe(() => this.forceUpdate());
  }

  getFavouriteList = () => {
    AsyncStorage.getItem("userToken")
      .then((results) => {
        if (results) {
          let data = {
            data: ''
          }
          this.setState({ loggedInUser: true, })
          apiClient
            .postRequest("/get-favorites", data, results)
            .then(favListRes => {
              this.setState({ favListRes: favListRes, isLoading: false }, () => {
                const exists = this.state.favListRes.some(item => (item.id === this.props.item.id))
                this.setState({ fav: exists })
              })

            }).catch(err => console.log(err));
        }
        else {
          this.setState({ isLoading: false, loggedInUser: false })
        }
      })
  }
  showAddToCartAction = async data => {
    this.setState({ showMaxLimitAlert: '', cartReached: false })
    const net = network();
    let cartlength = store.getState().Storage.cartArrayChanged ?
      store.getState().Storage.cartArrayLength :
      (this.state.array !== null ? this.state.array.length : 0)
    let length = this.state.maxcartProducts - 1;
    if (length > 0) {
    if (cartlength <= length) {
      this.setState({ cartActionSheet: true, showMaxLimitAlert: '2', cartReached: false })
      await AsyncStorage.getItem("CartItemList").then(alreadyExists => {
        if (alreadyExists !== null) {
          JSON.parse(alreadyExists).map((element, index) => {
            if (data.item.id == element.id) {
              this.setState({ testarray: JSON.parse(alreadyExists) }, () => {
                let newArray = [...this.state.testarray]
                let quantity = element.prodQuantity ? (element.prodQuantity) + 1 : (element.quantity) + 1;
                newArray[index].prodQuantity = quantity;
                AsyncStorage.setItem("CartItemList", JSON.stringify(newArray));
                this.setState({ alreadyExists: true });
              })
            }
          });
        }
      });
      if (net == false) {
        this.props.netcheck(net);
      } else {
        store.dispatch(setCartArrayChanged(true));
        StorageFunc.addToCart(JSON.stringify(this.props.item));
        AsyncStorage.getItem("CartItemList").then(cartArray => {
          if (
            cartArray === null ||
            (cartArray !== null && JSON.parse(cartArray).length == 0)
          ) {
            store.dispatch(setCartArrayLength(1));
          } else {
            let totalQuantity = 0;
            this.setState({ cartarray: JSON.parse(cartArray) }, () => {
              for (let i = 0; i < this.state.cartarray.length; i++) {
                this.state.cartarray[i].prodQuantity = this.state.cartarray[i].prodQuantity
                  ? this.state.cartarray[i].prodQuantity : 1;
                totalQuantity += this.state.cartarray[i].prodQuantity
              }
            })
            store.dispatch(
              setCartArrayLength(
                this.state.alreadyExists
                  ? totalQuantity
                  : totalQuantity + 1
              )
            );
          }
          this.setState({ alreadyExists: false })
          this.forceUpdate();
        });
      }
    }
    else {
      this.setState({ showMaxLimitAlert: '1', cartReached: true })
    }
  }
  };

  addtoFav = (data) => {
    const net = network();
    if (net == false) {
      this.props.netcheck(net);
    } else {
      AsyncStorage.getItem("userToken").then((results) => {
        if (results) {
          let favData = {
            productId: data.item.id,
            action: "add",
          };
          apiClient
            .postRequest("change-favorites", favData, results)
            .then((addFavRes) => {
              this.setState({ fav: true });
              Toast.show(I18n.t("Succesfully added to favourite list"));
            })
            .catch((err) => console.log(err));
        } else {
          Toast.show(I18n.t("Please login first"));
          let goToLogin = "Login";
          this.props.goToLogin(goToLogin);
        }
      });
    }
  };

  goToCartPage = () => {
    this.setState({ cartActionSheet: false });
    let goToCart = "Cart";
    this.props.goToCart(goToCart);
  };

  _getMerchants = async () => {
    apiClient
      .postRequest("/merchant/Products", {
        merchantId: this.props.item.merchantId,
        categoryId: this.props.item.categoryId,
      })
      .then((productApiResponse) => {
        this.props.navigation.navigate("CatagoryView",
          { merchantInfo: this.props.item, categoryId: productApiResponse.categoryModel.id, productApiResponse, back: "Marchent", fromWhere: 'tabs' });


      })
      .catch((err) => {
        console.log("err", err);
      }
      );
  };

  getCartFromSettings() {
    AsyncStorage.getItem("userToken")
      .then((token) => {
        apiClient
          .getRequest("/bitaqatySettings", token)
          .then((bitaqatySettingsRes) => {
            this.setState(
              { applicationSettings: bitaqatySettingsRes.applicationSettings },
              () => {
                for (
                  let i = 0;
                  i < this.state.applicationSettings.length;
                  i++
                ) {
                  if (
                    this.state.applicationSettings[i].propertyName ==
                    "maxcartProducts"
                  ) {
                    this.setState({
                      maxcartProducts: Number(this.state.applicationSettings[i]
                        .propertyValue),
                    });
                  }
                }
              }
            );
          })
          .catch((err) => {
            this.setState({ isLoading: false });
          });
      })
      .catch((err) => console.log(err));
  }

  deleteFromFav = data => {
    const net = network();
    if (net == false) {
      this.props.netcheck(net);
    } else {
      this.setState({ isLoading: true })
      AsyncStorage.getItem("userToken")
        .then((results) => {
          if (results) {
            let favData = {
              "productId": data.item.id,
              "action": "delete"
            }
            apiClient
              .postRequest("change-favorites", favData, results)
              .then(deletedFromFav => {
                this.setState({ isLoading: false })
                this.setState({ fav: false })
                Toast.show(I18n.t("Succesfully deleted from favourite list"))
              }).catch(err => console.log(err));
          } else {
            this.setState({ isLoading: false })
            Toast.show(I18n.t("Please login first"))
            let goToLogin = "Login";
            this.props.goToLogin(goToLogin);
          }
        })
    }
  }

  render() {

    return (
      <>
        <View style={{ flex: 1 }}>

          {/* add to cart modal */}
          {this.state.showMaxLimitAlert == "2" ? (
            <Modal transparent={true} visible={this.state.cartActionSheet}>
              <View style={{ backgroundColor: "#000000aa", flex: 1 }}>
                <View style={styles.addCartMsgCnt}>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? [
                          commonStyle.rowSec,
                          { marginBottom: 15, alignItems: "center" },
                        ]
                        : [
                          commonStyle.rowSec_ar,
                          { marginBottom: 15, alignItems: "center" },
                        ]
                    }
                  >
                    <Image
                      source={othersImage.rightTick}
                      style={{ width: 20, height: 20 }}
                    />
                    <Text style={{ marginHorizontal: 10 }}>
                      {I18n.t("Added to cart")}
                    </Text>
                  </View>

                  <View
                    style={
                      this.state.locale == "eng"
                        ? commonStyle.rowSec
                        : commonStyle.rowSec_ar
                    }
                  >
                    <View style={styles.productImg}>
                      <Image
                        source={{ uri: imageUrl.productImageUrl + this.props.productImagePath }}
                        style={styles.imageItem}
                      />
                    </View>
                    <View style={{ width: "80%", paddingHorizontal: "3%" }}>
                      <Text
                        style={
                          this.state.locale == "eng"
                            ? [styles.prodText, commonStyle.align_en]
                            : [styles.prodText, commonStyle.align_ar]
                        }
                      >
                        {this.props.productHzTitle}
                      </Text>
                      <Text
                        style={
                          this.state.locale == "eng"
                            ? [styles.storeText, commonStyle.align_en]
                            : [styles.storeText, commonStyle.align_ar]
                        }
                      >
                        {this.props.productHzStore}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={
                      this.state.locale == "eng"
                        ? styles.addCartBtnCntn
                        : styles.addCartBtnCntn_ar
                    }
                  >
                    <TouchableOpacity
                      style={styles.shopCartBtn}
                      onPress={this.goToCartPage}
                    >
                      <Text style={styles.shopCartText}>
                        {I18n.t("Go To Shopping Cart")}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.addMoreBtn}
                      onPress={() => {
                        this.setState({ cartActionSheet: false });
                      }}
                    >
                      <Text style={styles.addMoreText}>
                        {I18n.t("Add More")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          ) :
            <Modal transparent={true} visible={this.state.cartReached}>
              <View style={{ backgroundColor: '#00000aa', flex: 1 }}>
                <View style={{ width: '90%', padding: 10, marginHorizontal: '5%', borderColor: 'red', borderWidth: 2, borderRadius: 4, backgroundColor: '#fff', position: 'absolute', bottom: '11%', alignItems: 'center' }}>

                  <Image source={othersImage.forgotError} style={{ width: 40, height: 40 }} />
                  <Text style={{ ...styles.dupLoginBoldMsg, marginVertical: 10 }}>{I18n.t("Maximum cards per day reached")}</Text>

                  <TouchableOpacity
                    style={{ ...styles.shopCartBtn, width: '100%', marginTop: 10 }}
                    onPress={() => this.setState({ cartReached: false })}
                  >
                    <Text style={styles.shopCartText}>
                      {I18n.t("Close")}
                    </Text>
                  </TouchableOpacity>
                </View>

              </View>
            </Modal>}
        </View>

        <View style={styles.recProduct}>
          <View style={{ width: "100%" }}>
            <Image
              source={{ uri: imageUrl.productImageUrl + this.props.productImagePath }}
              style={{
                ...styles.productImg,
                opacity: this.props.outOfStock ? 0.6 : 1,
              }}
            />
            {this.state.loggedInUser ?
              <TouchableOpacity
                style={
                  this.state.locale == "eng"
                    ? styles.wishlist
                    : styles.wishlist_ar
                }
                onPress={() => this.state.fav ?
                  this.deleteFromFav(this.props) :
                  this.addtoFav(this.props)}
              >
                <Image
                  style={styles.productLike}
                  source={
                    this.state.fav
                      ? productImagePath.heartY
                      : productImagePath.heart
                  }
                />
              </TouchableOpacity>
              : null}

            <TouchableOpacity
              style={
                this.state.locale == "eng" ? styles.varPar : styles.varPar_ar
              }
            >
              <Image style={styles.productDis} source={this.props.prodVarPar} />
            </TouchableOpacity>

            <Text style={{ ...styles.medText, marginTop: 5 }}>
              {this.props.productHzTitle}
            </Text>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => this._getMerchants()}
            >
              <Text style={styles.smBlueText}>{this.props.productHzStore}</Text>
            </TouchableOpacity>

            <View
              style={
                this.state.locale == "eng" ? styles.revRow : styles.revRow_ar
              }
            >
              <Text style={styles.tagSym}>{this.props.beforeHz} </Text>
              <Text style={styles.bePrice}>{this.props.beforeHzPrice} </Text>
              <Text style={styles.tagSym}>{this.props.beforeHzUnit}</Text>
            </View>

            <View
              style={
                this.state.locale == "eng" ? styles.revRow : styles.revRow_ar
              }
            >
              <Text style={styles.tagSym}>{this.props.productHzPriceH}</Text>
              <Text style={styles.priceTag}>{this.props.productHzPrice}</Text>
              <Text style={styles.tagSym}>{this.props.productHzTag}</Text>
            </View>
          </View>

          <TouchableOpacity
            onPress={() => this.showAddToCartAction(this.props)}
            disabled={this.props.outOfStock}
            style={
              this.state.locale == "eng"
                ? [
                  styles.cartBtn,
                  styles.revRow,
                  { opacity: this.props.outOfStock ? 0.6 : 1 },
                ]
                : [
                  styles.cartBtn,
                  styles.revRow_ar,
                  { opacity: this.props.outOfStock ? 0.6 : 1 },
                ]
            }
          >
            {!this.props.outOfStock && (
              <Image
                source={productImagePath.whiteCart}
                style={{ marginHorizontal: 5 }}
              />
            )}
            <Text style={styles.BoldWhiteText}>
              {" "}
              {this.props.outOfStock
                ? I18n.t("Out Of Stock")
                : I18n.t("Add To Cart")}{" "}
            </Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }
}
mapStateToProps = (state) => ({
  setlanguage: state.Language.setlanguage,
});
export default connect(
  mapStateToProps,
  {}
)(ProductHorizontal);
